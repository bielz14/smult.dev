<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@mail.com',//логин(e-mail) админа
            'password' => Hash::make('1111'),//пароль админа
            'role' => 'admin',
        ]);

        User::create([
            'email' => 'manager@mail.com',//логин(e-mail) менеджера
            'password' => Hash::make('1111'),//пароль менеджера
            'role' => 'manager',
        ]);
    }
}
