<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@existsEmail');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('login-manager', 'Auth\LoginController@loginManager')->name('loginManager');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout-manager', 'Auth\LoginController@logoutManager')->name('logoutManager');
Route::post('register', 'Auth\RegisterController@register');

Route::get('mail/send', 'MailController@send')->name('mail');

Route::get('/contacts.html', 'IndexController@callback');
Route::post('/mail/send-callback', 'MailController@sendCallback');

Route::post('/search', 'IndexController@search');

Route::get('/studies.html', 'IndexController@studiosCategory');
Route::get('/studies/{alias}.html', 'IndexController@studio');

Route::get('/news.html', 'IndexController@posts');
Route::get('/news/{alias}.html', 'IndexController@article');

Route::get('/sitemap.html', 'IndexController@sitemap');
Route::get('/kto-myi.html', 'IndexController@about');

Route::get('/konkursi.html', 'IndexController@competitions');
Route::get('/konkursi/{alias}.html', 'IndexController@competition');

Route::get('/manager/wallps-and-posters-download', 'ManagerController@downloadWallpaperAndPoster');

Route::get('/manager/competition-info', 'ManagerController@competitionInfo');
Route::get('/manager/competition-participants-and-winners', 'ManagerController@competitionParticipantsAndWinners');
Route::post('/manager/competition-delete-participants', 'ManagerController@competitionDeleteParticipants');
Route::post('/manager/editcompetition', 'ManagerController@editCompetition');

Route::get('/tolko-na-smultru.html', 'IndexController@articles');
Route::get('/na-sladkoe.html', 'IndexController@forWseetCategories');
Route::get('/smult-live.html', 'IndexController@smultlive');

Route::get('/uzhe-v-prokate.html', 'MultController@multsRental');
Route::get('/zakaji-multfilm.html', 'MultController@orders');
Route::get('/multfilmyi.html', 'MultController@mults');
Route::get('/multserialyi.html', 'MultController@multseries');
Route::get('/new-mult.html', 'MultController@mults');
Route::get('/razdely.html', 'MultController@multCategories');
Route::get('/{mult}.html', 'MultController@mult');
Route::get('/razdely/{category}.html', 'MultController@multsCategory');

Route::get('/', 'IndexController@index');

Route::get('/tolko-na-smultru/{category}.html', 'IndexController@articlesCategory');
Route::get('/tolko-na-smultru/{article_type}/{alias}.html', 'IndexController@article');

Route::get('/na-sladkoe/trejleryi.html', 'IndexController@trailers');
Route::get('/{mult}/treilers/{trailer}.html', 'IndexController@trailer');
Route::get('/{mult}/treilers.html', 'IndexController@trailer');
Route::get('/na-sladkoe/oboi-i-posteryi.html', 'IndexController@wallpaperAndPosterCategories');
Route::get('/{alias}/posters.html', 'IndexController@wallpapersAndPostersCategory');
Route::get('/na-sladkoe/saundtreki.html', 'IndexController@soundtracks');
Route::get('/{mult}/music.html', 'IndexController@soundtrack');
Route::get('/multgeroi/{alias}.html', 'IndexController@multhero');
Route::get('/na-sladkoe/multgeroi.html', 'IndexController@multheroes');
Route::get('/{alias}/geroi.html', 'IndexController@multheroesCategory');
Route::get('/na-sladkoe/interesnyie-faktyi.html', 'IndexController@interestings');
Route::get('/na-sladkoe/{category}.html', 'IndexController@articlesCategory');
Route::get('/{mult}/{soundtrack_alias}.html', 'IndexController@soundtrack');
Route::get('/{mult}/audio-{nubmer}.html', 'IndexController@soundtrack');	

Route::get('/{mult}/{category}/{alias}.html', 'IndexController@interesting');
Route::get('/{mult}/fakts/{alias}.html', 'IndexController@interesting');
Route::get('/{category}/fakts.html', 'IndexController@interestingsCategory');

Route::get('/manager', 'ManagerController@index');
Route::get('/manager/mult', 'ManagerController@mult');

Route::get('/manager/multseries', 'ManagerController@multseries');
Route::get('/manager/season', 'ManagerController@season');
Route::get('/manager/serie', 'ManagerController@serie');

Route::get('/manager/multhero', 'ManagerController@multhero');
Route::get('/manager/quote', 'ManagerController@quote');
Route::get('/manager/smultlive', 'ManagerController@smultlive');
Route::get('/manager/competition', 'ManagerController@competition');
Route::get('/manager/newtest', 'ManagerController@newtest');
Route::get('/manager/newpoll', 'ManagerController@newpoll');
Route::get('/manager/resource', 'ManagerController@resource');

Route::get('/manager/videoservices', 'ManagerController@videoservices');

Route::post('/manager/addmult', 'ManagerController@addMult');

Route::post('/manager/addmultseries', 'ManagerController@addMultseries');
Route::post('/manager/addseason', 'ManagerController@addSeason');
Route::post('/manager/addserie', 'ManagerController@addSerie');
Route::get('/manager/seasons', 'ManagerController@seasons');
Route::post('/manager/delete-serie', 'ManagerController@deleteSerie');

Route::post('/manager/addmulthero', 'ManagerController@addMulthero');
Route::post('/manager/addquote', 'ManagerController@addQuote');
Route::post('/manager/addsmultlive', 'ManagerController@addSmultLive');
Route::post('/manager/addcompetition', 'ManagerController@addCompetition');
Route::post('/manager/addnewtest', 'ManagerController@addNewtest');
Route::post('/manager/addnewpoll', 'ManagerController@addNewpoll');

Route::get('/manager/additionalparams', 'ManagerController@additionalparams');
Route::post('/manager/additionalparams', 'ManagerController@additionalparams');

Route::post('/manager/editadditionalparams', 'ManagerController@editAdditionalparams');
Route::post('/manager/editmult', 'ManagerController@editMult');
Route::post('/manager/editsmultlive', 'ManagerController@editSmultlive');

Route::post('/manager/videoservices', 'ManagerController@videoservices');

Route::post('/addcomment', 'MultController@addComment');
Route::get('/manager/comments', 'ManagerController@comments');
Route::get('/manager/comments', 'ManagerController@comments');
Route::post('/manager/comments', 'ManagerController@comments');
Route::post('/manager/moderationcomment', 'ManagerController@moderationComment');
Route::post('/manager/deletecomment', 'ManagerController@deleteComment');

Route::post('/addTicket', 'MultController@addTicket');
Route::post('/outTicketForm', 'MultController@outTicketForm');
Route::get('/manager/tickets/{type}', 'ManagerController@tickets');
Route::post('/manager/moderationticket', 'ManagerController@moderationTicket');
Route::post('/manager/deleteticket', 'ManagerController@deleteTicket');

Route::post('/manager/addresource', 'ManagerController@addResource');
Route::post('/manager/editresource', 'ManagerController@editResource');

Route::post('/addorder', 'MultController@addOrder');
Route::post('/reportorder', 'MultController@reportOrder');
Route::post('/deleteorder', 'MultController@deleteOrder');

Route::get('/users-transfer', 'UserController@usersTransfer');

Route::post('/manager/send-validation', 'ManagerController@sendValidation');
Route::get('/manager/send-validation', 'ManagerController@sendValidation');