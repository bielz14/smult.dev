@extends('layouts.app')
@section('container')
<div class="content">
   <h1>Анимационные студии</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
         <p>&gt;</p>
      </span>
      <span>Анимационные студии</span>
   </div>
   @if (isset($contents))
   <section>
      @foreach ($contents as $content) 
      <div class="mult items clr  big">
         @foreach ($content['items'] as $mult) 
         <h2>{{ $mult['title'] }}</h2>
         <div class="item">
            <a href="/{{ $mult->uri }}">
               <div class="title">{{ $mult->pagetitle }}</div>
            </a>
            <div class="jt">
               <div class="jtc">
                  {{ $mult->description }}                       
               </div>
            </div>
            <div class="poster jtg">
               @foreach ($mult->kpxzxsitetmplvarcontentvalue as $value)
               @if ($value->tmplvarid == 142)
               <a href="/{{ $mult->uri }}">
                     <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                     ?>
               <img src="{{ $image }}" alt="{{ $mult->pagetitle }}">
               </a>
               @endif
               @endforeach
            </div>
         </div>
         @endforeach
      </div>
      <div class="m-i"><a href="/studies/{{ $content['alias'] }}.html">Все мультфильмы студии {{ $content['title'] }}</a></div>
      @endforeach
   </section>
   @endif
</div>
@endsection