@extends('layouts.app')

@section('container')
@if (!isset($pageTitle))
  <?php $pageTitle = ''; ?>
@endif
<div class="content">
   <h1>Саундтреки</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
         <p>&gt;</p>
      </span>
      <span typeof="v:Breadcrumb">
      <a href="/na-sladkoe.html" rel="v:url" class="hov-bord" property="v:title">На сладкое</a>
        <p>&gt;</p>
      </span>
      <span>Саундтреки</span>
   </div>
   <section class="ajax_content_add" data-parent="0" data-id="2434" data-page="1" data-template="121">
      <div class="mult items clr  big">
        @if (isset($soundtracks))
          @foreach ($soundtracks as $soundtrack)
              <div class="item">
                  <a href="{{ '/'  . $soundtrack->uri }}">
                     <div class="title">{{ $soundtrack->pagetitle }}</div>
                  </a>
                  <div class="jt left" style="top: 143px; left: 228px;">
                     <div class="jtc">
                      {{ $soundtrack->description }}        
                     </div>
                  </div>
                  <div class="poster jtg">
                     @foreach ($soundtrack->kpxzxsitetmplvarcontentvalue as $value)
                        @if ($value->tmplvarid == 142)
                          <a href="{{ '/'  . $soundtrack->uri }}">
                            <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                            ?>
                            <img src="{{ $image }}" alt="{{ $soundtrack->pagetitle }}">
                          </a>      
                          @break
                        @endif
                     @endforeach
                  </div>
              </div> 
          @endforeach
        @endif
      </div>
  <section>
  <?php //if (\Auth::check() && \Auth::user()->can('addResource')) ?>
  @if (true)
    <div id="manager">
      @if (isset($resourceId))
        <a href="/manager/resource?parent={{ $resourceId }}">Добавить саундтрэк</a>
      @endif
    </div>
  @endif
 @if (isset($soundtracks) && (count($soundtracks) > 1 || !is_null(app('request')->input('page'))))
    <div id="preloader" class="add_remove">
       <div id="show_more">
          <img src="/public/img/_.gif" alt="Загрузка"><br>
          <span>ПОКАЗАТЬ ЕЩЕ</span>
       </div>
    </div>
    <div id="nav" class="add_remove">
      {{ $soundtracks->render() }}
    </div>
  @endif
  <div id="search-result" class="hide"></div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var nextPage = 0;
    var uri = null;
    $('body').on('click', '#show_more', function(event){
      console.log(nextPage);
      var prevImgs = $('div.item').get().reverse();
      var page = window.location.href.match(/(.*)\?page=(.*)/);
      if (page == null && nextPage == 0) {
        nextPage = 2;
        uri = window.location.href + '?page=' + nextPage;
      } else if (nextPage == 0) {
        if (page instanceof Array && page.length > 0) {
          nextPage = parseInt(page[2], 10) + 1;
          uri = page[1] + '?page=' + nextPage;
        } else {
          nextPage = parseInt(page, 10) + 1;
          uri = window.location.href + '?page=' + nextPage;
        }
      } else {
        nextPage++;
        if (page instanceof Array && page.length > 0) {
          uri = page[1] + '?page=' + nextPage;
        } else {
          uri = window.location.href + '?page=' + nextPage;
        }
      }

      $.get(uri, function( data ) {
        document.body.innerHTML = data;
      });

      setTimeout(function(){
        prevImgs.forEach(function(item) {
          $('.big').prepend(item);
        });

        $('div.item').each(function() {
          var matches = $(this).prop('src').match(/:\/\/(.*)\/.*\/(public.*)/);
          if (matches != null) {
            $(this).prop('src', '/' + matches[2]);
          }
        });
      }, 1000);
    });
  });
</script>
<style>
  li.page-item {
    display: inline-block;
  }

  li.page-item > a {
    color: black;
  }

  li.page-item > a:hover {
    color: #ffb324;
  }

  li.page-item.active {
    color: #ffb324;
  }

  span.page-link:hover {
    color: #ffb324;
    cursor: pointer;
  }
</style>
@endsection