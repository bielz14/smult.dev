@extends('layouts.app')

@section('container')
<div class="content">
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
         <p>&gt;</p>
      </span>
      <span>Кто мы?</span>
   </div>
   <div class="promo">
      <section class="logo">
         <h2>Только на</h2>
         <img src="/public/img/promo/Smult_ru_logo_promo.png">
      </section>
      <section class="we-have clr">
         <div class="item">
            <img src="/public/img/promo/1_block/mult.gif" alt="">
            <div class="text">Мультфильмы</div>
         </div>
         <div class="item">
            <img src="/public/img/promo/1_block/art.gif" alt="">
            <div class="text">Статьи</div>
         </div>
         <div class="item">
            <img src="/public/img/promo/1_block/video.gif" alt="">
            <div class="text">Видеоролики</div>
         </div>
         <div class="item">
            <img src="/public/img/promo/1_block/music.gif" alt="">
            <div class="text">Саундтреки</div>
         </div>
         <div class="item">
            <img src="/public/img/promo/1_block/poster.gif" alt="">
            <div class="text">Обои и постеры</div>
         </div>
         <div class="item">
            <img src="/public/img/promo/1_block/premier.gif" alt="">
            <div class="text">Премьеры</div>
         </div>
      </section>
      <section class="minions clr">
         <img src="/public/img/promo/2_block/minion_1.png" alt="">
         <img src="/public/img/promo/2_block/minion_2.png" alt="">
         <img src="/public/img/promo/2_block/minion_3.png" alt="">
      </section>
      <section class="diff clr">
         <h2>Чем мы отличаемся от других?</h2>
         <div class="item">
            <img src="/public/img/promo/3_block/clock.png" alt="">
            <div class="text">Ежедневное обновление</div>
         </div>
         <div class="item">
            <img src="/public/img/promo/3_block/content.png" alt="">
            <div class="text">Никакой рекламы</div>
         </div>
         <div class="item">
            <img src="/public/img/promo/3_block/rekl.png" alt="">
            <div class="text">Уникальный контент</div>
         </div>
      </section>
      <section class="tom-jerry clr">
         <img src="/public/img/promo/4_block/gerry.png" alt="">
         <img src="/public/img/promo/4_block/spyke.png" alt="">
         <img src="/public/img/promo/4_block/tom.png" alt="">
      </section>
      <section class="loves">
         <div class="text">Просто мы</div>
         <div class="img">
            <img src="/public/img/promo/5_block/promo_lov.png" alt="">
         </div>
         <div class="text">Мультфильмы.</div>
      </section>
      <section class="m-color clr">
         <img src="/public/img/promo/6_block/mike_sketch_fix.png" alt="">
         <img src="/public/img/promo/6_block/turbo_sketch_fix.png" alt="">
         <img src="/public/img/promo/6_block/mcqueen_sketch_fix.png" alt="">
      </section>
      <section class="info clr">
         <div class="item">
            <div class="text">В нашей базе</div>
            <div class="text gold">{{ $multsCount }}</div>
            <div class="text">Мультфильмов</div>
         </div>
         <div class="item">
            <div class="text">У нас</div>
            <div class="text gold">{{ $trailersCount }}</div>
            <div class="text">Видеороликов</div>
         </div>
         <div class="item">
            <div class="text">Мы опубликовали</div>
            <div class="text gold">{{ $articlesCount }}</div>
            <div class="text">Статей и новостей</div>
         </div>
         <div class="item">
            <div class="text">Мы нравимся</div>
            <div class="text gold"></div>
            <div class="text">Пользователям</div>
         </div>
      </section>
      <section class="promo-slider">
         <h2>Лучшие цитаты</h2>
         <div id="slider-quoter" class="es-carousel-wrapper">
            <div class="es-carousel">
               <ul class="slider-quoter">
                  @if (isset($quotes))
                    <?php $i = 0; ?>
                    @foreach ($quotes as $quote)
                      @if ($i == 0)
                        <li class="item quote" style="margin-right: 3px; width: 721px;" data-number="{{ $i + 1 }}">
                      @else
                        <li class="item quote" style="margin-right: 3px; width: 721px; display: none" data-number="{{ $i + 1 }}">
                      @endif
                            <div class="poster">
                              <?php 
                                $image = str_replace('assets', 'public', $quote->poster);
                                 if ($image[0] !== '/' && $image[0] !== ' ') {
                                     $image = '/' . $image;
                                 }
                              ?>
                             <img src="{{ $image }}" alt="">
                          </div>
                          <div class="right">
                            <div class="text">{{ $quote->text }}</div>
                            <?php 
                                preg_match('/.*\/(.*\.html)/', $quote->mult_uri, $multURI);
                                if (isset($multURI[1])) {
                                  $multURI = $multURI[1];
                                } else {
                                  $multURI = '/';
                                }

                                preg_match('/.*\/(multgeroi\/.*\.html)/', $quote->heroy_uri, $heroURI);
                                if (isset($heroURI[1])) {
                                  $heroURI = $heroURI[1];
                                } else {
                                  $heroURI = '/';
                                } 
                            ?>
                            <div class="author">© <a href="/{{ $multURI }}">{{ $quote->heroy }}</a> -  <a href="/{{ $heroURI }}"></a>
                            </div>
                          </div>
                      </li>
                      <?php $i++; ?>
                    @endforeach
                  @endif
               </ul>
            </div>
            <div class="es-nav">
              <span class="es-nav-prev" style="display: none;">Previous</span>
              <span class="es-nav-next" style="display: block;">Next</span>
            </div>
         </div>
      </section>
      <section class="promo_reg">
         <input type="email" placeholder="Адрес электронной почты" id="promo_email">
         <input type="button" id="promo-reg-btn" class="sbmBtnBlack sbmBtn" value="Присоединяйтесь">
      </section>
   </div>
   <div id="search-result" class="hide"></div>
</div>
<script type="text/javascript">
  $(document).ready(function(){ 
    $('head').append('<link rel="stylesheet" href="/public/css/promo.css">');

    $('.es-nav-prev').on('click', function(event){
      var itemNumber = $('li.item.quote:visible').data('number');
      var newitemNumber = itemNumber - 1;
      if (newitemNumber < 1) {
        newitemNumber = 1;
      }

      if (newitemNumber == 1 && $('.es-nav-prev').is(':visible')) {
        $('.es-nav-prev').hide();
      } else if(!$('.es-nav-next').is(':visible')) {
        $('.es-nav-next').toggle();
      }

      if (itemNumber != newitemNumber) {
        $('li.item.quote[data-number=' + itemNumber + ']').toggle();
        $('li.item.quote[data-number=' + newitemNumber + ']').toggle();
      }
    });

    $('.es-nav-next').on('click', function(event){
      var itemNumber = $('li.item.quote:visible').data('number');
      var newitemNumber = itemNumber + 1;
      if (newitemNumber > 10 ) {
        newitemNumber = 10;
      }

      if (newitemNumber == 10) {
        $('.es-nav-next').toggle();
      } else if(!$('.es-nav-prev').is(':visible')) {
        $('.es-nav-prev').toggle();
      }

      if (itemNumber != newitemNumber) {
        $('li.item.quote[data-number=' + itemNumber + ']').toggle();
        $('li.item.quote[data-number=' + newitemNumber + ']').toggle();
      }
    });
  });
</script>
<style type="text/css">
  .es-carousel ul li {
    float: left;
    display: block;
  }
</style>
@endsection