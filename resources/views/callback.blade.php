@extends('layouts.app')

@section('container')
<div class="content">
   <h1>Пишите Нам</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
         <p>&gt;</p>
      </span>
      <span>Пишите Нам</span>
    </div>
   <div class="feedback">
      <p>Есть вопросы, замечания, идеи, по развитию нашего необычного проекта? Не медлите, свяжитесь с нами,
         обязательно
         ответим всем и каждому на грамотно поставленные вопросы или предложения.
      </p>
      @if (app('request')->session()->exists('message'))
          <div class="msg">
              {{ $value = session('message') }}
          </div>
      @endif
      <div class="form">
        {{ Form::open(array('url' => '/mail/send-callback', 'method' => 'post', 'id' => 'add_callback_form', 'enctype' => 'multipart/form-data')) }}
          {{ csrf_field() }}
          <label for="f_name">Имя<span class="nd_red">*</span></label><br>
          {!! Form::text('name', NULL, ['id' => 'f_name', 'class' => 'ned']) !!}<br>
          @if ($errors->has('name'))
            <span class="help-block">
               <strong>{!! $errors->first('name') !!}</strong>
            </span>
          @endif
          <label for="f_email">Ваш e-mail<span class="nd_red">*</span></label><br>
          {!! Form::email('email', NULL, ['id' => 'f_email', 'class' => 'ned']) !!}<br>
          @if ($errors->has('email'))
            <span class="help-block">
               <strong>{!! $errors->first('email') !!}</strong>
            </span>
          @endif
           <label for="f_text">Сообщение<span class="nd_red">*</span></label><br>
          {!! Form::textarea('text', NULL, ['class' => 'ned', 'id' => 'f_text', 'cols' => '50', 'rows' => '5']) !!}    
          @if ($errors->has('text'))
            <span class="help-block">
               <strong>{!! $errors->first('text') !!}</strong>
            </span>
          @endif
          {!! Form::button('Отправить', ['type' => 'submit', 'class' => 'sbmBtnBlack sbmBtn']) !!}
        {{ Form::close() }}
      </div>
   </div>
   <div id="search-result" class="hide"></div>
</div>
<script type="text/javascript">
  if ($('.msg').is(':visible')) {
    setTimeout(function(){ 
      $('.msg').toggle();
    }, 3000);
  }
</script>
<style type="text/css">
  .feedback .msg {
    color: green;
    margin: 1% 0;
  }
</style>
@endsection