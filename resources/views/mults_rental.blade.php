@extends('layouts.app')

@section('container')
<div class="content">
   <h1>Уже в прокате</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
         <p>&gt;</p>
      </span>
      <span>Уже в прокате</span>
   </div>
   <section class="ajax_content_add" data-parent="0" data-id="2434" data-page="1" data-template="121">
      <div class="mult items clr  big">
        @if (isset($mults)) 
          @foreach ($mults as $mult)
              <div class="item">
                <a href="{{ '/'  . $mult->alias . '.html' }}">
                   <div class="title">{{ $mult->pagetitle }}</div>
                </a>
                <div class="jt left" style="top: 143px; left: 228px;">
                   <div class="jtc">
                    {{ $mult->description }}        
                   </div>
                </div>
                <div class="poster jtg">
                   @foreach ($mult->kpxzxsitetmplvarcontentvalue as $value)
                      @if ($value->tmplvarid == 142)
                          <?php 
                                  $image = str_replace('assets', 'public', $value->value);
                                  if ($image[0] !== '/' && $image[0] !== ' ') {
                                    $image = '/' . $image;
                                  }
                          ?>
                        <a href="{{ '/'  . $mult->alias . '.html' }}">
                          <img src="{{ $image }}" alt="{{ $mult->pagetitle }}">
                        </a>      
                        @break
                      @endif
                   @endforeach
                </div>
              </div>
          @endforeach
        @elseif (isset($multsCategory))
          @foreach ($multsCategory as $mult)
              <div class="item">
                  <a href="{{ '/'  . $mult->alias . '.html' }}">
                     <div class="title">{{ $mult->pagetitle }}</div>
                  </a>
                  <div class="jt left" style="top: 143px; left: 228px;">
                     <div class="jtc">
                      {{ $mult->description }}        
                     </div>
                  </div>
                  <div class="poster jtg">
                     @foreach ($mult->kpxzxsitetmplvarcontentvalue as $value)
                        @if ($value->tmplvarid == 142)
                          <a href="{{ '/'  . $mult->alias . '.html' }}">
                            <?php 
                                    $image = str_replace('assets', 'public', $value->value);
                                    if ($image[0] !== '/' && $image[0] !== ' ') {
                                      $image = '/' . $image;
                                    }
                            ?>
                            <img src="{{ $image }}" alt="{{ $mult->pagetitle }}">
                          </a>      
                          @break
                        @endif
                     @endforeach
                  </div>
              </div> 
          @endforeach
        @endif
      </div>
 @if (isset($mults) && (count($mults) > 1 || !is_null(app('request')->input('page'))))
    <div id="preloader" class="add_remove">
       <div id="show_more">
          <img src="/public/img/_.gif" alt="Загрузка"><br>
          <span>ПОКАЗАТЬ ЕЩЕ</span>
       </div>
    </div>
    <div id="nav" class="add_remove">
      {{ $mults->render() }}
    </div>
  @elseif (isset($multsCategory) && (count($multsCategory) > 1 || !is_null(app('request')->input('page'))))
    <div id="preloader" class="add_remove">
       <div id="show_more">
          <img src="/public/img/_.gif" alt="Загрузка"><br>
          <span>ПОКАЗАТЬ ЕЩЕ</span>
       </div>
    </div>
    <div id="nav" class="add_remove">
      {{ $multsCategory->render() }}
    </div>
  @endif
  <div id="search-result" class="hide"></div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var nextPage = 0;
    var uri = null;
    $('body').on('click', '#show_more', function(event){
      console.log(nextPage);
      var prevImgs = $('div.item').get().reverse();
      var page = window.location.href.match(/(.*)\?page=(.*)/);
      if (page == null && nextPage == 0) {
        nextPage = 2;
        uri = window.location.href + '?page=' + nextPage;
      } else if (nextPage == 0) {
        if (page instanceof Array && page.length > 0) {
          nextPage = parseInt(page[2], 10) + 1;
          uri = page[1] + '?page=' + nextPage;
        } else {
          nextPage = parseInt(page, 10) + 1;
          uri = window.location.href + '?page=' + nextPage;
        }
      } else {
        nextPage++;
        if (page instanceof Array && page.length > 0) {
          uri = page[1] + '?page=' + nextPage;
        } else {
          uri = window.location.href + '?page=' + nextPage;
        }
      }

      $.get(uri, function( data ) {
        document.body.innerHTML = data;
      });

      setTimeout(function(){
        prevImgs.forEach(function(item) {
          $('.big').prepend(item);
        });

        $('div.item').each(function() {
          var matches = $(this).prop('src').match(/:\/\/(.*)\/.*\/(public.*)/);
          if (matches != null) {
            $(this).prop('src', '/' + matches[2]);
          }
        });
      }, 1000);
    });
  });
</script>
<style>
  li.page-item {
    display: inline-block;
  }

  li.page-item > a {
    color: black;
  }

  li.page-item > a:hover {
    color: #ffb324;
  }

  li.page-item.active {
    color: #ffb324;
  }

  span.page-link:hover {
    color: #ffb324;
    cursor: pointer;
  }
</style>
@endsection