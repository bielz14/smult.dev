@extends('layouts.app')

@section('container')
<div class="content">
   <h1>Закажи Мультфильм</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">smult.ru</a>
         <p>&gt;</p>
      </span>
      <span>Закажи Мультфильм</span>
   </div>
   <div id="order">
      <div class="form">
         {{ Form::open(array('url' => '/addorder', 'method' => 'post', 'id' => 'add_order')) }}
            {{ csrf_field() }}
            <label for="order_name">Ваше имя<span class="req">*</span></label> 
            <br>
              {!! Form::text('order_name', NULL, ['id' => 'order_name']) !!}
              @if ($errors->has('order_name'))
                <span class="help-block">
                  <strong>{!! $errors->first('order_name') !!}</strong>
                </span>
              @endif
            <br>
            <label for="order_email">Ваш e-mail<span class="req">*</span></label> 
            <br>
            {!! Form::email('order_email', NULL, ['id' => 'order_email']) !!}
              @if ($errors->has('order_email'))
                <span class="help-block">
                  <strong>{!! $errors->first('order_email') !!}</strong>
                </span>
              @endif
            <br>
            <label for="order_title">Введите название мультфильма или другого материала по мультикам <br>
            (статьи, трейлеры, герои, и так далее..)<span class="req">*</span></label> 
            <br>
            {!! Form::text('order_title', NULL, ['id' => 'order_title']) !!}
              @if ($errors->has('order_title'))
                <span class="help-block">
                  <strong>{!! $errors->first('order_title') !!}</strong>
                </span>
              @endif
            <br>
            <label for="order_text">Дополнительная информация</label> 
            <br>
            {!! Form::textarea('order_text', NULL, ['id' => 'order_text']) !!}
              @if ($errors->has('order_text'))
                <span class="help-block">
                  <strong>{!! $errors->first('order_text') !!}</strong>
                </span>
              @endif
            <br>
            {!! Form::submit('Отправить заказ', ['id' => 'newOrder', 'class' => 'btn btn-default']) !!}
         {{ Form::close() }}
      </div>
      <div id="test"></div>
      @foreach ($orders as $order)
        <div class="items" data-id="{{ $order->id }}">
           <div class="order_item clr">
              <div class="poster">
                 @if ($order->order_status)
                    <img id="order_status" src="/public/cache_image/noimage_300x150_391.jpg" alt="">
                 @else
                    <img id="order_status" src="/public/cache_image/img/zak_ogidanya_300x150_391.jpg" alt="">
                 @endif
              </div>
              <div class="text">
                 <div class="name">
                    <b>Имя: </b>{{ $order->name }}      
                 </div>
                 <div class="title">
                    <b>Название материала: </b> {{ $order->title }}             
                 </div>
                 <div class="info">
                    <b>Дополнительная информация: </b> {{ $order->comment }}                 
                 </div>
                 <div class="date">
                    <b>Дата заказа: </b> {{ date_format($order->dateAdd, 'Y-m-d') }}            
                 </div>
                 <div class="date">
                    @if($order->dateExecution != gmdate('Y-m-d', '0000000000'))
                      <b>Дата выполнения: </b>  {{ date_format($order->dateExecution, 'Y-m-d') }}
                    @else
                      <b>Дата выполнения: </b>  0000-00-00
                    @endif            
                 </div>
                 <div class="order_admin_form">
                    <div class="response_message" data-orderid="{{ $order->id }}" style="display: none; text-align: center;"></div>
                    <p>Тип ответа</p>
                    <p class="status_box">
                       @if ($order->order_status)
                        <label>Виполнено</label>
                        {{ Form::radio('status_' . $order->id, 1, true) }}
                        <label>Не виполнено</label>
                        {{ Form::radio('status_' . $order->id, 0, false) }}
                       @else
                        <label>Виполнено</label>
                        {{ Form::radio('status_' . $order->id, 1, false) }}
                        <label>Не виполнено</label>
                        {{ Form::radio('status_' . $order->id, 0, true) }}
                       @endif
                    </p>
                    <p>Тип постера</p>
                    <p class="orient_box">
                       @if ($order->postertype == 'vert')
                         <label>мульты и сериалы</label>
                         {{ Form::radio('orient_' . $order->id, 'vert', true) }}
                         <label>остальные материалы</label>
                         {{ Form::radio('orient_' . $order->id, 'goriz', false) }}
                       @else
                         <label>мульты и сериалы</label>
                         {{ Form::radio('orient_' . $order->id, 'vert', false) }}
                         <label>остальные материалы</label>
                         {{ Form::radio('orient_' . $order->id, 'goriz', true) }}
                       @endif
                    </p>
                    {!! Form::text('href_' . $order->id, $order->href) !!}
                    <p>Комментарий к заказу:</p>
                    {!! Form::textarea('comment_completed_' . $order->id, $order->comment_completed) !!}
                    <button class="btn btn-danger deleteOrder" data-orderid="{{ $order->id }}">Удалить</button>
                    <button class="btn btn-success reportOrder" data-orderid="{{ $order->id }}">Выполнить отчет о заказе</button>
                 </div>
              </div>
           </div>
           <div class="separator_order"></div>
         </div>
         @endforeach
          <div class="separator_order"></div>
      </div>
   </div>
   <div id="search-result" class="hide"></div>
</div>
<script>
  $('head').append('<link rel="stylesheet" href="public/css/order.css">');
</script>
<script>
  $('.reportOrder').on('click', function(event) {
    var order_id = $(this).data('orderid');
    $.post('/reportorder', {
        order_id: order_id,
        order_status: $('[name=status_' + order_id + ']:checked').val(),
        order_postertype: $('[name=orient_' + order_id + ']:checked').val(),
        order_href: $('[name=href_' + order_id + ']').val(),
        order_comment_completed: $('[name=comment_completed_' + order_id + ']').val(),
        type: 'POST',
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function(t) { 
        if (t[0] == 1) {
          $('.response_message[data-orderid=' + order_id + ']').css('background-color', 'rgba(20,189,20,.72)');
          $('.response_message[data-orderid=' + order_id + ']').append('<span style="color: white">Успешно сохранено</span>');
          $('.response_message[data-orderid=' + order_id + ']').toggle('display');  
          setTimeout(
            function() {
            $('.response_message[data-orderid=' + order_id + ']').toggle('display')
          }, 1500);
          if (t['order_status']) {
            $('.items[data-id=' + order_id + '] > div > div > img').attr('src', '/public/cache_image/noimage_300x150_391.jpg');
          } else {
            $('.items[data-id=' + order_id + '] > div > div > img').attr('src', '/public/cache_image/img/zak_ogidanya_300x150_391.jpg');
          }
        }   
    })
  });

  $('.deleteOrder').on('click', function(event) {
    var order_id = $(this).data('orderid');
    $.post('/deleteorder', {
      order_id: order_id,
      type: 'POST',
      _token: $('meta[name="csrf-token"]').attr('content')
    }, function(t) { 
        if (t == 1) {
          $('.response_message[data-orderid=' + order_id + ']').css('background-color', 'red');
          $('.response_message[data-orderid=' + order_id + ']').append('<span style="color: white">Успешно удалено</span>');
          $('.response_message[data-orderid=' + order_id + ']').toggle('display');  
          setTimeout(
            function() {
            $('.response_message[data-orderid=' + order_id + ']').toggle('display');
          }, 1500);

          setTimeout(
            function() {
            $('.items[data-id=' + order_id + ']').remove();
          }, 2000);
        }
    })
  });
</script>
@endsection