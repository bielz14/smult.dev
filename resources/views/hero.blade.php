@extends('layouts.app')

@section('container')
@if (!isset($mult))
    <?php $multAlias = ''; ?>
    <?php $multPagetitle = ''; ?>
@else 
    <?php $multAlias = $mult['alias']; ?>
    <?php $multPagetitle = $mult['pagetitle']; ?>
@endif
<div class="content">
  @if (isset($hero))
    @foreach ($hero as $value)
       <?php $hero = $value; ?>
       <h1>{{ $value->pagetitle }}</h1>
       <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
          <span typeof="v:Breadcrumb">
             <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
             <p>&gt;</p>
          </span>
          <span typeof="v:Breadcrumb">
             <a rel="v:url" property="v:title" class="hov-bord" href="/{{ $multAlias }}.html">{{ $multPagetitle }}</a>
             <p>&gt;</p>
          </span>
          <span typeof="v:Breadcrumb">
            <a href="/{{ $multAlias }}/geroi.html" rel="v:url" class="hov-bord" property="v:title" style="border-bottom: none">
              {{ $multPagetitle }} Герои
            </a>
            <p>&gt;</p>
          </span>
          <span>{{ $value->pagetitle }}</span>
          <input id="content_id" type="hidden" value="{{ $value->id }}" />
        </div>
        @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
          @if ($value2->tmplvarid == 142)
          <img src="{{ $image = str_replace('assets', 'public', $value2->value) }}" alt="{{ $value->pagetitle }}">
          @endif
          <div class="butInfo"></div>
          <div style="display: none; z-index:100; left:100px;" class="jt" id="jttip-mult-poster">
             <div class="jtc">
                <div>
                   <b>{{ $value->pagetitle }}</b>
                </div>
                @if ($value2->tmplvarid == 143)
                  <div><b>Год выпуска</b>: {{ $value2->value }}</div>
                @elseif ($value2->tmplvarid == 144)
                  <div><b>Страна</b>: {{ $value2->value }}</div>
                @elseif ($value2->tmplvarid == 149)
                  <div><b>Режисер</b>: {{ $value2->value }}</div>
                @elseif ($value2->tmplvarid == 155)
                  <div><b>Возрастное ограничение</b>: {{ $value2->value }}</div>
                @elseif ($value2->tmplvarid == 156)
                  <div><b>Оригинальное название</b>: {{ $value2->value }}</div>
                @elseif ($value2->tmplvarid == 137)
                  <?php $videoURL = $value2->value; ?>
                @elseif ($value2->tmplvarid == 163)
                  <?php $videoURL = $value2->value; ?>
                @endif
             </div>
          </div>
        @endforeach
        <div class="m-cnt clr">
                   <div class="text">
             <div class="text-s" data-type="small">
                <?php 
                  $value->content = str_replace('src="assets/', 'src="/public/', $value->content);
                  $value->content = preg_replace('/href="(.*)"/', 'href="\1"', $value->content); 
                  echo $value->content;
                ?>
             </div>
             <span id="isReadMore" class="t-btn"><a>Подробнее...</a></span><span id="isReadMini" class="t-btn"><a>Свернуть текст</a></span>
          </div>
        </div>
        <?php //if (\Auth::check() && \Auth::user()->can('editResource')) ?>
          @if (true)
            <div id="manager" style="float: right">
              @if (isset($itemId))
                <a href="/manager/resource?item_id={{ $itemId }}">Редактировать</a>
              @endif
            </div>
        @endif
        @if (isset($otherSelfContent))
            <section class="mult-category clr">
               <h2>Категории</h2>
               @foreach ($otherSelfContent as $value)
               <?php $inspect = false; ?>
               <?php
                  if (strpos($value->pagetitle, 'Трейлеры и видео')) {
                    $otherContentTitle = 'Трейлеры';
                    $uri = $value->alias . '/treilers.html';
                  } else if (strpos($value->pagetitle, 'Интересные факты')) {
                    $otherContentTitle = 'Интересные факты';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Саундтрек')) {
                    $otherContentTitle = 'Музыка';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Герои')) {
                    $otherContentTitle = 'Герои';
                    $uri = 'multgeroi/' . $value->alias . '.html';
                  } else if (strpos($value->pagetitle, 'Обои')) {
                    $otherContentTitle = 'Обои и постеры';
                    $uri = $value->uri;
                  } else {
                    $uri = $value->alias . '.html';
                  }
               ?>
               @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
               @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
               <?php $inspect = true; ?>
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/{{ $image = str_replace('assets', 'public', $value2->value) }}" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @break
               @endif
               @endforeach
               @if (!$inspect)
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/public/img/other_content.jpg" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @endif
               @endforeach
            </section>
     @endif
        @if (isset($otherHeroesSelfCategory) && count($otherHeroesSelfCategory))
          <section>
             <div class="mult items clr ">
                <h2>Другие герои</h2>
                @foreach ($otherHeroesSelfCategory as $hero)
                <div class="item">
                   <a href="{{ '/' . $hero->alias . '.html' }}">
                      <div class="title">{{ $hero->pagetitle }}</div>
                   </a>
                   <div class="jt left" style="top: 143px; left: 228px;">
                      <div class="jtc">
                         {{ $hero->description }}        
                      </div>
                   </div>
                   <div class="poster jtg">   
                      @foreach ($hero->kpxzxsitetmplvarcontentvalue as $value)
                        @if ($value->tmplvarid == 142)
                          <a href="{{ '/' . $hero->alias . '.html' }}">
                          <?php 
                             $image = str_replace('assets', 'public', $value->value);
                             if ($image[0] !== '/' && $image[0] !== ' ') {
                               $image = '/' . $image;
                             }
                             ?>
                          <img src="{{ $image }}" alt="{{ $hero->pagetitle }}">
                          </a>      
                          @break
                        @endif
                      @endforeach
                   </div>
                </div>
                @endforeach    
             </div>
             <div class="m-i"><a href="/{{ $multAlias }}/geroi.html">Другие герои</a></div>
          </section>
        @endif
        @if (isset($otherHeroes))
          <section>
             <div class="mult items clr ">
                <h2>Больше Героев</h2>
                @foreach ($otherHeroes as $hero)
                <div class="item">
                   <a href="{{ '/' . $hero->alias . '.html' }}">
                      <div class="title">{{ $hero->pagetitle }}</div>
                   </a>
                   <div class="jt left" style="top: 143px; left: 228px;">
                      <div class="jtc">
                         {{ $hero->description }}        
                      </div>
                   </div>
                   <div class="poster jtg">   
                      @foreach ($hero->kpxzxsitetmplvarcontentvalue as $value)
                        @if ($value->tmplvarid == 142)
                          <a href="{{ '/' . $hero->alias . '.html' }}">
                          <?php 
                             $image = str_replace('assets', 'public', $value->value);
                             if ($image[0] !== '/' && $image[0] !== ' ') {
                               $image = '/' . $image;
                             }
                             ?>
                          <img src="{{ $image }}" alt="{{ $hero->pagetitle }}">
                          </a>      
                          @break
                        @endif
                      @endforeach
                   </div>
                </div>
                @endforeach    
             </div>
             <div class="m-i"><a href="/na-sladkoe/multgeroi.html">Другие герои</a></div>
          </section>
        @endif
       <div class="repost-quoter clr">
          <div class="sktch">
              @foreach ($hero->kpxzxsitetmplvarcontentvalue as $value2)
                @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
                    <?php 
                      $image = str_replace('assets', 'public', $value2->value);
                      if ($image[0] !== '/' && $image[0] !== ' ') {
                        $image = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image;
                      }
                    ?>
                    <?php break; ?>
                  @endif
                @endforeach
                <div class="repost clr" data-title="{{ $value->pagetitle }}" data-description="{{ $value->description }}" data-uri="http://{{ $_SERVER['SERVER_NAME'] }}/{{ $value->uri }}" data-poster="{{ $image }}">
                  <div data-type="vk" class="soz-repost">
                     <img src="/public/img/vkshare.png" alt="">
                  </div>
                  <div data-type="fb" class="soz-repost">
                     <img src="/public/img/fbshare.png" alt="">
                  </div>
                  <div data-type="tw" class="soz-repost">
                     <img src="/public/img/twshare.png" alt="">
                  </div>
               </div>
               <img class="sketch" src="/public/img/soz-sketch.png" alt="sketch">
            </div>

          <div class="quotes" id="quotes" style="width: 494px;">
              <div class="title">Цитаты великих...</div>
              @if (isset($quotes))
                @foreach ($quotes as $quote)
                  <div class="item">
                    <div class="poster">
                        <?php 
                          $image = str_replace('assets', 'public', $quote->poster);
                          if ($image[0] !== '/' && $image[0] !== ' ') {
                              $image = '/' . $image;
                          }
                        ?>
                       <img src="{{ $image }}" alt="">
                    </div>
                    <div class="right">
                       <div class="text">{{ $quote->text }}</div>
                       <?php 
                          preg_match('/.*\/(.*\.html)/', $quote->mult_uri, $multURI);
                          if (isset($multURI[1])) {
                            $multURI = $multURI[1];
                          } else {
                            $multURI = '/';
                          }

                          preg_match('/.*\/(multgeroi\/.*\.html)/', $quote->heroy_uri, $heroURI);
                          if (isset($heroURI[1])) {
                            $heroURI = $heroURI[1];
                          } else {
                            $heroURI = '/';
                          } 
                        ?>
                       <div class="author">© <a href="/{{ $multURI }}">{{ $quote->heroy }}</a> -  <a href="/{{ $heroURI }}"></a></div>
                    </div>
                  </div>
                @endforeach
              @endif
          </div>
          
       </div>
    @endforeach
  @endif
</div>
@endsection