@extends('layouts.manager')

@section('container')
<div class="col-md-9"> 
   <h2 class="text-center">Таблица с участниками и победителями конкурса</h2>
   @if (isset($winners))  
      <table class="table table-striped table-bordered" id="competitionMember">
         <thead>
            <tr>
               <th>Тип участника</th>
               <th>Тип конкурса</th>
               <th>Имя фамилия</th>
               <th>Комментарий</th>
               <th>Ссылка</th>
               <th><span class="glyphicon glyphicon-repeat"></span> / <span class="glyphicon glyphicon-remove"></span></th>
            </tr>
         </thead>
         <tbody>
            @foreach ($winners as $winner)   
               <tr>
                  @if ($winner->winnertype != 1) 
                     <td>Участник</td>
                  @else
                     <td>Победитель</td>
                  @endif
                  <td>{{ $winner->type }}</td>
                  @if (!$winner->deleted)
                     <td class="title ">{{ $winner->name }}</td>
                  @else
                     <td class="title "><s>{{ $winner->name }}</s></td>
                  @endif
                  <td>{{ $winner->comment }}</td>
                  <td><a href="{{ $winner->uri }}">{{ $winner->uri }}</a></td>
                  <td>
                     @if (!$winner->deleted)
                        {{ Form::open(array('url' => '/manager/competition-delete-participants', 'method' => 'post', 'id' => 'competition-delete-participants', 'style' => 'margin-bottom: 2.5%')) }}
                           {{ csrf_field() }}
                           {!! Form::hidden('participants_id', $winner->id, ['id' => 'winner']) !!}
                           {!! Form::hidden('content_id', $winner->contentid, ['id' => 'contentid']) !!}
                           {!! Form::submit('Вычеркнуть', ['id' => 'getTable', 'class' => 'tvFieldSave']) !!}
                        {{ Form::close() }}
                     @else
                        <span>Вычеркнут</span>
                     @endif
                  </td>
               </tr>
            @endforeach
         </tbody>
      </table>
   @endif
</div>
<?php if (isset($mults)): ?> 
   <div class="col-md-3" style="width: 20%; margin: 0 0 0 5%;">
      <h3>Последние материалы</h3>
      <ul class="list-group">
         @foreach ($mults as $mult)
            <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
         @endforeach
      </ul>
   </div>
<?php endif; ?>
<style>
   ul {
      margin-left: 5%;
   }
</style>
@endsection