@extends('layouts.manager')

@section('container')
<div class="col-md-12">
<h2 class="text-center" style="margin-bottom: 3.5%">Просмотр и изменение дополнительных параметров</h2>
{{ Form::open(array('url' => '/manager/additionalparams', 'method' => 'post', 'id' => 'add_additional_params', 'style' => 'margin-bottom: 2.5%')) }}
   {{ csrf_field() }}
   <div class="form-inline">
      <div class="form-group">
         <label for="countOnPageSelect">Кол-во</label>
         {!! Form::select('', [
             '10' => '10',
             '50' => '50',
             '100' => '100',
             '500' => '500',
             '99999' => 'Все',
         ], $count_data, ['id' => 'countOnPageSelect', 'class' => 'form-control']) !!}
         {!! Form::hidden('countOnPageSelectValue', $count_data, ['id' => 'countOnPageSelectValue']) !!}
      </div>
      <div class="form-group">
         <label for="tvId">Тип</label>
         {!! Form::select('', [
             '0' => '----------',
             '143' => 'Год выпуска',
             '144' => 'Выпустившая страна',
             '154' => 'Анимационные студии',
             '156' => 'Оригинальное название мультфильма',
             '170' => 'Список мультфильмов',
             '171' => 'Тип постера',
             '173' => 'Дата премьеры мульта',
         ], $tmplvarid, ['id' => 'tvId', 'class' => 'form-control']) !!}
         {!! Form::hidden('tvIdValue', $tmplvarid, ['id' => 'tvIdValue']) !!}
      </div>
      <div class="form-group">
         {!! Form::submit('Обновить', ['id' => 'getTable', 'class' => 'btn btn-success']) !!}
      </div>
   </div>
{{ Form::close() }}
<!--<div id="fixedLoader" style="display: none;"><img src="/img/load.gif"><span>Обновление таблици</span></div>-->
<?php if (isset($data)): ?>
   <?php $i = 0; ?>
   <?php if ($data['tmplvarid'] != 171): ?>
      <div id="tableWrap">
         <div id="imgShow"></div>
         <table class="table table-striped table-bordered table-condensed" id="tagTableMain" style="font-size: 10px">
            <thead>
               <tr>
                  <th>№</th>
                  <th>id</th>
                  <th>Название</th>
                  <th>Значение</th>
                  <th></th>
               </tr>
            </thead>
            <tbody>    
               <?php foreach ($data as $value): ?>
                  <?php $i++; ?>
                  <?php if ($i < 3): ?>
                     <?php continue; ?>
                  <?php endif; ?>
                  <tr>
                     <td>{{ $i-2 }}</td>
                     <td>{{ $value['id'] }}</td>
                     <td><a href="{{ URL::to($value['uri']) }}">{{ $value['title'] }}</a></td>
                     {{ Form::open(array('url' => '/manager/editadditionalparams', 'method' => 'post', 'id' => 'edit_additional_params')) }}
                        {{ csrf_field() }}
                        <td style="width: auto;">
                           <?php if ($data['tmplvarid'] == 154): ?>
                              <div class="form-inline form-group">
                                 {!! Form::hidden('current_value', $value['value'], ['id' => 'current_value', 'data-id' => $value['id']]) !!}
                                 <select id="studios_list" size="1" class="form-control" style="max-width: 60%" data-id="{{ $value['id'] }}">
                                    @foreach ($studios as $studio)
                                       @if ($value['value'] == $studio['id'])
                                          <option selected="" value="{{ $studio['id'] }}">{{ $studio['title'] }}</option>
                                       @else
                                          <option value="{{ $studio['id'] }}">{{ $studio['title'] }}</option>
                                       @endif
                                    @endforeach
                                 </select>
                              </div>
                              <script type="text/javascript">
                                 $('select[data-id=' + 
                                       <?php echo $value['id']; ?> +
                                    ']').on('change', function(event) { 
                                    $('input[data-id=' + 
                                       <?php echo $value['id']; ?> +
                                       ']').val(this.value);
                                 });
                              </script>
                           <?php elseif ($data['tmplvarid'] == 170): ?>
                              {!! gmdate("Y.m.d", $value['value']); !!}
                           <?php elseif ($data['tmplvarid'] == 173): ?>
                              {!! gmdate("Y.m.d", $value['value']); !!}
                           <?php else: ?>
                              {!! Form::text('current_value', $value['value'], ['id' => 'current_value', 'data-id' => $value['id']]) !!}
                           <?php endif; ?>
                        </td>
                        <td>
                           {!! Form::submit('Сохранить', ['id' => 'getTable', 'class' => 'tvFieldSave']) !!}
                        </td>
                        {!! Form::hidden('value_id', $value['value_id'], ['id' => 'value_id']) !!}
                        {!! Form::hidden('countOnPageSelectValue', $data['count_on_page_select_value'], ['id' => 'countOnPageSelectValue']) !!}
                     {{ Form::close() }}
                  </tr>
               <?php endforeach; ?>
            </tbody>
         </table>
      </div>
   <?php else: ?>
      <div id="tableWrap">
         <div id="imgShow"></div>
         <table class="table table-striped table-bordered table-condensed" id="tagTableMain" style="font-size: 10px">
            <thead>
               <tr>
                  <th>№</th>
                  <th>id</th>
                  <th>Название</th>
                  <th>Английский постер</th>
                  <th>Русский постер</th>
                  <th>Неоффициальный постер</th>
                  <th>К замене</th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               <?php foreach ($data as $value): ?>
                  <?php $i++; ?>
                  <?php if ($i < 3): ?>
                     <?php continue; ?>
                  <?php endif; ?>
                  <tr>
                     <td>{{ $i-2 }}</td>
                     <td>{{ $value['id'] }}</td>
                     <td><a href="{{ $value['uri'] }}">{{ $value['title'] }}</a></td>
                     {{ Form::open(array('url' => '/manager/editadditionalparams', 'method' => 'post', 'id' => 'edit_additional_params')) }}
                        {{ csrf_field() }}
                        <div class="form-inline form-group">
                           {!! Form::hidden('current_value', $value['value'], ['id' => 'current_value', 'data-id' => $value['id']]) !!}
                           <td>
                              {!! Form::radio('radio_poster_' . $value['id'], '0', false, ['data-id' => $value['id'], 'data-value' => 0]) !!}
                           </td>
                           <td>
                              {!! Form::radio('radio_poster_' . $value['id'], '1', false, ['data-id' => $value['id'], 'data-value' => 1]) !!}
                           </td>
                           <td>
                              {!! Form::radio('radio_poster_' . $value['id'], '2', false, ['data-id' => $value['id'], 'data-value' => 2]) !!}
                           </td>
                           <td>
                              {!! Form::radio('radio_poster_' . $value['id'], '3', false, ['data-id' => $value['id'], 'data-value' => 3]) !!}
                           </td>
                           <?php if (!is_null($value['value']) && $value['value'] != ''): ?>
                              <script type="text/javascript">
                                 $(':radio[data-id={{ $value['id'] }}][data-value={{ $value['value'] }}]').attr('checked', true);

                                 $(':radio[data-id={{ $value['id'] }}]').change(function (event) {
                                    $('#current_value[data-id={{ $value['id'] }}]').val(this.value);
                                 });
                              </script>
                           <?php endif; ?>
                        </div>
                        <td>
                           {!! Form::submit('Сохранить', ['id' => 'getTable', 'class' => 'tvFieldSave']) !!}
                        </td>
                        {!! Form::hidden('value_id', $value['value_id'], ['id' => 'value_id']) !!}
                        {!! Form::hidden('countOnPageSelectValue', $data['count_on_page_select_value'], ['id' => 'countOnPageSelectValue']) !!}
                     {{ Form::close() }}
                  </tr>
               <?php endforeach; ?>
            </tbody>
         </table>
      </div>
   <?php endif; ?>
<?php else: ?>
   <div style="margin-bottom: 2.5%">
      <span>Выберете данные для вывода</span>
   </div>
<?php endif; ?>
<script type="text/javascript">
   $(document).ready(function(){
      $('#countOnPageSelect').on('change', function(event) { 
         $('#countOnPageSelectValue').val(this.value);
      });

      $('#tvId').on('change', function(event) { 
         $('#tvIdValue').val(this.value);
      });
   });

   var regTemplate = new RegExp('(.*)\\?.*');
   var value = location.href.replace(regTemplate, '$1');
   window.history.pushState(null, null, value);
</script>
<style>
   #imgShow {
      display:none;
      position: fixed;
      top:100px;
      left:350px;
      min-height:150px;
      min-width:150px;
   }

   .imgShow i {
      cursor:pointer;
   }
</style>
@endsection