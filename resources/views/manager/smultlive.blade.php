@extends('layouts.manager')
@section('container')
<div class="col-md-9">
   <h2 class="text-center">Добавление SmultLive</h2>
   @if (!isset($item))
   {{ Form::open(array('url' => '/manager/addsmultlive', 'method' => 'post', 'id' => 'add_mult_form', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <input type="hidden" name="resource_id" id="resource_id" value="">
   <div class="form-group">
      {!! Form::text('first_title', '', ['id' => 'first_title', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Название...']) !!}
      @if ($errors->has('first_title'))
      <span class="help-block">
      <strong>{!! $errors->first('first_title') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('multId', '', ['id' => 'multId', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'id мультфильма...']) !!}
      @if ($errors->has('multId'))
      <span class="help-block">
      <strong>{!! $errors->first('multId') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::textarea('second_title', NULL, ['class' => 'form-control', 'placeholder' => 'Описание для материала для поисковиков...', 'style' => 'height: 150px;']) !!}
      @if ($errors->has('second_title'))
      <span class="help-block">
      <strong>{!! $errors->first('second_title') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Картинка для smultlive</div>
               <script> 
                  /*
                     document.getElementById('main_uploadButton').addEventListener('click', function() {
                                                                     document.getElementById('main_file').click();
                                                                  });     
                  */
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });                 
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;']) !!}
               @if ($errors->has('main_file'))
               <span class="help-block">
               <strong>{!! $errors->first('main_file') !!}</strong>
               </span>
               @endif
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6">
            <h4>Курсор не на изображении</h4>
            <div class="form-group form-inline">
               {!! Form::hidden('first_title_vertical', NULL) !!}
               @if ($errors->has('first_title_vertical'))
               <span class="help-block">
               <strong>{!! $errors->first('first_title_vertical') !!}</strong>
               </span>
               @endif
               <label>Расположение заголовка</label>
               <select id="first_title_vertical" size="1" class="form-control" name="first_title_vertical">
                  <option value="top: 10px;">Сверху</option>
                  <option value="bottom: 10px;">Снизу</option>
               </select>
            </div>
            <div class="form-group form-inline">
               <label>Расположение заголовка по горизонтали</label>
               {!! Form::hidden('first_title_align', NULL) !!}
               @if ($errors->has('first_title_align'))
               <span class="help-block">
               <strong>{!! $errors->first('first_title_align') !!}</strong>
               </span>
               @endif
               <select id="first_title_align" size="1" class="form-control">
                  <option value="text-align:left;">Слева</option>
                  <option value="text-align:center;">По центру</option>
                  <option value="text-align:right;">Справа</option>
               </select>
            </div>
         </div>
         <div class="col-md-6">
            <h4>Курсор на изображении</h4>
            <div class="form-group form-inline">
               <label>Расположение заголовка</label>
               {!! Form::hidden('second_title_vertical', NULL) !!}
               @if ($errors->has('second_title_vertical'))
               <span class="help-block">
               <strong>{!! $errors->first('second_title_vertical') !!}</strong>
               </span>
               @endif
               <select id="second_title_vertical" size="1" class="form-control">
                  <option value="top: 10px;">Сверху</option>
                  <option value="bottom: 10px;">Снизу</option>
               </select>
            </div>
            <div class="form-group form-inline">
               <label>Расположение заголовка по горизонтали</label>
               {!! Form::hidden('second_title_align', NULL) !!}
               @if ($errors->has('second_title_align'))
               <span class="help-block">
               <strong>{!! $errors->first('second_title_align') !!}</strong>
               </span>
               @endif
               <select id="second_title_align" size="1" class="form-control">
                  <option value="text-align:left;">Слева</option>
                  <option value="text-align:center;">По центру</option>
                  <option value="text-align:right;">Справа</option>
               </select>
            </div>
         </div>
      </div>
   </div>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::button('Разместить SmultLive', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {!! Form::hidden('af_action', 'eddfe6cfa4f0a386515dad406ace3b0b') !!}
   {{ Form::close() }}
   @else
   <?php 
      $posterURI = NULL;
      $firstTitleVertical = NULL;
      $firstTitleAlign = NULL;
      $secondTitleVertical = NULL;
      $secondTitleAlign = NULL;
      $settings = $item->settings;
      $pattern = '/color:.*;(.*: .*;)font-size:40;(.*)color:.*;(.*: .*;)font-size:32;(.*)/'; 
      preg_match($pattern, $settings, $matches);
      if (isset($matches[1])) {
        $firstTitleVertical = $matches[1];
      }
      if (isset($firstTitleVertical[2])) {
        $firstTitleAlign = $matches[2];
      }
      if (isset($firstTitleVertical[3])) {
        $secondTitleVertical = $matches[3];
      }
      if (isset($firstTitleVertical[4])) {
        $secondTitleAlign = $matches[4];
      }
   ?>
   @foreach ($item->kpxzxsitetmplvarcontentvalue as $value)
      @if ($value->tmplvarid == 142)
         <?php $posterURI = $value->value; ?>
      @endif
   @endforeach
   {{ Form::open(array('url' => '/manager/editsmultlive', 'method' => 'post', 'id' => 'edit_mult_form', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   {!! Form::hidden('item_id', $item->id) !!}
   @if ($errors->has('item_id'))
   <span class="help-block">
   <strong>{!! $errors->first('item_id') !!}</strong>
   </span>
   @endif
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <input type="hidden" name="resource_id" id="resource_id" value="">
   <div class="form-group">
      {!! Form::text('first_title', $item->first_title, ['id' => 'first_title', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Название...']) !!}
      @if ($errors->has('first_title'))
      <span class="help-block">
      <strong>{!! $errors->first('first_title') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('multId', $item->multId, ['id' => 'multId', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'id мультфильма...']) !!}
      @if ($errors->has('multId'))
      <span class="help-block">
      <strong>{!! $errors->first('multId') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::textarea('second_title', $item->second_title, ['class' => 'form-control', 'placeholder' => 'Описание для материала для поисковиков...', 'style' => 'height: 150px;']) !!}
      @if ($errors->has('second_title'))
      <span class="help-block">
      <strong>{!! $errors->first('second_title') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Картинка для smultlive</div>
               <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  }); 

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }                          
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if (!is_null($posterURI))
                  <img id="poster" src="{{ preg_replace('/assets/', 'public', $posterURI) }}" height="80" alt="Постер..." style="margin-left: 2.5%">
               @else
                  <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
               @endif
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-6">
            <h4>Курсор не на изображении</h4>
            <div class="form-group form-inline">
               {!! Form::hidden('first_title_vertical', $firstTitleVertical) !!}
               @if ($errors->has('first_title_vertical'))
               <span class="help-block">
               <strong>{!! $errors->first('first_title_vertical') !!}</strong>
               </span>
               @endif
               <label>Расположение заголовка</label>
               <select id="first_title_vertical" size="1" class="form-control" name="first_title_vertical">
                  @if ($firstTitleVertical == 'bottom: 10px;') 
                  <option value="top: 10px;">Сверху</option>
                  <option value="bottom: 10px;" selected="selected">Снизу</option>
                  @else
                  <option value="top: 10px;" selected="selected">Сверху</option>
                  <option value="bottom: 10px;">Снизу</option>
                  @endif
               </select>
            </div>
            <div class="form-group form-inline">
               <label>Расположение заголовка по горизонтали</label>
               {!! Form::hidden('first_title_align', $firstTitleAlign) !!}
               @if ($errors->has('first_title_align'))
               <span class="help-block">
               <strong>{!! $errors->first('first_title_align') !!}</strong>
               </span>
               @endif
               <select id="first_title_align" size="1" class="form-control">
                  @if ($firstTitleAlign == 'text-align:left;')
                  <option value="text-align:left;"  selected="selected">Слева</option>
                  <option value="text-align:center;">По центру</option>
                  <option value="text-align:right;">Справа</option>
                  @elseif($firstTitleAlign == 'text-align:center;')
                  <option value="text-align:left;">Слева</option>
                  <option value="text-align:center;" selected="selected">По центру</option>
                  <option value="text-align:right;">Справа</option>
                  @else
                  <option value="text-align:left;">Слева</option>
                  <option value="text-align:center;">По центру</option>
                  <option value="text-align:right;" selected="selected">Справа</option>
                  @endif
               </select>
            </div>
         </div>
         <div class="col-md-6">
            <h4>Курсор на изображении</h4>
            <div class="form-group form-inline">
               <label>Расположение заголовка</label>
               {!! Form::hidden('second_title_vertical', $secondTitleVertical) !!}
               @if ($errors->has('second_title_vertical'))
               <span class="help-block">
               <strong>{!! $errors->first('second_title_vertical') !!}</strong>
               </span>
               @endif
               <select id="second_title_vertical" size="1" class="form-control">
                  @if ($secondTitleVertical == 'bottom: 10px;')
                  <option value="top: 10px;">Сверху</option>
                  <option value="bottom: 10px;" selected="selected">Снизу</option>
                  @else
                  <option value="top: 10px;" selected="selected">Сверху</option>
                  <option value="bottom: 10px;">Снизу</option>
                  @endif
               </select>
            </div>
            <div class="form-group form-inline">
               <label>Расположение заголовка по горизонтали</label>
               {!! Form::hidden('second_title_align', $secondTitleAlign) !!}
               @if ($errors->has('second_title_align'))
               <span class="help-block">
               <strong>{!! $errors->first('second_title_align') !!}</strong>
               </span>
               @endif
               <select id="second_title_align" size="1" class="form-control">
                  @if ($secondTitleAlign == 'text-align:left;')
                  <option value="text-align:left;"  selected="selected">Слева</option>
                  <option value="text-align:center;">По центру</option>
                  <option value="text-align:right;">Справа</option>
                  @elseif($secondTitleAlign == 'text-align:center;')
                  <option value="text-align:left;">Слева</option>
                  <option value="text-align:center;" selected="selected">По центру</option>
                  <option value="text-align:right;">Справа</option>
                  @else
                  <option value="text-align:left;">Слева</option>
                  <option value="text-align:center;">По центру</option>
                  <option value="text-align:right;" selected="selected">Справа</option>
                  @endif
               </select>
            </div>
         </div>
      </div>
   </div>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::button('Сохранить SmultLive', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {!! Form::hidden('af_action', 'eddfe6cfa4f0a386515dad406ace3b0b') !!}
   {{ Form::close() }}
   @endif
   <div id="after-form"></div>
   <div id="seotable"></div>
</div>
<script>
   $('head').append('<link rel="stylesheet" href="/public/css/admin/smultlive.css">');
   
   $('input[name=first_title_vertical]').val($('#first_title_vertical').val());
   $('#first_title_vertical').on('change', function(event) {
     $('input[name=first_title_vertical]').val($(this).val());
   })
   
   $('input[name=first_title_align]').val($('#first_title_align').val());
   $('#first_title_align').on('change', function(event) {
     $('input[name=first_title_align]').val($(this).val());
   })
   
   $('input[name=second_title_vertical]').val($('#second_title_vertical').val());
   $('#second_title_vertical').on('change', function(event) {
     $('input[name=second_title_vertical]').val($(this).val());
   })
   
   $('input[name=second_title_align]').val($('#second_title_align').val());
   $('#second_title_align').on('change', function(event) {
     $('input[name=second_title_align]').val($(this).val());
   })
   
   /**
   * Created by vova on 05.10.2015.
   */
   if( typeof posterList !=="undefined")
     for(var i=0;i<posterList.length;i++){
   
         var posterFromData=posterList[i]
   
   
         var fileName=posterFromData.prefix+'_file'
         $.ajax_upload($('#'+posterFromData.prefix+'_uploadButton'), {
             action : '/test',
             name : fileName,
             onSubmit : function(file, ext) {
                 var prefix=$(this.button).data('prefix')
                 if (! (ext  && /^(jpg|jpeg)$/.test(ext))){
                     $('#'+prefix+'_message').html('только JPG файлы могут быть загружены');
                     return  false;
                 }
                 $('#'+prefix+'_loader').removeClass('hide');
   
                 this.disable();
   
             },
             onComplete : function(file, response) {
                 var prefix=$(this.button).data('prefix')
                 var input = $(this.input);
                 $('#'+prefix+'_loader').addClass('hide')
                 // снова включаем кнопку
                 this.enable();
   
                 input.hide()
                 input.attr('id',input.attr('name'))
   
                 //// показываем что файл загружен
                 $('#'+prefix+'_poster').val(response);
                 $('#'+prefix+'_posterChange').val('1');
                 $('#'+prefix+'_preview').removeClass('hide').html('<img width="50px" src="/'+response+'"/>');
   
                 if ($("div").is(".live_adminka")) {
                     settingsLive.img = response;
                     setStyleLive(settings)
                 }
   
   
             }
         });
         var fileButton=$('[name="'+posterFromData.prefix+'_file"]')
         fileButton.hide()
         fileButton.attr('id',fileName)
         fileButton.attr('data-prefix',posterFromData.prefix)
         $('#'+posterFromData.prefix+'_uploadButton').click(function(){
             $('#'+$(this).data('button-id')).click()
         })
   
   
     }
     //alert(posterList[i].prefix  )
   
</script>
@endsection