@extends('layouts.manager')
@section('container')
<div class="col-md-9">
   <h2 class="text-center">Добавление мультфильма</h2>
   @if (!isset($item)) 
   {{ Form::open(array('url' => '/manager/addmult', 'method' => 'post', 'id' => 'add_mult_form', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <input type="hidden" name="resource_id" id="resource_id" value="">
   <div class="form-group">
      {!! Form::text('pagetitle', '', ['id' => 'pagetitle', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Заголовок...']) !!}
      @if ($errors->has('pagetitle'))
      <span class="help-block">
      <strong>{!! $errors->first('pagetitle') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('alias', '', ['id' => 'alias', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Url']) !!}
      @if ($errors->has('alias'))
      <span class="help-block">
      <strong>{!! $errors->first('alias') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Загрузить постер</div>
               <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if ($errors->has('main_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first('main_file') !!}</strong>
                  </span>
               @endif
               <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   {!! Form::hidden('main_poster', '', ['id' => 'main_poster']) !!}
   {!! Form::hidden('main_posterChange', '', ['id' => 'main_posterChange']) !!}
   <script>
      var arr = {'prefix': 'main','uid':'787'};
      posterList.push(arr)
   </script>
   <div class="form-group form-inline">
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '0', true, ['class' => 'type-poster-radio']) !!}      
         Английский постер   
         </label>
      </div>
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '1', false, ['class' => 'type-poster-radio']) !!}      
         Неоффициальный постер   
         </label>
      </div>
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '2', false, ['class' => 'type-poster-radio']) !!}      
         Русский постер   
         </label>
      </div>
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '3', false, ['class' => 'type-poster-radio']) !!}      
         К замене  
         </label>
      </div>
      {!! Form::hidden('typeposter', '0', ['id' => 'type-poster-input']) !!}
      @if ($errors->has('typeposter'))
      <span class="help-block">
      <strong>{!! $errors->first('typeposter') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline">
      {!! Form::label('date_of_premiere', ' Дата премьеры:') !!}
      {!! Form::date('date_of_premiere', '', ['id' => 'date_of_premiere', 'class' => 'form-control']) !!}
      @if ($errors->has('date_of_premiere'))
      <span class="help-block">
      <strong>{!! $errors->first('date_of_premiere') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline">
      {!! Form::label('input_notranslate', 'Нет русской озвучки') !!}
      {!! Form::checkbox('name', '0', false, ['id' => 'input_notranslate']) !!}
      {!! Form::hidden('notranslate', 0, ['id' => 'notranslate']) !!}
      @if ($errors->has('notranslate'))
      <span class="help-block">
      <strong>{!! $errors->first('notranslate') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline">
      {!! Form::label('input_nooriginal', 'Нет русского официального названия') !!}
      {!! Form::checkbox('name', '0', false, ['id' => 'input_nooriginal']) !!}
      {!! Form::hidden('nooriginal', 0, ['id' => 'nooriginal']) !!}
      @if ($errors->has('nooriginal'))
      <span class="help-block">
      <strong>{!! $errors->first('nooriginal') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('istok', '', ['placeholder' => 'Ссылка на источник материала', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('istok'))
      <span class="help-block">
      <strong>{!! $errors->first('istok') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('url_full_mult', '', ['placeholder' => 'Ссылка на видео', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('url_full_mult'))
      <span class="help-block">
      <strong>{!! $errors->first('url_full_mult') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('original_title', '', ['placeholder' => 'Оригинальное название', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('original_title'))
      <span class="help-block">
      <strong>{!! $errors->first('original_title') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">   
      {!! Form::text('slogan', '', ['placeholder' => 'Слоган', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('slogan'))
      <span class="help-block">
      <strong>{!! $errors->first('slogan') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('agelimit', '', ['placeholder' => 'Возрастное ограничение', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('agelimit'))
      <span class="help-block">
      <strong>{!! $errors->first('agelimit') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_year', '', ['placeholder' => 'Год выпуска мультфильма', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
   </div>
   <div class="form-group">
      {!! Form::text('mult_released', '', ['placeholder' => 'Выпустившая страна', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_released'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_released') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_sound', '', ['placeholder' => 'Кто озвучивал', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_sound'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_sound') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_quality', '', ['placeholder' => 'Качество ролика на сайте', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_quality'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_quality') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_director', '', ['placeholder' => 'Режиссеры', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_director'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_director') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-inline form-group">
      <select id="studios_list" size="1" class="form-control">
         <?php $lastStudio = end($studios); ?>
         @foreach ($studios as $studio)
         @if ($studio['id'] != $lastStudio['id'])
         <option value="{{ $studio['id'] }}">{{ $studio['title'] }}</option>
         @else
         <option selected="" value="{{ $studio['id'] }}">{{ $studio['title'] }}</option>
         @endif
         @endforeach
      </select>
   </div>
   {!! Form::hidden('studios', NULL, ['id' => 'studios']) !!}
   @if ($errors->has('studios'))
   <span class="help-block">
   <strong>{!! $errors->first('studios') !!}</strong>
   </span>
   @endif
   <div class="container-fluid form-group">
      <div class="row form-inline">
         <div class="checkbox" id="razdel_list">
            @foreach ($categories as $category)
            <div class="col-md-6">
               {!! Form::checkbox(NULL, ':' . $category->id . ':', false, ['id' => 'category_' . $category->id, 'data-categoryid' => $category->id]) !!}
               {!! Form::label('category_' . $category->id, $category->pagetitle) !!}
            </div>
            @endforeach
         </div>
      </div>
   </div>
   {!! Form::hidden('Lict_Razdel', NULL, ['id' => 'Lict_Razdel']) !!}
   <div class="form-group">
      {!! Form::textarea('description', NULL, ['class' => 'form-control', 'placeholder' => 'Описание для материала для поисковиков...', 'style' => 'height: 150px;']) !!}
      @if ($errors->has('description'))
      <span class="help-block">
      <strong>{!! $errors->first('description') !!}</strong>
      </span>
      @endif
   </div>
   <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}" charset="utf-8" ></script>
   <div class="form-group{!! $errors->has('editor1') ? ' has-error' : '' !!}">
      {!! Form::label('editor1', ' ', ['class' => 'control-label']) !!}
      {!! Form::textarea('editor1', NULL, ['class' => 'form-control', 'id' => 'editor1']) !!}
      @if ($errors->has('editor1'))
      <span class="help-block">
      <strong>{!! $errors->first('editor1') !!}</strong>
      </span>
      @endif
   </div>
   <script>
      var editor = CKEDITOR.replace('editor1', {
                     filebrowserBrowseUrl : '/elfinder/ckeditor',
                     disableNativeSpellChecker: false
                   });
   </script>
   <div class="form-group">
      {!! Form::text('articles_with_articles', '', ['placeholder' => 'Статьи по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('articles_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('articles_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_with_articles', '', ['placeholder' => 'Мультфильмы по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('custom_button', '', ['placeholder' => 'Текст кастомной кнопки', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
   </div>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/public/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::text('ispublish', '1', ['id' => 'ispublish', 'style' => 'display:none']) !!}
      {!! Form::button('Проверить текст', ['id' => 'text-validate', 'class' => 'btn btn-success']) !!}
      {!! Form::button('Разместить мультфильм', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {!! Form::hidden('af_action', 'eddfe6cfa4f0a386515dad406ace3b0b') !!}
   {{ Form::close() }}
   @if (app()->request->session()->has('link'))
      <a href='/{{app()->request->session()->get("link") }}'>Ссылка на новодобавленный материал</a>
   @endif
   <div id="after-form"></div>
   <div id="seotable"></div>
   @else
   <?php $posterType = NULL; ?>
   <?php $dateOfPremiere = NULL; ?>
   <?php $noorigin = NULL; ?>
   <?php $istok = NULL; ?>
   <?php $notranslate = NULL; ?>
   <?php $urlFullMult = NULL; ?>
   <?php $customButton = NULL; ?>
   <?php $multWithArticles = NULL; ?>
   <?php $articlesWithArticles = NULL; ?>
   <?php $lictRazdel = NULL; ?>
   <?php $multYear = NULL; ?>
   <?php $multReleased = NULL; ?>
   <?php $slogan = NULL; ?>
   <?php $multQuality = NULL; ?>
   <?php $multDirector = NULL; ?>
   <?php $multStudio = NULL; ?>
   <?php $agelimit = NULL; ?>
   <?php $originalTitle = NULL; ?>
   <?php $posterURI = NULL; ?>
   <?php $sound = NULL; ?>
   @foreach ($item->kpxzxsitetmplvarcontentvalue as $value)
   @if ($value->tmplvarid == 171)
   <?php $posterType = $value->value; ?>  
   @elseif ($value->tmplvarid == 173)
   <?php $dateOfPremiere = $value->value; ?>
   @elseif ($value->tmplvarid == 189)
   <?php $notranslate = $value->value; ?>
   @elseif ($value->tmplvarid == 195)
   <?php $noorigin = $value->value; ?>
   @elseif ($value->tmplvarid == 163)
   <?php $istok = $value->value; ?>
   @elseif ($value->tmplvarid == 137)
   <?php $urlFullMult = $value->value; ?>
   @elseif ($value->tmplvarid == 181)
   <?php $customButton = $value->value; ?>
   @elseif ($value->tmplvarid == 179)
   <?php $multWithArticles = $value->value; ?>
   @elseif ($value->tmplvarid == 180)
   <?php $articlesWithArticles = $value->value; ?>
   @elseif ($value->tmplvarid == 174)
   <?php $lictRazdel = $value->value; ?>
   @elseif ($value->tmplvarid == 143)
   <?php $multYear = $value->value; ?>
   @elseif ($value->tmplvarid == 144)
   <?php $multReleased = $value->value; ?>
   @elseif ($value->tmplvarid == 145)
   <?php $slogan = $value->value; ?>
   @elseif ($value->tmplvarid == 148)
   <?php $multQuality = $value->value; ?>
   @elseif ($value->tmplvarid == 149)
   <?php $multDirector = $value->value; ?>
   @elseif ($value->tmplvarid == 154)
   <?php $multStudio = $value->value; ?>
   @elseif ($value->tmplvarid == 155)
   <?php $agelimit = $value->value; ?>
   @elseif ($value->tmplvarid == 156)
   <?php $originalTitle = $value->value; ?>
   @elseif ($value->tmplvarid == 147)
   <?php $sound = $value->value; ?>
   @elseif ($value->tmplvarid == 142)
   <?php $posterURI = $value->value; ?>
   @endif
   @endforeach
   {{ Form::open(array('url' => '/manager/editmult', 'method' => 'post', 'id' => 'edit_mult_form', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   {!! Form::hidden('itemId', $item->id) !!}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <div class="form-group">
      {!! Form::text('pagetitle', $item->pagetitle, ['id' => 'pagetitle', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Заголовок...']) !!}
      @if ($errors->has('pagetitle'))
      <span class="help-block">
      <strong>{!! $errors->first('pagetitle') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('alias', $item->alias, ['id' => 'alias', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Url']) !!}
      @if ($errors->has('alias'))
      <span class="help-block">
      <strong>{!! $errors->first('alias') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Загрузить постер</div>
                              <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }          
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if ($errors->has('main_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first('main_file') !!}</strong>
                  </span>
               @endif
               @if (!is_null($posterURI))
                  <img id="poster" src="{{ preg_replace('/assets/', 'public', $posterURI) }}" height="80" alt="Постер..." style="margin-left: 2.5%">
               @else
                  <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
               @endif
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   {!! Form::hidden('main_poster', '', ['id' => 'main_poster']) !!}
   {!! Form::hidden('main_posterChange', '', ['id' => 'main_posterChange']) !!}
   <script>
      var arr = {'prefix': 'main','uid':'787'};
      posterList.push(arr)
   </script>
   <div class="form-group form-inline">
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '0', true, ['class' => 'type-poster-radio']) !!}      
         Английский постер   
         </label>
      </div>
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '1', false, ['class' => 'type-poster-radio']) !!}      
         Неоффициальный постер   
         </label>
      </div>
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '2', false, ['class' => 'type-poster-radio']) !!}      
         Русский постер   
         </label>
      </div>
      <div class="radio">
         <label>     
         {!! Form::radio('type-poster-radio', '3', false, ['class' => 'type-poster-radio']) !!}      
         К замене  
         </label>
      </div>
      {!! Form::hidden('typeposter', $posterType, ['id' => 'type-poster-input']) !!}
      @if ($errors->has('typeposter'))
      <span class="help-block">
      <strong>{!! $errors->first('typeposter') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline">
      {!! Form::label('date_of_premiere', ' Дата премьеры:') !!}
      {!! Form::date('date_of_premiere',  \Carbon\Carbon::createFromTimestamp($dateOfPremiere), ['id' => 'date_of_premiere', 'class' => 'form-control'], 'd/m/Y') !!}
      @if ($errors->has('date_of_premiere'))
      <span class="help-block">
      <strong>{!! $errors->first('date_of_premiere') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline">
      {!! Form::label('input_notranslate', 'Нет русской озвучки') !!}
      {!! Form::checkbox('name', '0', false, ['id' => 'input_notranslate']) !!}
      {!! Form::hidden('notranslate', $notranslate, ['id' => 'notranslate']) !!}
      @if ($errors->has('notranslate'))
      <span class="help-block">
      <strong>{!! $errors->first('notranslate') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline">
      {!! Form::label('input_nooriginal', 'Нет русского официального названия') !!}
      {!! Form::checkbox('name', '0', false, ['id' => 'input_nooriginal']) !!}
      {!! Form::hidden('nooriginal', $noorigin, ['id' => 'nooriginal']) !!}
      @if ($errors->has('nooriginal'))
      <span class="help-block">
      <strong>{!! $errors->first('nooriginal') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('istok', $istok, ['placeholder' => 'Ссылка на источник материала', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('istok'))
      <span class="help-block">
      <strong>{!! $errors->first('istok') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('url_full_mult', $urlFullMult, ['placeholder' => 'Ссылка на видео', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('url_full_mult'))
      <span class="help-block">
      <strong>{!! $errors->first('url_full_mult') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('original_title', $originalTitle, ['placeholder' => 'Оригинальное название', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('original_title'))
      <span class="help-block">
      <strong>{!! $errors->first('original_title') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('slogan', $slogan, ['placeholder' => 'Слоган', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('slogan'))
      <span class="help-block">
      <strong>{!! $errors->first('slogan') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('agelimit', $agelimit, ['placeholder' => 'Возрастное ограничение', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('agelimit'))
      <span class="help-block">
      <strong>{!! $errors->first('agelimit') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_year', $multYear, ['placeholder' => 'Год выпуска мультфильма', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_year'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_year') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_released', $multReleased, ['placeholder' => 'Выпустившая страна', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_released'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_released') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_sound', $sound, ['placeholder' => 'Кто озвучивал', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_sound'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_sound') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_quality', $multQuality, ['placeholder' => 'Качество ролика на сайте', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_quality'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_quality') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_director', $multDirector, ['placeholder' => 'Режиссеры', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_director'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_director') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-inline form-group">
      <select id="studios_list" size="1" class="form-control">
         <?php $lastStudio = end($studios); ?>
         @foreach ($studios as $studio)
         @if ($studio['id'] != $multStudio)
         <option value="{{ $studio['id'] }}">{{ $studio['title'] }}</option>
         @else
         <option selected="" value="{{ $studio['id'] }}">{{ $studio['title'] }}</option>
         @endif
         @endforeach
      </select>
   </div>
   {!! Form::hidden('studios', $multStudio, ['id' => 'studios']) !!}
   @if ($errors->has('studios'))
   <span class="help-block">
   <strong>{!! $errors->first('studios') !!}</strong>
   </span>
   @endif
   <div class="container-fluid form-group">
      <div class="row form-inline">
         <div class="checkbox" id="razdel_list">
            <?php
               $categoriesIds = preg_replace('/^:|:$/', '', $lictRazdel);
               $categoryIds = explode('::', $categoriesIds);
               ?>
            @foreach ($categories as $category)
            @if (in_array($category->id, $categoryIds))
            <div class="col-md-6">
               {!! Form::checkbox(NULL, ':' . $category->id . ':', true, ['id' => 'category_' . $category->id, 'data-categoryid' => $category->id]) !!}
               {!! Form::label('category_' . $category->id, $category->pagetitle) !!}
            </div>
            @else
            <div class="col-md-6">
               {!! Form::checkbox(NULL, ':' . $category->id . ':', false, ['id' => 'category_' . $category->id, 'data-categoryid' => $category->id]) !!}
               {!! Form::label('category_' . $category->id, $category->pagetitle) !!}
            </div>
            @endif
            @endforeach
         </div>
      </div>
   </div>
   {!! Form::hidden('Lict_Razdel', $lictRazdel, ['id' => 'Lict_Razdel']) !!}
   <div class="form-group">
      {!! Form::textarea('description', $item->description, ['class' => 'form-control', 'placeholder' => 'Описание для материала для поисковиков...', 'style' => 'height: 150px;']) !!}
      @if ($errors->has('description'))
      <span class="help-block">
      <strong>{!! $errors->first('description') !!}</strong>
      </span>
      @endif
   </div>
   <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}" charset="utf-8" ></script>
   <div class="form-group{!! $errors->has('editor1') ? ' has-error' : '' !!}">
      {!! Form::label('editor1', ' ', ['class' => 'control-label']) !!}
      {!! Form::textarea('editor1', $item->content, ['class' => 'form-control', 'id' => 'editor1']) !!}
      @if ($errors->has('editor1'))
      <span class="help-block">
      <strong>{!! $errors->first('editor1') !!}</strong>
      </span>
      @endif
   </div>
   <script>
      var editor = CKEDITOR.replace('editor1', {
                     filebrowserBrowseUrl : '/elfinder/ckeditor',
                     disableNativeSpellChecker: false,
                   });
   </script>
   <div class="form-group">
      {!! Form::text('articles_with_articles', $articlesWithArticles, ['placeholder' => 'Статьи по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('articles_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('articles_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_with_articles', $multWithArticles, ['placeholder' => 'Мультфильмы по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('custom_button', $customButton, ['placeholder' => 'Текст кастомной кнопки', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
   </div>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::text('ispublish', '1', ['id' => 'ispublish', 'style' => 'display:none']) !!}
      {!! Form::button('Проверить текст', ['id' => 'text-validate', 'class' => 'btn btn-success']) !!}
      {!! Form::button('Сохранить мультфильм', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {!! Form::hidden('af_action', 'eddfe6cfa4f0a386515dad406ace3b0b') !!}
   {{ Form::close() }}
   <div id="after-form"></div>
   <div id="seotable"></div>
   @endif
</div>
<script>
   $(document).ready(function(){
      var value;
      var regTemplate;
      
      value = $('#studios_list').val();
      if (value) {
         $('#studios').val(value);
      }
      
      value = $('#Lict_Razdel').val();
      regTemplate = new RegExp('^:', 'g');
      value = value.replace(regTemplate, '');
      regTemplate = new RegExp(':$', 'g');
      value = value.replace(regTemplate, '');
      var values = value.split('::');
      values.forEach(function(value, index) { 
         $('input[data-categoryid=' + value + ']').attr('checked', true);
      });
      
      $('#studios_list').on('change', function(event) { 
         $('#studios').val(this.value);
      });
      
      $('#razdel_list').on('click', '.col-md-6', function(event) { 
         if (event.target.nodeName === 'INPUT') {
            var input = $(this).find($('input'));
            var currentValue = $('#Lict_Razdel').val();
            if (input.is(':checked')) {
               value = currentValue + input.val();
               $('#Lict_Razdel').val(value);
            } else {
               value = input.val();
               regTemplate = new RegExp(value, 'i');
               value = currentValue.replace(regTemplate, '');
               $('#Lict_Razdel').val(value);
            }
         }
      });
      
      $('#input_notranslate').on('change', function(event){
         if ($('#notranslate').val() == 1) {
            $('#notranslate').val(0);
         } else {
            $('#notranslate').val(1);
         }
      });
      
      $('#input_nooriginal').on('change', function(event){
         if ($('#nooriginal').val() == 1) {
            $('#nooriginal').val(0);
         } else {
            $('#nooriginal').val(1);
         }
      });
      $('body').on('click', 'iframe', function(){
         alert(3);
      });
      $(window.frames[0].document.getElementsByClassName('cke_show_borders')).prop('spellcheck', true);
});
</script>
@endsection