@extends('layouts.manager')

@section('container')
<div class="col-md-9"> 
   <h2 class="text-center">Таблица для редактирования серий</h2>
   @if (isset($seasons))  
      <table class="table table-striped table-bordered" id="competitionMember">
         <thead>
            <tr>
               <th>id</th>
               <th>Название</th>
               <th>Дата создания</th>
               <th><span class="glyphicon glyphicon-repeat"></span> / <span class="glyphicon glyphicon-remove"></span></th>
            </tr>
         </thead>
         <tbody>
            @foreach ($seasons as $season)
               <tr>
                  <td class="title ">{{ $season['id'] }}</td>
                  @if (!$season['deleted'])
                     <td class="title ">{{ $season['title'] }}</td>
                  @else
                     <td class="title "><s>{{ $season['title'] }}</s></td>
                  @endif
                  <td class="title ">{{ gmdate('Y-m-d h:m:s', $season['start_date']) }}</td>
                  <td>
                     @if (!$season['deleted'])
                        {{ Form::open(array('url' => '/manager/delete-serie', 'method' => 'post', 'id' => 'delete-serie', 'style' => 'margin-bottom: 2.5%')) }}
                           {{ csrf_field() }}
                           {!! Form::hidden('id', $season['id'], ['id' => 'contentid']) !!}
                           {!! Form::submit('Вычеркнуть', ['id' => 'getTable', 'class' => 'tvFieldSave']) !!}
                        {{ Form::close() }}
                     @else
                        <span>Вычеркнут</span>
                     @endif
                  </td>
               </tr>
            @endforeach
         </tbody>
      </table>
   @endif
</div>
<?php if (isset($mults)): ?> 
   <div class="col-md-3" style="width: 20%; margin: 0 0 0 5%;">
      <h3>Последние материалы</h3>
      <ul class="list-group">
         @foreach ($mults as $mult)
            <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
         @endforeach
      </ul>
   </div>
<?php endif; ?>
<style>
   ul {
      margin-left: 5%;
   }
</style>
@endsection