@extends('layouts.manager')

@section('container')
<div class="col-md-9">
   <h2 class="text-center">Информация о количестве видеозаписей определенных сервисов</h2>
   <div id="servicesTableWrap">
      @if (isset($videoservicesAmount))
      {{ Form::open(array('url' => '/manager/videoservices', 'method' => 'post', 'id' => 'add_videoservices', 'style' => 'margin-bottom: 2.5%')) }}
      {{ csrf_field() }}
      <div class="form-inline">
         <div class="form-group">
            <label for="countOnPageSelect">Кол-во</label>
            {!! Form::select('', [
            '5' => '5',
            '10' => '10',
            '50' => '50',
            '100' => '100',
            '500' => '500',
            '99999' => 'Все',
            ], $count_data, ['id' => 'countOnPageSelect', 'class' => 'form-control']) !!}
            {!! Form::hidden('countOnPageSelectValue', $count_data, ['id' => 'countOnPageSelectValue']) !!}
         </div>
         <div class="form-group">
            {!! Form::submit('Обновить', ['id' => 'getTable', 'class' => 'btn btn-success']) !!}
         </div>
      </div>
      {{ Form::close() }}
      <table class="table table-striped table-bordered table-condensed" id="tagTableMain" style="font-size: 10px">
         <thead>
            <tr>
               <th>Номер</th>
               <th id="id">Ресурс</th>
               <th id="pagetitle">Количество</th>
            </tr>
         </thead>
         <tbody>
            <?php $i = 1; ?>
            @foreach ($videoservicesAmount as $videoserviceAmount)
            <tr>
               <td>{{ $i }} </td>
               <td class="services_click">{{ $videoserviceAmount['videoservice_name'] }}</td>
               <td>{{ $videoserviceAmount['amount'] }}</td>
            </tr>
            <?php $i++; ?> 
            @endforeach
         </tbody>
      </table>
      <div id="nav" class="add_remove">
          {{ $videoservicesAmount->links() }}
      </div>
      @else
      <div style="margin-bottom: 2.5%">
         <span>Данные отсутствуют</span>
      </div> 
      @endif
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function(){
      $('#countOnPageSelect').on('change', function(event) { 
         $('#countOnPageSelectValue').val(this.value);
      });
   });
</script>
@endsection