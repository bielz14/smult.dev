   @extends('layouts.manager')

@section('container')
<div class="col-md-9">
   <h2 class="text-center">Загрузка постеров</h2>
   <h4>Что делать после сохранения</h4>
   @if (isset($parentId))
      {{ Form::open(array('url' => '/manager/addresource', 'method' => 'post', 'id' => 'addResource', 'enctype' => 'multipart/form-data')) }}
               {{ csrf_field() }}
               {!! Form::hidden('parent', $parentId) !!}
               <h4>Что делать после сохранения</h4>
               <div class="radio">
                  <label>          
                     {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
                     Создать новый материал   
                  </label>
               </div>
               <div class="radio">
                  <label>     
                     {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
                     Продолжить редактирование    
                  </label>
               </div>
               <table class="table table-bordered" id="poster_list">
                  <thead>
                     <tr>
                        <th>Полный адрес картинки</th>
                        <th>Оригинальное название</th>
                        <th>Кто добавил</th>
                        <th>Кадр</th>
                     </tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
               <div class="form-inline">
                  <h4>Тип изображения</h4>
                  <div class="radio">
                     <label>
                        {!! Form::radio('isCadr', 0, true) !!}
                        Постер
                     </label>
                  </div>
                  <div class="radio">
                     <label>
                        {!! Form::radio('isCadr', 1, true) !!}
                        Кадр
                     </label>
                  </div>
               </div>
               <input type="hidden" name="multId" value="79246">
               <div class="form-group">
                  <div class="container-fluid">
                     <div class="row">
                        <div style="position: relative;">
                           <div class="col-md-4 btn btn-info" id="cadr_uploadButton" data-prefix="cadr" data-button-id="cadr_file">Загрузить постер или кадр</div>
                                             <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }          
               </script>
               {!! Form::file('cadr_file', ['id' => 'cadr_file', 'data-prefix' => 'card', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;']) !!}
               @if ($errors->has('cadr_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first(cadr_file') !!}</strong>
                  </span>
               @endif
               <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
                        </div>
                        <div class="col-md-1 hide" id="cadr_loader"><img src="/img/load.gif" alt="" width="30px"></div>
                        <div class="col-md-1" id="cadr_preview"></div>
                        <div class="col-md-6" id="cadr_message"></div>
                     </div>
                  </div>
               </div>
      {{ Form::close() }}
      <div id="after-form"></div>
      <div id="seotable"></div>
   @endif
</div>
@if (isset($mults))
   <div class="col-md-3" style="width: 20%; margin: 2.5% 0 0 5%;">
      <h3>Последние материалы</h3>
      <ul class="list-group">
         @foreach ($mults as $mult)
            <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
         @endforeach
      </ul>
   </div>
@endif
@endsection