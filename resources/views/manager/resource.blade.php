@extends('layouts.manager')
@section('container')
<div class="col-md-9">
   <h2 class="text-center">Добавление материала</h2>
   @if (isset($parentId))
   {{ Form::open(array('url' => '/manager/addresource', 'method' => 'post', 'id' => 'addResource', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   {!! Form::hidden('parent', $parentId) !!}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <div class="form-group">
      {!! Form::text('pagetitle', '', ['id' => 'pagetitle', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Заголовок...']) !!}
      @if ($errors->has('pagetitle'))
      <span class="help-block">
      <strong>{!! $errors->first('pagetitle') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('alias', '', ['id' => 'alias', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Url']) !!}
      @if ($errors->has('alias'))
      <span class="help-block">
      <strong>{!! $errors->first('alias') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Загрузить постер</div>
                              <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }          

      
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if ($errors->has('main_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first('main_file') !!}</strong>
                  </span>
               @endif
               <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="wide_uploadButton" data-prefix="wide" data-button-id="wide_file">Загрузить широкую картинку</div>
               <script> 
                  $('#wide_uploadButton').on('click', function(e) {
                        $('#wide_file').click();
                  });          

                  function previewFileWide() {
                    var preview = document.querySelector('img#poster_wide');
                    var file    = document.querySelector('input[type=file][name=wide_file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster_wide').is(':visible')) {
                           $('#poster_wide').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster_wide').is(':visible')) {
                           $('#poster_wide').toggle();
                        }
                    }
                  }          
               </script>
               {!! Form::file('wide_file', ['id' => 'wide_file', 'data-prefix' => 'wide', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none; top: 15px; left: 87.0469px;', 'onchange' => 'previewFileWide()']) !!}
               @if ($errors->has('wide_file'))
               <span class="help-block">
               <strong>{!! $errors->first('wide_file') !!}</strong>
               </span>
               @endif
               <img id="poster_wide" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
            </div>
            <div class="col-md-1 hide" id="wide_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="wide_preview"></div>
            <div class="col-md-6" id="wide_message"></div>
         </div>
      </div>
   </div>
   <div class="form-group">
      {!! Form::text('istok', '', ['placeholder' => 'Ссылка на источник материала', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('istok'))
      <span class="help-block">
      <strong>{!! $errors->first('istok') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('url_full_mult', '', ['placeholder' => 'Ссылка на видео', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('url_full_mult'))
      <span class="help-block">
      <strong>{!! $errors->first('url_full_mult') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::textarea('description', NULL, ['class' => 'form-control', 'placeholder' => 'Описание для материала для поисковиков...', 'style' => 'height: 150px;']) !!}
      @if ($errors->has('description'))
      <span class="help-block">
      <strong>{!! $errors->first('description') !!}</strong>
      </span>
      @endif
   </div>
   <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}" charset="utf-8" ></script>
   <div class="form-group{!! $errors->has('editor1') ? ' has-error' : '' !!}">
      {!! Form::label('editor1', ' ', ['class' => 'control-label']) !!}
      {!! Form::textarea('editor1', NULL, ['class' => 'form-control', 'id' => 'editor1']) !!}
      @if ($errors->has('editor1'))
      <span class="help-block">
      <strong>{!! $errors->first('editor1') !!}</strong>
      </span>
      @endif
   </div>
   <script>
      var editor = CKEDITOR.replace('editor1', {
                     filebrowserBrowseUrl : '/elfinder/ckeditor',
                     disableNativeSpellChecker: false 
                   });
   </script>
   <div class="form-group">
      {!! Form::text('articles_with_articles', '', ['placeholder' => 'Статьи по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('articles_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('articles_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_with_articles', '', ['placeholder' => 'Мультфильмы по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('custom_button', '', ['placeholder' => 'Текст кастомной кнопки', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
   </div>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::text('ispublish', '1', ['id' => 'ispublish', 'style' => 'display:none']) !!}
      {!! Form::button('Проверить текст', ['id' => 'text-validate', 'class' => 'btn btn-success']) !!}
      {!! Form::button('Разместить ресурс', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {{ Form::close() }}
   <div id="after-form"></div>
   <div id="seotable"></div>
   @elseif (isset($item))
   <?php $posterURI = NULL; ?>
   <?php $posterWideURI = NULL; ?> 
   @if (count($item->kpxzxtmplvarcontentvalue))
   @foreach ($item->kpxzxtmplvarcontentvalue as $value)
   @if ($value->tmplvarid == 142)
    <?php $posterURI = $value->value; ?>
   @elseif ($value->tmplvarid == 129)
    <?php $posterWideURI = $value->value; ?>
   @endif
   @endforeach
   @endif
   {{ Form::open(array('url' => '/manager/editresource', 'method' => 'post', 'id' => 'addResource', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   {!! Form::hidden('itemId', $item->id) !!}
   {!! Form::hidden('parent', $item->parent) !!}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <div class="form-group">
      {!! Form::text('pagetitle', $item->pagetitle, ['id' => 'pagetitle', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Заголовок...']) !!}
      @if ($errors->has('pagetitle'))
      <span class="help-block">
      <strong>{!! $errors->first('pagetitle') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('alias', $item->alias, ['id' => 'alias', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Url']) !!}
      @if ($errors->has('alias'))
      <span class="help-block">
      <strong>{!! $errors->first('alias') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Загрузить постер</div>
                              <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }          
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if ($errors->has('main_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first('main_file') !!}</strong>
                  </span>
               @endif
               @if (!is_null($posterURI))
                  <img id="poster" src="{{ preg_replace('/assets/', 'public', $posterURI) }}" height="80" alt="Постер..." style="margin-left: 2.5%">
               @else
                  <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
               @endif
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="wide_uploadButton" data-prefix="wide" data-button-id="wide_file">Загрузить широкую картинку</div>
               <script> 
                  $('#wide_uploadButton').on('click', function(e) {
                        $('#wide_file').click();
                  });                 

                  function previewFileWide() {
                    var preview = document.querySelector('img#poster_wide');
                    var file    = document.querySelector('input[type=file][name=wide_file]').files[0];files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster_wide').is(':visible')) {
                           $('#poster_wide').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster_wide').is(':visible')) {
                           $('#poster_wide').toggle();
                        }
                    }
                  }         
               </script>
               {!! Form::file('wide_file', ['id' => 'wide_file', 'data-prefix' => 'wide', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;  top: 15px; left: 87.0469px;', 'onchange' => 'previewFileWide()']) !!}
                @if (!is_null($posterWideURI))
                  <img id="poster_wide" src="{{ preg_replace('/assets/', 'public', $posterWideURI) }}" height="80" alt="Постер..." style="margin-left: 2.5%">
                @else
                  <img id="poster_wide" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
               @endif
            </div>
            <div class="col-md-1 hide" id="wide_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="wide_preview"></div>
            <div class="col-md-6" id="wide_message"></div>
         </div>
      </div>
   </div>
   <?php $articlesWithArticles = ''; ?>
   <?php $multWithArticles = ''; ?>
   @foreach ($item->kpxzxsitetmplvarcontentvalue as $value)
   @if ($value->tmplvarid == 137)
   <div class="form-group">
      {!! Form::text('istok', $value->value, ['placeholder' => 'Ссылка на источник материала', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('istok'))
      <span class="help-block">
      <strong>{!! $errors->first('istok') !!}</strong>
      </span>
      @endif
   </div>
   @elseif ($value->tmplvarid == 163)
   <div class="form-group">
      {!! Form::text('url_full_mult', $value->value, ['placeholder' => 'Ссылка на видео', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('url_full_mult'))
      <span class="help-block">
      <strong>{!! $errors->first('url_full_mult') !!}</strong>
      </span>
      @endif
   </div>
   @elseif ($value->tmplvarid == 178)
   <?php $articlesWithArticles = $value->value ?>
   @elseif ($value->tmplvarid == 179)
   <?php $multWithArticles = $value->value ?>
   @endif
   @endforeach
   <div class="form-group">
      {!! Form::textarea('description', $item->description, ['class' => 'form-control', 'placeholder' => 'Описание для материала для поисковиков...', 'style' => 'height: 150px;']) !!}
      @if ($errors->has('description'))
      <span class="help-block">
      <strong>{!! $errors->first('description') !!}</strong>
      </span>
      @endif
   </div>
   <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}" charset="utf-8" ></script>
   <div class="form-group{!! $errors->has('editor1') ? ' has-error' : '' !!}">
      {!! Form::label('editor1', ' ', ['class' => 'control-label']) !!}
      {!! Form::textarea('editor1', $item->content, ['class' => 'form-control', 'id' => 'editor1']) !!}
      @if ($errors->has('editor1'))
      <span class="help-block">
      <strong>{!! $errors->first('editor1') !!}</strong>
      </span>
      @endif
   </div>
   <script>
      var editor = CKEDITOR.replace('editor1', {
                     filebrowserBrowseUrl : '/elfinder/ckeditor',
                     disableNativeSpellChecker: false 
                   });
   </script>
   <div class="form-group">
      {!! Form::text('articles_with_articles', $articlesWithArticles, ['placeholder' => 'Статьи по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('articles_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('articles_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_with_articles', $multWithArticles, ['placeholder' => 'Мультфильмы по статье', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('mult_with_articles'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_with_articles') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('custom_button', '', ['placeholder' => 'Текст кастомной кнопки', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
   </div>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::text('ispublish', '1', ['id' => 'ispublish', 'style' => 'display:none']) !!}
      {!! Form::button('Проверить текст', ['id' => 'text-validate', 'class' => 'btn btn-success']) !!}
      {!! Form::button('Сохранить', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {{ Form::close() }}
   <div id="after-form"></div>
   <div id="seotable"></div>
   @else
   <div>
      <span style="color: red">Неизвестный тип ресурса, отсутствует параметр в URL адресе</span> 
   </div>
   @endif
</div>
@if (isset($mults))
<div class="col-md-3" style="width: 20%; margin: 2.5% 0 0 5%;">
   <h3>Последние материалы</h3>
   <ul class="list-group">
      @foreach ($mults as $mult)
      <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
      @endforeach
   </ul>
</div>
@endif
@endsection