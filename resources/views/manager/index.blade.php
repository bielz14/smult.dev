@extends('layouts.manager')

@section('container')
<div class="col-md-9">
   @if (\Auth::check() && \Auth::user()->can('managerPage'))
      <h2 class="text-center">Администрирование</h2>
      <ul>
         <li class="list-group-item">
            Добавление материалов
            <ul>
               <a class="text-info" href="/manager/mult">Добавление мультфильма</a>
               <br>
               <a class="text-info" href="/manager/multseries">Добавление мультсериала</a>
               <br>
               <a class="text-info" href="/manager/multhero">Добавление героя</a>
               <br>
               <a class="text-info" href="/manager/quote">Добавление цитат</a>
               <br>
               <a class="text-info" href="/manager/smultlive">добавление SmultLive</a>
               <br>
               <a class="text-info" href="/manager/competition">Добавление конкурса</a>
               <br><a class="text-info" href="/manager/newtest">добавление нового теста</a>
               <br><a class="text-info" href="/manager/newpoll">Добавлення нового опроса</a>
               <br>
            </ul>
         </li>
         <li class="list-group-item">
            Сервисы
            <ul>
               <a class="text-info" href="/manager/additionalparams">просмотр и изменение дополнительными параметрами</a>
               <br><a class="text-info" href="/manager/comments">Модерация коментариев</a>
               <br><a class="text-info" href="/manager/tickets/unfulfilled">Вывод заявок по ошибкам и нерабочим видео</a>
               <br><a class="text-info" href="/manager/videoservices">список видео по сервисах</a>
               <br>
            </ul>
         </li>
      </ul>
   @else 
      <h2 class="text-center">Вход в менеджер-панель</h2>
      <div class="form" style=" margin-left: 35%; width: 100%">
         {{ Form::open(array('url' => '/login-manager', 'method' => 'post', 'id' => 'add_mult_form', 'enctype' => 'multipart/form-data')) }}
            {{ csrf_field() }}
            {!! Form::email('email', '', ['id' => 'email', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Login/Email', 'style' => 'width: 30%']) !!}
            {!! Form::password('password', ['id' => 'password', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Пароль', 'style' => 'width: 30%']) !!}
            {!! Form::submit('Войти', ['id' => 'c_publish', 'class' => 'sbmBtnBlack sbmBtn', 'style' => 'width: 30%']) !!}
         {{ Form::close() }}
      </div>
   @endif
</div>
@if (\Auth::check() && \Auth::user()->can('managerPage'))
   <?php if (isset($mults)): ?> 
      <div class="col-md-3" style="width: 20%; margin: 0 0 0 5%;">
         <h3>Последние материалы</h3>
         <ul class="list-group">
            @foreach ($mults as $mult)
               <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
            @endforeach
         </ul>
      </div>
   <?php endif; ?>
@endif
<style>
   ul {
      margin-left: 5%;
   }
</style>
@endsection