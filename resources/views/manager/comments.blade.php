@extends('layouts.manager')

@section('container')
<div class="col-md-9">
   <h2 class="text-center" style="margin-bottom: 3.5%">Модерация коментариев</h2>
   {{ Form::open(array('url' => '/manager/comments', 'method' => 'post', 'id' => 'output_comments', 'style' => 'margin-bottom: 2.5%')) }}
      {{ csrf_field() }}
      <div class="form-inline">
         <div class="form-group">
            <label class="control-label" for="countOnPageSelect">Количество</label>
            {!! Form::select('', [
                '50' => '50',
                '100' => '100',
                '500' => '500'
            ], $count_data, ['id' => 'countOnPageSelect', 'class' => 'form-control']) !!}
            {!! Form::hidden('countOnPageSelectValue', $count_data, ['id' => 'countOnPageSelectValue']) !!}
         </div>
         <div class="form-group">
            <label for="comment_type">Тип</label>
            {!! Form::select('', [
                'no_moder' => 'Не отмодерированные',
                'moder' => 'Отмодерированные',
                'all' => 'Все'
            ], $type_comments, ['id' => 'comment_type', 'class' => 'form-control']) !!}
            {!! Form::hidden('comment_type_value', $type_comments, ['id' => 'comment_type_value']) !!}
         </div>
         <div class="form-group">
            {!! Form::submit('Обновить', ['id' => 'get_data', 'class' => 'btn btn-success']) !!}
         </div>
      </div>
   {{ Form::close() }}
   <?php if (isset($comments)): ?> 
      <div id="mainlist">
         <table class="table table-bordered">
            <thead>
               <tr>
                  <td>№</td>
                  <td>Дата</td>
                  <td>Название</td>
                  <td>login</td>
                  <td>Коментарий</td>
                  <th>Модерация</th>
                  <td>Удалить</td>
               </tr>
            </thead>
            <tbody>
               <?php  $i = 0; ?>
               @foreach ($comments as $comment)
                  <?php $i++; ?>
                  <?php $commentContent = $comment->kpxzxsitecontent; //dd($comment); ?>
                  <tr>
                     <td>{{ $i }}</td>
                     <td style="width: 15%;">{!! gmdate('H:i Y.m.d', $comment['datetime']); !!}</td>
                     <td>
                        <div class="multName"><a href="{{ URL::to($commentContent['alias']) }}.html">{{ $commentContent['pagetitle'] }}</a></div>
                     </td>
                     <td>
                        <div class="name">{{ $comment['name'] }}</div>
                     </td>
                     <td>
                        <div class="comm">{{ $comment['comment'] }}</div>
                     </td>
                     {{ Form::open(array('url' => '/manager/moderationcomment', 'method' => 'post', 'id' => 'moderation_comment', 'style' => 'margin-bottom: 2.5%')) }}
                        {{ csrf_field() }}
                        {!! Form::hidden('comment_id', $comment['comment_id'], ['id' => 'comment_id_' . $comment['comment_id']]) !!}
                        {!! Form::hidden('is_moder', $comment['is_moder'], ['id' => 'is_moder_' . $comment['comment_id']]) !!}
                        <td>
                           <?php if ($comment['is_moder'] == 1): ?>
                              {!! Form::submit('Да', ['id' => 'moderation_comment', 'class' => 'btn btn-warning btn-moder']) !!}
                           <?php else: ?>
                              {!! Form::submit('Нет', ['id' => 'moderation_comment', 'class' => 'btn btn-warning btn-moder']) !!}
                           <?php endif; ?>
                        </td>
                     {{ Form::close() }}
                     {{ Form::open(array('url' => '/manager/deletecomment', 'method' => 'post', 'id' => 'delete_comment', 'style' => 'margin-bottom: 2.5%')) }}
                        {{ csrf_field() }}
                        {!! Form::hidden('comment_id', $comment['comment_id'], ['id' => 'comment_id_' . $comment['comment_id']]) !!}
                        <td>
                           {!! Form::submit('удалить', ['id' => 'delete_comment', 'class' => 'btn btn-danger btn-delete']) !!}
                        </td>
                     {{ Form::close() }}   
                  </tr>
               @endforeach
            </tbody>
         </table>
         <div id="tagNavigation"></div>
      </div>
   <?php else: ?>
      <div style="margin-bottom: 2.5%">
         <span>Комментарии отсутствуют</span>
      </div> 
   <?php endif; ?>
</div>
<?php if (isset($mults)): ?> 
   <div class="col-md-3" style="width: 20%; margin: 10% 0 0 5%;">
      <h3>Последние материалы</h3>
      <ul class="list-group">
         @foreach ($mults as $mult)
            <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
         @endforeach
      </ul>
   </div>
<?php endif; ?>
<script type="text/javascript">
   $(document).ready(function(){
      $('#countOnPageSelect').on('change', function(event) { 
         $('#countOnPageSelectValue').val(this.value);
      });

      $('#comment_type').on('change', function(event) { 
         $('#comment_type_value').val(this.value);
      });
   });
</script>
<style>
   .comm {
      max-width: 250px;
   }
</style>
@endsection