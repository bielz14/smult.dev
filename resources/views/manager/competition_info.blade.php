@extends('layouts.manager')

@section('container')
<?php $startDate = NULL; ?>
<?php $endDate = NULL; ?>
<?php $winnerName = NULL; ?>
<?php $winnerURI = NULL; ?>
<div class="col-md-9">
   <h2 class="text-center">Информация о конкурсе конкурса</h2>
   <h4 class="text-center">Админка не предназначеня для етого типа ресурса</h4>
   @if (isset($competition))       
      @if (isset($winner))
         <?php $winner = $winner[0]; ?>
         <?php $winnerName = $winner->name; ?>
         <?php $winnerURI = $winner->uri; ?>
      @endif
      @foreach ($competition[0]->kpxzxsitetmplvarcontentvalue as $value) 
         @if ($value->tmplvarid == 184)
            <?php $startDate = gmdate("Y-m-d h:m:s", (int) $value->value); ?>
         @elseif ($value->tmplvarid == 183)
            <?php $endDate = gmdate("Y-m-d h:m:s", (int) $value->value); ?>
         @endif
      @endforeach
      <div id="tableInfoWrap">
         <table class="table">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Тип</th>
                  <th>Дата</th>
                  <th>Пользователь</th>
                  <th>Страница в соц. сети</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>1</td>
                  <td>Создан</td>
                  <td>{{ $startDate }}</td>
                  <td>{{ $winnerName }}</td>
                  <td><a href="{{ $winnerURI }}">{{ $winnerURI }}</a></td>
               </tr>
               <tr>
                  <td>2</td>
                  <td>Опубликован</td>
                  <td>{{ $endDate }}</td>
                  <td>{{ $winnerName }}</td>
                  <td><a href="{{ $winnerURI }}">{{ $winnerURI }}</a></td>
               </tr>
            </tbody>
         </table>
      </div>
      <a id="new_tabs_this" class="newTabsThis" href="/{{ $competition[0]->uri }}">Открыть материал в новой вкладке</a>
   @endif
</div>
<?php if (isset($mults)): ?> 
   <div class="col-md-3" style="width: 20%; margin: 0 0 0 5%;">
      <h3>Последние материалы</h3>
      <ul class="list-group">
         @foreach ($mults as $mult)
            <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
         @endforeach
      </ul>
   </div>
<?php endif; ?>
<style>
   ul {
      margin-left: 5%;
   }
</style>
@endsection