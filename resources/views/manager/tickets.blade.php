@extends('layouts.manager')
@section('container')
<style>
   td { 
   border: 1px solid #ddd;
   }
</style>
<div class="col-md-9">
   <h2 class="text-center">Вывод заявок по ошибкам и нерабочим видео</h2>
   <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
      <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
         <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="ui-id-2" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
            <a href="/manager/tickets/unfulfilled" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Не выполнение</a>
         </li>
         <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="ui-id-4" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
            <a href="/manager/tickets/performed" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">Выполнение</a>
         </li>
      </ul>
      <div id="ui-id-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom" aria-live="polite" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
         @if (isset($tickets) && isset($type))
         @if ($type == 'unfulfilled')
         <table class="table table-striped table-bticketed table-condensed" style="font-size: 10px" id="tableSeries">
            <thead>
               <tr>
                  <th>Дата</th>
                  <th>Название материала</th>
                  <th>Тип тикета</th>
                  <th>Текст</th>
                  <th>Коментарий модератора</th>
                  <th title="Отметить если виполнен">Статус</th>
                  <th></th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               @foreach ($tickets as $ticket)
               <tr data-id="{!! $ticket['id'] !!}">
                  <?php 
                     $dateData = date_parse_from_format('H:i Y.m.d', $ticket['date_poch']);
                     $dateAdd = gmdate("H:i Y.m.d", mktime($dateData['second'], $dateData['minute'], $dateData['hour'], $dateData['day'], $dateData['month'], $dateData['year']));
                     ?>
                  <td>{!! $dateAdd !!}</td>
                  <?php $contentData = $ticket->kpxzxsitecontent; ?>
                  <td>                              
                     @if (isset($ticket->kpxzxsitecontent->uri) && isset($ticket->kpxzxsitecontent->pagetitle))
                     <a href="{!! URL::to($ticket->kpxzxsitecontent->uri); !!}">{!! $ticket->kpxzxsitecontent->pagetitle !!}</a>
                     @else
                     Отсутствует
                     @endif
                  </td>
                  <td>
                     {!! $ticket['type'] !!}
                  </td>
                  <td style="width: 30%">
                     {!! $ticket['text'] !!}
                  </td>
                  {{ Form::open(array('url' => '/manager/moderationticket', 'method' => 'post', 'id' => 'save_tickets', 'style' => 'margin-bottom: 2.5%')) }}
                  {{ csrf_field() }}
                  {!! Form::hidden('ticket_id', $ticket['id'], ['id' => 'ticket_id_' . $ticket['id']]) !!}
                  <td style="width: 100%">
                     {!! Form::textarea('comment', $ticket['comment'], ['class' => 'form-control', 'rows' => '3', 'style' => 'height: 150px']) !!}
                     @if ($errors->has('comment'))
                     <span class="help-block">
                     <strong>{!! $errors->first('comment') !!}</strong>
                     </span>
                     @endif
                  </td>
                  <td>
                     {{ Form::checkbox('ticket_status', $ticket['ticket_status'], false, ['data-id' => $ticket['id']]) }}
                     @if ($errors->has('ticket_status'))
                     <span class="help-block">
                     <strong>{!! $errors->first('ticket_status') !!}</strong>
                     </span>
                     @endif
                  </td>
                  <td>
                     {!! Form::submit('сохранить', ['class' => 'btn btn-success saveTicket']) !!}
                  </td>
                  {{ Form::close() }}
                  {{ Form::open(array('url' => '/manager/deleteticket', 'method' => 'post', 'id' => 'delete_tickets', 'style' => 'margin-bottom: 2.5%')) }}
                  {{ csrf_field() }}
                  {!! Form::hidden('ticket_id', $ticket['id'], ['class' => 'ticket_status', 'id' => 'ticket_id_' . $ticket['id'], 'data-id' => $ticket['id']]) !!}
                  @if ($errors->has('ticket_id'))
                  <span class="help-block">
                  <strong>{!! $errors->first('ticket_id') !!}</strong>
                  </span>
                  @endif
                  <td>
                     {!! Form::submit('удалить', ['class' => 'btn btn-warning delTicket']) !!}
                  </td>
                  {{ Form::close() }}
               </tr>
               @endforeach
            </tbody>
         </table>
         @else
         <table class="table table-striped table-bticketed table-condensed" style="font-size: 10px" id="tableSeries">
            <thead>
               <tr>
                  <th>Дата создания</th>
                  <th>Дата выполнения</th>
                  <th>Название материала</th>
                  <th>Тип тикета</th>
                  <th>Текст</th>
                  <th>Коментарий модератора</th>
               </tr>
            </thead>
            <tbody>
               @foreach ($tickets as $ticket)
               <tr data-id="{!! $ticket['id'] !!}">
                  <?php 
                     $dateData = date_parse_from_format('H:i Y.m.d', $ticket['date_poch']);
                     $dateAdd = gmdate("H:i Y.m.d", mktime($dateData['second'], $dateData['minute'], $dateData['hour'], $dateData['day'], $dateData['month'], $dateData['year']));
                     ?>
                  <td>{!! $dateAdd !!}</td>
                  <?php 
                     $dateData = date_parse_from_format('H:i Y.m.d', $ticket['date_kin']);
                     $dateExecution = gmdate("H:i Y.m.d", mktime($dateData['second'], $dateData['minute'], $dateData['hour'], $dateData['day'], $dateData['month'], $dateData['year']));
                     ?>
                  <td>{!! $dateExecution !!}</td>
                  <td>
                     @if (isset($ticket->kpxzxsitecontent->uri) && isset($ticket->kpxzxsitecontent->pagetitle))
                     <a href="{!! URL::to($ticket->kpxzxsitecontent->uri); !!}">{!! $ticket->kpxzxsitecontent->pagetitle !!}</a>
                     @else
                     Отсутствует
                     @endif
                  </td>
                  <td>{!! $ticket['type'] !!}</td>
                  <td style="width: 100%">
                     <!--{!! Form::textarea('comment', $ticket['comment'], ['class' => 'form-control', 'rows' => '3', 'style' => 'height: 150px;']) !!}-->
                     @if (strlen($ticket['text']) > 85)
                     <?php $text = str_split($ticket['text'], 85) ?>
                     @foreach ($text as $string) 
                     <p> {!! $string !!} </p>
                     {!! chr(10) !!}
                     @endforeach
                     @else
                     {!! $ticket['text'] !!}
                     @endif
                  </td>
                  <td style="width: 30%">
                     {!! $ticket['comment'] !!}
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
         @endif
         @else
         <div style="margin-bottom: 2.5%">
            <span>Заявки отсутствуют</span>
         </div>
         @endif
      </div>
      <div id="ui-id-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom" aria-live="polite" aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true" style="display: none;"></div>
   </div>
</div>
@if (isset($mults))
<div class="col-md-3" style="width: 20%; margin: 10% 0 0 5%;">
   <h3>Последние материалы</h3>
   <ul class="list-group">
      @foreach ($mults as $mult)
      <a href="{{ $mult['alias'] }}.html" class="list-group-item">{{ $mult['pagetitle'] }}</a>
      @endforeach
   </ul>
</div>
@endif
<script type="text/javascript">
   $(document).ready(function(){
      $('input:checkbox[name=ticket_status]').on('change', function(event) { 
         var id = this.dataset.id;
         if (this.checked) {
            $('input:checkbox[name=ticket_status]').val(1);
         } else {
            $('input:checkbox[name=ticket_status]').val(0);
         }
      });
   });
</script>
@endsection