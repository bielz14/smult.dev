@extends('layouts.manager')
@section('container')
<div class="col-md-9">
   <h2 class="text-center">Добавление конкурса</h2>
   @if (!isset($item))
   {{ Form::open(array('url' => '/manager/addcompetition', 'method' => 'post', 'id' => 'add_mult_form', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <input type="hidden" name="resource_id" id="resource_id" value="">
   <div class="form-group">
      {!! Form::text('pagetitle', '', ['id' => 'pagetitle', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Заголовок...']) !!}
      @if ($errors->has('pagetitle'))
      <span class="help-block">
      <strong>{!! $errors->first('pagetitle') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('alias', '', ['id' => 'alias', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Url']) !!}
      @if ($errors->has('alias'))
      <span class="help-block">
      <strong>{!! $errors->first('alias') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('fond', '', ['id' => 'fond', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Призовой фонд']) !!}
      @if ($errors->has('fond'))
      <span class="help-block">
      <strong>{!! $errors->first('fond') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Загрузить постер</div>
                              <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }          
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if ($errors->has('main_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first('main_file') !!}</strong>
                  </span>
               @endif
               <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   <div class="form-group form-inline" style="display: inline-block">
      {!! Form::label('comp_start', ' Дата начала конкурса:') !!}
      {!! Form::date('comp_start', '', ['id' => 'comp_start', 'class' => 'form-control']) !!}
      @if ($errors->has('comp_start'))
      <span class="help-block">
      <strong>{!! $errors->first('comp_start') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline" style="display: inline-block; margin-left: 3%;">
      {!! Form::label('comp_end', ' Дата конца конкурса:') !!}
      {!! Form::date('comp_end', '', ['id' => 'comp_end', 'class' => 'form-control']) !!}
      @if ($errors->has('comp_end'))
      <span class="help-block">
      <strong>{!! $errors->first('comp_end') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('url_full_mult', '', ['placeholder' => 'Ссылка на видео', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('url_full_mult'))
      <span class="help-block">
      <strong>{!! $errors->first('url_full_mult') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('istok', '', ['placeholder' => 'Ссылка на пост в вк', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('istok'))
      <span class="help-block">
      <strong>{!! $errors->first('istok') !!}</strong>
      </span>
      @endif
   </div>
   <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}" charset="utf-8" ></script>
   <div class="form-group{!! $errors->has('editor1') ? ' has-error' : '' !!}">
      {!! Form::label('editor1', ' ', ['class' => 'control-label']) !!}
      {!! Form::textarea('editor1', NULL, ['class' => 'form-control', 'id' => 'editor1']) !!}
      @if ($errors->has('editor1'))
      <span class="help-block">
      <strong>{!! $errors->first('editor1') !!}</strong>
      </span>
      @endif
   </div>
   <script>
      var editor = CKEDITOR.replace('editor1', {
                     filebrowserBrowseUrl : '/elfinder/ckeditor',
                     disableNativeSpellChecker: false
                   });
   </script>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::text('ispublish', '1', ['id' => 'ispublish', 'style' => 'display:none']) !!}
      {!! Form::button('Разместить конкурс', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {!! Form::hidden('af_action', 'eddfe6cfa4f0a386515dad406ace3b0b') !!}
   {{ Form::close() }}
   <div id="after-form"></div>
   <div id="seotable"></div>
   @else
   <?php $fond = NULL; ?>
   <?php $startDate = NULL; ?>
   <?php $endDate = NULL; ?>
   <?php $urlForMult = NULL; ?>
   <?php $istok = NULL; ?> 
   @if (count($item->kpxzxtmplvarcontentvalue))
   @foreach ($item->kpxzxtmplvarcontentvalue as $value)
   @if ($value->tmplvarid == 185)
   <?php $fond = $value->value; ?>
   @elseif ($value->tmplvarid == 184)
   <?php $startDate = $value->value; ?>
   @elseif ($value->tmplvarid == 183)
   <?php $endDate = $value->value; ?>
   @elseif ($value->tmplvarid == 179)
   <?php $urlForMult = $value->value; ?>
   @elseif ($value->tmplvarid == 163)
   <?php $istok = $value->value; ?>
   @endif
   @endforeach
   @endif
   {{ Form::open(array('url' => '/manager/editcompetition', 'method' => 'post', 'id' => 'edit_mult_form', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   {!! Form::hidden('itemId', $item->id) !!}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <input type="hidden" name="resource_id" id="resource_id" value="">
   <div class="form-group">
      {!! Form::text('pagetitle', $item->pagetitle, ['id' => 'pagetitle', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Заголовок...']) !!}
      @if ($errors->has('pagetitle'))
      <span class="help-block">
      <strong>{!! $errors->first('pagetitle') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('alias', $item->alias, ['id' => 'alias', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Url']) !!}
      @if ($errors->has('alias'))
      <span class="help-block">
      <strong>{!! $errors->first('alias') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('fond', $fond, ['id' => 'fond', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Призовой фонд']) !!}
      @if ($errors->has('fond'))
      <span class="help-block">
      <strong>{!! $errors->first('fond') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Загрузить постер</div>
                              <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }          
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if ($errors->has('main_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first('main_file') !!}</strong>
                  </span>
               @endif
               @if (!is_null($posterURI))
                  <img id="poster" src="{{ preg_replace('/assets/', 'public', $posterURI) }}" height="80" alt="Постер..." style="margin-left: 2.5%">
               @else
                  <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
               @endif
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   <div class="form-group form-inline" style="display: inline-block">
      {!! Form::label('comp_start', ' Дата начала конкурса:') !!}
      {!! Form::date('comp_start', gmdate('d.m.Y', $startDate), ['id' => 'comp_start', 'class' => 'form-control']) !!}
      @if ($errors->has('comp_start'))
      <span class="help-block">
      <strong>{!! $errors->first('comp_start') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group form-inline" style="display: inline-block; margin-left: 3%;">
      {!! Form::label('comp_end', ' Дата конца конкурса:') !!}
      {!! Form::date('comp_end', gmdate('d.m.Y', $endDate), ['id' => 'comp_end', 'class' => 'form-control']) !!}
      @if ($errors->has('comp_end'))
      <span class="help-block">
      <strong>{!! $errors->first('comp_end') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('url_full_mult', $urlForMult, ['placeholder' => 'Ссылка на видео', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('url_full_mult'))
      <span class="help-block">
      <strong>{!! $errors->first('url_full_mult') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('istok', $istok, ['placeholder' => 'Ссылка на пост в вк', 'autocomplete' => 'off', 'class' => 'form-control']) !!}
      @if ($errors->has('istok'))
      <span class="help-block">
      <strong>{!! $errors->first('istok') !!}</strong>
      </span>
      @endif
   </div>
   <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}" charset="utf-8" ></script>
   <div class="form-group{!! $errors->has('editor1') ? ' has-error' : '' !!}">
      {!! Form::label('editor1', ' ', ['class' => 'control-label']) !!}
      {!! Form::textarea('editor1', $item->content, ['class' => 'form-control', 'id' => 'editor1']) !!}
      @if ($errors->has('editor1'))
      <span class="help-block">
      <strong>{!! $errors->first('editor1') !!}</strong>
      </span>
      @endif
   </div>
   <script>
      var editor = CKEDITOR.replace('editor1', {
                     filebrowserBrowseUrl : '/elfinder/ckeditor',
                     disableNativeSpellChecker: false 
                   });
   </script>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::text('ispublish', '1', ['id' => 'ispublish', 'style' => 'display:none']) !!}
      {!! Form::button('Разместить конкурс', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {!! Form::hidden('af_action', 'eddfe6cfa4f0a386515dad406ace3b0b') !!}
   {{ Form::close() }}
   <div id="after-form"></div>
   <div id="seotable"></div>
   @endif
</div>
@endsection