@extends('layouts.manager')
@section('container')
<div class="col-md-9">
   <h2 class="text-center">Добавление цитаты</h2>
   {{ Form::open(array('url' => '/manager/addquote', 'method' => 'post', 'id' => 'add_quote_form', 'enctype' => 'multipart/form-data')) }}
   {{ csrf_field() }}
   <h4>Что делать после сохранения</h4>
   <div class="radio">
      <label>          
      {!! Form::radio('afterSave', 'new', ['class' => 'afterSave', 'id' => 'afterSave_new', 'checked' => false]) !!}  
      Создать новый материал   
      </label>
   </div>
   <div class="radio">
      <label>     
      {!! Form::radio('afterSave', 'edit', false, ['class' => 'afterSave', 'id' => 'afterSave_edit']) !!}      
      Продолжить редактирование    
      </label>
   </div>
   <input type="hidden" name="resource_id" id="resource_id" value="">
   <div class="form-group">
      {!! Form::text('hero', '', ['id' => 'hero', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Герой...']) !!}
      @if ($errors->has('hero'))
      <span class="help-block">
      <strong>{!! $errors->first('hero') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('hero_uri', '', ['id' => 'hero_uri', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Ссылка на героя...']) !!}
      @if ($errors->has('hero_uri'))
      <span class="help-block">
      <strong>{!! $errors->first('hero_uri') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult', '', ['id' => 'mult', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Мультфильм по цитате']) !!}
      @if ($errors->has('mult'))
      <span class="help-block">
      <strong>{!! $errors->first('mult') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      {!! Form::text('mult_uri', '', ['id' => 'mult_uri', 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Ссылка на мультфильмы']) !!}
      @if ($errors->has('mult_uri'))
      <span class="help-block">
      <strong>{!! $errors->first('mult_uri') !!}</strong>
      </span>
      @endif
   </div>
   <div class="form-group">
      <div class="container-fluid">
         <div class="row">
            <div style="position: relative;">
               <div class="col-md-4 btn btn-info" id="main_uploadButton" data-prefix="main" data-button-id="main_file">Загрузить постер</div>
                              <script> 
                  $('#main_uploadButton').on('click', function(e) {
                     $('#main_file').click();
                  });       

                  function previewFile() {
                    var preview = document.querySelector('img#poster');
                    var file    = document.querySelector('input[type=file]').files[0];
                    var reader  = new FileReader();

                    reader.onloadend = function () {
                      preview.src = reader.result;
                    }

                    if (file) {
                      reader.readAsDataURL(file);
                        if (!$('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    } else {
                      preview.src = "";
                        if ($('#poster').is(':visible')) {
                           $('#poster').toggle();
                        }
                    }
                  }          
               </script>
               {!! Form::file('main_file', ['id' => 'main_file', 'data-prefix' => 'main', 'style' => 'position: absolute; margin: 0px; padding: 0px; width: 220px; opacity: 0; display: none;', 'onchange' => 'previewFile()']) !!}
               @if ($errors->has('main_file'))
                  <span class="help-block">
                     <strong>{!! $errors->first('main_file') !!}</strong>
                  </span>
               @endif
               <img id="poster" src="" height="80" alt="Постер..." style="display: none; margin-left: 2.5%">
            </div>
            <div class="col-md-1 hide" id="main_loader"><img src="/img/load.gif" alt="" width="30px"></div>
            <div class="col-md-1" id="main_preview"></div>
            <div class="col-md-6" id="main_message"></div>
         </div>
      </div>
   </div>
   <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}" charset="utf-8" ></script>
   <div class="form-group{!! $errors->has('editor1') ? ' has-error' : '' !!}">
      {!! Form::label('editor1', ' ', ['class' => 'control-label']) !!}
      {!! Form::textarea('editor1', NULL, ['class' => 'form-control', 'id' => 'editor1']) !!}
      @if ($errors->has('editor1'))
      <span class="help-block">
      <strong>{!! $errors->first('editor1') !!}</strong>
      </span>
      @endif
   </div>
   <script>
      var editor = CKEDITOR.replace('editor1', {
                     filebrowserBrowseUrl : '/elfinder/ckeditor',
                     disableNativeSpellChecker: false 
                   });
   </script>
   <div id="valid-loader-wrap"><img id="valid-loader" style="display: none" src="/img/load.gif"></div>
   <div id="message"></div>
   <div class="form-inline form-group">
      <!--<div class="checkbox">
         {!! Form::checkbox(NULL, NULL, true, ['id' => 'published']) !!}
         {!! Form::label(NULL, ' Опубликовать материал') !!}
         </div>-->
      {!! Form::button('Разместить цитату', ['type' => 'submit', 'id' => 'text-validate', 'class' => 'btn btn-success add-button']) !!}
   </div>
   {!! Form::hidden('af_action', 'eddfe6cfa4f0a386515dad406ace3b0b') !!}
   {{ Form::close() }}
   <div id="after-form"></div>
   <div id="seotable"></div>
</div>
@endsection