@extends('layouts.app')

@section('container')
<div class="content">
   <h1>На сладкое</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">
          {{ $_SERVER['SERVER_NAME'] }}
         </a>
         <p>&gt;</p>
      </span>
      <span>На сладкое</span>
   </div>
    @if (isset($forSweetCategories['trailers']))
      <section>
       <div class="art items clr  big">
          <h2>Трейлеры и Видео</h2>
          @foreach ($forSweetCategories['trailers'] as $forSweetCategory)
            <div class="item">
              <a href="/{{ $forSweetCategory->uri }}">
                <div class="title">{{ $forSweetCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $forSweetCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $forSweetCategory->uri }}">
                @foreach ($forSweetCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $forSweetCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/na-sladkoe/trejleryi.html">Все трейлеры и видео</a></div>
      </section>
    @endif
    @if (isset($forSweetCategories['heroes']))
      <section>
       <div class="art items clr  big">
          <h2>Мультгерои</h2>
          @foreach ($forSweetCategories['heroes'] as $forSweetCategory)
            <div class="item">
              <a href="/{{ $forSweetCategory->uri }}">
                <div class="title">{{ $forSweetCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $forSweetCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $forSweetCategory->uri }}">
                @foreach ($forSweetCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $forSweetCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/na-sladkoe/multgeroi.html">Все мультгерои</a></div>
      </section>
    @endif
    @if (isset($forSweetCategories['interestingCategoryItems']))
      <section>
       <div class="art items clr  big">
          <h2>Это Интересно</h2>
          @foreach ($forSweetCategories['interestingCategoryItems'] as $forSweetCategory)
            <div class="item">
              <a href="/{{ $forSweetCategory->uri }}">
                <div class="title">{{ $forSweetCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $forSweetCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $forSweetCategory->uri }}">
                @foreach ($forSweetCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $forSweetCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/na-sladkoe/interesnyie-faktyi.html">Все это интересно</a></div>
      </section>
    @endif
    @if (isset($forSweetCategories['wallpsAndPosters']))
      <section>
       <div class="art items clr  big">
          <h2>Обои и Постеры</h2>
          @foreach ($forSweetCategories['wallpsAndPosters'] as $forSweetCategory)
            <div class="item">
              <a href="/{{ $forSweetCategory->uri }}">
                <div class="title">{{ $forSweetCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $forSweetCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $forSweetCategory->uri }}">
                @foreach ($forSweetCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $forSweetCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/na-sladkoe/oboi-i-posteryi.html">Все обои и постеры</a></div>
      </section>
    @endif
    @if (isset($forSweetCategories['soundtracks']))
      <section>
       <div class="art items clr  big">
          <h2>Саундтреки</h2>
          @foreach ($forSweetCategories['soundtracks'] as $forSweetCategory)
            <div class="item">
              <a href="/{{ $forSweetCategory->uri }}">
                <div class="title">{{ $forSweetCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $forSweetCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $forSweetCategory->uri }}">
                @foreach ($forSweetCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $forSweetCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/saundtreki.html">Все саундтреки</a></div>
      </section>
    @endif
</div>
@endsection