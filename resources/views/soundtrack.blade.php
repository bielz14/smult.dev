@extends('layouts.app')

@section('container')
@if (!isset($mult))
    <?php $multAlias = ''; ?>
    <?php $multPagetitle = ''; ?>
@else 
    <?php $multAlias = $mult['alias']; ?>
    <?php $multPagetitle = $mult['pagetitle']; ?>
@endif
<div class="content">
    @foreach ($soundtrack as $value)
        <h1>{{ $value->pagetitle }}</h1>
        <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
          <span typeof="v:Breadcrumb">
            <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
            <p>&gt;</p>
            <a href="/{{ $mult['alias'] }}.html" rel="v:url" class="hov-bord" property="v:title">{{ $mult['pagetitle'] }}</a>
            <p>&gt;</p>
          </span>
          <span>{{ $value->pagetitle }}</span>
          <input id="content_id" type="hidden" value="{{ $value->id }}" />
        </div>
        <div class="mult-music">
    <div class="mult-music-poster">
      @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
          @if ($value2->tmplvarid == 142)
            <img src="{{ $image = str_replace('assets', 'public', $value2->value) }}" alt="{{ $value->pagetitle }}">
            <?php break; ?>
          @endif
      @endforeach
    </div>
     <div class="mult-music-main">
       <div class="mult-music-main-player">
        <audio id="player" src="/public/images/101-dalmatinec/audio/101-dalmatinec_002.mp3" controls style="width: 0px; height: 0px; visibility: hidden; position: absolute;">          </audio>
        <div class="audioplayer audioplayer-stopped">

            <div class="audioplayer-playpause" title="Play">
              <div>
                
              </div>
            </div>
            <div id="play-border-my"></div>
              <div class="audioplayer-time audioplayer-time-current">00:00</div>
              <div class="audioplayer-bar" style="display: block;">
                <div class="audioplayer-bar-loaded" style="width: 0px;">
                  
                </div>
                <div class="audioplayer-bar-played" style="width: 0px;">
        
                </div>
              </div><div class="audioplayer-time audioplayer-time-duration">…</div>
              <div class="audioplayer-volume">
                <div id="play-border-my2"></div>
                <div class="audioplayer-volume-button" title="novolume"></div>
                <div class="audioplayer-volume-adjust"><div><div style="height: 100%;"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @if (isset($tracks))
        <div class="mult-music-player clr" style="width:100%;">
          @foreach ($tracks as $track)
              <?php
                $trackFileName = NULL;
                preg_match('/.*(images\/.*)/', $track->fileName, $matches);
                if (count($matches) > 1 && !is_null($matches)) {
                  $trackFileName = $matches[1];
                  $trackFileName = '/public/' . preg_replace('/\\\/', '\/', $trackFileName);
                } else {
                 $trackTitle = NULL;
                }

                preg_match('/.*_(\d*)\..*/', $track->origName, $matches);
                if (count($matches) > 1 && !is_null($matches)) {
                  $trackTitle = $matches[1];
                  $trackTitle = preg_replace('/\//', '\\', $trackTitle);
                } else {
                 $trackTitle = NULL;
                }
              ?> 

              <li id="track" status="pause" data-src="{{ $trackFileName }}" style="width: 31%; float: left; margin: -4px 0px;">
                <div class="play-icon" style="height: 18px; width: 18px; margin-top: 3px; opacity: 0;"></div>
                <span>Трэк №{{ $trackTitle }}</span>
              </li>
            
          @endforeach
        </div>
      @endif
    </div>
        <div class="clr"></div>
            <div class="mult-music-content">
              <?php echo $value->content; ?>
            </div>
            <?php //if (\Auth::check() && \Auth::user()->can('editResource')) ?>
              @if (true)
                <div id="manager" style="float: right">
                  @if (isset($itemId))
                    <a href="/manager/resource?item_id={{ $itemId }}">Редактировать</a>
                  @endif
                </div>
              @endif
        @endforeach
    </div>
    @if (isset($otherSoundtracks))
      <section>
         <div class="mult items clr ">
            <h2>Саундтреки из других мультфильмов</h2>
            @foreach ($otherSoundtracks as $soundtrack)
            <div class="item">
               <a href="/{{ $soundtrack->uri }}">
                  <div class="title">{{ $soundtrack->pagetitle }}</div>
               </a>
               <div class="jt left" style="top: 143px; left: 228px;">
                  <div class="jtc">
                     {{ $soundtrack->description }}        
                  </div>
               </div>
               <div class="poster jtg">   
                  @foreach ($soundtrack->kpxzxsitetmplvarcontentvalue as $value)
                    @if ($value->tmplvarid == 142)
                      <a href="/{{ $soundtrack->uri }}">
                      <?php 
                         $image = str_replace('assets', 'public', $value->value);
                         if ($image[0] !== '/' && $image[0] !== ' ') {
                           $image = '/' . $image;
                         }
                         ?>
                      <img src="{{ $image }}" alt="{{ $soundtrack->pagetitle }}">
                      </a>      
                      @break
                    @endif
                  @endforeach
               </div>
            </div>
            @endforeach    
         </div>
         <div class="m-i"><a href="/na-sladkoe/saundtreki.html">Вся музыка...</a></div>
      </section>
    @endif
</div>
<script>
  $('head').append('<link rel="stylesheet" href="public/css/music-player.css">');

  $('.audioplayer-playpause').on('click', function(event){
    var title = null;
    var currentTitle = $('.audioplayer-playpause').prop('title');
    if (currentTitle == 'Play') {
      document.getElementById('player').play();
      title = 'Pause';
    } else if (currentTitle == 'Pause') {
      document.getElementById('player').pause();
      title = 'Play';
    }
    if (title != null) {
      $('.audioplayer-playpause').prop('title', title);
    }
  });

  $('li#track').on('click', function(event){
      var src = this.dataset.src;
      $('audio').prop('src', src);
      $('.audioplayer-playpause').prop('title', 'Pause');
      $('li#track > .play-icon').css('opacity', 0);
      var icon = $(this).children()[0];
      $(icon).css('opacity', 1);
      document.getElementById('player').play();
  });
</script>
<style>

.mult-music-player li {
    cursor: pointer;
    margin-bottom: 1px;
    padding-bottom: 1px;
}

.mult-music-player li {
    overflow: hidden;
    white-space: nowrap;
}

li {
    list-style: none;
}

  @charset "UTF-8";#play-border-my,.audioplayer {
    height: 2.5em;
    -webkit-filter: none
  }

  .audio-list,.audio-name {
      color: #ffb324;
      text-overflow: ellipsis
  }

  .audio-list,.audio-name,.mult-music-player li {
      overflow: hidden;
      white-space: nowrap
  }

  .audioplayer-volume:not(:hover) .audioplayer-volume-adjust,.mult-music-player .icon-play {
      opacity: 0
  }

  .mult-music-poster {
      float: left;
      vertical-align: top;
      margin: 1%;
      width: 31%;
      border-radius: 15px;
      box-shadow: inset 0 0 15px 5px #000
  }

  .mult-music-poster img {
      width: 100%
  }

  .play-border-right {
      width: 1px;
      height: 2.5em;
      background-color: rgba(165,167,169,.35);
      left: 2.575em;
      top: 0;
      position: absolute
  }

  #play-border-my {
      border-left: 1px solid rgba(165,167,169,.35);
      width: 0;
      left: 2.5em;
      top: 0
  }

  .audioplayer {
      border: 1px solid rgba(165,167,169,.35);
      background-color: rgba(219,219,219,0);
      background-image: -webkit-linear-gradient(bottom,rgba(0,0,0,.088),rgba(255,255,255,.15));
      background-image: -moz-linear-gradient(bottom,rgba(0,0,0,.088),rgba(255,255,255,.15));
      background-image: -o-linear-gradient(bottom,rgba(0,0,0,.088),rgba(255,255,255,.15));
      background-image: -ms-linear-gradient(bottom,rgba(0,0,0,.088),rgba(255,255,255,.15));
      background-image: linear-gradient(to top,rgba(0,0,0,.088),rgba(255,255,255,.15));
      position: relative;
      z-index: 1
  }

  .audioplayer-mini {
      width: 2.5em;
      margin: 0 auto
  }

  .audioplayer>div {
      position: absolute
  }

  .audioplayer-playpause {
      width: 2.5em;
      height: 100%;
      text-align: left;
      text-indent: -9999px;
      cursor: pointer;
      z-index: 2;
      top: 0;
      left: 0
  }

  .audioplayer-mini .audioplayer-playpause {
      width: 100%
  }

  .audioplayer-playpause a {
      display: block
  }

  .audioplayer-stopped .audioplayer-playpause a {
      width: 0;
      height: 0;
      border: .5em solid transparent;
      border-right: none;
      border-left-color: #000;
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      margin: -.5em 0 0 -.25em
  }

  .audioplayer-playing .audioplayer-playpause a {
      width: .75em;
      height: .75em;
      position: absolute;
      top: 50%;
      left: 50%;
      margin: -.375em 0 0 -.375em
  }

  .audioplayer-playing .audioplayer-playpause a:after,.audioplayer-playing .audioplayer-playpause a:before {
      width: 40%;
      height: 100%;
      background-color: #000;
      content: '';
      position: absolute;
      top: 0
  }

  .audioplayer-playing .audioplayer-playpause a:before {
      left: 0
  }

  .audioplayer-playing .audioplayer-playpause a:after {
      right: 0
  }

  .audioplayer-time {
      display: none;
      width: 4.375em;
      height: 100%;
      line-height: 2.375em;
      text-align: center;
      z-index: 2;
      top: 0
  }

  .audioplayer-time-current {
      border-left: 1px solid #111;
      border-left-color: rgba(0,0,0,.25);
      left: 2.5em
  }

  .audioplayer-time-duration {
      border-right: 1px solid #555;
      border-right-color: rgba(255,255,255,.1);
      right: 2.5em
  }

  .audioplayer-novolume .audioplayer-time-duration {
      border-right: 0;
      right: 0
  }

  .audioplayer-bar {
      background-color: rgba(136,169,189,.23);
      background-image: -webkit-linear-gradient(top,rgba(0,0,0,.2),rgba(255,255,255,.2));
      background-image: -moz-linear-gradient(top,rgba(0,0,0,.2),rgba(255,255,255,.2));
      background-image: -o-linear-gradient(top,rgba(0,0,0,.2),rgba(255,255,255,.2));
      background-image: -ms-linear-gradient(top,rgba(0,0,0,.2),rgba(255,255,255,.2));
      background-image: linear-gradient(to bottom,rgba(0,0,0,.2),rgba(255,255,255,.2));
      height: .875em;
      cursor: pointer;
      z-index: 1;
      top: 50%;
      right: 2.875em;
      left: 2.875em;
      margin-top: -.438em
  }

  .audioplayer-novolume .audioplayer-bar {
      right: 4.375em
  }

  .audioplayer-bar div {
      width: 0;
      height: 100%;
      position: absolute;
      left: 0;
      top: 0
  }

  .audioplayer-bar-loaded {
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      border-radius: 0;
      background-color: #828282;
      z-index: 1
  }

  .audioplayer-bar .audioplayer-bar-played {
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      border-radius: 0;
      background-color: #ff9a00;
      background-image: -webkit-linear-gradient(bottom,rgba(0,0,0,.31),rgba(255,255,255,.31));
      background-image: -moz-linear-gradient(bottom,rgba(0,0,0,.31),rgba(255,255,255,.31));
      background-image: -o-linear-gradient(bottom,rgba(0,0,0,.31),rgba(255,255,255,.31));
      background-image: -ms-linear-gradient(bottom,rgba(0,0,0,.31),rgba(255,255,255,.31));
      background-image: linear-gradient(to top,rgba(0,0,0,.31),rgba(255,255,255,.31))
  }

  .audioplayer-bar-played {
      z-index: 2
  }

  .audioplayer-volume {
      width: 2.5em;
      height: 100%;
      border-left: 1px solid rgba(165,167,169,.35);
      text-align: left;
      text-indent: -9999px;
      cursor: pointer;
      z-index: 2;
      top: 0;
      right: 0
  }

  .audioplayer-volume-button a {
      width: .313em;
      height: .375em;
      background-color: #000;
      display: block;
      position: relative;
      z-index: 1;
      top: 40%;
      left: 35%
  }

  .audioplayer-bar,.audioplayer-novolume .audioplayer-volume {
      display: none
  }

  .audioplayer:not(.audioplayer-muted) .audioplayer-volume-button a:after {
      width: .313em;
      height: .313em;
      border: .25em double #fff;
      border-width: .25em .25em 0 0;
      left: .563em;
      top: -.063em;
      -webkit-border-radius: 0 .938em 0 0;
      -moz-border-radius: 0 .938em 0 0;
      border-radius: 0 .938em 0 0;
      -webkit-transform: rotate(45deg);
      -moz-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      -o-transform: rotate(45deg);
      transform: rotate(45deg)
  }

  .audioplayer-volume-adjust {
      height: 6.25em;
      cursor: default;
      position: absolute;
      left: 0;
      right: -1px;
      top: -9999px
  }

  .audioplayer-volume:hover .audioplayer-volume-adjust {
      top: auto;
      bottom: 100%;
      border: 1px solid rgba(165,167,169,.35);
      background: rgba(244,236,236,.61)
  }

  .audioplayer-volume-adjust>div {
      width: 40%;
      height: 80%;
      background-color: rgba(0,0,0,.088);
      cursor: pointer;
      position: relative;
      z-index: 1;
      margin: 30% auto 0
  }

  .audioplayer-bar-played {
      background: -webkit-gradient(linear,left top,right top,from(#007fd1),to(#c600ff));
      background: -webkit-linear-gradient(left,#007fd1,#c600ff);
      background: -moz-linear-gradient(left,#007fd1,#c600ff);
      background: -ms-radial-gradient(left,#007fd1,#c600ff);
      background: -o-linear-gradient(left,#007fd1,#c600ff);
      background: linear-gradient(to right,#007fd1,#c600ff)
  }

  .audioplayer-volume-adjust div div {
      width: 100%;
      height: 100%;
      position: absolute;
      bottom: 0;
      left: 0;
      background: #ffb324
  }

  .audioplayer-bar,.audioplayer-bar div,.audioplayer-volume-adjust div {
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      border-radius: 0
  }

  .audioplayer {
      -webkit-border-radius: 2px;
      -moz-border-radius: 2px;
      border-radius: 2px
  }

  .audioplayer-volume-adjust {
      -webkit-border-top-left-radius: 2px;
      -webkit-border-top-right-radius: 2px;
      -moz-border-radius-topleft: 2px;
      -moz-border-radius-topright: 2px;
      border-top-left-radius: 2px;
      border-top-right-radius: 2px
  }

  .audioplayer *,.audioplayer :after,.audioplayer :before {
      -webkit-transition: color .25s ease,background-color .25s ease,opacity .5s ease;
      -moz-transition: color .25s ease,background-color .25s ease,opacity .5s ease;
      -ms-transition: color .25s ease,background-color .25s ease,opacity .5s ease;
      -o-transition: color .25s ease,background-color .25s ease,opacity .5s ease;
      transition: color .25s ease,background-color .25s ease,opacity .5s ease
  }

  .mult-music-player {
      width: 48%
  }

  .audio-list h2:before {
      font-family: FontAwesome;
      content: "\f04b";
      cursor: pointer
  }

  .mult-music-main {
      float: left;
      margin: 1%;
      width: 62%
  }

  .audio-name span {
      display: table-cell;
      vertical-align: middle
  }

  .audio-name {
      width: 80%;
      display: table;
      height: 100%;
      text-align: center;
      cursor: pointer;
      z-index: 1;
      right: 2.5em;
      left: 2.875em
  }

  .audio-list {
      cursor: pointer;
      height: 24px;
      font-size: 12px
  }

  .mult-music-player li {
      cursor: pointer;
      margin-bottom: 1px;
      padding-bottom: 1px
  }

  .audioplayer-playpause div,.audioplayer-volume-button {
      margin: 20%;
      width: 60%;
      height: 60%;
      background-size: cover
  }

  .audioplayer-playpause div {
      position: absolute;
      -webkit-transition: none;
      transition: none;
      -webkit-filter: none
  }

  .audioplayer-playpause {
      transition: none;
      filter: none
  }

  .audioplayer-playpause[title=Play] div {
      background-image: url(/public/img/player-play-image1.png);
      box-shadow: none;
      transition: none
  }

  .audioplayer-playpause[title=Play] div:hover {
      background-image: url(/public/img/player-play-image1-hover.png)
  }

  .audioplayer-playpause[title=Pause] div {
      background-image: url(/public/img/pause.png)
  }

  .audioplayer-playpause[title=Pause] div:hover {
      background-image: url(/public/img/pause-hover.png)
  }

  .audioplayer-volume-button {
      background-repeat: no-repeat;
      background-image: url(/public/img/volume.png)
  }

  .audioplayer-volume:hover .audioplayer-volume-button {
      background-image: url(/public/img/volume-hover.png)
  }

  .audioplayer-muted .audioplayer-volume-button {
      background-image: url(/public/img/NoVolume.png);
      margin: 10%;
      width: 80%;
      height: 80%
  }

  .audioplayer-muted .audioplayer-volume:hover .audioplayer-volume-button {
      background-image: url(/public/img/NoVolumeHover.png)
  }

  .play-icon {
      display: inline-block;
      opacity: 0;
      background-image: url(/public/img/player-play-image1.png);
      background-size: cover;
      margin-right: 5px
  }

  .mult-music-player li span {
      padding-bottom: 0;
      border-bottom: 1px solid rgba(165,167,169,.35)
  }

  .mult-music-player li:hover span {
      padding-bottom: 0;
      border-bottom: 1px solid #FFB324;
      color: #FFB324
  }

  .mult-music-player li[status=play]:hover .play-icon {
      background-image: url(/public/img/playerPlayImage1Hover.png)
  }
</style>
@endsection