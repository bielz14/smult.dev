@extends('layouts.app')

@section('container')
<div class="content">
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://smult.ru/" rel="v:url" class="hov-bord" property="v:title">smult.ru</a>
         <p>&gt;</p>
      </span>
      <span>Смульт LIVE!</span>
   </div>
   @if (isset($data))
     <div class="live-wrap clr" data-page="1" style="position: relative;">
      <?php $i = 0; ?>
      <?php $topValue = 0; ?>
      @foreach ($data as $item)
          <div class="item" style="position: relative; display: inline-block;">
              <a class="black"></a>
              <?php
                  $image = str_replace('assets', 'public', $item->poster);
                  if ($image[0] !== '/' && $image[0] !== ' ') {
                      $image = '/' . $image;
                  }
              ?>
              <img src="{{ $image }}" alt="">
              <div style="color: rgb(255, 255, 255); top: 10px; text-align: left; display: block;" class="first-title l-text">{{ $item->first_title }}</div>
              <div style="color: rgb(255, 255, 255); top: 10px; text-align: left; display: none;" class="second-title l-text">{{ $item->second_title }}</div>
              <?php //if (\Auth::check() && \Auth::user()->can('editSmultlive')) ?>
              @if (true)
                <a class="editLink" href="/manager/smultlive?item_id={{ $item->id }}">Редактировать</a>
              @endif 
          </div>
          <?php $i++; ?>
      @endforeach
    </div>
   @endif
  @if (isset($data) && (count($data) > 1 || !is_null(app('request')->input('page'))))
    <div id="preloader" class="add_remove">
       <div id="show_more">
          <img src="/public/img/_.gif" alt="Загрузка"><br>
          <span>ПОКАЗАТЬ ЕЩЕ</span>
       </div>
    </div>
    <div id="nav" class="add_remove">
      {{ $data->render() }}
    </div>
   @endif
   <div id="search-result" class="hide"></div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var nextPage = 0;
    var uri = null;
    $('body').on('click', '#show_more', function(event){
      console.log(nextPage);
      var prevImgs = $('div.item').get().reverse();
      var page = window.location.href.match(/(.*)\?page=(.*)/);
      if (page == null && nextPage == 0) {
        nextPage = 2;
        uri = window.location.href + '?page=' + nextPage;
      } else if (nextPage == 0) {
        if (page instanceof Array && page.length > 0) {
          nextPage = parseInt(page[2], 10) + 1;
          uri = page[1] + '?page=' + nextPage;
        } else {
          nextPage = parseInt(page, 10) + 1;
          uri = window.location.href + '?page=' + nextPage;
        }
      } else {
        nextPage++;
        if (page instanceof Array && page.length > 0) {
          uri = page[1] + '?page=' + nextPage;
        } else {
          uri = window.location.href + '?page=' + nextPage;
        }
      }

      $.get(uri, function( data ) {
        document.body.innerHTML = data;
      });

      setTimeout(function(){
        prevImgs.forEach(function(item) {
          $('.big').prepend(item);
        });

        $('div.item').each(function() {
          var matches = $(this).prop('src').match(/:\/\/(.*)\/.*\/(public.*)/);
          if (matches != null) {
            $(this).prop('src', '/' + matches[2]);
          }
        });
      }, 1000);
    });
  });
</script>
<style>
  li.page-item {
    display: inline-block;
  }

  li.page-item > a {
    color: black;
  }

  li.page-item > a:hover {
    color: #ffb324;
  }

  li.page-item.active {
    color: #ffb324;
  }

  span.page-link:hover {
    color: #ffb324;
    cursor: pointer;
  }
  /*Отдельный от навигации css*/
  .live-wrap .item {
    cursor: pointer;
    width: 46%;
    position: relative;
    margin: 2%;
    float: left;
  }

  .live-wrap .item .black {
    position: absolute;
    left: 0;
    z-index: 10;
    width: 100%;
    height: 100%;
    opacity: .2;
    background-color: #000;
  }

  .live-wrap .item img {
    width: 100%;
  }

  a.editLink {
    bottom: -24px;
    position: absolute;
  }

  .live-wrap .item .l-text {
    z-index: 11;
    position: absolute;
    left: 0;
    font-size: 40px;
    color: #fff;
    width: 100%;
  }
</style>
@endsection