@extends('layouts.app')

@section('container')
<div class="content">
   <h1>Самое интересное</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
         <p>&gt;</p>
      </span>
      <span>Самое интересное</span>
   </div>
    @if (isset($articlesCategories['smult_reading']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Чтиво</h2>
          @foreach ($articlesCategories['smult_reading'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                    <a href="/{{ $articleCategory->uri }}">
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                      <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}">
                    </a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-chtivo.html">Все статьи из раздела Смульт Чтиво</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['posts']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Новости</h2>
          @foreach ($articlesCategories['posts'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/news.html">Все статьи из раздела Смульт Новости</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['exclusive']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Эксклюзив</h2>
          @foreach ($articlesCategories['exclusive'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-eksklyuziv.html">Все статьи из раздела Смульт Эксклюзив</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['reviews']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Обзоры</h2>
          @foreach ($articlesCategories['reviews'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-obzor.html">Все статьи из раздела Смульт Обзоры</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['arts']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Художества</h2>
          @foreach ($articlesCategories['arts'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-xudozhestva.html">Все статьи из раздела Смульт Художества</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['universe']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Вселенная</h2>
          @foreach ($articlesCategories['universe'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-xudozhestva.html">Все статьи из раздела Смульт Вселенная</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['newtest']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Тесты</h2>
          @foreach ($articlesCategories['newtest'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/testi.html">Все статьи из раздела Смульт Тесты</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['newpoll']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Опросы</h2>
          @foreach ($articlesCategories['newpoll'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/oprosi.html">Все статьи из раздела Смульт Опросы</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['interviews']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Интервью</h2>
          @foreach ($articlesCategories['interviews'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-intervyu.html">Все статьи из раздела Смульт Интервью</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['person_day']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Личность Дня</h2>
          @foreach ($articlesCategories['person_day'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-licnost-dnia.html">Все статьи из раздела Смульт Личность Дня</a></div>
      </section>
    @endif
    @if (isset($articlesCategories['person_ussr']))
      <section>
       <div class="art items clr  big">
          <h2>Смульт Личность СССР</h2>
          @foreach ($articlesCategories['person_ussr'] as $articleCategory)
            <div class="item">
              <a href="/{{ $articleCategory->uri }}">
                <div class="title">{{ $articleCategory->pagetitle }}</div>
              </a>
              <div class="jt">
                <div class="jtc">
                  {{ $articleCategory->description }}                       
                </div>
              </div>
              <div class="poster jtg">
                <a href="/{{ $articleCategory->uri }}">
                @foreach ($articleCategory->kpxzxsitetmplvarcontentvalue as $value)
                  @if ($value->tmplvarid == 142)
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                    <img src="{{ $image }}" alt="{{ $articleCategory->pagetitle }}"></a>
                  @endif
                @endforeach
              </div>
            </div>
          @endforeach
       </div>
       <div class="m-i"><a href="/tolko-na-smultru/smult-lichnost-sssr.html">Все статьи из раздела Смульт Личность СССР</a></div>
      </section>
    @endif
</div>
@endsection