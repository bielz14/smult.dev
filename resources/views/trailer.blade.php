@extends('layouts.app')

@section('container')
@if (!isset($mult))
    <?php $multAlias = ''; ?>
    <?php $multPagetitle = ''; ?>
@else 
    <?php $multAlias = $mult['alias']; ?>
    <?php $multPagetitle = $mult['pagetitle']; ?>
@endif
<div class="content">
   @foreach ($trailer as $value)
        <h1>{{ $value->pagetitle }}</h1>
        <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
          <span typeof="v:Breadcrumb">
            <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
            <p>&gt;</p>
            <a href="/{{ $multAlias }}.html" rel="v:url" class="hov-bord" property="v:title">{{ $multPagetitle }}</a>
            <p>&gt;</p>
          </span>
          @if (!$isParentTrailer)
            <span typeof="v:Breadcrumb">
              <a href="/{{ $multAlias }}/treilers.html" rel="v:url" class="hov-bord" property="v:title" style="border-bottom: none">
                {{ $multPagetitle }} Трейлеры и видео
              </a>
              <p>&gt;</p>
            </span>
          @endif
          <span>{{ $value->pagetitle }}</span>
          <input id="content_id" type="hidden" value="{{ $value->id }}" />
        </div>
        <?php $posterURI = ''; ?>
        @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
          @if ($value2->tmplvarid == 142)
              <?php $posterURI = $value2->value; ?>
          @elseif ($value2->tmplvarid == 137)
              <?php $videoURL = $value2->value; ?>
          @elseif ($value2->tmplvarid == 163)
              <?php $videoURL = $value2->value; ?>
          @endif
        @endforeach
        <div class="player" id="wrapPlayer" style="height: 425.294px;">
          @if (isset($videoURL))
            <script>
              var playerWrapObj = document.getElementById("wrapPlayer")
              var playerWidth = playerWrapObj.offsetWidth
              var playerHeight = playerWidth / 1.7
              console.log(playerHeight)
              console.log(playerWidth)
              playerWrapObj.style.height = playerHeight + "px"
            </script>
            <iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="{{ $videoURL }}"></iframe>
          @else
            @if (!$isParentTrailer)
              <div class="message_novideo">
                <b><span>Видео отсутствует</span></b>
              </div>
              <div class="m-i tickets-btn"><span>Что-то не так...</span></div>
            @else 
              Загрузка плеера...
            @endif
          @endif   
        </div>
        @if (!$isParentTrailer)
          <div class="m-cnt clr">
            <div class="mult-content ndButInfo poster">
                <img src="{{ $image = str_replace('assets', 'public', $posterURI) }}" alt="{{ $value->pagetitle }}">
            </div>
            <div class="text">
                <div class="text-s" data-type="small">
                  <?php echo $value->content ?>
                </div>
              <span id="isReadMore" class="t-btn"><a>Подробнее...</a></span><span id="isReadMini" class="t-btn"><a>Свернуть текст</a></span>
            </div>
          </div>
          <?php //if (\Auth::check() && \Auth::user()->can('editResource')) ?>
          @if (true)
            <div id="manager" style="float: right">
              @if (isset($itemId))
                <a href="/manager/resource?item_id={{ $itemId }}">Редактировать</a>
              @endif
            </div>
          @endif
          <div class="repost-quoter clr">
            <div class="sktch">
              @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
                @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
                    <?php 
                      $image = str_replace('assets', 'public', $value2->value);
                      if ($image[0] !== '/' && $image[0] !== ' ') {
                        $image = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image;
                      }
                    ?>
                    <?php break; ?>
                  @endif
                @endforeach
                <div class="repost clr" data-title="{{ $value->pagetitle }}" data-description="{{ $value->description }}" data-uri="http://{{ $_SERVER['SERVER_NAME'] }}/{{ $value->uri }}" data-poster="{{ $image }}">
                  <div data-type="vk" class="soz-repost">
                     <img src="/public/img/vkshare.png" alt="">
                  </div>
                  <div data-type="fb" class="soz-repost">
                     <img src="/public/img/fbshare.png" alt="">
                  </div>
                  <div data-type="tw" class="soz-repost">
                     <img src="/public/img/twshare.png" alt="">
                  </div>
               </div>
               <img class="sketch" src="/public/img/soz-sketch.png" alt="sketch">
            </div>
            <div class="quotes" id="quotes" style="width: 494px;">
              <div class="title">Цитаты великих...</div>
              @if (isset($quotes))
                @foreach ($quotes as $quote)
                  <div class="item">
                    <div class="poster">
                        <?php 
                          $image = str_replace('assets', 'public', $quote->poster);
                          if ($image[0] !== '/' && $image[0] !== ' ') {
                              $image = '/' . $image;
                          }
                        ?>
                       <img src="{{ $image }}" alt="">
                    </div>
                    <div class="right">
                       <div class="text">{{ $quote->text }}</div>
                       <?php 
                          preg_match('/.*\/(.*\.html)/', $quote->mult_uri, $multURI);
                          if (isset($multURI[1])) {
                            $multURI = $multURI[1];
                          } else {
                            $multURI = '/';
                          }

                          preg_match('/.*\/(multgeroi\/.*\.html)/', $quote->heroy_uri, $trailerURI);
                          if (isset($trailerURI[1])) {
                            $trailerURI = $trailerURI[1];
                          } else {
                            $trailerURI = '/';
                          } 
                        ?>
                       <div class="author">© <a href="/{{ $multURI }}">{{ $quote->heroy }}</a> -  <a href="/{{ $trailerURI }}"></a></div>
                    </div>
                  </div>
                @endforeach
              @endif
          </div>   
         </div>
         @if (isset($otherSelfContent))
            <section class="mult-category clr">
               <h2>Категории</h2>
               @foreach ($otherSelfContent as $value)
               <?php $inspect = false; ?>
               <?php
                  if (strpos($value->pagetitle, 'Трейлеры и видео')) {
                    $otherContentTitle = 'Трейлеры';
                    $uri = $value->alias . '/treilers.html';
                  } else if (strpos($value->pagetitle, 'Интересные факты')) {
                    $otherContentTitle = 'Интересные факты';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Саундтрек')) {
                    $otherContentTitle = 'Музыка';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Герои')) {
                    $otherContentTitle = 'Герои';
                    $uri = 'multgeroi/' . $value->alias . '.html';
                  } else if (strpos($value->pagetitle, 'Обои')) {
                    $otherContentTitle = 'Обои и постеры';
                    $uri = $value->uri;
                  } else {
                    $uri = $value->alias . '.html';
                  }
               ?>
               @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
               @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
               <?php $inspect = true; ?>
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/{{ $image = str_replace('assets', 'public', $value2->value) }}" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @break
               @endif
               @endforeach
               @if (!$inspect)
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/public/img/other_content.jpg" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @endif
               @endforeach
            </section>
     @endif
        @if (isset($otherTrailersSelfCategory) && count($otherTrailersSelfCategory))
          <section>
             <div class="mult items clr ">
                <h2>Другие трейлеры</h2>
                @foreach ($otherTrailersSelfCategory as $trailer)
                <div class="item">
                   <a href="{{ '/' . $trailer->alias . '.html' }}">
                      <div class="title">{{ $trailer->pagetitle }}</div>
                   </a>
                   <div class="jt left" style="top: 143px; left: 228px;">
                      <div class="jtc">
                         {{ $trailer->description }}        
                      </div>
                   </div>
                   <div class="poster jtg">   
                      @foreach ($trailer->kpxzxsitetmplvarcontentvalue as $value)
                        @if ($value->tmplvarid == 142)
                          <a href="{{ '/' . $trailer->alias . '.html' }}">
                          <?php 
                             $image = str_replace('assets', 'public', $value->value);
                             if ($image[0] !== '/' && $image[0] !== ' ') {
                               $image = '/' . $image;
                             }
                             ?>
                          <img src="{{ $image }}" alt="{{ $trailer->pagetitle }}">
                          </a>      
                          @break
                        @endif
                      @endforeach
                   </div>
                </div>
                @endforeach    
             </div>
             <div class="m-i"><a href="/{{ $multAlias }}/treilers.html">Другие трейлеры</a></div>
          </section>
        @endif
        @if (isset($otherTrailers))
          <section>
            <div class="mult items clr ">
               <h2>Больше других трейлеров</h2>
                  @foreach ($otherTrailers as $trailer)
                      <div class="item">
                          <a href="{{ '/' . $trailer->alias . '.html' }}">
                              <div class="title">{{ $trailer->pagetitle }}</div>
                          </a>
                          <div class="jt left" style="top: 143px; left: 228px;">
                              <div class="jtc">
                                  {{ $trailer->description }}        
                              </div>
                          </div>
                          <div class="poster jtg">   
                              @foreach ($trailer->kpxzxsitetmplvarcontentvalue as $value)
                                  @if ($value->tmplvarid == 142)
                                    <a href="{{ '/' . $trailer->alias . '.html' }}">
                                      <?php  
                                        $image = str_replace('assets', 'public', $value->value);
                                        if ($image[0] !== '/' && $image[0] !== ' ') {  
                                            $image = '/' . $image;
                                        }
                                      ?>
                                      <img src="{{ $image }}" alt="{{ $trailer->pagetitle }}">
                                    </a>      
                                    @break
                                  @endif
                              @endforeach
                          </div>
                      </div>
                  @endforeach    
            </div>
            <div class="m-i"><a href="/na-sladkoe/trejleryi.html">Все трейлеры</a></div>
          </section>
        @endif
       @else
        @if (isset($trailersChildren))
          @foreach ($trailersChildren as $trailer)
            <section>
              <div class="art items clr ">
                <div class="item">
                    <a href="/ {{ $trailer->uri }}">
                      <div class="title">{{ $trailer->pagetitle }}</div>
                    </a>
                    @foreach ($trailer->kpxzxsitetmplvarcontentvalue as $value)
                      @if ($value->tmplvarid == 142)
                        <div class="poster jtg">
                          <a href="/ {{ $trailer->uri }}">
                            <?php 
                                    $image = str_replace('assets', 'public', $value->value);
                                    if ($image[0] !== '/' && $image[0] !== ' ') {
                                      $image = '/' . $image;
                                    }
                            ?>
                            <img src="{{ $image }}" alt="{{ $trailer->pagetitle }}">
                          </a>
                        </div>
                      @endif
                    @endforeach
                    <div class="descr">
                      <?php echo $trailer->content ?>
                    </div>
                    <div class="button">
                      <a href="/{{ $trailer->uri }}"> 
                        Подробнее
                      </a>
                    </div>
                </div>
              </div>
            </section>
          @endforeach
        @endif
       @endif
       @if (isset($comments))
          <div id="comment">
            @foreach ($comments as $comment)
              <div class="com_list">
                 <div class="item" data-id="{{ $comment['comment']->comment_id }}">
                    <div class="clr">
                       <div class="avatar">
                          <img src="/public/cache_image/minion_avatar/21_100x100_ba9.png" alt="">
                       </div>
                       <div class="text">
                          <div class="hdr clr">
                             <b><div class="name">{{ $comment['comment']->name }}</div></b>
                             <?php  
                                $monthes = array(
                                    1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                                    5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                                    9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                                );
                             ?>
                             <div class="date">{!! gmdate("j " . $monthes[gmdate('m')] . " g:i", $comment['comment']->datetime); !!}</div>
                          </div>
                          <p>{{ $comment['comment']->comment }}</p>
                       </div>
                    </div>
                    <div class="clr">
                      <div class="ans" data-id="{{ $comment['comment']->comment_id }}" style="color: blue">Ответить</div>
                    </div>
                    @if (!is_null($comment['sub_comments']))
                      @foreach ($comment['sub_comments'] as $subComment)
                        <div class="item" data-id="{{ $subComment->comment_id }}">
                          <div class="clr">
                             <div class="avatar">
                                <img src="/public/cache_image/minion_avatar/21_100x100_ba9.png" alt="">
                             </div>
                             <div class="text">
                                <div class="hdr clr">
                                   <b><div class="name">{{ $subComment->name }}</div></b>
                                   <?php  
                                      $monthes = array(
                                          1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                                          5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                                          9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                                      );
                                   ?>
                                   <div class="date">{!! gmdate("j " . $monthes[gmdate('m')] . " g:i", $subComment->datetime); !!}</div>
                                </div>
                                <p>{{ $subComment->comment }}</p>
                             </div>
                          </div>
                        </div>
                      @endforeach
                    @endif
                 </div>
              </div>
            @endforeach
            @if (!$isParentTrailer)
              <input type="button" id="c_show" class="sbmBtnBlack sbmBtn comment_show" value="Комментарий">
            @endif
          </div>
       @endif
       <div id="search-result" class="hide"></div>
    @endforeach
</div>
<script type="text/javascript">
  $('.comment_show').on('click', function(event) {    
    $('.comment_show').hide();
    $('.dn').show();
    if ($('.dn').is(':visible')) {
      var html = '<span>Добавление комментария</span>' +
                 '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                  'отменить' +
                 '</div>';
      $('#comment_type').html(html);
      $('html, body').animate({
        scrollTop: $('.dn').offset().top
      }, 1000);
    }
  });

  $('.ans').on('click', function(event) {
    if ($('.comment_show').is(':visible')) {
      $('.comment_show').hide();
    }
    if (!$('.dn').is(':visible')) {
      $('.dn').show();
    }
    var html = '<span>Добавление ответа на комментарий</span>' +
               '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                'отменить' +
               '</div>';
    $('#comment_type').html(html);
    var id = $(this).data('id');  
    var val = $('[name=parent_id]').value;
    if (val == undefined) {
      $('[name=parent_id]').val(id);
    }
    $('html, body').animate({
        scrollTop: $('.dn').offset().top
    }, 1000);
  });

  $('body').on('click', '#collapse_form', function(event) {
    $('.dn').hide();
    $('.comment_show').show();
    $('[name=parent_id]').val(null);
  });
</script>
@endsection