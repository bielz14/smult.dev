@extends('layouts.app')

@section('container')
<?php $contentOutInspect = false; ?>
<?php $imageViewInspect = false; ?>
<?php $winnerViewInspect = false; ?>
<?php  
    $monthes = array(
        1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
        5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
        9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
    );
?>
<div class="content">
   @if (isset($competition)) 
     @foreach ($competition as $value)
         <h1>{{ $value->pagetitle }}</h1> 
         <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
            <span typeof="v:Breadcrumb">
               <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
               <p>&gt;</p>
            </span>
            <span typeof="v:Breadcrumb">
               <a href="/konkursi.html" rel="v:url" class="hov-bord" property="v:title">Смульт Конкурсы</a>
               <p>&gt;</p>
            </span>
            <span>{{ $value->pagetitle }}</span>
            <input id="content_id" type="hidden" value="{{ $value->id }}" />
         </div>
          <?php //if (\Auth::check() && \Auth::user()->can('editResource')) ?>
            @if (true)
              <div id="manager" style="position: absolute; margin: 7% 0 0 47%">
                @if (isset($itemId))
                  <a href="/manager/competition?item_id={{ $itemId }}">Редактировать</a><br>
                  <a href="/manager/competition-info?item_id={{ $itemId }}">Инфо</a><br>
                  <a href="/manager/competition-participants-and-winners?item_id={{ $itemId }}">Список участников и победителей</a>
                @endif
              </div>
          @endif
          <div class="comp full">
            <div class="item">
              @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2) 
                @if ($value2->tmplvarid == 142) 
                    <?php
                      $image = str_replace('assets', 'public', $value2->value);
                      if ($image[0] !== '/' && $image[0] !== ' ') {
                         $image = '/' . $image;
                      }
                    ?>
                @elseif ($value2->tmplvarid == 184)
                  <?php $startDate = gmdate("j " . $monthes[gmdate('m')] . " g:i", (int) $value2->value); ?>
                @elseif ($value2->tmplvarid == 183) 
                  <?php $endDate = gmdate("j " . $monthes[gmdate('m')] . " g:i", (int) $value2->value); ?>
                @elseif ($value2->tmplvarid == 185)   
                  <?php $prizePool = $value2->value; ?>
                @elseif ($value2->tmplvarid == 163) 
                  <?php $competitionLink = $value2->value; ?>
                @endif
                @if (isset($startDate) && isset($endDate))
                  <div class="top clr">
                     <div class="title">{{ $value->pagetitle }}</div>
                     <div class="date">Сроки проведения: C {{ $startDate }} до {{ $endDate }}</div>
                  </div>
                  <?php 
                    $imageViewInspect = true;
                    unset($startDate);
                    unset($endDate);
                  ?>
                @endif
                  @if (isset($image) && $imageViewInspect)  
                    <div class="cnt clr">   
                      <div class="left">    
                        <a href="{{ '/' . $value->alias . '.html' }}">
                          <?php
                            $image = str_replace('assets', 'public', $image);
                            if ($image[0] !== '/' && $image[0] !== ' ') {
                              $image = '/' . $image;
                            }
                          ?>
                          <img src="{{ $image }}" alt="{{ $value->pagetitle }}">
                        </a>      
                      </div> 
                      <?php 
                        $contentOutInspect = true;
                        $imageViewInspect = false;
                        unset($image);
                      ?>
                    </div>
                  @endif
                  @if ($contentOutInspect && isset($prizePool) && isset($competitionLink))
                    <div class="cnt clr">
                      <div class="cond">
                        <div class="clr">
                          <?php echo $value->content ?>
                          <?php $contentOutInspect = false; ?>
                        </div>
                      </div>
                      <div class="right">
                        @if (isset($prizePool)) 
                          <div class="fond">
                              <b>Призовой фонд: </b>{{  $prizePool }}        
                          </div>
                        @endif
                        @if (isset($competitionLink))   
                          <div class="comp-rel"><a rel="nofollow" href="{{ $competitionLink }}">Ссылка на конкурс</a></div>
                        @endif
                      </div>
                      <?php $winnerViewInspect = true; ?>
                    </div>
                  @endif
                @if ($winnerViewInspect)
                  <div class="comp-winner">
                    <div class="comp-items clr">
                      <div class="comp-item">
                         @if ($value2->tmplvarid == 111)
                           <h2>
                              Победитель:
                              <a rel="nofollow" href="https://vk.com/id198449773">Максим Фазулов</a>
                           </h2>
                         @endif
                      </div>
                    </div>
                  </div>
                @endif
              @endforeach
              <div class="bottom">
              </div>
            </div>
          </div>
         <input type="button" class="btn-comment sbmBtnBlack sbmBtn comment_show" value="Как Вам Конкурс?">
          <div class="comt-part comment clr">
            @if (isset($competition[0]->kpxzxwinner))
              <h2>Все участники</h2>
              @foreach ($competition[0]->kpxzxwinner as $winner)
                <div class="comt-part item">
                  <a href="{{ $winner->uri }}" rel="nofollow">{{ $winner->name }}</a>
                </div>
              @endforeach
             @else
              <h2>Участники отсутствуют</h2>
             @endif
          </div>
           @if (isset($other_mults))
           <section>
              <div class="mult items clr ">
                 <h2>Другие мультсериалы</h2>
                    @foreach ($other_mults as $other_mult)
                        <div class="item">
                            <a href="{{ '/' . $other_mult->alias . '.html' }}">
                                <div class="title">{{ $other_mult->pagetitle }}</div>
                            </a>
                            <div class="jt left" style="top: 143px; left: 228px;">
                                <div class="jtc">
                                    {{ $other_mult->description }}        
                                </div>
                            </div>
                            <div class="poster jtg">   
                                @foreach ($other_mult->kpxzxsitetmplvarcontentvalue as $value2)
                                    @if ($value2->tmplvarid == 142)
                                      <a href="{{ '/' . $other_mult->alias . '.html' }}">
                                        <?php 
                                          $image = str_replace('assets', 'public', $value2->value);
                                          if ($image[0] !== '/' && $image[0] !== ' ') {
                                            $image = '/' . $image;
                                          }
                                        ?>
                                        <img src="{{ $image }}" alt="{{ $other_mult->pagetitle }}">
                                      </a>      
                                      @break
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach    
              </div>
              <div class="m-i"><a href="/multserialyi.html">Больше интересных мультфильмов</a></div>
           </section>
          @endif
         <div class="repost-quoter clr">
            <div class="sktch">
              @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
                @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
                    <?php 
                      $image = str_replace('assets', 'public', $value2->value);
                      if ($image[0] !== '/' && $image[0] !== ' ') {
                        $image = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image;
                      }
                    ?>
                    <?php break; ?>
                  @endif
                @endforeach
                <div class="repost clr" data-title="{{ $value->pagetitle }}" data-description="{{ $value->description }}" data-uri="http://{{ $_SERVER['SERVER_NAME'] }}/{{ $value->uri }}" data-poster="{{ $image }}">
                  <div data-type="vk" class="soz-repost">
                     <img src="/public/img/vkshare.png" alt="">
                  </div>
                  <div data-type="fb" class="soz-repost">
                     <img src="/public/img/fbshare.png" alt="">
                  </div>
                  <div data-type="tw" class="soz-repost">
                     <img src="/public/img/twshare.png" alt="">
                  </div>
               </div>
               <img class="sketch" src="/public/img/soz-sketch.png" alt="sketch">
            </div>
          </div>
            <div class="quotes" id="quotes" style="width: 494px;">
              <div class="title">Цитаты великих...</div>
              @if (isset($quotes))
                @foreach ($quotes as $quote)
                  <div class="item">
                    <div class="poster">
                        <?php 
                          $image = str_replace('assets', 'public', $quote->poster);
                          if ($image[0] !== '/' && $image[0] !== ' ') {
                              $image = '/' . $image;
                          }
                        ?>
                       <img src="{{ $image }}" alt="">
                    </div>
                    <div class="right">
                       <div class="text">{{ $quote->text }}</div>
                       <?php 
                          preg_match('/.*\/(.*\.html)/', $quote->mult_uri, $multURI);
                          if (isset($multURI[1])) {
                            $multURI = $multURI[1];
                          } else {
                            $multURI = '/';
                          }

                          preg_match('/.*\/(multgeroi\/.*\.html)/', $quote->heroy_uri, $heroURI);
                          if (isset($heroURI[1])) {
                            $heroURI = $heroURI[1];
                          } else {
                            $heroURI = '/';
                          } 
                        ?>
                       <div class="author">© <a href="/{{ $multURI }}">{{ $quote->heroy }}</a> -  <a href="/{{ $heroURI }}"></a></div>
                    </div>
                  </div>
                @endforeach
              @endif
          </div>
         <div id="comment">
            @if (isset($comments))
              @foreach ($comments as $comment)
                <div class="com_list">
                   <div class="item" data-id="{{ $comment['comment']->comment_id }}">
                      <div class="clr">
                         <div class="avatar">
                            <img src="/public/cache_image/minion_avatar/21_100x100_ba9.png" alt="">
                         </div>
                         <div class="text">
                            <div class="hdr clr">
                               <b><div class="name">{{ $comment['comment']->name }}</div></b>
                               <?php  
                                  $monthes = array(
                                      1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                                      5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                                      9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                                  );
                               ?>
                               <div class="date">{!! gmdate("j " . $monthes[gmdate('m')] . " g:i", $comment['comment']->datetime); !!}</div>
                            </div>
                            <p>{{ $comment['comment']->comment }}</p>
                         </div>
                      </div>
                      <div class="clr">
                        <div class="ans" data-id="{{ $comment['comment']->comment_id }}" style="color: blue">Ответить</div>
                      </div>
                      @if (!is_null($comment['sub_comments']))
                        @foreach ($comment['sub_comments'] as $subComment)
                          <div class="item" data-id="{{ $subComment->comment_id }}">
                            <div class="clr">
                               <div class="avatar">
                                  <img src="/public/cache_image/minion_avatar/21_100x100_ba9.png" alt="">
                               </div>
                               <div class="text">
                                  <div class="hdr clr">
                                     <b><div class="name">{{ $subComment->name }}</div></b>
                                     <?php  
                                        $monthes = array(
                                            1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                                            5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                                            9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                                        );
                                     ?>
                                     <div class="date">{!! gmdate("j " . $monthes[gmdate('m')] . " g:i", $subComment->datetime); !!}</div>
                                  </div>
                                  <p>{{ $subComment->comment }}</p>
                               </div>
                            </div>
                          </div>
                        @endforeach
                      @endif
                   </div>
                </div>
              @endforeach
            @endif
            <input type="button" id="c_show" class="sbmBtnBlack sbmBtn comment_show" value="Комментарий">
            <div class="com-f-wrap">
               <div class="com-f dn" id="com-f">
                  <div id="comment_type" style="margin: 0 0 1.5% 0;">
                  </div>
                  {{ Form::open(array('url' => '/addcomment', 'method' => 'post', 'id' => 'add_comment')) }}
                    {{ csrf_field() }}
                    <div class="top-b">
                       {!! Form::text('name', NULL, ['id' => 'com_name', 'placeholder' => 'Имя*']) !!}
                       {!! Form::email('email', NULL, ['id' => 'com_email', 'placeholder' => 'E-mail']) !!}
                       <div class="email-tylt">
                          <div class="jt">
                             <div class="jtc">
                                Для получения ответов на Ваши комментарии, оставьте свой E-mail
                             </div>
                          </div>
                          <img src="/public/img/quetion.png" alt="" class="jtg">
                       </div>
                    </div>
                    {!! Form::textarea('text', NULL, ['cols' => '20', 'rows' => '5', 'id' => 'com_text', 'placeholder' => 'Текст комментария *']) !!}
                    <!--<label> От имени Smult
                      {!! Form::checkbox('fromSmult', 1, NULL, ['id' => 'from-admin']) !!}
                    </label>-->
                    {!! Form::hidden('content_id', $value->id) !!}
                    {!! Form::hidden('parent_id', 0) !!}
                    {!! Form::submit('Опубликовать', ['id' => 'c_publish', 'class' => 'sbmBtnBlack sbmBtn']) !!}
                  {{ Form::close() }}
               </div>
            </div>
         </div>
         <div id="search-result" class="hide"></div>
      @endforeach
    @endif
</div>
<script type="text/javascript">
  $('.comment_show').on('click', function(event) {    
    $('.comment_show').hide();
    $('.dn').show();
    if ($('.dn').is(':visible')) {
      var html = '<span>Добавление комментария</span>' +
                 '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                  'отменить' +
                 '</div>';
      $('#comment_type').html(html);
      $('html, body').animate({
        scrollTop: $('.dn').offset().top
      }, 1000);
    }
  });

  $('.ans').on('click', function(event) {
    if ($('.comment_show').is(':visible')) {
      $('.comment_show').hide();
    }
    if (!$('.dn').is(':visible')) {
      $('.dn').show();
    }
    var html = '<span>Добавление ответа на комментарий</span>' +
               '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                'отменить' +
               '</div>';
    $('#comment_type').html(html);
    var id = $(this).data('id');  
    var val = $('[name=parent_id]').value;
    if (val == undefined) {
      $('[name=parent_id]').val(id);
    }
    $('html, body').animate({
        scrollTop: $('.dn').offset().top
    }, 1000);
  });

  $('body').on('click', '#collapse_form', function(event) {
    $('.dn').hide();
    $('.comment_show').show();
    $('[name=parent_id]').val(null);
  });
</script>
<style>
  .comt-part .item {
    width: 33%;
    float: left;
    padding: .5% 1%;
  }
</style>
@endsection