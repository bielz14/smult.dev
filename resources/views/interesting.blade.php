@extends('layouts.app')

@section('container')
<div class="content">
  @if (isset($interesting))
     @foreach ($interesting as $value)
          <h1>{{ $value->pagetitle }}</h1>
          <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
            <span typeof="v:Breadcrumb">
               <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
               <p>&gt;</p>
            </span>
            <span typeof="v:Breadcrumb">
               <a href="/{{ $multAlias }}.html" rel="v:url" class="hov-bord" property="v:title">
                {{ $multPagetitle }}
               </a>
               <p>&gt;</p>
            </span>
            <span typeof="v:Breadcrumb">
               <a href="/{{ $multAlias }}/fakts.html" rel="v:url" class="hov-bord" property="v:title">
                {{ $multPagetitle }} ({{ $multPagetitleOrigin }}) Интересные факты
               </a>
               <p>&gt;</p>
            </span>
            <span>{{ $value->pagetitle }}</span>
            <input id="content_id" type="hidden" value="{{ $value->id }}" />
          </div>
          @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
            @if ($value2->tmplvarid == 142)
                <?php $posterURI = $value2->value; ?>
            @elseif ($value2->tmplvarid == 137)
                <?php $videoURL = $value2->value; ?>
            @endif
          @endforeach
          <div class="m-cnt clr">
            <div class="mult-content ndButInfo poster">
                <img src="{{ $image = str_replace('assets', 'public', $posterURI) }}" alt="{{ $value->pagetitle }}">
            </div>
            <div class="text">
                <div class="text-s" data-type="small">
                  <?php echo str_replace('src="assets/', 'src="/public/', $value->content) ?>
                </div>
              <span id="isReadMore" class="t-btn"><a>Подробнее...</a></span><span id="isReadMini" class="t-btn"><a>Свернуть текст</a></span>
            </div>
          </div>
          <?php //if (\Auth::check() && \Auth::user()->can('editResource')) ?>
            @if (true)
              <div id="manager" style="float: right">
                @if (isset($itemId))
                  <a href="/manager/resource?item_id={{ $itemId }}">Редактировать</a>
                @endif
              </div>
          @endif
          <div class="sktch">
              @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
                @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
                    <?php 
                      $image = str_replace('assets', 'public', $value2->value);
                      if ($image[0] !== '/' && $image[0] !== ' ') {
                        $image = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image;
                      }
                    ?>
                    <?php break; ?>
                  @endif
                @endforeach
                <div class="repost clr" data-title="{{ $value->pagetitle }}" data-description="{{ $value->description }}" data-uri="http://{{ $_SERVER['SERVER_NAME'] }}/{{ $value->uri }}" data-poster="{{ $image }}">
                  <div data-type="vk" class="soz-repost">
                     <img src="/public/img/vkshare.png" alt="">
                  </div>
                  <div data-type="fb" class="soz-repost">
                     <img src="/public/img/fbshare.png" alt="">
                  </div>
                  <div data-type="tw" class="soz-repost">
                     <img src="/public/img/twshare.png" alt="">
                  </div>
               </div>
               <img class="sketch" src="/public/img/soz-sketch.png" alt="sketch">
            </div>

            <div class="quotes" id="quotes" style="width: 494px;">
                <div class="title">Цитаты великих...</div>
                @if (isset($quotes))
                  @foreach ($quotes as $quote)
                    <div class="item">
                      <div class="poster">
                          <?php 
                            $image = str_replace('assets', 'public', $quote->poster);
                            if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                            }
                          ?>
                         <img src="{{ $image }}" alt="">
                      </div>
                      <div class="right">
                         <div class="text">{{ $quote->text }}</div>
                         <?php 
                            preg_match('/.*\/(.*\.html)/', $quote->mult_uri, $multURI);
                            if (isset($multURI[1])) {
                              $multURI = $multURI[1];
                            } else {
                              $multURI = '/';
                            }

                            preg_match('/.*\/(multgeroi\/.*\.html)/', $quote->heroy_uri, $heroURI);
                            if (isset($heroURI[1])) {
                              $heroURI = $heroURI[1];
                            } else {
                              $heroURI = '/';
                            } 
                          ?>
                         <div class="author">© <a href="/{{ $multURI }}">{{ $quote->heroy }}</a> -  <a href="/{{ $heroURI }}"></a></div>
                      </div>
                    </div>
                  @endforeach
                @endif
            </div>
         </div>
           @if (isset($otherSelfContent))
            <section class="mult-category clr">
               <h2>Категории</h2>
               @foreach ($otherSelfContent as $value)
               <?php $inspect = false; ?>
               <?php
                  if (strpos($value->pagetitle, 'Трейлеры и видео')) {
                    $otherContentTitle = 'Трейлеры';
                    $uri = $value->alias . '/treilers.html';
                  } else if (strpos($value->pagetitle, 'Интересные факты')) {
                    $otherContentTitle = 'Интересные факты';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Саундтрек')) {
                    $otherContentTitle = 'Музыка';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Герои')) {
                    $otherContentTitle = 'Герои';
                    $uri = 'multgeroi/' . $value->alias . '.html';
                  } else if (strpos($value->pagetitle, 'Обои')) {
                    $otherContentTitle = 'Обои и постеры';
                    $uri = $value->uri;
                  } else {
                    $uri = $value->alias . '.html';
                  }
               ?>
               @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
               @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
               <?php $inspect = true; ?>
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/{{ $image = str_replace('assets', 'public', $value2->value) }}" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @break
               @endif
               @endforeach
               @if (!$inspect)
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/public/img/other_content.jpg" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @endif
               @endforeach
            </section>
     @endif
            @if (isset($otherInterestingSelfCategory) && count($otherInterestingSelfCategory))
            <section>
             <div class="mult items clr ">
                <h2>Больше Фактов</h2>
                @foreach ($otherInterestingSelfCategory as $interesting)
                <div class="item">
                   <a href="{{ '/' . $interesting->alias . '.html' }}">
                      <div class="title">{{ $interesting->pagetitle }}</div>
                   </a>
                   <div class="jt left" style="top: 143px; left: 228px;">
                      <div class="jtc">
                         {{ $interesting->description }}        
                      </div>
                   </div>
                   <div class="poster jtg">   
                      @foreach ($interesting->kpxzxsitetmplvarcontentvalue as $value)
                        @if ($value->tmplvarid == 142)
                          <a href="{{ '/' . $interesting->alias . '.html' }}">
                          <?php 
                             $image = str_replace('assets', 'public', $value->value);
                             if ($image[0] !== '/' && $image[0] !== ' ') {
                               $image = '/' . $image;
                             }
                             ?>
                          <img src="{{ $image }}" alt="{{ $interesting->pagetitle }}">
                          </a>      
                          @break
                        @endif
                      @endforeach
                   </div>
                </div>
                @endforeach    
              </div>
            </section>
            @endif
            @if (isset($otherInterestings))
              <section>
                 <div class="mult items clr ">
                    <h2>Больше Фактов</h2>
                    @foreach ($otherInterestings as $interesting)
                    <div class="item">
                       <a href="{{ '/' . $interesting->alias . '.html' }}">
                          <div class="title">{{ $interesting->pagetitle }}</div>
                       </a>
                       <div class="jt left" style="top: 143px; left: 228px;">
                          <div class="jtc">
                             {{ $interesting->description }}        
                          </div>
                       </div>
                       <div class="poster jtg">   
                          @foreach ($interesting->kpxzxsitetmplvarcontentvalue as $value)
                          @if ($value->tmplvarid == 142)
                          <a href="{{ '/' . $interesting->alias . '.html' }}">
                          <?php  
                             $image = str_replace('assets', 'public', $value->value);
                             if ($image[0] !== '/' && $image[0] !== ' ') {  
                                 $image = '/' . $image;
                             }
                             ?>
                          <img src="{{ $image }}" alt="{{ $interesting->pagetitle }}">
                          </a>      
                          @break
                          @endif
                          @endforeach
                       </div>
                    </div>
                    @endforeach    
                 </div>
                 <div class="m-i"><a href="/na-sladkoe/trejleryi.html">Все трейлеры</a></div>
              </section>
              @endif
         <div id="search-result" class="hide"></div>
      @endforeach
  @endif
<?php //if (\Auth::check() && \Auth::user()->can('addResource')) ?>
@if (true)
  <div id="manager">
    @if (isset($resourceId))
      <a href="/manager/resource?parent={{ $resourceId }}">Добавить материал</a>
    @endif
  </div>
@endif
</div>
<script type="text/javascript">
  $('.comment_show').on('click', function(event) {    
    $('.comment_show').hide();
    $('.dn').show();
    if ($('.dn').is(':visible')) {
      var html = '<span>Добавление комментария</span>' +
                 '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                  'отменить' +
                 '</div>';
      $('#comment_type').html(html);
      $('html, body').animate({
        scrollTop: $('.dn').offset().top
      }, 1000);
    }
  });

  $('.ans').on('click', function(event) {
    if ($('.comment_show').is(':visible')) {
      $('.comment_show').hide();
    }
    if (!$('.dn').is(':visible')) {
      $('.dn').show();
    }
    var html = '<span>Добавление ответа на комментарий</span>' +
               '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                'отменить' +
               '</div>';
    $('#comment_type').html(html);
    var id = $(this).data('id');  
    var val = $('[name=parent_id]').value;
    if (val == undefined) {
      $('[name=parent_id]').val(id);
    }
    $('html, body').animate({
        scrollTop: $('.dn').offset().top
    }, 1000);
  });

  $('body').on('click', '#collapse_form', function(event) {
    $('.dn').hide();
    $('.comment_show').show();
    $('[name=parent_id]').val(null);
  });
</script>
@endsection