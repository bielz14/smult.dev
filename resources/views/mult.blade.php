@extends('layouts.app')

@section('container')
<div class="content">
   @if (isset($mult))
     @foreach ($mult as $value)
         <h1>{{ $value->pagetitle }}</h1>
         <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
            <span typeof="v:Breadcrumb">
              <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">
                {{ $_SERVER['SERVER_NAME'] }}
              </a>
              <p>&gt;</p>
            </span>
            <span>{{ $value->pagetitle }}</span>
            <input id="content_id" type="hidden" value="{{ $value->id }}" />
         </div>
         <div class="m-cnt clr">
            @if (isset($itemId))
              @if (!isset($siteTrailersCategoryId))
                <?php $siteTrailersCategoryId = NULL; ?>
              @endif
              @if (!isset($siteWallpsAndPostersCategoryId))
                <?php $siteWallpsAndPostersCategoryId = NULL; ?>
              @endif
              @if (!isset($siteInterestingCategoryId))
                <?php $siteInterestingCategoryId = NULL; ?>
              @endif
              @if (!isset($siteHeroCategoryId))
                <?php $siteHeroCategoryId = NULL; ?>
              @endif

              @if (!isset($trailersCategoryId))
                <?php $trailersCategoryId = NULL; ?>
              @endif
              @if (!isset($wallpsAndPostersCategoryId))
                <?php $wallpsAndPostersCategoryId = NULL; ?>
              @endif
              @if (!isset($interestingCategoryId))
                <?php $interestingCategoryId = NULL; ?>
              @endif
              @if (!isset($heroCategoryId))
                <?php $heroCategoryId = NULL; ?>
              @endif
              <?php //if (\Auth::check() && \Auth::user()->can('editMult')) ?>
              @if (true)
                  <div id="manager" style="float: right">
                      <a href="/manager/mult?item_id={{ $itemId }}">Редактировать</a>
                  </div><br>
              @endif
              <?php //if (\Auth::check() && \Auth::user()->can('addResource')) ?>
              @if (true)
                  <div id="manager" style="float: right">
                      <a href="/manager/resource?category_id={{ $siteTrailersCategoryId }}&parent={{ $trailersCategoryId }}">Добавить трейлер</a>
                  </div><br>
                  <div id="manager" style="float: right">
                      <a href="/manager/resource?category_id={{ $siteInterestingCategoryId }}&parent={{ $interestingCategoryId }}">Добавить интересных фактов</a>
                  </div><br>
                  <div id="manager" style="float: right">
                      <a href="/manager/resource?category_id={{ $siteHeroCategoryId }}&parent={{ $heroCategoryId }}">Добавить героев к мультфильму</a>
                  </div><br>
              @endif
              <?php //if (\Auth::check() && \Auth::user()->can(''addWallpAndPoster'')) ?>
              @if (true)
                  <div id="manager" style="float: right">
                      <a href="/manager/resource?category_id={{ $siteWallpsAndPostersCategoryId }}&parent={{ $wallpsAndPostersCategoryId }}">Добавить обои и постеры</a>
                  </div><br>
              @endif
             @endif
            <div class="mult-content ndButInfo poster">
                @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
                   @if ($value2->tmplvarid == 142)
                      <?php 
                        $image = str_replace('assets', 'public', $value2->value);
                        if ($image[0] !== '/' && $image[0] !== ' ') {
                          $image = '/' . $image;
                        }
                      ?>
                      <img src="{{ $image }}" alt="{{ $value->pagetitle }}">
                   @endif
                   <div class="butInfo"></div>
                   <div style="display: none; z-index:100; left:100px;" class="jt" id="jttip-mult-poster">
                      <div class="jtc">
                         <div>
                            <b>{{ $value->pagetitle }}</b>
                         </div>
                          @if ($value2->tmplvarid == 143)
                              <div><b>Год выпуска</b>: {{ $value2->value }}</div>
                          @elseif ($value2->tmplvarid == 144)
                              <div><b>Страна</b>: {{ $value2->value }}</div>
                          @elseif ($value2->tmplvarid == 149)
                              <div><b>Режиссер</b>: {{ $value2->value }}</div>
                          @elseif ($value2->tmplvarid == 155)
                              <div><b>Возрастное ограничение</b>: {{ $value2->value }}</div>
                          @elseif ($value2->tmplvarid == 156)
                              <div><b>Оригинальное название</b>: {{ $value2->value }}</div>
                          @elseif ($value2->tmplvarid == 137)
                              <?php $videoURL = $value2->value; ?>
                          @elseif ($value2->tmplvarid == 163)
                              <?php $videoURL = $value2->value; ?>
                          @endif
                      </div>
                    </div>
                  @endforeach
            </div>
            <div class="text">
               <div class="text-s" data-type="small">
                  <?php echo  $value->content ?>
               </div>
               <span id="isReadMore" class="t-btn"><a>Подробнее...</a></span><span id="isReadMini" class="t-btn"><a>Свернуть текст</a></span>
            </div>
         </div>
         <div class="player" id="wrapPlayer" style="height: 425.294px;">
            @if (isset($videoURL))
              <script>
                var playerWrapObj = document.getElementById("wrapPlayer")
                var playerWidth = playerWrapObj.offsetWidth
                var playerHeight = playerWidth / 1.7
                console.log(playerHeight)
                console.log(playerWidth)
                playerWrapObj.style.height = playerHeight + "px"
              </script>
              <iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="{{ $videoURL }}"></iframe>
            @else
              <div class="message_novideo">
                <b><span>Видео пока отсутствует</span></b>
              </div>
            @endif
            <div class="m-i tickets-btn"><span>Что-то не так...</span></div>
         </div>
         <input type="button" class="btn-comment sbmBtnBlack sbmBtn comment_show" value="Как Вам Мультфильм?">
         @if (isset($otherSelfContent))
            <section class="mult-category clr">
               <h2>Категории</h2>
               @foreach ($otherSelfContent as $value)
               <?php $inspect = false; ?>
               <?php
                  if (strpos($value->pagetitle, 'Трейлеры и видео')) {
                    $otherContentTitle = 'Трейлеры';
                    $uri = $value->alias . '/treilers.html';
                  } else if (strpos($value->pagetitle, 'Интересные факты')) {
                    $otherContentTitle = 'Интересные факты';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Саундтрек')) {
                    $otherContentTitle = 'Музыка';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Герои')) {
                    $otherContentTitle = 'Герои';
                    $uri = 'multgeroi/' . $value->alias . '.html';
                  } else if (strpos($value->pagetitle, 'Обои')) {
                    $otherContentTitle = 'Обои и постеры';
                    $uri = $value->uri;
                  } else {
                    $uri = $value->alias . '.html';
                  }
               ?>
               @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
               @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
               <?php $inspect = true; ?>
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/{{ $image = str_replace('assets', 'public', $value2->value) }}" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @break
               @endif
               @endforeach
               @if (!$inspect)
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/public/img/other_content.jpg" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @endif
               @endforeach
            </section>
     @endif
         @if (isset($other_mults))
           <section>
              <div class="mult items clr ">
                 <h2>Другие мультсериалы</h2>
                    @foreach ($other_mults as $other_mult)
                        <div class="item">
                            <a href="{{ '/' . $other_mult->alias . '.html' }}">
                                <div class="title">{{ $other_mult->pagetitle }}</div>
                            </a>
                            <div class="jt left" style="top: 143px; left: 228px;">
                                <div class="jtc">
                                    {{ $other_mult->description }}        
                                </div>
                            </div>
                            <div class="poster jtg">   
                                @foreach ($other_mult->kpxzxsitetmplvarcontentvalue as $value2)
                                    @if ($value2->tmplvarid == 142)
                                      <a href="{{ '/' . $other_mult->alias . '.html' }}">
                                        <?php 
                                          $image = str_replace('assets', 'public', $value2->value);
                                          if ($image[0] !== '/' && $image[0] !== ' ') {
                                            $image = '/' . $image;
                                          }
                                        ?>
                                        <img src="{{ $image }}" alt="{{ $other_mult->pagetitle }}">
                                      </a>      
                                      @break
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach    
              </div>
              <div class="m-i"><a href="/multserialyi.html">Больше интересных мультфильмов</a></div>
           </section>
          @endif
         <div class="repost-quoter clr">
            <div class="sktch">
              @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
                @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
                    <?php 
                      $image = str_replace('assets', 'public', $value2->value);
                      if ($image[0] !== '/' && $image[0] !== ' ') {
                        $image = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image;
                      }
                    ?>
                    <?php break; ?>
                  @endif
                @endforeach
                <div class="repost clr" data-title="{{ $value->pagetitle }}" data-description="{{ $value->description }}" data-uri="http://{{ $_SERVER['SERVER_NAME'] }}/{{ $value->uri }}" data-poster="{{ $image }}">
                  <div data-type="vk" class="soz-repost">
                     <img src="/public/img/vkshare.png" alt="">
                  </div>
                  <div data-type="fb" class="soz-repost">
                     <img src="/public/img/fbshare.png" alt="">
                  </div>
                  <div data-type="tw" class="soz-repost">
                     <img src="/public/img/twshare.png" alt="">
                  </div>
               </div>
               <img class="sketch" src="/public/img/soz-sketch.png" alt="sketch">
            </div>
            <div class="quotes" id="quotes" style="width: 494px;">
                <div class="title">Цитаты великих...</div>
                @if (isset($quotes))
                  @foreach ($quotes as $quote)
                    <div class="item">
                      <div class="poster">
                          <?php 
                            $image = str_replace('assets', 'public', $quote->poster);
                            if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                            }
                          ?>
                         <img src="{{ $image }}" alt="">
                      </div>
                      <div class="right">
                         <div class="text">{{ $quote->text }}</div>
                         <?php 
                            preg_match('/.*\/(.*\.html)/', $quote->mult_uri, $multURI);
                            if (isset($multURI[1])) {
                              $multURI = $multURI[1];
                            } else {
                              $multURI = '/';
                            }

                            preg_match('/.*\/(multgeroi\/.*\.html)/', $quote->heroy_uri, $heroURI);
                            if (isset($heroURI[1])) {
                              $heroURI = $heroURI[1];
                            } else {
                              $heroURI = '/';
                            } 
                          ?>
                         <div class="author">© <a href="/{{ $multURI }}">{{ $quote->heroy }}</a> -  <a href="/{{ $heroURI }}"></a></div>
                      </div>
                    </div>
                  @endforeach
                @endif
            </div>
         </div>
         <div id="comment">
            @if (isset($comments))
              @foreach ($comments as $comment)
                <div class="com_list">
                   <div class="item" data-id="{{ $comment['comment']->comment_id }}">
                      <div class="clr">
                         <div class="avatar">
                            <img src="/public/cache_image/minion_avatar/21_100x100_ba9.png" alt="">
                         </div>
                         <div class="text">
                            <div class="hdr clr">
                               <b><div class="name">{{ $comment['comment']->name }}</div></b>
                               <?php  
                                  $monthes = array(
                                      1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                                      5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                                      9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                                  );
                               ?>
                               <div class="date">{!! gmdate("j " . $monthes[gmdate('m')] . " g:i", $comment['comment']->datetime); !!}</div>
                            </div>
                            <p>{{ $comment['comment']->comment }}</p>
                         </div>
                      </div>
                      <div class="clr">
                        <div class="ans" data-id="{{ $comment['comment']->comment_id }}" style="color: blue">Ответить</div>
                      </div>
                      @if (!is_null($comment['sub_comments']))
                        @foreach ($comment['sub_comments'] as $subComment)
                          <div class="item" data-id="{{ $subComment->comment_id }}">
                            <div class="clr">
                               <div class="avatar">
                                  <img src="/public/cache_image/minion_avatar/21_100x100_ba9.png" alt="">
                               </div>
                               <div class="text">
                                  <div class="hdr clr">
                                     <b><div class="name">{{ $subComment->name }}</div></b>
                                     <?php  
                                        $monthes = array(
                                            1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                                            5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                                            9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                                        );
                                     ?>
                                     <div class="date">{!! gmdate("j " . $monthes[gmdate('m')] . " g:i", $subComment->datetime); !!}</div>
                                  </div>
                                  <p>{{ $subComment->comment }}</p>
                               </div>
                            </div>
                          </div>
                        @endforeach
                      @endif
                   </div>
                </div>
              @endforeach
            @endif
            <input type="button" id="c_show" class="sbmBtnBlack sbmBtn comment_show" value="Комментарий">
            <div class="com-f-wrap">
               <div class="com-f dn" id="com-f">
                  <div id="comment_type" style="margin: 0 0 1.5% 0;">
                  </div>
                  {{ Form::open(array('url' => '/addcomment', 'method' => 'post', 'id' => 'add_comment')) }}
                    {{ csrf_field() }}
                    <div class="top-b">
                       {!! Form::text('name', NULL, ['id' => 'com_name', 'placeholder' => 'Имя*']) !!}
                       {!! Form::email('email', NULL, ['id' => 'com_email', 'placeholder' => 'E-mail']) !!}
                       <div class="email-tylt">
                          <div class="jt">
                             <div class="jtc">
                                Для получения ответов на Ваши комментарии, оставьте свой E-mail
                             </div>
                          </div>
                          <img src="/public/img/quetion.png" alt="" class="jtg">
                       </div>
                    </div>
                    {!! Form::textarea('text', NULL, ['cols' => '20', 'rows' => '5', 'id' => 'com_text', 'placeholder' => 'Текст комментария *']) !!}
                    <!--<label> От имени Smult
                      {!! Form::checkbox('fromSmult', 1, NULL, ['id' => 'from-admin']) !!}
                    </label>-->
                    {!! Form::hidden('content_id', $value->id) !!}
                    {!! Form::hidden('parent_id', 0) !!}
                    {!! Form::submit('Опубликовать', ['id' => 'c_publish', 'class' => 'sbmBtnBlack sbmBtn']) !!}
                  {{ Form::close() }}
               </div>
            </div>
         </div>
         <div id="search-result" class="hide"></div>
      @endforeach
    @endif
</div>
<script type="text/javascript">
  $('.comment_show').on('click', function(event) {    
    $('.comment_show').hide();
    $('.dn').show();
    if ($('.dn').is(':visible')) {
      var html = '<span>Добавление комментария</span>' +
                 '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                  'отменить' +
                 '</div>';
      $('#comment_type').html(html);
      $('html, body').animate({
        scrollTop: $('.dn').offset().top
      }, 1000);
    }
  });

  $('.ans').on('click', function(event) {
    if ($('.comment_show').is(':visible')) {
      $('.comment_show').hide();
    }
    if (!$('.dn').is(':visible')) {
      $('.dn').show();
    }
    var html = '<span>Добавление ответа на комментарий</span>' +
               '<div id="collapse_form" style="margin-left: 1.5%; color: red; display: inline-block; cursor: pointer">' +
                'отменить' +
               '</div>';
    $('#comment_type').html(html);
    var id = $(this).data('id');  
    var val = $('[name=parent_id]').value;
    if (val == undefined) {
      $('[name=parent_id]').val(id);
    }
    $('html, body').animate({
        scrollTop: $('.dn').offset().top
    }, 1000);
  });

  $('body').on('click', '#collapse_form', function(event) {
    $('.dn').hide();
    $('.comment_show').show();
    $('[name=parent_id]').val(null);
  });
</script>
@endsection