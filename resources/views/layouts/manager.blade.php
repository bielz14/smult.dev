<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Менеджер-панель</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="Менеджер-панель">
    <meta property="og:image" content="/uploads/2/2013-08-15_17-31-15_1.jpg"><!-- -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/public/favicon.ico" type="image/x-icon"><!-- -->
    <meta name="description" content="Менеджер-панель">
    <link rel="stylesheet" href="/public/css/style.css">
    <link rel="stylesheet" href="/public/css/bootstrap.css">
    <link rel="stylesheet" href="/public/css/admin/admin.css">
    <link rel="stylesheet" href="/public/css/admin/menu.css">
    <script type="text/javascript" async="" src="http://mc.yandex.ru/metrika/watch.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>	
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>
      $(document).ready(function(){
        $('#valid-loader-wrap').html('<img id="valid-loader" style="display: none" src="/public/img/load.gif">')

         $('body').on('click','#text-validate',function(){  
           var pagetitle = $('#pagetitle').val()
           if(pagetitle==''){
               alert('Сначало заполните название')
               return '';
           }

           $('#valid-loader').show()
           var text = window.frames[0].document.getElementsByClassName('cke_show_borders')[0].children[0].innerHTML;
           if (text == null) {
              alert('Заполните текст');
           }
           $.ajax({
               type:"POST",
               url: "/manager/send-validation",
               data:{
                   text: text,
                   _token: $('meta[name="csrf-token"]').attr('content')
               },
               success:function(data){
                  $('#valid-loader-wrap').html('<span style="color: red;">' + data + '</span>');
               }
           })
       });

      $('#pagetitle').keyup(function( event ) {
          var value = $('#pagetitle').val();
          value = transliterate(transliterate(value, false));
          $('#alias').val(value);
      })

      $('#alias').keyup(function( event ) {
          var value = $('#pagetitle').val();
          value = transliterate(transliterate(value, false));
          $('#alias').val(value);
      })
    });

    transliterate = (
      function() {
        var
          rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь" . split(/ +/g),
          eng = "shh sh ch cz yu ya yo zh `` y' e` a b v g d e z i j k l m n o p r s t u f x `" . split(/ +/g);
          return function(text, engToRus) {
              var x;
              for(x = 0; x < rus.length; x++) {
                  text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
                  text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase()); 
              }
              return text;
          }
      }
    )();
    </script>
    <style>
    	.mainWrap nav {
    		background: #00994d;
    	}

    	.menu > li > a:hover,
    	.mobile-menu {
    		background: white;
    		color: #00994d;
    	}

    input[type=email], 
    input[type=text], 
    textarea {
      width: 100%;
      height: 34px;
      padding: 6px 12px;
      font-size: 14px;
      line-height: 1.42857143;
      color: #555;
      background-color: #fff;
      background-image: none;
      border: 1px solid #ccc;
      border-radius: 4px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
      -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
      -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
      transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
    </style>
</head>
<body>
    <header style="position: fixed; top: 0px; left: 0px; width: 90%; margin-left: 66.6563px; z-index: 100;">
        <div class="mainWrap">
           <a id="touch-menu" class="mobile-menu" href="#">
            <i class="icon-reorder"></i>Меню
           </a>
           <nav>
              <ul class="menu"> 
                 <li class="logo sprite-sprite"> 
                    <a class="" href="#"></a>
                 </li>
                 <li class=""> 
                    <a class="" href="/manager">Менеджер панель</a>
                 </li>
                 @if (\Auth::check() && \Auth::user()->can('managerPage'))
                   <li class=""> 
                      {{ Form::open(array('url' => '/logout-manager', 'method' => 'post', 'id' => 'logout_form', 'enctype' => 'multipart/form-data')) }}
                        {{ csrf_field() }}
                        {!! Form::submit('Выйти', ['id' => 'btn-logout', 'style' => 'background: #00994d; color: white; border: none']) !!}
                      {{ Form::close() }}
                      <!--<a class="" href="/logout">Выход</a>-->
                   </li>
                 @endif
              </ul>
           </nav>
        </div>
        <div id="mask" style="height: 2054px; display: none;"></div>
        <div class="mdl" id="reg-mdl" style="display: none;">
           <div class="hdr clr">
              <div class="title">Регистрация</div>
              <div class="btn close">x</div>
           </div>
           <div class="cnt">
              @guest
                <div class="reg-form">
                   <input id="email" placeholder="@" type="email" class="email ned" name="email" required autofocus>
                </div>
              @else
                <div class="office">
                  <div class="userName">
                    {{ Auth::user()->email }}
                  </div>
                  <input type="button" class="sbmBtn" id="logout-btn" value="Выход">
                </div>
              @endguest
           </div>
           <div class="ftr"></div>
        </div>
    </header>
    <div class="container clr" style="margin-top: 44px;">
        @yield('container')
    </div>
    <footer>
        <script src="/public/js/lib/copyright.js"></script>
        <script src="/public/js/lib/main-functions.js"></script>
        <script src="/public/js/main.js"></script>
    </footer>
</body>
</html>