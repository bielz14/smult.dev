@extends('layouts.app')

@section('container')
@if (!isset($pageTitle))
  <?php $pageTitle = ''; ?>
@endif
<?php $contentOutInspect = false; ?>
<?php $imageViewInspect = false; ?>
<?php $winnerViewInspect = false; ?>
<div class="content">
   <h1>Смульт Конкурсы</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
         <p>&gt;</p>
      </span>
      <span>Смульт Конкурсы</span>
   </div>
   <?php //if (\Auth::check() && \Auth::user()->can('addResource')) ?>
   @if (true)
      <div id="manager" style="float: right">
        @if (isset($resourceId))
          <a href="/manager/resource?parent={{ $resourceId }}">Добавить конкурс</a>
        @endif
      </div>
   @endif
   @if (isset($competitions))
    @foreach ($competitions as $competition)
      <section class="comp clr ajax_content_add" data-parent="" data-id="22316" data-page="1" data-template="125">
         <div class="item">
            <?php  
                $monthes = array(
                  1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                  5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                  9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                );
            ?>
            @foreach ($competition->kpxzxsitetmplvarcontentvalue as $value) 
              @if ($value->tmplvarid == 142) 
                  <?php
                    $image = str_replace('assets', 'public', $value->value);
                    if ($image[0] !== '/' && $image[0] !== ' ') {
                       $image = '/' . $image;
                    }
                  ?>
              @elseif ($value->tmplvarid == 184)
                <?php $startDate = gmdate("j " . $monthes[gmdate('m')] . " g:i", (int) $value->value); ?>
              @elseif ($value->tmplvarid == 183) 
                <?php $endDate = gmdate("j " . $monthes[gmdate('m')] . " g:i", (int) $value->value); ?>
              @elseif ($value->tmplvarid == 185)   
                <?php $prizePool = $value->value; ?>
              @elseif ($value->tmplvarid == 163)
                <?php $competitionLink = $value->value; ?>
              @endif
              @if (isset($startDate) && isset($endDate))
                <div class="top clr">
                   <div class="title">{{ $competition->pagetitle }}</div>
                   <div class="date">Сроки проведения: C {{ $startDate }} до {{ $endDate }}</div>
                </div>
                <?php 
                  $imageViewInspect = true;
                  unset($startDate);
                  unset($endDate);
                ?>
              @endif
                @if (isset($image) && $imageViewInspect)  
                  <div class="cnt clr">   
                    <div class="left">    
                      <a href="{{ '/' . $competition->alias . '.html' }}">
                        <?php
                          $image = str_replace('assets', 'public', $image);
                          if ($image[0] !== '/' && $image[0] !== ' ') {
                            $image = '/' . $image;
                          }
                        ?>
                        <img src="{{ $image }}" alt="{{ $competition->pagetitle }}">
                      </a>      
                    </div> 
                    <?php 
                      $contentOutInspect = true;
                      $imageViewInspect = false;
                      unset($image);
                    ?>
                  </div>
                @endif
                @if ($contentOutInspect && isset($prizePool) && isset($competitionLink))
                  <div class="cnt clr">
                    <div class="cond">
                      <div class="clr">
                        @if (mb_strlen($competition->content) > 250)
                          <?php echo substr($competition->content, 0, 249) . '...'; ?>
                        @else
                          <?php echo $competition->content ?>
                        @endif  
                        <?php $contentOutInspect = false; ?>
                      </div>
                    </div>
                    <div class="right">
                      @if (isset($prizePool)) 
                        <div class="fond">
                            <b>Призовой фонд: </b>{{  $prizePool }}        
                        </div>
                      @endif
                      @if (isset($competitionLink))   
                        <div class="comp-rel"><a rel="nofollow" href="{{ $competitionLink }}">Ссылка на конкурс</a></div>
                      @endif
                    </div>
                    <?php $winnerViewInspect = true; ?>
                  </div>
                @endif
              @if ($winnerViewInspect)
                <div class="comp-winner">
                  <div class="comp-items clr">
                    <div class="comp-item">
                       @if ($value->tmplvarid == 111)
                         <h2>
                            Победитель:
                            <a rel="nofollow" href="https://vk.com/id198449773">Максим Фазулов</a>
                         </h2>
                       @endif
                    </div>
                  </div>
                </div>
              @endif
            @endforeach
            @if (isset($competitionLink))
              <div class="bottom">
                <a href="/konkursi/{{ $competition->alias }}.html" class="sbmBtnBlack sbmBtn">Подробнее о конкурсе</a>
              </div>
            @endif
         </div>
         <div class="separator"></div>
      </section>
    @endforeach
  @endif
  @if (isset($competitions) && (count($competitions) > 1 || !is_null(app('request')->input('page'))))
    <div id="preloader" class="add_remove">
       <div id="show_more">
          <img src="/public/img/_.gif" alt="Загрузка"><br>
          <span>ПОКАЗАТЬ ЕЩЕ</span>
       </div>
    </div>
    <div id="nav" class="add_remove">
      {{ $competitions->render() }}
    </div>
  @endif
  <div id="search-result" class="hide"></div>
  </div>
  <script type="text/javascript">
  $(document).ready(function(){
    var nextPage = 0;
    var uri = null;
    var page = null;
    $('body').on('click', '#show_more', function(event){
      var prevImgs = $('section.ajax_content_add').get().reverse();
      if (page == null) {
        page = window.location.href.match(/(.*)\?page=(.*)/);
      }

      if (page == null) {
        page = 2;
        uri = window.location.href + '?page=' + page;
      } else if (nextPage == 0) {
        if (page.length > 0) {
          nextPage = parseInt(page[2], 10) + 1;
          uri = page[1] + '?page=' + nextPage;
        } else {
          nextPage = parseInt(page, 10) + 1;
          uri = window.location.href + '?page=' + nextPage;
        }
      } else {
        nextPage++;
        if (page.length > 0) {
          uri = page[1] + '?page=' + nextPage;
        } else {
          uri = window.location.href + '?page=' + nextPage;
        }
      }
      
      $.get(uri, function( data ) {
        document.body.innerHTML = data;
      });

      setTimeout(function(){
        prevImgs.forEach(function(item) {
          $('.ajax_content_add:first').before(item);
        });

        $('img').each(function() {
          var matches = $(this).prop('src').match(/:\/\/(.*)\/.*\/(public.*)/);
          if (matches != null) {
            $(this).prop('src', '/' + matches[2]);
          }
        });
      }, 2000);
    });
   });
</script>
  <style>
    li.page-item {
      display: inline-block;
    }

    li.page-item > a {
      color: black;
    }

    li.page-item > a:hover {
      color: #ffb324;
    }

    li.page-item.active {
      color: #ffb324;
    }

    span.page-link:hover {
      color: #ffb324;
      cursor: pointer;
    }
  </style>
@endsection