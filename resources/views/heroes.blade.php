@extends('layouts.app')

@section('container')
@if (!isset($pageTitle))
  <?php $pageTitle = ''; ?>
@endif
<div class="content">
   <h1Мультгерои</h1>
   @if (!$isCaegoryMulthero) 
      <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
        <span typeof="v:Breadcrumb">
           <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
           <p>&gt;</p>
        </span>
        <span typeof="v:Breadcrumb">
        <a href="/na-sladkoe.html" rel="v:url" class="hov-bord" property="v:title">На сладкое</a>
          <p>&gt;</p>
        </span>
        <span>Мультгерои</span>
      </div>
      <section class="ajax_content_add" data-parent="0" data-id="2434" data-page="1" data-template="121">
        <div class="mult items clr  big">
          @if (isset($heroes))
            @foreach ($heroes as $hero)
              <div class="item">
                <a href="{{ '/'  . $hero->uri }}">
                   <div class="title">{{ $hero->pagetitle }}</div>
                </a>
                <div class="jt left" style="top: 143px; left: 228px;">
                   <div class="jtc">
                      {{ $hero->description }}        
                   </div>
                </div>
                <div class="poster jtg">
                @foreach ($hero->kpxzxsitetmplvarcontentvalue as $value)
                   @if ($value->tmplvarid == 142)
                     <a href="{{ '/'  . $hero->uri }}">
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                     <img src="{{ $image }}" alt="{{ $hero->pagetitle }}">
                     </a>      
                     @break
                   @endif
                @endforeach
                </div>
              </div> 
            @endforeach     
          @endif    
        </div>
      <section>               
    @else
      @if (!isset($mult))
          <?php $multAlias = ''; ?>
          <?php $multPagetitle = ''; ?>
      @else 
          <?php $multAlias = $mult['alias']; ?>
          <?php $multPagetitle = $mult['pagetitle']; ?>
      @endif
      <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
        <span typeof="v:Breadcrumb">
           <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">{{ $_SERVER['SERVER_NAME'] }}</a>
           <p>&gt;</p>
        </span>
        <span typeof="v:Breadcrumb">
        <a href="/{{ $multAlias }}.html" rel="v:url" class="hov-bord" property="v:title">{{ $multPagetitle }}</a>
          <p>&gt;</p>
        </span>
        <span>{{ $multPagetitle }} Герои</span>
      </div>
      <section class="ajax_content_add" data-parent="0" data-id="2434" data-page="1" data-template="121">
        <div class="mult items clr  big">
          @if (isset($heroes))
            @foreach ($heroes as $hero)
              <div class="item">
                <a href="{{ '/'  . $hero->uri }}">
                   <div class="title">{{ $hero->pagetitle }}</div>
                </a>
                <div class="jt left" style="top: 143px; left: 228px;">
                   <div class="jtc">
                      {{ $hero->description }}        
                   </div>
                </div>
                <div class="poster jtg">
                @foreach ($hero->kpxzxsitetmplvarcontentvalue as $value)
                   @if ($value->tmplvarid == 142)
                     <a href="{{ '/'  . $hero->uri }}">
                      <?php 
                              $image = str_replace('assets', 'public', $value->value);
                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                $image = '/' . $image;
                              }
                      ?>
                     <img src="{{ $image }}" alt="{{ $hero->pagetitle }}">
                     </a>      
                     @break
                   @endif
                @endforeach
                </div>
                <div class="descr">
                  <?php echo $hero->content ?>
                </div>
                <div class="button">
                  <a href="/{{ $hero->uri }}"> 
                    Подробнее
                  </a>
                </div>
              </div> 
            @endforeach     
          @endif    
        </div>
      <section>  
    @endif
    <?php //if (\Auth::check() && \Auth::user()->can('addResource')) ?>
    @if (true)
      <div id="manager">
        @if (isset($resourceId))
          <a href="/manager/resource?parent={{ $resourceId }}">Добавить героя</a>
        @endif
      </div>
    @endif
   @if (isset($heroes) && (count($heroes) > 1 || !is_null(app('request')->input('page'))))
      <div id="preloader" class="add_remove">
         <div id="show_more">
            <img src="/public/img/_.gif" alt="Загрузка"><br>
            <span>ПОКАЗАТЬ ЕЩЕ</span>
         </div>
      </div>
      <div id="nav" class="add_remove">
        {{ $heroes->render() }}
      </div>
    @endif
    <div id="search-result" class="hide"></div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var nextPage = 0;
    var uri = null;
    $('body').on('click', '#show_more', function(event){
      console.log(nextPage);
      var prevImgs = $('div.item').get().reverse();
      var page = window.location.href.match(/(.*)\?page=(.*)/);
      if (page == null && nextPage == 0) {
        nextPage = 2;
        uri = window.location.href + '?page=' + nextPage;
      } else if (nextPage == 0) {
        if (page instanceof Array && page.length > 0) {
          nextPage = parseInt(page[2], 10) + 1;
          uri = page[1] + '?page=' + nextPage;
        } else {
          nextPage = parseInt(page, 10) + 1;
          uri = window.location.href + '?page=' + nextPage;
        }
      } else {
        nextPage++;
        if (page instanceof Array && page.length > 0) {
          uri = page[1] + '?page=' + nextPage;
        } else {
          uri = window.location.href + '?page=' + nextPage;
        }
      }

      $.get(uri, function( data ) {
        document.body.innerHTML = data;
      });

      setTimeout(function(){
        prevImgs.forEach(function(item) {
          $('.big').prepend(item);
        });

        $('div.item').each(function() {
          var matches = $(this).prop('src').match(/:\/\/(.*)\/.*\/(public.*)/);
          if (matches != null) {
            $(this).prop('src', '/' + matches[2]);
          }
        });
      }, 1000);
      $('.jtg > a > img').height(103);
    });
  });
</script>
<style>
  li.page-item {
    display: inline-block;
  }

  li.page-item > a {
    color: black;
  }

  li.page-item > a:hover {
    color: #ffb324;
  }

  li.page-item.active {
    color: #ffb324;
  }

  span.page-link:hover {
    color: #ffb324;
    cursor: pointer;
  }
</style>
@endsection
@section('other')
@if (isset($otherHeroes))
  <section>
     <div class="mult items clr ">
        <h2>Герои из других мультфильмов</h2>
        @foreach ($otherHeroes as $hero)
        <div class="item">
           <a href="{{ '/' . $hero->alias . '.html' }}">
              <div class="title">{{ $hero->pagetitle }}</div>
           </a>
           <div class="jt left" style="top: 143px; left: 228px;">
              <div class="jtc">
                 {{ $hero->description }}        
              </div>
           </div>
           <div class="poster jtg">   
              @foreach ($hero->kpxzxsitetmplvarcontentvalue as $value)
                @if ($value->tmplvarid == 142)
                  <a href="{{ '/' . $hero->alias . '.html' }}">
                  <?php 
                     $image = str_replace('assets', 'public', $value->value);
                     if ($image[0] !== '/' && $image[0] !== ' ') {
                       $image = '/' . $image;
                     }
                     ?>
                  <img src="{{ $image }}" alt="{{ $hero->pagetitle }}">
                  </a>      
                  @break
                @endif
              @endforeach
           </div>
        </div>
        @endforeach    
     </div>
     <div class="m-i"><a href="/na-sladkoe/multgeroi.html">Все герои...</a></div>
  </section>
@endif
@endsection