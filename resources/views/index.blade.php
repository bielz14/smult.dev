@extends('layouts.app')

@section('container')
<div class="content">
    <h1>Мультфильмы онлайн. Смотреть бесплатно мультики для детей в хорошем качестве на Smult.ru</h1>
    @if (isset($data))
        @if (!is_null($data['mults_expected']))
            <div class="primera-with-procat clr">
                <div class="primera">
                    <section>
                        <div class="mult items clr ">
                            <h2>Скоро премьера</h2>
                            @foreach ($data['mults_expected'] as $mult)  
                                <div class="item">
                                    <a href="/{{ $mult->uri }}">
                                        <div class="title">{{ $mult->pagetitle }}</div>
                                    </a>
                                    <div class="jt left" style="top: 134px; left: 215px;">
                                        <div class="jtc">
                                            {{ $mult->description }}                        
                                        </div>
                                    </div>
                                    <div class="poster jtg">
                                        <a href="/{{ $mult->uri }}">
                                        @foreach ($mult->kpxzxsitetmplvarcontentvalue as $value)
                                            @if ($value->tmplvarid == 142)
                                                <?php 
                                                          $image = str_replace('assets', 'public', $value->value);
                                                          if ($image[0] !== '/' && $image[0] !== ' ') {
                                                            $image = '/' . $image;
                                                          }
                                                ?>
                                                <img src="{{ $image }}" alt="{{ $value->pagetitle }}"></a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>  
                            @endforeach
                        </div>
                        <div class="m-i"><a href="/razdely/ozhidaemyie.html">Все ожидаемые</a></div>
                    </section>
                </div>
                @if (!is_null($data['mults_rental']))
                    <div class="procat primera">
                        <section>
                            <div class="mult items clr ">
                                <h2>Уже в прокате</h2>
                                @foreach ($data['mults_rental'] as $mult)  
                                    <div class="item">
                                        <a href="/{{ $mult->uri }}">
                                            <div class="title">{{ $mult->pagetitle }}</div>
                                        </a>
                                        <div class="jt left" style="top: 134px; left: 215px;">
                                            <div class="jtc">
                                                {{ $mult->description }}                        
                                            </div>
                                        </div>
                                        <div class="poster jtg">
                                            <a href="/{{ $mult->uri }}">
                                            @foreach ($mult->kpxzxsitetmplvarcontentvalue as $value)
                                                @if ($value->tmplvarid == 142)
                                                    <?php 
                                                              $image = str_replace('assets', 'public', $value->value);
                                                              if ($image[0] !== '/' && $image[0] !== ' ') {
                                                                $image = '/' . $image;
                                                              }
                                                    ?>
                                                    <img src="{{ $image }}" alt="{{ $value->pagetitle }}"></a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>  
                                @endforeach
                            </div>
                            <div class="m-i"><a href="/uzhe-v-prokate.html">Все премьеры</a></div>
                        </section>
                    </div>
                @endif
            </div>
        @endif
                @if (!is_null($data['posts']))
            <section>
                <div class="art items clr ">
                    <h2>Новости</h2>
                     @foreach ($data['posts'] as $post)  
                        <div class="item">
                            <a href="/{{ $post->uri }}">
                                <div class="title">{{ $post->pagetitle }}</div>
                            </a>
                            <div class="jt">
                                <div class="jtc">
                                    {{ $post->description }}                       
                                </div>
                            </div>
                            <div class="poster jtg">
                                <a href="/{{ $post->uri }}">
                                @foreach ($post->kpxzxsitetmplvarcontentvalue as $value)
                                    @if ($value->tmplvarid == 142)
                                         <?php 
                                                  $image = str_replace('assets', 'public', $value->value);
                                                  if ($image[0] !== '/' && $image[0] !== ' ') {
                                                    $image = '/' . $image;
                                                  }
                                          ?>
                                        <img src="{{ $image }}" alt="{{ $post->pagetitle }}"></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="m-i"><a href="/news.html">Все новости</a></div>
            </section>
        @endif
        @if (!is_null($data['mults_popular']))
            <section>
                <div class="mult items clr ">
                    <h2>Популярные мультфильмы</h2>
                    @foreach ($data['mults_popular'] as $mult)  
                        <div class="item">
                            <a href="/{{ $mult->uri }}">
                                <div class="title">{{ $mult->pagetitle }}</div>
                            </a>
                            <div class="jt">
                                <div class="jtc">
                                    {{ $mult->description }}                       
                                </div>
                            </div>
                            <div class="poster jtg">
                                <a href="/{{ $mult->uri }}">
                                @foreach ($mult->kpxzxsitetmplvarcontentvalue as $value)
                                    @if ($value->tmplvarid == 142)
                                          <?php 
                                                  $image = str_replace('assets', 'public', $value->value);
                                                  if ($image[0] !== '/' && $image[0] !== ' ') {
                                                    $image = '/' . $image;
                                                  }
                                          ?>
                                        <img src="{{ $image }}" alt="{{ $mult->pagetitle }}"></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="m-i"><a href="/razdely/populyarnyie-multfilmyi.html">Все популярные мультфильмы</a></div>
            </section>
        @endif
        @if (!is_null($data['new_trailers']))
            <section>
                <div class="mult items clr ">
                    <h2>Новые видео</h2>
                    <?php $i = 0; ?>
                     @foreach ($data['new_trailers'] as $trailer)
                        @if ($i > 0)  
                        <div class="item" style="margin-left: 5%">
                        @else
                        <div class="item">
                        @endif
                            <a href="/{{ $trailer->uri }}">
                                <div class="title">{{ $trailer->pagetitle }}</div>
                            </a>
                            <div class="jt">
                                <div class="jtc">
                                    {{ $trailer->description }}                       
                                </div>
                            </div>
                            <div class="poster jtg">
                                <a href="/{{ $trailer->uri }}">
                                @foreach ($trailer->kpxzxsitetmplvarcontentvalue as $value)
                                    @if ($value->tmplvarid == 142)
                                          <?php 
                                                  $image = str_replace('assets', 'public', $value->value);
                                                  if ($image[0] !== '/' && $image[0] !== ' ') {
                                                    $image = '/' . $image;
                                                  }
                                          ?>
                                        <img src="{{ $image }}" alt="{{ $trailer->pagetitle }}" style="height: 142px; width: 257px"></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <?php $i++; ?>
                    @endforeach
                </div>
                <div class="m-i"><a href="/na-sladkoe/trejleryi.html">Все видео</a></div>
            </section>
        @endif
        @if (!is_null($data['new_mults']))
            <section>
                <div class="mult items clr ">
                    <h2>Новые мультфильмы</h2>
                    @foreach ($data['new_mults'] as $mult)  
                        <div class="item">
                            <a href="/{{ $mult->uri }}">
                                <div class="title">{{ $mult->pagetitle }}</div>
                            </a>
                            <div class="jt">
                                <div class="jtc">
                                    {{ $mult->description }}                       
                                </div>
                            </div>
                            <div class="poster jtg">
                                <a href="/{{ $mult->uri }}">
                                @foreach ($mult->kpxzxsitetmplvarcontentvalue as $value)
                                    @if ($value->tmplvarid == 142)
                                         <?php 
                                                  $image = str_replace('assets', 'public', $value->value);
                                                  if ($image[0] !== '/' && $image[0] !== ' ') {
                                                    $image = '/' . $image;
                                                  }
                                          ?>
                                        <img src="{{ $image }}" alt="{{ $mult->pagetitle }}"></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="m-i"><a href="/new-mult.html">Все мультфильмы</a></div>
            </section>
        @endif
        @if (!is_null($data['new_articles']))
            <section>
                <div class="art items clr ">
                    <h2>Новые статьи</h2>
                     @foreach ($data['new_articles'] as $article)  
                        <div class="item">
                            <a href="/{{ $article->uri }}">
                                <div class="title">{{ $article->pagetitle }}</div>
                            </a>
                            <div class="jt">
                                <div class="jtc">
                                    {{ $article->description }}                       
                                </div>
                            </div>
                            <div class="poster jtg">
                                <a href="/{{ $article->uri }}">
                                @foreach ($article->kpxzxsitetmplvarcontentvalue as $value)
                                    @if ($value->tmplvarid == 142)
                                          <?php 
                                                  $image = str_replace('assets', 'public', $value->value);
                                                  if ($image[0] !== '/' && $image[0] !== ' ') {
                                                    $image = '/' . $image;
                                                  }
                                          ?>
                                        <img src="{{ $image }}" alt="{{ $article->pagetitle }}"></a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="m-i"><a href="/tolko-na-smultru.html">Все статьи</a></div>
            </section>
        @endif
        @if (!is_null($data['multseries_updates']))
            <section>
                <div class="mult items clr ">
                    <h2>Обновление в мультсериалах</h2>
                    @foreach ($data['multseries_updates'] as $multseries)
                        <div class="item">
                            <a href="/{{ $multseries['uri'] }}">
                                <div class="title">{{ $multseries['title'] }}</div>
                            </a>
                            <div class="jt">
                                <div class="jtc">
                                    {{ $multseries['description'] }}                       
                                </div>
                            </div>
                            <div class="poster jtg">
                                @if (!is_null($multseries['lable']))
                                    @if ($multseries['type'] == 'season')
                                        <?php
                                            preg_match('/.*(\d\s.*)/', $multseries['title'], $matches);
                                            if (isset($matches[1])) {
                                                $alt = $matches[1];
                                            } else {
                                                $alt = '';
                                            }
                                        ?>
                                        <div class="upd-img">
                                            <a href="/{{ $multseries['uri'] }}">
                                                <img src="{{ $multseries['lable'] }}" alt="Добавлен {{ $alt }}" title="Добавлен {{ $alt }}">
                                            </a>
                                        </div>
                                    @elseif ($multseries['type'] == 'series')
                                        <?php
                                            preg_match('/.*(\d\s.*)/', $multseries['title'], $matches);
                                            if (isset($matches[1])) {
                                                $alt = $matches[1];
                                            } else {
                                                $alt = '';
                                            }
                                        ?>
                                        <div class="upd-img">
                                            <a href="/{{ $multseries['uri'] }}">
                                                <img src="{{ $multseries['lable'] }}" alt="Добавлена {{ $alt }}" title="Добавлена {{ $alt }}">
                                            </a>
                                        </div>
                                    @else 
                                        <div class="upd-img">
                                            <a href="/{{ $multseries['uri'] }}">
                                                <img src="{{ $multseries['lable'] }}" alt="Добавлен мульфильм" title="Добавлен мульфильм">
                                            </a>
                                        </div>
                                    @endif
                                @endif
                                <a href="/{{ $multseries['uri'] }}">
                                    <?php 
                                            $image = str_replace('assets', 'public', $multseries['poster']);
                                            if ($image[0] !== '/' && $image[0] !== ' ') {
                                                $image = '/' . $image;
                                            }
                                    ?>
                                    <img src="{{ $image }}" alt="{{ $multseries['title'] }}">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="m-i"><a href="/multserialyi.html">Все мультсериалы</a></div>
            </section>
        @endif
    @else 
        <div id="message_no_content">
            <span>Контент отсутствует</span>
        </div>
    @endif
    <div id="search-result" class="hide"></div>
</div>
@endsection
