@extends('layouts.app')

@section('container')
@if (!isset($mult))
    <?php $multAlias = ''; ?>
    <?php $multPagetitle = ''; ?>
@else 
    <?php $multAlias = $mult['alias']; ?>
    <?php $multPagetitle = $mult['pagetitle']; ?>
@endif
@if (!isset($pagetitle))
    <?php $pagetitle = ''; ?>
@endif
<div class="content">
   <h1>{{ $pagetitle }}</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://smult.ru/" rel="v:url" class="hov-bord" property="v:title">smult.ru</a>
         <p>&gt;</p>
      </span>
      <span typeof="v:Breadcrumb">
         <a rel="v:url" property="v:title" class="hov-bord" href="/{{ $multAlias }}.html">{{ $multPagetitle }}</a>
         <p>&gt;</p>
      </span>
      <span>{{ $pagetitle }}</span>
   </div>
   <?php //if (\Auth::check() && \Auth::user()->can('addResource')) ?>
    @if (true)
      <div id="manager">
        @if (isset($resourceId))
          <a href="/manager/resource?parent={{ $resourceId }}">Добавить обои</a>
        @endif
      </div>
    @endif
   @if (isset($wallpsAndPosters))
    <div class="poster-list">
      @foreach ($wallpsAndPosters as $wallpAndPoster)
        <a rel="example_group" title="" href="/{{ $wallpAndPoster }}">
          <img src="/{{ $wallpAndPoster }}">
        </a>
      @endforeach
    </div>
   @endif
   @if (isset($otherSelfContent))
            <section class="mult-category clr">
               <h2>Категории</h2>
               @foreach ($otherSelfContent as $value)
               <?php $inspect = false; ?>
               <?php
                  if (strpos($value->pagetitle, 'Трейлеры и видео')) {
                    $otherContentTitle = 'Трейлеры';
                    $uri = $value->alias . '/treilers.html';
                  } else if (strpos($value->pagetitle, 'Интересные факты')) {
                    $otherContentTitle = 'Интересные факты';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Саундтрек')) {
                    $otherContentTitle = 'Музыка';
                    $uri = $value->uri;
                  } else if (strpos($value->pagetitle, 'Герои')) {
                    $otherContentTitle = 'Герои';
                    $uri = 'multgeroi/' . $value->alias . '.html';
                  } else if (strpos($value->pagetitle, 'Обои')) {
                    $otherContentTitle = 'Обои и постеры';
                    $uri = $value->uri;
                  } else {
                    $uri = $value->alias . '.html';
                  }
               ?>
               @foreach ($value->kpxzxsitetmplvarcontentvalue as $value2)
               @if ($value2->tmplvarid == 142 || $value2->tmplvarid == 129)
               <?php $inspect = true; ?>
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/{{ $image = str_replace('assets', 'public', $value2->value) }}" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @break
               @endif
               @endforeach
               @if (!$inspect)
               <a class="cat" href="/{{ $value->alias }}.html">
                  <img src="/public/img/other_content.jpg" alt="{{ $otherContentTitle }}">
                  <div class="text">
                     <div class="tbl">
                        <span>{{ $otherContentTitle }}</span>
                     </div>
                  </div>
               </a>
               @endif
               @endforeach
            </section>
     @endif
   <div class="repost-quoter clr">
      <div class="sktch">
                <?php $categpryPagetitle = NULL; ?>
                <?php $categpryDescription = NULL; ?>
                <?php $categpryURI = NULL; ?>
                @if (isset($wallpsAndPostersCategory))
                  <?php $categpryPagetitle = $wallpsAndPostersCategory->pagetitle; ?>
                  <?php $categpryDescription = $wallpsAndPostersCategory->description; ?>
                  <?php $categpryURI = $wallpsAndPostersCategory->uri; ?>
                  @foreach ($wallpsAndPostersCategory->kpxzxsitetmplvarcontentvalue as $value)
                    @if ($value->tmplvarid == 142 || $value->tmplvarid == 129)
                      <?php 
                        $image = str_replace('assets', 'public', $value->value);
                        if ($image[0] !== '/' && $image[0] !== ' ') {
                          $image = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $image;
                        }
                      ?>
                      <?php break; ?>
                    @endif
                  @endforeach
                @endif
                <div class="repost clr" data-title="{{ $categpryPagetitle }}" data-description="{{ $categpryDescription }}" data-uri="http://{{ $_SERVER['SERVER_NAME'] }}/{{ $categpryURI }}" data-poster="{{ $image }}">
                  <div data-type="vk" class="soz-repost">
                     <img src="/public/img/vkshare.png" alt="">
                  </div>
                  <div data-type="fb" class="soz-repost">
                     <img src="/public/img/fbshare.png" alt="">
                  </div>
                  <div data-type="tw" class="soz-repost">
                     <img src="/public/img/twshare.png" alt="">
                  </div>
               </div>
               <img class="sketch" src="/public/img/soz-sketch.png" alt="sketch">
            </div>
      <div class="quotes" id="quotes" style="width: 494px;">
              <div class="title">Цитаты великих...</div>
              @if (isset($quotes))
                @foreach ($quotes as $quote)
                  <div class="item">
                    <div class="poster">
                        <?php 
                          $image = str_replace('assets', 'public', $quote->poster);
                          if ($image[0] !== '/' && $image[0] !== ' ') {
                              $image = '/' . $image;
                          }
                        ?>
                       <img src="{{ $image }}" alt="">
                    </div>
                    <div class="right">
                       <div class="text">{{ $quote->text }}</div>
                       <?php 
                          preg_match('/.*\/(.*\.html)/', $quote->mult_uri, $multURI);
                          if (isset($multURI[1])) {
                            $multURI = $multURI[1];
                          } else {
                            $multURI = '/';
                          }

                          preg_match('/.*\/(multgeroi\/.*\.html)/', $quote->heroy_uri, $heroURI);
                          if (isset($heroURI[1])) {
                            $heroURI = $heroURI[1];
                          } else {
                            $heroURI = '/';
                          } 
                        ?>
                       <div class="author">© <a href="/{{ $multURI }}">{{ $quote->heroy }}</a> -  <a href="/{{ $heroURI }}"></a></div>
                    </div>
                  </div>
                @endforeach
              @endif
          </div>
   </div>
</div>
@endsection