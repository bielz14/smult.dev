@extends('layouts.app')

@section('container')
<?php 
   if (!isset($page)) {
      $page = NULL;
   }
?>
<div class="content ">
   <h1>Карта сайта {{ ucfirst($_SERVER['SERVER_NAME']) }}</h1>
   <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
      <span typeof="v:Breadcrumb">
         <a href="http://{{ $_SERVER['SERVER_NAME'] }}/" rel="v:url" class="hov-bord" property="v:title">
            {{ $_SERVER['SERVER_NAME'] }}
         </a>
         <p>&gt;</p>
      </span>
      <span>Карта сайта {{ ucfirst($_SERVER['SERVER_NAME']) }}</span>
   </div>
   @if (is_null($page))
      <ul>
         <li><a href="sitemap.html?page=studios">Анимационные студии</a></li>
         <li><a href="new-mult.html">Мультфильмы 2016 года. Смотреть бесплатно мультфильмы 2016, 2017 года в хорошем качестве</a></li>
         <li><a href="/sitemap.html?page=sections">Все разделы</a></li>
         <li><a href="/multserialyi.html">Мультсериалы</a></li>
         <li><a href="/uzhe-v-prokate.html">Уже в прокате</a></li>
         <li><a href="/news.html">Смульт Новости</a></li>
         <li><a href="/sitemap.html?page=interesting">Самое интересное</a></li>
         <li><a href="/na-sladkoe.html">На Сладкое</a></li>
         <li><a href="/konkursi.html">Смульт Конкурсы</a></li>
      </ul>
   @elseif ($page == 'studios') 
      <ul>
         <li><a href="/studies/walt-disney.html">Walt Disney (Уолт Дисней)</a></li>
         <li><a href="/studies/pixar.html">Pixar (Пиксар)</a></li>
         <li><a href="/studies/blue-sky-studios.html">Blue Sky Studios (Блу Скай)</a></li>
         <li><a href="/studies/dreamworks-animation.html">DreamWorks Animation (Дримворкс)</a></li>
         <li><a href="/studies/sony-pictures-animation.html">Sony Pictures Animation (Сони Пикчерс)</a></li>
         <li><a href="/studies/illumination-entertainment.html">Illumination Entertainment (Иллюминейшен Интертеймент)</a></li>
         <li><a href="/studies/melnica-animation.html">Мельница</a></li>
         <li><a href="/studies/nickelodeon-nikelodeon.html">Nickelodeon (Никелодеон)</a></li>
         <li><a href="/studies/cgbros.html">CGBros</a></li>
         <li><a href="/studies/other.html">Прочие студии</a></li>
      </ul>
   @elseif ($page == 'sections')
      <ul>
         <li><a href="/razdely/zarubezhnie-multfilmi.html">Зарубежные мультфильмы</a></li>
         <li><a href="/razdely/multfilmy-pro-transformerov.html">Мультфильмы про Трансформеров</a></li>
         <li><a href="/razdely/multfilmy-pro-smesharikov.html">Мультики про Смешариков</a></li>
         <li><a href="/razdely/multfilmy-pro-dashu.html">Мультики про Дашу</a></li>
         <li><a href="/razdely/multfilmy-pro-barbi.html">Мультики про Барби</a></li>
         <li><a href="/razdely/tom-i-dzherri.html">Мультики про Тома и Джерри</a></li>
         <li><a href="/razdely/multfilmy-pro-betmena.html">Мультики про Бэтмена</a></li>
         <li><a href="/razdely/multfilmy-pro-skubi-du.html">Мультики про Скуби-Ду</a></li>
         <li><a href="/razdely/multiki-lego.html">Мультики Лего</a></li>
         <li><a href="/razdely/shkola-monstrov-shkola-hey-monstr-hay.html">Мультики Монстр Хай</a></li>
         <li><a href="/razdely/multfilmy-pro-cheloveka-pauka.html">Мультики про Человека-паука</a></li>
         <li><a href="/razdely/multfilmy-dlya-malyshey.html">Мультики для Малышей</a></li>
         <li><a href="/razdely/cherepashki-nindzya.html">Мультики про Черепашек-ниндзя</a></li>
         <li><a href="/razdely/multfilmy-ben-10.html">Мультики Бен 10</a></li>
         <li><a href="/razdely/anime.html">Мультики Аниме</a></li>
         <li><a href="/razdely/razvivayushchie-multfilmy.html">Развивающие мультики</a></li>
         <li><a href="/razdely/multfilmy-pro-griffinov.html">Мультики про Гриффинов</a></li>
         <li><a href="/razdely/skazki.html">Мультики Сказки</a></li>
         <li><a href="/razdely/multiki-bratc.html">Мультики Братц</a></li>
         <li><a href="/razdely/multfilmy-futurama.html">Мультики Футурама</a></li>
         <li><a href="/razdely/multfilmy-pro-simpsonov.html">Мультики про Симпсонов</a></li>
         <li><a href="/razdely/multiki-yuzhnyy-park.html">Мультики Южный парк</a></li>
         <li><a href="/razdely/multfilmy-pro-futbol.html">Мультики про футбол</a></li>
         <li><a href="/razdely/multfilmy-pro-dinozavrov.html">Мультики про Динозавров</a></li>
         <li><a href="/razdely/multiki-gubka-bob.html">Мультики Губка Боб</a></li>
         <li><a href="/razdely/multfilmy-pro-tigrov.html">Мультики про Тигров</a></li>
         <li><a href="/razdely/multfilmy-pro-rycarey.html">Мультики про Рыцарей</a></li>
         <li><a href="/razdely/multfilmy-pro-bogov.html">Мультики про Богов</a></li>
         <li><a href="/razdely/multfilmy-pro-indeycev.html">Мультики про Индейцев</a></li>
         <li><a href="/razdely/multfilmy-pro-priklyucheniya.html">Мультики про Приключения</a></li>
         <li><a href="/razdely/multfilmy-pro-gnomov.html">Мультики про Гномов</a></li>
         <li><a href="/razdely/muzykalnye-multfilmy.html">Музыкальные мультики</a></li>
         <li><a href="/razdely/razdel-multfilmy-pro-elfov.html">Мультики про эльфов</a></li>
         <li><a href="/razdely/multfilmy-pro-nasekomyh.html">Мультики про Насекомых</a></li>
         <li><a href="/razdely/multfilmy-pro-sov.html">Мультики про Сов</a></li>
         <li><a href="/razdely/multfilmy-pro-shkolu.html">Мультики про Школу</a></li>
         <li><a href="/razdely/multfilmy-pro-voynu.html">Мультики про Войну</a></li>
         <li><a href="/razdely/multiki-pro-druzhbu.html">Мультики про Дружбу</a></li>
         <li><a href="/razdely/multiki-pro-medvedey.html">Мультики про Медведей</a></li>
         <li><a href="/razdely/multiki-pro-myshey.html">Мультики про Мышей</a></li>
         <li><a href="/razdely/multfilmy-pro-rybok.html">Мультики про Рыбок</a></li>
         <li><a href="/razdely/multiki-pro-samolety.html">Мультики про Самолеты</a></li>
         <li><a href="/razdely/multiki-pro-kosmos.html">Мультики про Космос</a></li>
         <li><a href="/razdely/multiki-pro-poezda.html">Мультики про паровозики</a></li>
         <li><a href="/razdely/multiki-pro-koshek.html">Мультики про Кошек</a></li>
         <li><a href="/razdely/multiki-pro-piratov.html">Мультики про Пиратов</a></li>
         <li><a href="/razdely/multiki-pro-rusalok.html">Мультики про Русалок</a></li>
         <li><a href="/razdely/multiki-pro-zombi.html">Мультики про Зомби</a></li>
         <li><a href="/razdely/multiki-pro-lyubov.html">Мультики про Любовь</a></li>
         <li><a href="/razdely/multiki-pro-drakonov.html">Мультики про Драконов</a></li>
         <li><a href="/razdely/multiki-pro-sobak.html">Мультики про Собак</a></li>
         <li><a href="/razdely/otechestvennye-multfilmi.html">Отечественные мультфильмы</a></li>
         <li><a href="/razdely/sovetskie-multfilmi.html">Советские Мультфильмы</a></li>
         <li><a href="/razdely/korotkometrajki.html">Короткометражки</a></li>
         <li><a href="/razdely/populyarnyie-multfilmyi.html">Популярные мультфильмы</a></li>
         <li><a href="/razdely/luchshie-i-populiarnie-sovetskie-multfilmi.html">Лучшие и популярные советские мультфильмы</a></li>
         <li><a href="/razdely/lutshie-multfilmi-2013.html">Лучшие мультфильмы 2013 года</a></li>
         <li><a href="/razdely/lutshie-multfilmi-2012.html">Лучшие мультфильмы 2012 года</a></li>
         <li><a href="/razdely/top-100.html">Tоп-100</a></li>
         <li><a href="/razdely/ozhidaemyie.html">Ожидаемые мультфильмы</a></li>
         <li><a href="/razdely/multfilmi-dlya-malchikov.html">Мультфильмы для Мальчиков</a></li>
         <li><a href="/razdely/multfilmi-dlya-devochek.html">Мультфильмы для Девочек</a></li>
         <li><a href="/razdely/multfilmi-pro-mashini.html">Мультфильмы про Машины</a></li>
         <li><a href="/razdely/multfilmi-pro-supergeroev.html">Мультфильмы про Супергероев</a></li>
         <li><a href="/razdely/multfilmi-pro-princess.html">Мультфильмы про Принцесс</a></li>
         <li><a href="/razdely/multfilmi-pro-pony-i-loshadei.html">Мультфильмы про Пони</a></li>
         <li><a href="/razdely/multfilmi-pro-bogatirei.html">Мультфильмы про Богатырей</a></li>
         <li><a href="/razdely/multfilmi-pro-monstrov.html">Мультфильмы про Монстров</a></li>
         <li><a href="/razdely/multfilmi-pro-angelov.html">Мультфильмы про Ангелов</a></li>
         <li><a href="/razdely/multfilmi-pro-robotov.html">Мультфильмы про Роботов</a></li>
         <li><a href="/razdely/multfilmi-pro-novii-god-i-rojdestvo.html">Мультфильмы про Новый Год и Рождество</a></li>
         <li><a href="/razdely/multfilmyi-pro-zhivotnyix.html">Мультфильмы про Животных</a></li>
         <li><a href="/razdely/multfilmi-pro-mikki-mausa.html">Мультфильмы про Микки Мауса и его друзей</a></li>
         <li><a href="/razdely/multiki-winx.html">Мультики Винкс</a></li>
         <li><a href="/razdely/avtorskie-multfilmi.html">Авторские Мультфильмы</a></li>
         <li><a href="/razdely/luchshie-i-populyarnyie-multiki-2014-goda.html">Лучшие и популярные мультики 2014 года</a></li>
         <li><a href="/">Развивающие Мультфильмы</a></li>
         <li><a href="/razdely/multfilmy-dlya-vzroslyh.html">Мультфильмы для взрослых</a></li>
         <li><a href="/razdely/multfilmy-pro-kung-fu-pandu.html">Мультфильмы про Кунг-фу панду</a></li>
         <li><a href="/razdely/multiki-pro-pokemonov.html">Мультики про Покемонов</a></li>
         <li><a href="/razdely/multiki-zvezdnye-voyny.html">Мультики Звездные войны</a></li>
         <li><a href="/razdely/multiki-lednikovyy-period.html">Мультики Ледниковый период</a></li>
         <li><a href="/razdely/multiki-pro-volkov.html">Мультики про волков</a></li>
         <li><a href="/razdely/multiki-pro-tanki.html">Мультики про танки</a></li>
         <li><a href="/razdely/multiki-mstiteli.html">Мультики Мстители</a></li>
         <li><a href="/razdely/multiki-pro-asteriksa-i-obeliksa.html">Мультики про Астерикса и Обеликса</a></li>
         <li><a href="/razdely/multiki-liga-spravedlivosti.html">Мультики Лига Справедливости</a></li>
         <li><a href="/razdely/multiki-hot-vils.html">Мультики Хот Вилс</a></li>
         <li><a href="/razdely/multiki-3d.html">Мультики 3D</a></li>
         <li><a href="/razdely/multfilmy-pro-nindzya.html">Мультики про Ниндзя</a></li>
         <li><a href="/razdely/multiki-zheleznyy-chelovek.html">Мультики Железный человек</a></li>
         <li><a href="/razdely/multiki-pro-lilo-i-sticha.html">Мультики про Лило и Стича</a></li>
         <li><a href="/razdely/multiki-fei.html">Мультики феи</a></li>
         <li><a href="/razdely/smeshnye-multiki.html">Смешные мультики</a></li>
         <li><a href="/razdely/zlye-multiki.html">Злые мультики</a></li>
         <li><a href="/razdely/multiki-rapuncel.html">Мультики Рапунцель</a></li>
         <li><a href="/razdely/multiki-pro-kamazy.html">Мультики про Камазы</a></li>
         <li><a href="/razdely/multiki-uolles-i-gromit.html">Мультики Уоллес и Громит</a></li>
         <li><a href="/razdely/multiki-pro-tachki.html">Мультики про Тачки</a></li>
         <li><a href="/razdely/multiki-timon-i-pumba.html">Мультики Тимон и Пумба</a></li>
         <li><a href="/razdely/multfilmy-poluchivshie-oskar.html">Мультфильмы получившие Оскар</a></li>
         <li><a href="/razdely/multiki-pro-belochek.html">Мультики про Белочек</a></li>
         <li><a href="/razdely/multiki-dlya-podrostkov.html">Мультики для Подростков</a></li>
         <li><a href="/razdely/multfilmy-fines-i-ferb.html">Мультфильмы Финес и Ферб</a></li>
         <li><a href="/razdely/multiki-sonik.html">Мультики Соник</a></li>
         <li><a href="/razdely/multiki-pro-pozharnyh.html">Мультики про пожарных</a></li>
         <li><a href="/razdely/volshebnye-multiki.html">Волшебные мультики</a></li>
         <li><a href="/razdely/strashnye-multiki.html">Страшные мультики</a></li>
      </ul>
   @elseif ($page == 'interesting')
      <ul>
         <li><a href="/tolko-na-smultru/smult-licnost-dnia.html">Смульт Личность Дня</a></li>
         <li><a href="/tolko-na-smultru/smult-chtivo.html">Смульт Чтиво</a></li>
         <li><a href="/tolko-na-smultru/smult-eksklyuziv.html">Смульт Эксклюзив</a></li>
         <li><a href="/tolko-na-smultru/smult-lichnost-sssr.html">Смульт Личность СССР</a></li>
         <li><a href="/tolko-na-smultru/smult-xudozhestva.html">Смульт Художества</a></li>
         <li><a href="/tolko-na-smultru/smult-intervyu.html">Смульт Интервью</a></li>
         <li><a href="/tolko-na-smultru/smult-obzor.html">Смульт Обзоры</a></li>
         <li><a href="/tolko-na-smultru/smult-vselennaya.html">Смульт Вселенная</a></li>
         <li><a href="/tolko-na-smultru/testi.html">Смульт Тесты</a></li>
         <li><a href="/tolko-na-smultru/oprosi.html">Смульт Опросы</a></li>
      </ul>
   @endif
   <div id="search-result" class="hide"></div>
</div>
<style>
   * {
      padding: 0;
      margin: 0;
   }
   li {
      list-style: none;
   }
   body {
      padding: 10px;
   }
   li {
      margin: 5px;
   }
</style>
@endsection