<aside class="sidebar">
    <div class="sdbBlock">
        <div class="clr">
            @guest
              <div id="reg-btn" class="reg-btn">Вход</div>
            @else
              <div id="reg-btn" class="reg-btn">Выход</div>
            @endguest  
            <div class="blue-btn reg-btn">
                <!-- Put this script tag to the <head> of your page -->
                <script type="text/javascript" src="https://vk.com/js/api/openapi.js?160"></script>

                <script type="text/javascript">
                  VK.init({apiId: 6797741, onlyWidgets: true});
                </script>

                <!-- Put this div tag to the place, where the Like block will be -->
                <div id="vk_like"></div>
                <script type="text/javascript">
                VK.Widgets.Like("vk_like", {type: "mini"});
                </script>
            </div>
    </div>
    </div>
    <div class="sdbBlock">
        <section class="rd-m clr">
            <h3 class="sp2">Смотрите мультфильмы</h3>
            @foreach ($multsToSidebar as $multToSidebar)
                <div class="mult-i">
                    <div class="text-on-i">
                        <a href="{{ $multToSidebar->alias }}.html">
                        <img src="/public/cache_image/multfilms/{{ $multToSidebar->alias }}/mainposter_200x267_2d1.jpg" alt="{{ $multToSidebar->title }}">
                        </a>
                        <div class="text btm">{{ $multToSidebar->pagetitle }}</div>
                    </div>
                </div>
            @endforeach
        </section>
    </div>
    <div class="sdbBlock">
        <section class="rd-a">
            <h3>Читайте статьи</h3>
            <ul>
                <?php $i = 0; ?>
                @foreach ($articlesToSidebar as $articleToSidebar)
                    @if ($i == 0)
                        <li class="item withImage">
                            <a href="{{ $articleToSidebar->uri }}">
                                <div class="image-wrap">
                                    @foreach ($articleToSidebar->kpxzxsitetmplvarcontentvalue as $value)
                                      @if ($value->tmplvarid == 142)
                                          <?php 
                                                  $image = str_replace('assets', 'public', $value->value);
                                                  if ($image[0] !== '/' && $image[0] !== ' ') {
                                                    $image = '/' . $image;
                                                  }
                                          ?>
                                        <img src="{{ $image }}" alt="{{ $articleToSidebar->pagetitle }}"></a>
                                      @endif
                                    @endforeach
                                    <span>{{ $articleToSidebar->description }}</span>
                                </div>
                            </a>
                        </li>
                    @else
                        <li class="item">
                            <a href="{{ $articleToSidebar->uri }}">
                            <span>{{ $articleToSidebar->longtitle }}</span>
                            </a>
                        </li> 
                    @endif
                    <?php $i++; ?>
                @endforeach
            </ul>
        </section>
    </div>
    <div class="soz-b clr">
        <a href="http://vk.com/smultru" rel="nofollow">
        <img src="/public/img/vklogo.png" alt="">
        </a>
        <a href="https://www.facebook.com/Smult.Ru" rel="nofollow">
        <img src="/public/img/fblogo.png" alt="">
        </a>
        <a href="https://twitter.com/SmultRu" rel="nofollow">
        <img src="/public/img/twlogo.png" alt="">
        </a>
    </div>
</aside> 