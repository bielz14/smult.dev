<?php

namespace App\Classes\Authorization;

use Dlnsk\HierarchicalRBAC\Authorization;


/**
 *  This is example of hierarchical RBAC authorization configiration.
 */

class AuthorizationClass extends Authorization
{
	public function getPermissions() {
		return [
			'managerPage' => [
					'description' => 'Looking manager index page',
				],
			'addMult' => [
					'description' => 'Adding cartoon',   
				],
			'addMultseries' => [
					'description' => 'Adding cartoon series',   
				],
			'addMulthero' => [
					'description' => 'Adding cartoon hero',   
				],
			'addQuote' => [
					'description' => 'Adding quote',   
				],
			'addSmultLive' => [
					'description' => 'Adding Smult Live',   
				],
			'addCompetition' => [
					'description' => 'Adding сompetition',   
				],
			'addNewtest' => [
					'description' => 'Adding new test',   
				],	
			'addNewpoll' => [
					'description' => 'Adding new poll',   
				],
			'addResource' => [
					'description' => 'Adding resource',   
				],
			'addWallpAndPoster' => [
					'description' => 'Adding wallpapers and posters',   
				],
			'addSeason' => [
					'description' => 'Adding season to cartoon',   
				],
			'addSerie' => [
					'description' => 'Adding serie to cartoon',   
				],
			/*'additionalparams' => [
					'description' => 'View additional params',   
				],*/
			'editadditionalparams' => [
					'description' => 'Editing additional params',   
				],
			'moderationComment' => [
					'description' => 'Moderation comment',   
				],
			'deleteComment' => [
					'description' => 'Deleting comment',   
				],
			'moderationTicket' => [
					'description' => 'Saving ticket',   
				],
			'deleteTicket' => [
					'description' => 'Deleting ticket',   
				],
			'reportOrder' => [
					'description' => 'Reporting order',   
				],
			'deleteOrder' => [
					'description' => 'Deleting order',   
				],
			'editResource' => [
					'description' => 'Editing resource',   
				],
			'deleteCompetition' => [
					'description' => 'Deleting competition',   
				],
			'editCompetition' => [
					'description' => 'Editing competition',   
				],
			'editSmultlive' => [
					'description' => 'Editing smultlive',   
				],
			'editMult' => [
					'description' => 'Editing mult',   
				],
			'editMultseries' => [
					'description' => 'Editing mult',   
				],
			'editSerie' => [
					'description' => 'Editing serie',   
			]
		];
	}

	public function getRoles() {
		return [
			'admin' => [
					'managerPage',
					'addMult',
					'addMultseries',
					'addMulthero',
					'addCompetition',
					'addQuote',
					'addSmultLive',
					'addNewPoll',
					'addNewTest',
					//'additionalparams',
					'editadditionalparams',
					'moderationComment',
					'deleteComment',
					'moderationTicket',
					'deleteTicket',
					'reportOrder',
					'deleteOrder',
					'addResource',
					'editResource',
					'deleteCompetition',
					'editCompetition',
					'editSmultlive',
					'editMult',
					'addWallpAndPoster',
					'editMultseries',
					'addSeason',
					'addSerie',
					'editSerie'
				],
			'manager' => [
					'managerPage',
					'addMult',
					'addMultseries',
					'addMulthero',
					'addCompetition',
					'addQuote',
					'addSmultLive',
					'addNewpoll',
					'addNewtest',
					//'additionalparams',
					'editadditionalparams',
					'moderationComment',
					'deleteComment',
					'moderationTicket',
					'deleteTicket',
					'reportOrder',
					'deleteOrder',
					'addResource',
					'editResource',
					'deleteCompetition',
					'editCompetition',
					'editSmultlive',
					'editMult',
					'addWallpAndPoster',
					'editMultseries',
					'addSeason',
					'addSerie',
					'editSerie'
				],
			'user' => [
					//права залогиненного юзера
				],
		];
	}

	/*public function getPermissions() {
		return [
			'editPost' => [
					'description' => 'Edit any posts',   // optional property
					'next' => 'editOwnPost',            // used for making chain (hierarchy) of permissions
				],
			'editOwnPost' => [
					'description' => 'Edit own post',
				],

			//---------------
			'deletePost' => [
					'description' => 'Delete any posts',
				],
		];
	}

	public function getRoles() {
		return [
			'manager' => [
					'editPost',
					'deletePost',
				],
			'user' => [
					'editOwnPost',
				],
		];
	}*/


	/**
	 * Methods which checking permissions.
	 * Methods should be present only if additional checking needs.
	 */

	/*public function editOwnPost($user, $post) {
		$post = $this->getModel(\App\Post::class, $post);  // helper method for geting model

		return $user->id === $post->user_id;
	}*/

}
