<?php

namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Mail\Email;
use App\Mail\Callback;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
 
class MailController extends Controller
{
    protected function validatorCallback(array $data)
    { 
        $messages = [
            'required' => 'Атрибут ":attribute" обязателен для заполнения.',
        ];

        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|email',
            'text' => 'required|string'
        ]);

        $niceNames = [
            'name' => '"Имя"',
            'email' => '"Email"',
            'text' => '"Текст"'
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    public function send(Request $request)
    {	 	
        $email = $request->input('email');
        $password = $request->input('password');

        $objMail = new \stdClass();
        $objMail->email = $email;
        $objMail->password = $password;
 
        Mail::to($email)->send(new Email($objMail));
    }

    public function sendCallback(Request $request)
    {	 	
        $this->validatorCallback($request->all())->validate();

    	$name = $request->input('password');
        $email = $request->input('email');
        $text = $request->input('text');

        $objMail = new \stdClass();
        $objMail->name = $name;
        $objMail->email = $email;
        $objMail->text = $text;
 		
        $siteEmail = 'slime1410@gmail.com';

        Mail::to($siteEmail)->send(new Callback($objMail));

        $request->session()->flash('message', 'Сообщение успешно отправлено!');
        return back();
    }
}
