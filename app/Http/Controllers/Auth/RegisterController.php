<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use GuzzleHttp\CLient;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    { 
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
        ]);
    }

    protected function create(array $data)
    {   
        if (!isset($data['email']) || !isset($data['password'])){
            return 0;
        }

        return User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => 'admin',
        ]);
    }

    public function register(Request $request)
    {   
        $this->validator($request->all())->validate();

        $password = str_random(8);
        $request->merge(['password' => $password]);

        if ($user = $this->create($request->all())) {
            event(new Registered($user));

            $this->guard()->login($user);

            $client = new \GuzzleHttp\CLient();
            $email = $request->input('email');
            $uri = url('/mail/send?email=' . $email . '&password=' . $password); 
            $client->get($uri);

            return 1;
        }

        return 0;
    }
}