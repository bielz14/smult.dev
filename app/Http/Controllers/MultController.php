<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\KpxzxSiteContent;
use App\Models\KpxzxComment;
use App\Models\KpxzxOrder;
use App\Models\KpxzxTicket;
use App\Models\KpxzxQuote;
use View;

class MultController extends Controller
{   
    public function __construct()
    {
        $multsTemplateId = 97;
        $multsToSidebar = [];

        $multsToSidebar = KpxzxSiteContent::limit(6000)->where('template', $multsTemplateId)
                                        ->get()
                                        ->random(2);

        $articlesTemplateId = 124;
        $articlesToSidebar = KpxzxSiteContent::limit(6000)->where('template', $articlesTemplateId)
                                    ->get()
                                    ->random(5);

       $quotes = KpxzxQuote::limit(3000)->get()->random(2);

        View::share('multsToSidebar', $multsToSidebar);
        View::share('articlesToSidebar', $articlesToSidebar);
        View::share('quotes', $quotes);
    }

    protected function validatorAddOrder(array $data)
    { 
        $messages = [
            'required' => 'Атрибут ":attribute" обязателен для заполнения.',
        ];

        $validator = Validator::make($data, [
            'order_name' => 'required|string|max:255',
            'order_email' => 'required|string|email|max:255',
            'order_text' => 'required|string'
        ]);

        $niceNames = [
            'order_name' => '"Имя"',
            'order_email' => '"Email"',
            'order_text' => '"Текст"'
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddComment(array $data)
    { 
        $messages = [
            'required' => 'Атрибут ":attribute" обязателен для заполнения.',
        ];

        $validator = Validator::make($data, [
            'email' => 'required|string|email|max:255',
        ]);

        $niceNames = [
            'email' => '"Email"'
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    public function mult(Request $request)
    {          
        $mult_alias = $request->mult;
        $mult = KpxzxSiteContent::where('alias', $mult_alias)->where('parent', 0)->get();
        $comments = [];
        $other_mults = [];
        if (count($mult)) {
            if ($mult[0]->getAttributes()['template'] == 97) {
                $multId = $mult[0]->getAttributes()['id'];
                $comments = KpxzxComment::where('content_id', $multId)->where('parent', 0)->get();
                $commentsAll = KpxzxComment::all();

                $intermediateСommentsArray = [];
                foreach ($comments as $comment) {
                    $subComments = KpxzxComment::where('parent', $comment->comment_id)->get();
                    if (count($subComments)) {
                        array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => $subComments]);
                    } else {
                        array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => null]);
                    }
                }
                $comments = $intermediateСommentsArray;

                $multsTemplateId = 97;
                $other_mults = KpxzxSiteContent::limit(6000)->where('template', $multsTemplateId)
                                                  ->where('alias', '!=', $mult_alias)
                                                  ->get()
                                                  ->random(4);//TOD: limit(6000) replace on all() would get()

                $trailersCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 102)->get(); 
                if (count($trailersCategory)) {
                    $trailersCategoryId = $trailersCategory->all()[0]['id'];
                } else {
                    $trailersCategoryId = NULL;
                }
                $wallpsAndPostersCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 109)->get();
                if (count($wallpsAndPostersCategory)) {
                    $wallpsAndPostersCategoryId = $wallpsAndPostersCategory->all()[0]['id'];
                } else {
                    $wallpsAndPostersCategoryId = NULL;
                }
                $interestingCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 116)->get();
                if (count($interestingCategory)) {  
                    $interestingCategoryId = $interestingCategory->all()[0]['id'];
                } else {
                    $interestingCategoryId = NULL;
                }
                $heroCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 113)->get();  
                if (count($heroCategory)) {  
                    $heroCategoryId = $heroCategory->all()[0]['id'];
                } else {
                    $heroCategoryId = NULL;
                }

                $siteTrailersCategoryId = 14054;
                $siteWallpsAndPostersCategoryId = 14055;
                $siteInterestingCategoryId = 14056;
                $siteHeroCategoryId = 14053;

                $otherSelfContent = KpxzxSiteContent::where('parent', $multId)//TOD: all()
                                        ->where('template', '!=', $multsTemplateId)
                                        ->get();
                                        
                return view('mult', [
                    'mult' => $mult,
                    'comments' => $comments,
                    'itemId' => $multId,
                    'trailersCategoryId' => $trailersCategoryId,
                    'siteTrailersCategoryId' => $siteTrailersCategoryId,
                    'wallpsAndPostersCategoryId' => $wallpsAndPostersCategoryId,
                    'siteWallpsAndPostersCategoryId' => $siteWallpsAndPostersCategoryId,
                    'interestingCategoryId' => $interestingCategoryId,
                    'siteInterestingCategoryId' => $siteInterestingCategoryId,
                    'heroCategoryId' => $heroCategoryId,
                    'siteHeroCategoryId' => $siteHeroCategoryId,
                    'otherSelfContent' => $otherSelfContent
                ])->with('other_mults', $other_mults); 
            } else if ($mult[0]->getAttributes()['template'] == 108) {
                $multId = $mult[0]->getAttributes()['id'];
                $multAlias = $mult[0]->getAttributes()['alias'];
                $seasonsTemplateId = 103;
                $seasons = KpxzxSiteContent::where('template', $seasonsTemplateId)
                                ->where('parent', $multId)->get();
                $seasonsSeries = [];
                $seriesTemplateId = 105;
                foreach ($seasons as $season) {
                    $seasonId = $season->id;
                    $seasonSeries = KpxzxSiteContent::where('template', $seriesTemplateId)
                                            ->where('parent', $seasonId)
                                            ->get();//TOD: all()
                    $seasonTitle = $season->pagetitle;     
                    $seasonIntrotext = $season->introtext;
                    $seasonContent = $season->content;
                    $seasonURI = $season->uri; 
                    $seasonPoster = '';
                    foreach ($season->kpxzxsitetmplvarcontentvalue as $value) {
                       if ($value->tmplvarid == 142) {
                            $seasonPoster = $value->value;
                            break;
                       }          
                    }          
                    array_push($seasonsSeries, [
                        'id' => $seasonId,
                        'title' => $seasonTitle,
                        'introtext' => $seasonIntrotext,
                        'content' => $seasonContent,
                        'uri' => $seasonURI,
                        'poster' => $seasonPoster,
                        'series' => $seasonSeries
                    ]);
                } 
                $comments = KpxzxComment::where('content_id', $multId)->where('parent', 0)->get();
                $commentsAll = KpxzxComment::all();

                $intermediateСommentsArray = [];
                foreach ($comments as $comment) {
                    $subComments = KpxzxComment::where('parent', $comment->comment_id)->get();
                    if (count($subComments)) {
                        array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => $subComments]);
                    } else {
                        array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => null]);
                    }
                }
                $comments = $intermediateСommentsArray;

                $multsTemplateId = 108;
                $other_multsesires = KpxzxSiteContent::limit(6000)->where('template', $multsTemplateId)
                                                  ->where('alias', '!=', $mult_alias)
                                                  ->get()
                                                  ->random(4);//TOD: limit(6000) replace on all() would get()

                $trailersCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 102)->get(); 
                if (count($trailersCategory)) { 
                    $trailersCategoryId = $trailersCategory->all()[0]['id'];
                } else {
                    $trailersCategoryId = NULL;
                }
                $wallpsAndPostersCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 109)->get();
                if (count($wallpsAndPostersCategory)) {
                    $wallpsAndPostersCategoryId = $wallpsAndPostersCategory->all()[0]['id'];
                } else {
                    $wallpsAndPostersCategoryId = NULL;
                }
                $interestingCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 116)->get();
                if (count($interestingCategory)) {  
                    $interestingCategoryId = $interestingCategory->all()[0]['id'];
                } else {
                    $interestingCategoryId = NULL;
                }
                $heroCategory = KpxzxSiteContent::where('parent', $multId)->where('template', 113)->get();  
                if (count($heroCategory)) {  
                    $heroCategoryId = $heroCategory->all()[0]['id'];
                } else {
                    $heroCategoryId = NULL;
                }

                $siteTrailersCategoryId = 14054;
                $siteWallpsAndPostersCategoryId = 14055;
                $siteInterestingCategoryId = 14056;
                $siteHeroCategoryId = 14053;

                $heroTemplateId = 106;       
                $otherSelfContent = KpxzxSiteContent::where('parent', $multId)//TOD: all()
                                                ->where('template', '!=', $multsTemplateId)
                                                ->where('template', '!=', $seasonsTemplateId)
                                                ->where('template', '!=', $seriesTemplateId)
                                                ->get();  

                return view('cartoon', [
                    'mult' => $mult,
                    'multAlias' => $multAlias,
                    'comments' => $comments,
                    'itemId' => $multId,
                    'trailersCategoryId' => $trailersCategoryId,
                    'siteTrailersCategoryId' => $siteTrailersCategoryId,
                    'wallpsAndPostersCategoryId' => $wallpsAndPostersCategoryId,
                    'siteWallpsAndPostersCategoryId' => $siteWallpsAndPostersCategoryId,
                    'interestingCategoryId' => $interestingCategoryId,
                    'siteInterestingCategoryId' => $siteInterestingCategoryId,
                    'heroCategoryId' => $heroCategoryId,
                    'siteHeroCategoryId' => $siteHeroCategoryId,
                    'seasonsSeries' => $seasonsSeries,
                    'otherSelfContent' => $otherSelfContent
                ])->with('other_multsesires', $other_multsesires); 
            }
        }

        return view('mult');
    }

    public function mults()
    {     
        $multsTemplateId = 97;
        $mults = KpxzxSiteContent::where('template', $multsTemplateId)
                        ->paginate(12);//TOD: all()

        return view('mults', ['mults' => $mults]);
    }

    public function multsCategory(Request $request)
    {   
        $categoryName = $request->category;
        $category = KpxzxSiteContent::where('alias', $categoryName)->get();
        if (count($category)) { 
            $categoryPagetitle = $category[0]->pagetitle;
            $categoryId = $category[0]['id'];
            $categoriesMultId = Array();
            $multsCategory = Array();
            $multsTemplateId = 97;
            $multseriesTemplateId = 108;
            $mults = KpxzxSiteContent::limit(4800)->where('template', $multsTemplateId)
                                ->orWhere('template', $multseriesTemplateId)
                                ->get();//TOD: limit(6000) replace on all()
            
            foreach ($mults as $mult) {
                foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                    if ($value->tmplvarid == 170) { 
                        $value->value = preg_replace('/^:|:$/', '', $value->value);
                        $categoriesMultIds = explode('::', $value->value);
                        if (in_array($categoryId, $categoriesMultIds)) {
                            array_push($multsCategory, $mult);
                            break; 
                        }
                    }
                }
            }  

            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $itemCollection = collect($multsCategory);
            $perPage = 12;
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            $paginatedMultsCategory = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            $paginatedMultsCategory->setPath($request->url());

            return view('mults', ['multsCategory' => $paginatedMultsCategory, 'categoryPagetitle' => $categoryPagetitle]);
        } else {
            return view('mults');
        }
    }

    public function multCategories(Request $request)
    {
        $multsTemplateId = 97;
        $mults = KpxzxSiteContent::where('template', $multsTemplateId)->get();
        $multsArray = [];                               
        foreach ($mults as $mult) {
            foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                if ($value->tmplvarid == 143) {
                    array_push($multsArray, ['id' => $mult->id, 'premiere_date' => $value->value]);
                }
            }
        }
        /*$collectionMult = collect($multsArray);        
        $sortMultsByPremiereDateInfo = $collectionMult->sortByDesc(function ($product, $key) {
            return $product['premiere_date'];
        });     
        $multsRentalCategory = [];
        foreach ($sortMultsByPremiereDateInfo as $mult) {
            $mult = KpxzxSiteContent::find($mult['id']);
            array_push($multCategories, $mult);
        }*/

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($multCategories);
        $perPage = 12;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedMultsCategories = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedMultsCategories->setPath($request->url());

        return view('mult_categories', ['multsCategories' => $paginatedMultsCategories]);
    }

    public function multsRental(Request $request)
    {
        $multsTemplateId = 97;
        $mults = KpxzxSiteContent::where('template', $multsTemplateId)->get();
        $multsArray = [];                               
        foreach ($mults as $mult) {
            foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                if ($value->tmplvarid == 143) {
                    array_push($multsArray, ['id' => $mult->id, 'premiere_date' => $value->value]);
                }
            }
        }
        $collectionMult = collect($multsArray);        
        $sortMultsByPremiereDateInfo = $collectionMult->sortByDesc(function ($product, $key) {
            return $product['premiere_date'];
        });     
        $multsRentalCategory = [];
        foreach ($sortMultsByPremiereDateInfo as $mult) {
            $mult = KpxzxSiteContent::find($mult['id']);
            array_push($multsRentalCategory, $mult);
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($multsRentalCategory);
        $perPage = 12;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedMultsRental = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedMultsRental->setPath($request->url());

        return view('mults_rental', ['mults' => $paginatedMultsRental]);
    }
    
    public function multseries()
    {          
        $multeriesTemplateId = 108;
        $multseries = KpxzxSiteContent::where('template', $multeriesTemplateId)
                            ->paginate(12);//TOD: all()
        
        return view('multseries', ['multseries' => $multseries]);
    }

    public function orders(Request $request) 
    {   
        $orders = KpxzxOrder::all();

        return view('orders', ['orders' => $orders]);
    }

    public function addComment(Request $request) 
    {   
        $this->validatorAddComment($request->all())->validate();

        if(\Auth::check()) {
            $userId = \Auth::user()->id;
        } else {
            $userId = null;
        }

        if (is_null($request->text)) {
            $text = '';
        } else {
            $text = $request->data['text'];
        }

        $data = [
            'content_id' => $request->content_id,
            'name' => $request->name,
            'email' => $request->email,
            'email' => $userId,
            'comment' => $text,
            'datetime' => time(),
            'img_id' => 1,
            'parent' => $request->parent_id,
            'is_moder' => 1
        ];

        KpxzxComment::create($data);

        return back();
    }

    public function addTicket(Request $request) 
    {   
        if (empty($request->data['text'])) {
            $text = '';
        } else {
            $text = $request->data['text'];
        }

        $data = [
            'type' => $request->data['ticketsType'],
            'contentid' => $request->data['content_id'],
            'text' => $text,
            'ready' => 1,
            'comment' => '',
            'date_poch' => time(),
            'date_kin' => 0
        ];
        
        KpxzxTicket::create($data);

        return '';
    }

    public function outTicketForm() 
    {   
        $formHTML = '<link rel="stylesheet" href="/public/css/tickets.css">
                     <script src="/public/js/tickets.js"></script>
                     <div class="mdl tckts" style="display: block;">
                        <div class="hdr clr">
                            <div class="title">Где ошибка</div>
                            <div class="btn close"></div>
                        </div>
                        <div class="cnt tickets clr">
                            <div class="left">
                                <div class="videotiket_video"><input type="radio" name="browser" value="video"> Не работает видео на
                                    странице
                                </div>
                                <div class="videotiket_content"><input type="radio" name="browser" value="content"> Ошибка в
                                    контенте
                                </div>
                                <div class="videotiket_anyerror"><input type="radio" name="browser" value="anyerror"> Другая ошибка
                                    на сайте
                                </div>
                            </div>
                            <div class="right">
                                <img src="/public/img/tickets/main.png" alt="">
                            </div>
                        </div>
                        <div class="ftr"></div>
                    </div>';

        return $formHTML;
    }

    public function addOrder(Request $request) 
    {   
        $this->validatorAddOrder($request->all())->validate();
        $orderTitle = $request->order_title;
        $currentOrderContent = KpxzxSiteContent::limit(1)
                                        ->where('alias', 'like', '%' . $orderTitle . '%')
                                        ->get();

        if (count($currentOrderContent)) {
            $uri = $currentOrderContent[0]->uri;
        } else {
            $uri = '';
        }

        $data = [
            'name' => $request->order_name,
            'title' => $orderTitle,
            'mail' => $request->order_email,
            'comment' => $request->order_text,
            'dateAdd' => time(),
            'dateExecution' => '00000000',
            'href' => $uri,
            'comment_completed' => '',
            'order_status' => 0,
            'postertype' => 'vert'
        ];

        $order = KpxzxOrder::create($data);

        return back();
    }

    public function reportOrder(Request $request)
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('reportOrder')) {
            $orderId = $request->order_id;
            $order = KpxzxOrder::find($orderId);

            $order->order_status = $request->order_status;
            $order->postertype = $request->order_postertype;
            $order->href = $request->order_href;
            if (is_null($order->href)) {
                $order->href = '';
            }
            $order->comment_completed = $request->order_comment_completed;
            if (is_null($order->comment_completed)) {
                $order->comment_completed = '';
            }
            
            $order->save();

            return [1, 'order_status' => $order->order_status];
        } else {
            return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function deleteOrder(Request $request)
    {
        if (true) {//if (!is_null($request->user()) && $request->user()->can('deleteOrder')) {  
            $orderId = $request->order_id;
            $order = KpxzxOrder::find($orderId);
            $order->delete();
            return 1;
        } else {
            return '<span>Права доступа отсутствуют!</span>';
        }
    }
}