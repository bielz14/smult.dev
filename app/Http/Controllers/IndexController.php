<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\KpxzxSiteContent;
use App\Models\KpxzxSmultlive;
use App\Models\KpxzxComment;
use App\Models\KpxzxQuote;
use App\Models\KpxzxMultPostersMusic;
use App\Classes\Translation;
use View;

class IndexController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $multsTemplateId = 97;
        $multsToSidebar = [];
        $multsToSidebar = KpxzxSiteContent::limit(6000)->where('template', $multsTemplateId)
                                        ->get()
                                        ->random(2);

        $articlesTemplateId = 124;
        $articlesToSidebar = KpxzxSiteContent::limit(6000)->where('template', $articlesTemplateId)
                                    ->get()
                                    ->random(5);

        $quotes = KpxzxQuote::limit(3000)->get()->random(2);

        View::share('multsToSidebar', $multsToSidebar);
        View::share('articlesToSidebar', $articlesToSidebar);
        View::share('quotes', $quotes);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $multsTemplateId = 97;
        $multsExpectedCategoryId = 14062;//id категории "ожидаемые"       
        $multsExpectedCategory = [];
        $mults = KpxzxSiteContent::limit(5000)->where('template', $multsTemplateId)->get();//TOD: limit(6000) replace on all()
        foreach ($mults as $mult) {
            foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                if ($value->tmplvarid == 170) {
                    $value->value = preg_replace('/^:|:$/', '', $value->value);
                    $categoriesMultIds = explode('::', $value->value);
                    if (in_array($multsExpectedCategoryId, $categoriesMultIds)) {
                        array_push($multsExpectedCategory, $mult);
                        break; 
                    }
                }
            }
            if (count($multsExpectedCategory) > 1) {
                break;
            }
        }
        if (!count($multsExpectedCategory)) {
            $multsExpectedCategory = null;
        }

        $multsPopularCategoryId = 13873;//id категории "популярные"
        $multsPopularCategory = [];
        //$mults = KpxzxSiteContent::where('template', $multsTemplateId)->get();//TOD: limit(6000) replace on all()
        foreach ($mults as $mult) {
            foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                if ($value->tmplvarid == 170) {
                    $value->value = preg_replace('/^:|:$/', '', $value->value);
                    $categoriesMultIds = explode('::', $value->value);
                    if (in_array($multsPopularCategoryId, $categoriesMultIds)) {
                        array_push($multsPopularCategory, $mult);
                        break; 
                    }
                }
            }
            if (count($multsPopularCategory) > 3) {
                break;
            }
        }
        if (!count($multsPopularCategory)) {
            $multsPopularCategory = null;
        }
 
        //$mults = KpxzxSiteContent::limit(6000)->where('template', $multsTemplateId)->get();
        $multsArray = [];                               
        foreach ($mults as $mult) {
            foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                if ($value->tmplvarid == 143) {
                    array_push($multsArray, ['id' => $mult->id, 'premiere_date' => $value->value]);
                }
            }
        }
        $collectionMult = collect($multsArray);        
        $sortMultsByPremiereDateInfo = $collectionMult->sortByDesc(function ($product, $key) {
            return $product['premiere_date'];
        });     
        $multsPremierDateInfo =  array_slice($sortMultsByPremiereDateInfo->all(), 0, 2);
        $multsRentalCategory = [];
        foreach ($multsPremierDateInfo as $multInfo) {
            $mult = KpxzxSiteContent::find($multInfo['id']);
            array_push($multsRentalCategory, $mult);
        }

        $articleTemplateId = 124;
        $articlePostId = 366;
        $posts = KpxzxSiteContent::limit(3)->where('template', $articleTemplateId)->where('parent', $articlePostId)
                                    ->orderBy('createdon', 'desc')
                                    ->get();
      
        if (!$posts) {
            $posts = null;
        }

        $trailersTemplateId = 102;
        $newTrailers = KpxzxSiteContent::limit(3)->where('template', $trailersTemplateId)
                                    ->orderBy('createdon', 'desc')
                                    ->get();

        $newMults = KpxzxSiteContent::limit(4)->where('template', $multsTemplateId)
                                    ->orderBy('createdon', 'desc')
                                    ->get();

        $newArticles = KpxzxSiteContent::limit(3)->where('template', $articleTemplateId)
                                    ->where('parent', '!=', $articlePostId)
                                    ->orderBy('createdon', 'desc')
                                    ->get();

        $seriesTemplateId = 105;
        $series = KpxzxSiteContent::limit(6000)->where('template', $seriesTemplateId)
                                ->orderBy('createdon', 'desc')
                                ->get()->uniqueStrict('parent')->take(4);
        $updatedSeriesMultIds = [];
        foreach ($series as $serie) {
            array_push($updatedSeriesMultIds, $serie->parent);
        }
        $multseriesUpdate = [];
        foreach ($updatedSeriesMultIds as $id) {
            $mult = KpxzxSiteContent::find($id);
            $multId = $mult->id;
            $multPagetitle = $mult->pagetitle;
            $multURI = $mult->uri;
            $multDescription = $mult->description;
            if ($mult->parent == 0) {
                foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                    if ($value->tmplvarid == 142) {
                        $multPoster = $value->value;
                    }
                }
            } else {
                $parentMult = KpxzxSiteContent::find($mult->parent);
                foreach ($parentMult->kpxzxsitetmplvarcontentvalue as $value) {
                    if ($value->tmplvarid == 142) {
                        $multPoster = $value->value;
                    }
                }
            }
            if ($mult->template == 103) {
                $multLable = '/public/img/upd_img/episode_icon_sm_fix.png';
                $type = 'season';
            } else if ($mult->template == 105) {
                $multLable = '/public/img/upd_img/season_icon_fix_sm.png';
                $type = 'series';
            } else {
                $multLable = NULL;
                $type = 'mult';
            }
           
            $multData = [
                'id' => $multId,
                'poster' => $multPoster,
                'title' => $multPagetitle,
                'uri' => $multURI,
                'description' => $multDescription,
                'type' => $type,
                'lable' => $multLable
            ];
            array_push($multseriesUpdate, $multData);
        }

        $data = [
            'mults_expected' => $multsExpectedCategory,
            'mults_rental' => $multsRentalCategory,
            'posts' => $posts,
            'mults_popular' => $multsPopularCategory,
            'new_trailers' => $newTrailers,
            'new_mults' => $newMults,
            'new_articles' => $newArticles,
            'multseries_updates' => $multseriesUpdate
        ];

        return view('index', ['data' => $data]);
    }

    public function posts()
    {
        $articleTemplateId = 124;
        $articlePostId = 366;
        $posts = KpxzxSiteContent::limit(6000)->where('template', $articleTemplateId)->where('parent', $articlePostId)
                            ->orderBy('createdon', 'desc')
                            ->paginate(12);//TOD: replace limit(6000) on all()
        $articleParent = KpxzxSiteContent::find($articlePostId);
        if (count($articleParent->all())) {
            $articleCategoryPagetitle = $articleParent->pagetitle;

            return view('articles', [
                'articlesCategory' => $posts, 'pageTitle' => $articleCategoryPagetitle
            ])->with(['resourceId' => $articlePostId]);
        } else {
            return view('articles');
        }                   
    }

    public function article(Request $request)
    {    
        $article_alias = $request->alias;
        $article = KpxzxSiteContent::where('alias', $article_alias)->get();
        if ((is_array($article) || is_object($article)) && count($article)) {
            $articleId = $article[0]->getAttributes()['id'];
            $comments = KpxzxComment::where('content_id', $articleId)->where('parent', 0)->get();
            $commentsAll = KpxzxComment::all();

            $intermediateСommentsArray = [];
            foreach ($comments as $comment) {
                $subComments = KpxzxComment::where('parent', $comment->comment_id)->get();
                if (count($subComments)) {
                    array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => $subComments]);
                } else {
                    array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => null]);
                }
            }
            $comments = $intermediateСommentsArray;

            $articleParentId = $article->all()[0]->parent;
            $articleParent = KpxzxSiteContent::find($articleParentId);
            if ((is_array($articleParent) || is_object($articleParent)) && count($articleParent)) {
                $articleCategoryPagetitle = $articleParent->pagetitle;
                if ($articleParent->alias != 'news') {
                    $articleCategoryURI = 'tolko-na-smultru/' . $articleParent->alias . '.html';
                } else {
                    $articleCategoryURI = $articleParent->alias . '.html';
                }
            } else {
                return view('articles');
            }
            $data = [
                'article' => $article, 
                'comments' => $comments, 
                'category' => [
                    'page_title' => $articleCategoryPagetitle,
                    'uri' => $articleCategoryURI
                ]
            ];
            $articleId = $article->all()[0]['id'];
            return view('article', $data)->with(['itemId' => $articleId, 'parent' => $articleParentId]);
        }

        return view('article');
    }

    public function articles()
    {
        $articleTemplateId = 124;

        $articleSmultReadingId = 2921;
        $smultReadingArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                                    ->where('parent', $articleSmultReadingId)
                                    ->orderBy('createdon', 'desc')
                                    ->get();

        $articlePostId = 366;
        $postArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                            ->where('parent', $articlePostId)
                            ->orderBy('createdon', 'desc')
                            ->get();

        $articleExclusiveId = 3071;
        $exclusiveArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                                ->where('parent', $articleExclusiveId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $articleReviewId = 13221;
        $reviewArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                            ->where('parent', $articleReviewId)
                            ->orderBy('createdon', 'desc')
                            ->get();

        $articleArtId = 13101;
        $artArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                            ->where('parent', $articleArtId)
                            ->orderBy('createdon', 'desc')
                            ->get();

        $articleUniverseId = 13220;
        $universeArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                                ->where('parent', $articleUniverseId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $articleIntervieweId = 13978;
        $interviewArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                                ->where('parent', $articleIntervieweId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $articlePersonDayId = 2912;
        $personDayArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                                ->where('parent', $articlePersonDayId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $articlePersonUSSRId = 2982;
        $personUSSRArticles = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                                ->where('parent', $articlePersonUSSRId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $testAndNewpollTemplateId = 144;
        $articleNewtestId = 23169;
        $newtestArticles = KpxzxSiteContent::limit(6)->where('template', $testAndNewpollTemplateId)
                                ->where('parent', $articleNewtestId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $articleNewpollId = 23170;
        $newpollArticles = KpxzxSiteContent::limit(6)->where('template', $testAndNewpollTemplateId)
                                ->where('parent', $articleNewpollId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $articlesCategories = [
            'smult_reading' => $smultReadingArticles,
            'posts' => $postArticles,
            'exclusive' => $exclusiveArticles,
            'reviews' => $reviewArticles,
            'arts' => $artArticles,
            'universe' => $universeArticles,
            'newtest' => $newtestArticles,
            'newpoll' => $newpollArticles,
            'interviews' => $interviewArticles,
            'person_day' => $personDayArticles,
            'person_ussr' => $personUSSRArticles
        ];

        return view('article_categories', ['articlesCategories' => $articlesCategories]);
    }

    public function articlesCategory(Request $request)
    {   
        $categoryName = $request->category;
        if ($categoryName != 'oprosi' && $categoryName != 'testi') {
            $articleParent = KpxzxSiteContent::where('alias', $categoryName)->get();
            if (isset($articleParent) && count($articleParent) && empty($articlesCategory)) { 
                $articleParentId = $articleParent->all()[0]->id;
                $articleInterestingId = 14056;
                $menuSecondItem = false;             
                if ($articleParentId != $articleInterestingId) {
                    $menuSecondItem = [
                        'uri' => '/tolko-na-smultru.html',
                        'pagetitle' => 'Самое интересное'
                    ];
                } else { 
                    $menuSecondItem = [
                        'uri' => '/na-sladkoe.html',
                        'pagetitle' => 'На сладкое'
                    ];
                }
                
                $articleCategoryPagetitle = $articleParent->all()[0]->pagetitle;
                $articleTemplateId = 124;
                $articlesCategory = KpxzxSiteContent::where('template', $articleTemplateId)//TOD: all()
                                        ->where('parent', $articleParentId)
                                        ->orderBy('createdon', 'desc')
                                        ->paginate(12);
                $parentId = $articleParentId;
            } 
        } else {
            $testAndNewpollTemplateId = 144;
            $menuSecondItem = [
                'uri' => '/tolko-na-smultru.html',
                'pagetitle' => 'Самое интересное'
            ];
            if ($categoryName == 'oprosi') {
                $parent = 23169;
                $articlesCategory = KpxzxSiteContent::where('template', $testAndNewpollTemplateId)
                                            ->where('parent', $parent)
                                            ->paginate(12);
                $articleCategoryPagetitle = 'Опросы';  
            } else if($categoryName == 'testi') {  
                $parent = 23170;
                $articlesCategory = KpxzxSiteContent::where('template', $testAndNewpollTemplateId)
                                            ->where('parent', $parent)
                                            ->paginate(12);
                $articleCategoryPagetitle = 'Тесты';                  
            }
            $parentId = $parent;
        }

        if (!empty($articlesCategory)) {              
            return view('articles', [
                'articlesCategory' => $articlesCategory,
                'pageTitle' => $articleCategoryPagetitle,
                'menuSecondItem' => $menuSecondItem
            ])->with(['resourceId' => $parentId]);
        } else {
            return view('articles');
        }
    }

    public function trailer(Request $request)
    {           
        $trailerAlias = $request->trailer;
        $multAlias = $request->mult;
        $mult = KpxzxSiteContent::where('alias', $multAlias)->get();
        $multPageTitle = $mult[0]->getAttributes()['pagetitle'];
        $comments = [];
        $trailersChildren = [];
        $otherTrailers = [];
        if (!is_null($trailerAlias)) {  
            $trailer = KpxzxSiteContent::where('alias', $trailerAlias)
                                    ->where('parent', '!=', 0)
                                    ->get();
            $trailerId = NULL;
            if (count($trailer)) {
                $trailerId = $trailer[0]->getAttributes()['id'];
                $comments = KpxzxComment::where('content_id', $trailerId)->where('parent', 0)->get();
                $commentsAll = KpxzxComment::all();

                $intermediateСommentsArray = [];
                foreach ($comments as $comment) {
                    $subComments = KpxzxComment::where('parent', $comment->comment_id)->get();
                    if (count($subComments)) {
                        array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => $subComments]);
                    } else {
                        array_push($intermediateСommentsArray, ['comment' => $comment, 'sub_comments' => null]);
                    }
                }
                $comments = $intermediateСommentsArray;
            }   
            $isParentTrailer = false;                           
        } else {   
            $trailer = $trailersChildren = KpxzxSiteContent::where('parent', $mult[0]->getAttributes()['id'])->get(); 
            if (count($trailer)) {
                $trailerId = $trailer[0]->getAttributes()['id'];
                $trailersChildren = KpxzxSiteContent::where('parent', $trailerId)->get();//TOD: all()
                $isParentTrailer = true;
            }  
        }

        $trailersTemplateId = 97;
        $otherTrailers = KpxzxSiteContent::limit(6000)->where('template', $trailersTemplateId)
                                    ->where('alias', '!=', $trailerAlias)
                                    ->get()
                                    ->random(4);//TOD: limit(6000) replace on all() would get()
        $otherSelfContent = KpxzxSiteContent::where('parent', $mult[0]->getAttributes()['id'])//TOD: all()
                                    ->where('template', '!=', $trailersTemplateId)
                                    ->get();
        $otherTrailersSelfCategory = KpxzxSiteContent::where('parent', $trailer->all()[0]->parent)
                                            ->where('id', '!=', $trailer->all()[0]->id)
                                            ->where('template', $trailersTemplateId)
                                            ->get();        

        $data = [
            'trailer' => $trailer, 
            'comments' => $comments, 
            'otherTrailers' => $otherTrailers,
            'trailersChildren' => $trailersChildren,
            'isParentTrailer' => $isParentTrailer,
            'mult' => [
                'alias' => $multAlias,
                'pagetitle' => $multPageTitle
            ],
        ];
      
        $category = KpxzxSiteContent::where('parent', $mult[0]->getAttributes()['id'])
                            ->where('alias', 'treilers')
                            ->get(); 
        if (count($category)) {
            $itemCategoryId = $category->all()[0]['id'];
        } else {
            $itemCategoryId = NULL;
        }
     
        return view('trailer', $data)->with([
            'itemId' => $trailerId,
            'itemCategoryId' => $itemCategoryId,
            'parent' => $mult[0]->getAttributes()['id'],
            'otherSelfContent' => $otherSelfContent,
            'otherTrailers' => $otherTrailers,
            'otherTrailersSelfCategory' => $otherTrailersSelfCategory
        ]);
    }

    public function trailers()
    {
        $trailersTemplateId = 102;
        $trailers = KpxzxSiteContent::limit(6000)->where('template', $trailersTemplateId)
                                ->orderBy('createdon', 'desc')
                                ->paginate(12);//TOD: replace limit(6000) on all()
        
        $parentId = 14054;                       
        return view('trailers', ['trailers' => $trailers])->with(['resourceId' => $parentId]);
    }

    public function wallpaperAndPosterCategories()
    {   
        $parentId = 14055;
        $wallpsAndPostersTemplateId = 109;
        $wallpsAndPosters = KpxzxSiteContent::where('template', $wallpsAndPostersTemplateId)//TOD: all()
                                            ->paginate(12); 
                                            
        return view('wallpaper_and_poster_categories', [
            'wallpsAndPosters' => $wallpsAndPosters
        ])->with(['resourceId' => $parentId]);
    }

    public function wallpapersAndPostersCategory(Request $request)
    {   
        $parentId = 14055;
        $wallpsAndPostersTemplateId = 109;
        $wallpsAndPostersCategory = KpxzxSiteContent::where('alias', $request->alias)
                                                ->where('template', $wallpsAndPostersTemplateId)
                                                ->get(); 
        if (isset($wallpsAndPostersCategory->all()[0])) {
            $wallpsAndPostersCategory = $wallpsAndPostersCategory->all()[0];
            $multId = $wallpsAndPostersCategory->parent;
            $mult = KpxzxSiteContent::find($multId);
            $multAlias = $mult->getAttributes()['alias'];
            $multPagetitle = $mult->getAttributes()['pagetitle'];
            $mult = [
                'alias' => $multAlias, 
                'pagetitle' => $multPagetitle
            ];
            $pagetitle = $wallpsAndPostersCategory->pagetitle;                                       
            $wallpsAndPostersDirectory = '';
            $tmplvarcontentvalues = $wallpsAndPostersCategory->kpxzxsitetmplvarcontentvalue;
            foreach ($tmplvarcontentvalues as $value) {
                if ($value->tmplvarid == 132) {
                    $wallpsAndPostersDirectory = 'public/cache_image/' . $value->value;
                }
            }  
            $files = scandir($wallpsAndPostersDirectory);
            $countFiles = count($files);
            $wallpsAndPosters = array_slice($files, 2, $countFiles);
            if (in_array('Posters', $wallpsAndPosters)) {
                $key = array_search('Posters', $wallpsAndPosters);
                unset($wallpsAndPosters[$key]);
            }
            if (in_array('Videos', $wallpsAndPosters)) {
                $key = array_search('Videos', $wallpsAndPosters);
                unset($wallpsAndPosters[$key]);
            }
            foreach ($wallpsAndPosters as $wallpAndPoster) {
                $key = array_search($wallpAndPoster, $wallpsAndPosters);
                $wallpAndPoster = $wallpsAndPostersDirectory . '/' . $wallpAndPoster;
                $wallpsAndPosters[$key] = $wallpAndPoster;
            } 

            $otherSelfContent = KpxzxSiteContent::where('parent', $multId)
                                            ->where('template', '!=', $wallpsAndPostersTemplateId)
                                            ->get();

            return view('wallpapers_and_posters', [
                'wallpsAndPosters' => $wallpsAndPosters,
                'otherSelfContent' => $otherSelfContent,
                'wallpsAndPostersCategory' => $wallpsAndPostersCategory
            ])->with(['pagetitle' => $pagetitle, 'mult' => $mult, 'resourceId' => $parentId]);
        }
                                       
        return view('wallpapers_and_posters')->with(['resourceId' => $parentId]);
    }

    public function interesting(Request $request)
    {  
        $interestingTemplateId = 114;//116
        $interesting = KpxzxSiteContent::where('template', $interestingTemplateId)
                                ->where('alias', $request->alias)
                                ->get();
        if (count($interesting) > 1) {
            $interesting = [$interesting[0]];
        }      

        if (count($interesting)) {
            $interestingId = $interesting->all()[0]['id'];
            $interestingParent = $interesting->all()[0]['parent'];
            $multAlias = $request->mult;
            //$multsTemplateId = 97;
            //$multseriesTemplateId = 108;
            $mult = KpxzxSiteContent::where('alias', $multAlias)
                                ->get(); 
            $multId = $mult->all()[0]->id;
            $multPagetitle = $mult->all()[0]->pagetitle;
            $otherSelfContent = KpxzxSiteContent::where('parent', $multId)
                                        ->where('template', '!=', $interestingTemplateId)
                                        ->get(); 

            $otherInterestingSelfCategory = KpxzxSiteContent::where('parent', $interesting->all()[0]->parent)
                                                    ->where('id', '!=', $interesting->all()[0]->id)
                                                    ->where('template', $interestingTemplateId)
                                                    ->get();
            $otherInterestings = KpxzxSiteContent::where('parent', '!=', $interesting->all()[0]->parent)
                                        ->where('template', $interestingTemplateId)
                                        ->take(3)
                                        ->get();  

            return view('interesting', [
                'interesting' => $interesting,
                'otherSelfContent' => $otherSelfContent,
                'otherInterestingSelfCategory' => $otherInterestingSelfCategory,
                'otherInterestings' => $otherInterestings,
                'multAlias' => $multAlias,
                'multPagetitle' => $multPagetitle,
                'multPagetitleOrigin' => Translation::rus2translit($multPagetitle)
            ])->with(['itemId' => $interestingId, 'parent' => $interestingParent]); 
        }
        return view('interesting', ['interesting' => []]);
    }

    public function interestings()
    {   
        $interestingTemplateId = 114;//116
        $interestings = KpxzxSiteContent::where('template', $interestingTemplateId)//TOD: all()
                                ->paginate(12);

        return view('interestings', ['interestings' => $interestings]);
    }

    public function interestingsCategory(Request $request)
    {   
        $interestingTemplateId = 114;//116
        $interesting = KpxzxSiteContent::where('template', $interestingTemplateId)
                                ->where('alias', $request->alias)
                                ->get();
        if (count($interesting) > 1) {
            $interesting = [$interesting[0]];
        }      

        if (count($interesting)) {
            $interestingId = $interesting->all()[0]['id'];
            $interestingParent = $interesting->all()[0]['parent'];
            $otherSelfContent = KpxzxSiteContent::where('parent', $multId)
                                        ->where('template', '!=', $heroTemplateId)
                                        ->get(); 
            return view('interesting', [
                'interesting' => $interesting,
                'otherSelfContent' => $otherSelfContent
            ])->with(['itemId' => $interestingId, 'parent' => $interestingParent]); 
        }
        return view('interesting', ['interesting' => []]);
    }

    public function soundtrack(Request $request)
    {
        $soundtracksTemplateId = 110;
        $multAlias = $request->mult;  
        $mult = KpxzxSiteContent::where('alias', $multAlias)->get();
        if (count($mult)) {
            $multId = $mult->all()[0]['id'];
            $multPagetitle = $mult->all()[0]['pagetitle'];
            $mult = [
                'alias' => $multAlias, 
                'pagetitle' => $multPagetitle
            ];
            $soundtrack = KpxzxSiteContent::where('template', $soundtracksTemplateId)
                                        ->where('parent', $multId)
                                        ->get();
            if (count($soundtrack)) {
                $soundtrackAlias = $soundtrack->all()[0]['alias'];
                $otherSoundtracks = KpxzxSiteContent::where('template', $soundtracksTemplateId)
                                                    ->where('alias', '!=', $soundtrackAlias)
                                                    ->get()
                                                    ->random(4);

                $soundtrackId = $soundtrack->all()[0]['id'];
                $soundtrackParent = $soundtrack->all()[0]['parent'];
                $tracks = KpxzxMultPostersMusic::where('multId', $multId)
                                    ->where('type', 2)
                                    ->get();

                return view('soundtrack', [
                    'soundtrack' => $soundtrack, 'tracks' => $tracks, 'otherSoundtracks' => $otherSoundtracks
                ])->with(['mult' => $mult, 'itemId' => $soundtrackId, 'parent' => $soundtrackParent]);
            }
        }
        return view('soundtrack');
    }

    public function soundtracks()
    {
        $soundtracksTemplateId = 110;
        $soundtracks = KpxzxSiteContent::limit(6000)->where('template', $soundtracksTemplateId)
                                ->orderBy('createdon', 'desc')
                                ->paginate(12);//TOD: replace limit(6000) on all()
        
        $parentId = 14057;                    
        return view('soundtracks', ['soundtracks' => $soundtracks])->with(['resourceId' => $parentId]);
    }

    public function multhero(Request $request)
    {
        $heroTemplateId = 106;
        $heroAlias = $request->alias;
        $hero = KpxzxSiteContent::where('template', $heroTemplateId)
                                ->where('alias', $heroAlias)
                                ->get();
        if (count($hero)) {
            $heroId = $hero->all()[0]['id'];
            $heroParent = $hero->all()[0]['parent'];
            $otherHeroes = KpxzxSiteContent::where('template', $heroTemplateId)
                                                ->where('alias', '!=', $heroAlias)
                                                ->get()
                                                ->random(4);
            $otherHeroesSelfCategory = [];
            $heroes = KpxzxSiteContent::where('template', $heroTemplateId)//TOD: all()
                                                ->get(); 
     
            preg_match('/Персонаж мультфильмов\s(.*)\s(.*)\s.*/', $hero->all()[0]['description'], $matches);
            if (isset($matches[1])) { 
                $multTitle = $matches[1]; 
                //TOD: переделать поиск мультгероев по имени мульта, которое находится описании героя
                foreach ($heroes as $value) {
                    preg_match('/.* \((.*)\)/', $value->pagetitle, $matches);
                    if (isset($matches[1])) {  
                        $heroMultTitle = $matches[1]; 
                        if (stristr($multTitle, $heroMultTitle)) { 
                            array_push($otherHeroesSelfCategory, $value);
                        }
                    }
                }
                //end TOD
                $mult = KpxzxSiteContent::limit(1)->where('pagetitle', 'like', '%' . $multTitle . '%')
                                        ->where('parent', 0)
                                        ->get();
                if (count($mult)) { 
                    $multId = $mult->all()[0]['id']; 
                    $multAlias = $mult->all()[0]['alias'];
                    $multPagetitle = $mult->all()[0]['pagetitle'];
                    $mult = [
                        'alias' => $multAlias, 
                        'pagetitle' => $multPagetitle
                    ];     
                    $otherSelfContent = KpxzxSiteContent::where('parent', $multId)//TOD: all()
                                                            ->where('template', '!=', $heroTemplateId)
                                                            ->get();      
                    return view('hero', [
                        'hero' => $hero, 
                        'otherSelfContent' => $otherSelfContent,
                        'otherHeroes' => $otherHeroes,
                        'otherHeroesSelfCategory' => $otherHeroesSelfCategory
                    ])->with(['mult' => $mult, 'itemId' => $heroId, 'parent' => $heroParent]);
                }
            } else {
                preg_match('/.*\((.*)\)/', $hero->all()[0]['pagetitle'], $matches);
                if (isset($matches[1]))  {
                    $multTitle = $matches[1];
                    $mult = KpxzxSiteContent::where('pagetitle', 'like', '%' . $multTitle . '%')->where('parent', 0)->get();
                    if (!count($mult)) {
                        preg_match('/(.*) \(.*\)/', $hero->all()[0]['pagetitle'], $matches);
                        if (isset($matches[1]))  { 
                            $multTitle = $matches[1];
                            $mult = KpxzxSiteContent::where('pagetitle', 'like', '%' . $multTitle . '%')
                                                ->where('parent', 0)->get();
                        }
                    }
                        
                    if (count($mult)) { 
                        $multId = $mult->all()[0]['id']; 
                        $multAlias = $mult->all()[0]['alias'];
                        $multPagetitle = $mult->all()[0]['pagetitle'];
                        $mult = [
                            'alias' => $multAlias, 
                            'pagetitle' => $multPagetitle
                        ];     
                        $otherSelfContent = KpxzxSiteContent::where('parent', $multId)//TOD: all()
                                                                    ->where('template', '!=', $heroTemplateId)
                                                                    ->get();                                           
                        return view('hero', [
                            'hero' => $hero, 
                            'otherSelfContent' => $otherSelfContent,
                            'otherHeroes' => $otherHeroes,
                            'otherHeroesSelfCategory' => $otherHeroesSelfCategory
                        ])->with(['mult' => $mult])->with(['itemId' => $heroId, 'parent' => $heroParent]);
                    }
                }
            }
            $mult = [
                'alias' => '#', 
                'pagetitle' => $multTitle
            ]; 
            return view('hero', ['hero' => $hero])->with(['itemId' => $heroId, 'parent' => $heroParent, 'mult' => $mult]);
        } else {
            return view('hero');
        }
    }

    public function multheroes()
    {
        $heroTemplateId = 106;
        $heroes = KpxzxSiteContent::limit(6000)->where('template', $heroTemplateId)//TOD: replace limit(6000) on all()
                                ->orderBy('createdon', 'desc')
                                ->paginate(12);

        $parentId = 14053;
        return view('heroes', ['heroes' => $heroes])->with(['isCaegoryMulthero' => false])->with(['resourceId' => $parentId]);
    }

    public function multheroesCategory(Request $request)
    {
        $heroAlias = $request->alias;
        $mult = KpxzxSiteContent::where('alias', $heroAlias)->get();
        $multId = $mult->all()[0]['id'];
        $multAlias = $mult->all()[0]['pagetitle'];
        $multPagetitle = $mult->all()[0]['pagetitle'];
        $mult = [
            'alias' => $multAlias, 
            'pagetitle' => $multPagetitle
        ];
        $heroTemplateId = 106;       
        $otherHeroes = KpxzxSiteContent::where('template', $heroTemplateId)
                                            ->where('alias', '!=', $heroAlias)
                                            ->get()
                                            ->random(4);
        $heroesCategory = [];
        $heroes = KpxzxSiteContent::where('template', $heroTemplateId)->get();
        foreach ($heroes as $hero) {
            preg_match('/.*\((.*)\)/', $hero->pagetitle, $matches);
            if (isset($matches[1]) && is_int(strripos($multAlias, $matches[1]))) {   
                $heroMultAlias = $matches[1];
                array_push($heroesCategory, $hero);
            }
        }

        if (count($heroesCategory)) {
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $itemCollection = collect($heroesCategory);
            $perPage = 12;
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            $heroesCategory = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            $heroesCategory->setPath($request->url());
            return view('heroes', [
                'heroes' => $heroesCategory,
                'otherHeroes' => $otherHeroes,
            ])->with(['mult' => $mult, 'isCaegoryMulthero' => true]);
        }

        $parentId = 14053;
        return view('heroes')->with(['mult' => $mult, 'isCaegoryMulthero' => true])->with(['resourceId' => $parentId]);                                 
    }

    public function competition(Request $request)
    {   
        $competitionAlias = $request->alias;
        $competitionTemplateId = 142;
        $competition = KpxzxSiteContent::where('template', $competitionTemplateId)
                                    ->where('alias', $request->alias)
                                    ->get();
        
        if (count($competition )) {
            $competitionId = $competition->all()[0]['id'];                           
            return view('competition', ['competition' => $competition])->with(['itemId' => $competitionId]);
        } 
        return view('competition');
    }

    public function competitions()
    {
        $competitionTemplateId = 142;
        $competitions = KpxzxSiteContent::limit(6000)//TOD all()
                                ->where('template', $competitionTemplateId)
                                ->paginate(12);

        $parentId = 22316;
        return view('competitions', ['competitions' => $competitions])->with(['resourceId' => $parentId]);
    }

    public function forWseetCategories()
    {
        $trailersTemplateId = 102;
        $trailers = KpxzxSiteContent::limit(6)->where('template', $trailersTemplateId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $wallpsAndPostersTemplateId = 109;
        $wallpsAndPosters = KpxzxSiteContent::limit(6)->where('template', $wallpsAndPostersTemplateId)//TOD: all()
                                ->get(); 

        $interestingCategoryItems = [];
        $categoryName = 'interesnyie-faktyi';
        $articleParent = KpxzxSiteContent::where('alias', $categoryName)->get();
        if (count($articleParent)) {
            $articleParentId = $articleParent->all()[0]->id;
            $articleCategoryPagetitle = $articleParent->all()[0]->pagetitle;
            $articleTemplateId = 124;
            $interestingCategoryItems = KpxzxSiteContent::limit(6)->where('template', $articleTemplateId)
                                ->where('parent', $articleParentId)
                                ->orderBy('createdon', 'desc')
                                ->get();
        } 

        $soundtracksTemplateId = 110;
        $soundtracks = KpxzxSiteContent::limit(6)->where('template', $soundtracksTemplateId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $heroTemplateId = 106;
        $heroes = KpxzxSiteContent::limit(6)->where('template', $heroTemplateId)
                                ->orderBy('createdon', 'desc')
                                ->get();

        $competitionTemplateId = 142;
        $competitions = KpxzxSiteContent::limit(6)
                                ->where('template', $competitionTemplateId)
                                ->get();

        $forSweetCategories = [
            'trailers' => $trailers,
            'wallpsAndPosters' => $wallpsAndPosters,
            'interestingCategoryItems' => $interestingCategoryItems,
            'soundtracks' => $soundtracks,
            'heroes' => $heroes,
            'competitions' => $competitions
        ];

        return view('forsweet_categories', ['forSweetCategories' => $forSweetCategories]);
    }

    public function studio(Request $request)
    {
        $studioAlias = $request->alias;
        $studio = KpxzxSiteContent::where('alias', $studioAlias)->get();
        $studioId = $studio->all()[0]->id;
        $studioTitle = $studio->all()[0]->pagetitle;
        $multsTemplateId = 97;
        $multsAndMultseries =  KpxzxSiteContent::where('template', $multsTemplateId)//TOD: all()
                                            ->get(); 
        $contents = [];
        foreach ($multsAndMultseries as $item) {
            foreach ($item->kpxzxsitetmplvarcontentvalue as $value) {
                if ($value->tmplvarid == 154) {
                    if ($studioId == $value->value) {
                        array_push($contents, $item);
                    }
                    break;
                }
            }
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($contents);
        $perPage = 12;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $contents = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $contents->setPath($request->url());

        return view('mults_studio', ['contents' => $contents])->with(['studioTitle' => $studioTitle]);
    }

    public function studiosCategory()
    {
        $studiosName = Array(
            'Walt Disney (Уолт Дисней)',
            'Pixar (Пиксар)',
            'Blue Sky Studios (Блу Скай)',
            'DreamWorks Animation (Дримворкс)',
            'Sony Pictures Animation (Сони Пикчерс)',
            'Illumination Entertainment (Иллюминейшен Интертеймент)',
            'Мельница',
            'Nickelodeon (Никелодеон)',
            'CGBros',
            'Прочие студии'
        );

        $studios = Array();
        foreach ($studiosName as $studioName) {
            $studio = KpxzxSiteContent::where('pagetitle', $studioName)->get();
            $studios = array_merge($studios, [
                ['id' => $studio[0]['id'], 'title' => $studioName, 'alias' => $studio[0]['alias']]
            ]);
        } 

        $multsTemplateId = 97;
        $multsAndMultseries =  KpxzxSiteContent::where('template', $multsTemplateId)//TOD: all()
                                            ->get(); 

        $contents = [];                                    
        foreach ($studios as $studio) { 
            foreach ($multsAndMultseries as $item) {
                foreach ($item->kpxzxsitetmplvarcontentvalue as $value) {
                    if ($value->tmplvarid == 154) {
                        if ($studio['id'] == $value->value) {
                            if (!array_key_exists($studio['id'], $contents)) {
                                $contents[$studio['id']] = [
                                    'alias' => $studio['alias'],
                                    'title' => $studio['title'],
                                    'items' => [$item]
                                ];
                            } else {
                                if (count($contents[$studio['id']]['items']) > 5) {
                                    break;
                                }
                                array_push($contents[$studio['id']]['items'], $item);
                            }
                        }
                        break;
                    }
                }
            }
        }
        
        return view('studio_categories', ['contents' => $contents]);
    }

    public function smultlive(Request $request)
    {
        $data = KpxzxSmultlive::get();//TOD: all()
        if (count($data)) {
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $itemCollection = collect($data);
            $perPage = 12;
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            $data = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            $data->setPath($request->url());
            return view('smultlive', ['data' => $data]);
        }
        return view('smultlive');
    }

    public function search(Request $request)
    {   
        $data = $request->data;
        $multsTemplateId = 97;
        $multseriesTemplateId = 108;
        $content =  KpxzxSiteContent::where('template', $multsTemplateId)
                                //->where('template', $multseriesTemplateId)
                                ->where('pagetitle', 'like', '%' . $data . '%')//TOD: all()
                                ->get(); 
        $result = [];
        $yearRelise = null;
        if (count($content) > 1) { 
            $i = 0;            
            foreach ($content as $item) {
                $partTitleContentName = preg_replace('/' . $content[$i]['pagetitle'] . ' /iu', '', $content->all()[$i]['pagetitle']);
                $items = [];
                array_push($items, [
                    'title' => $content->all()[$i]['pagetitle'],
                    'origin_title' => Translation::rus2translit($partTitleContentName),
                    'uri' => $content->all()[$i]['uri']
                ]);

                foreach ($content[$i]->kpxzxsitetmplvarcontentvalue as $value2) {
                    if ($value2->tmplvarid == 143 && count($items[0])) {
                        $yearRelise = $value2->value;
                        $items[0] = array_merge($items[0], ['year_relise' => $value2->value]);
                        break;
                    }
                }
                if ($i == 0) {
                    array_push($result, [
                        'category_title' => 'Мультфильмы',
                        'uri' => '/',
                        'items' => $items
                    ]); 
                } else {
                    $result[0]['items'] = array_merge($result[0]['items'], $items);
                }
                array_shift($items);
                $contentId = $content->all()[$i]['id'];
                $resultContent = KpxzxSiteContent::where('parent', $contentId)//TOD: all()
                                            ->get();
                $y = 1;
                foreach ($resultContent as $value) {  
                        $itemsContent = KpxzxSiteContent::where('parent', $value->id)//TOD: all()
                                                    ->get();
                        $items = []; 
                        foreach ($itemsContent as $item) {
                            if ($i == 0) {
                                array_push($items, [
                                    'title' => $item->pagetitle,
                                    'uri' => $item->uri
                                ]);
                            } else { 
                                $item = [
                                    'title' => $item->pagetitle,
                                    'uri' => $item->uri
                                ];
                                array_push($result[$y]['items'], $item);
                            }
                        } 
                        $partTitleCategory = preg_replace('/' . $content[$i]['pagetitle'] . ' /iu', '', $value->pagetitle);
                        $partTitleContentName = preg_replace('/' . ' ' . $partTitleCategory . '/iu', '', $value->pagetitle);

                        array_push($result, [
                            'category_title' => $partTitleCategory,
                            'uri' => $value->uri,
                            'items' => $items
                        ]);
                    $y++;
                }
                $i++;
            }
            
            return $result;
        } else if (count($content)) {
            $partTitleContentName = preg_replace('/' . $content[$i]['pagetitle'] . ' /iu', '', $content->all()[0]['pagetitle']);
            $items = [];
            array_push($items, [
                'title' => $content->all()[0]['pagetitle'],
                'origin_title' => Translation::rus2translit($partTitleContentName),
                'uri' => $content->all()[0]['uri']
            ]);

            foreach ($content[0]->kpxzxsitetmplvarcontentvalue as $value2) {
                if ($value2->tmplvarid == 143 && count($items[0])) {
                    $yearRelise = $value2->value;
                    $items[0] = array_merge($items[0], ['year_relise' => $value2->value]);
                    break;
                }
            }
            array_push($result, [
                'category_title' => 'Мультфильмы',
                'uri' => '/',
                'items' => $items
            ]); 
            array_shift($items);
            $contentId = $content->all()[0]['id'];
            $resultContent = KpxzxSiteContent::where('parent', $contentId)//TOD: all()
                                        ->get();
            foreach ($resultContent as $value) {
                    $itemsContent = KpxzxSiteContent::where('parent', $value->id)//TOD: all()
                                                ->get();
                    $items = [];
                    foreach ($itemsContent as $item) {
                        array_push($items, [
                            'title' => $item->pagetitle,
                            'uri' => $item->uri
                        ]);
                    } 
                    $partTitleCategory = preg_replace('/' . $content[$i]['pagetitle'] . ' /iu', '', $value->pagetitle);
                    $partTitleContentName = preg_replace('/' . ' ' . $partTitleCategory . '/iu', '', $value->pagetitle);
                    array_push($result, [
                        'category_title' => $partTitleCategory,
                        'uri' => $value->uri,
                        'items' => $items
                    ]);
            } 

            return $result;
        }
    }

    public function sitemap(Request $request)
    {   
        $page = $request->page;
        if (!is_null($page)) {
            return view('sitemap', ['page' => $page]);
        } else {
            return view('sitemap');
        }   
    }

    public function about()
    {  
        $multsTemplateId = 97;
        $trailersTemplateId = 102;
        $articleTemplateId = 124;
        $mults = KpxzxSiteContent::where('template', $multsTemplateId)->get();//TOD: all()
        $trailers = KpxzxSiteContent::where('template', $trailersTemplateId)->get();//TOD: all()
        $articles = KpxzxSiteContent::where('template', $articleTemplateId)->get();//TOD: all()
        $multsCount = count($mults);
        $trailersCount = count($trailers);
        $articlesCount = count($articles);

        $quotes = KpxzxQuote::take(10)->get();
        if (count($quotes)) {
            return view('about', [
                'quotes' => $quotes, 
                'multsCount' => $multsCount,
                'trailersCount' => $trailersCount,
                'articlesCount' => $articlesCount
            ]);
        } else {
            return view('about', [ 
                'multsCount' => $multsCount,
                'trailersCount' => $trailersCount,
                'articlesCount' => $articlesCount
            ]);
        }            
    }

    public function callback()
    {   
        return view('callback');
    }
}
    