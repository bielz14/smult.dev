<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\KpxzxSiteContent;
use App\Models\KpxzxSiteTmplvarContentvalue;
use App\Models\KpxzxSiteTmplvar;
use App\Models\KpxzxComment;
use App\Models\KpxzxOrder;
use App\Models\KpxzxTicket;
use App\Models\KpxzxQuote;
use App\Models\KpxzxSmultlive;
use App\Models\KpxzxWinner;
use GuzzleHttp\CLient;

class ManagerController extends Controller
{   
    protected function validatorAddMult(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'url_full_mult' => 'required|string',
            'description' => 'required|string',
            'alias' => 'required|string|max:255',
            'mult_year' => 'required|max:255',
            'mult_released' => 'required|string|max:255',
            //'slogan' => 'required|string|max:255',
            //'mult_quality' => 'required|string|max:255',
            //'mult_director' => 'required|string|max:255',
            'studios' => 'required',    
            'agelimit' => 'required|max:255',
            'original_title' => 'required|string|max:255',
            'istok' => 'required|string',
            'typeposter' => 'required',
            'date_of_premiere' => 'required|date',
            'notranslate' => 'required',
            'nooriginal' => 'required',
            'editor1' => 'required'
        ]);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'editor1' => '"Контент"',
            'description' => '"Описание для материалов поисковиков"',
            'mult_year' => '"Год выпуска мультфильма"',
            'mult_released' => '"Выпустившая страна"',
            //'slogan' => '"Слоган"',
            //'mult_quality' => '"Качество ролика на сайте"',
            //'mult_director' => '"Режиссер"',
            'studios' => '"Студии"',    
            'agelimit' => '"Возрастное ограничение"',
            'original_title' => '"Оригинальное название"',
            'typeposter' => '"Тип постера"',
            'date_of_premiere' => 'Дата премьеры',
            //'custom_button' => '"Текст кастомной кнопки"',
            'notranslate' => '"Нет русской озвучки"',
            'nooriginal' => '"Нет русского официального названия"',
            'editor1' => '"Контент"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddMultseries(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'url_full_mult' => 'required|string',
            'description' => 'required|string',
            'alias' => 'required|string|max:255',
            'mult_year' => 'required|max:255',
            'mult_released' => 'required|string|max:255',
            //'slogan' => 'required|string|max:255',
            //'mult_quality' => 'required|string|max:255',
            //'mult_director' => 'required|string|max:255',
            'studios' => 'required',    
            'agelimit' => 'required|max:255',
            'original_title' => 'required|string|max:255',
            'istok' => 'required|string',
            'typeposter' => 'required',
            'date_of_premiere' => 'required|date',
            //'custom_button' => 'required|string|max:255',
            'notranslate' => 'required',
            'nooriginal' => 'required',
            'editor1' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'editor1' => '"Контент"',
            'description' => '"Описание для материалов поисковиков"',
            'mult_year' => '"Год выпуска мультфильма"',
            'mult_released' => '"Выпустившая страна"',
            //'slogan' => '"Слоган"',
            //'mult_quality' => '"Качество ролика на сайте"',
            //'mult_director' => '"Режиссер"',
            'studios' => '"Студии"',    
            'agelimit' => '"Возрастное ограничение"',
            'original_title' => '"Оригинальное название"',
            'typeposter' => '"Тип постера"',
            'date_of_premiere' => 'Дата премьеры',
            //'custom_button' => '"Текст кастомной кнопки"',
            'notranslate' => '"Нет русской озвучки"',
            'nooriginal' => '"Нет русского официального названия"',
            'editor1' => '"Контент"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddMulthero(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'istok' => 'required',
            'description' => 'required|string|max:255',
            'editor1' => 'required',
            //'custom_button' => 'required|string|max:255'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'istok' => '"Ссылка на пост в вк"',
            'description' => '"Описание для материалов поисковиков"',
            'editor1' => '"Контент"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddQuote(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'hero' => 'required',
            'hero_uri' => 'required|string',
            'mult' => 'required',
            'mult_uri' => 'required|string',
            'editor1' => 'required|string'
        ], $messages);

        $niceNames = [
            'hero' => '"Герой"',
            'hero_uri' => '"Ссылка на героя"',
            'mult' => '"Мультфильм по цитате"',
            'mult_uri' => '"Ссылка на мультфильм"',
            'editor1' => '"Контент"',
            'main_file' => '"Постер"'
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddSmultLive(array $data)
    {   
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'first_title_vertical' => 'required|string',
            'first_title_align' => 'required|string',
            'second_title_vertical' => 'required|string',
            'second_title_align' => 'required|string',
            'first_title' => 'required|string',
            'second_title' => 'required|string',
            'multId' => 'required',
        ], $messages);

        $niceNames = [
            'first_title_vertical' => '"Расположение заголовка"',
            'first_title_align' => '"Расположение заголовка по горизонтали"',
            'second_title_vertical' => '"Расположение заголовка2"',
            'second_title_align' => '"Расположение заголовка по горизонтали2"',
            'first_title' => '"Название"',
            'second_title' => '"Описание для материалов поисковиков"',
            'multId' => '"item_id"',
            'main_file' => 'Картинка'
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddNewtest(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'editor1' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'editor1' => '"Контент"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddNewpoll(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'editor1' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'editor1' => '"Контент"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddCompetition(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'fond' => 'required',
            'comp_start' => 'required',
            'comp_end' => 'required',
            'url_full_mult' => 'required',
            'istok' => 'required',
            'editor1' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'fond' => '"Призовой фонд"',
            'comp_start' => '"Дата начала конкурса"',
            'comp_end' => '"Дата конца конкурса"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'editor1' => '"Контент"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddResource(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'wide_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'url_full_mult' => 'required',
            'istok' => 'required',
            'editor1' => 'required',
            'description' => 'required',
            //'custom_button' => 'required',
            'parent' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'editor1' => '"Контент"',
            'description' => '"Описание для материалов поисковиков"',
            'parent' => '"parent_id"',
            'main_file' => '"Постер"',
            'wide_file' => '"Широкая картинка"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddSeason(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'url_full_mult' => 'required',
            'istok' => 'required',
            'content' => 'required',
            'description' => 'required',
            'notranslate' => 'required',
            //'custom_button' => 'required',
            'itemId' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'content' => '"Контент"',
            'description' => '"Описание для материалов поисковиков"',
            'notranslate' => '"Нет русской озвучки"',
            'itemId' => '"item_id"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorAddSerie(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'main_file' => 'required|mimes:jpeg,bmp,png',
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'url_full_mult' => 'required',
            'istok' => 'required',
            'content' => 'required',
            'description' => 'required',
            'notranslate' => 'required',
            //'custom_button' => 'required',
            'itemId' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'content' => '"Контент"',
            'description' => '"Описание для материалов поисковиков"',
            'notranslate' => '"Нет русской озвучки"',
            'itemId' => '"item_id"',
            'main_file' => '"Постер"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorEditMult(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'pagetitle' => 'required|string|max:255',
            'url_full_mult' => 'required|string',
            'description' => 'required|string',
            'alias' => 'required|string|max:255',
            'mult_year' => 'required|max:255',
            'mult_released' => 'required|string|max:255',
            //'slogan' => 'required|string|max:255',
            //'mult_quality' => 'required|string|max:255',
            //'mult_director' => 'required|string|max:255',
            'studios' => 'required',    
            'agelimit' => 'required|max:255',
            'original_title' => 'required|string|max:255',
            'istok' => 'required|string',
            'typeposter' => 'required',
            'date_of_premiere' => 'required|date',
            //'custom_button' => 'required|string|max:255',
            'notranslate' => 'required',
            'nooriginal' => 'required',
            'editor1' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'editor1' => '"Контент"',
            'description' => '"Описание для материалов поисковиков"',
            'mult_year' => '"Год выпуска мультфильма"',
            'mult_released' => '"Выпустившая страна"',
            //'slogan' => '"Слоган"',
            //'mult_quality' => '"Качество ролика на сайте"',
            //'mult_director' => '"Режиссер"',
            'studios' => '"Студии"',    
            'agelimit' => '"Возрастное ограничение"',
            'original_title' => '"Оригинальное название"',
            'typeposter' => '"Тип постера"',
            'date_of_premiere' => 'Дата премьеры',
            //'custom_button' => '"Текст кастомной кнопки"',
            'notranslate' => '"Нет русской озвучки"',
            'nooriginal' => '"Нет русского официального названия"',
            'editor1' => '"Контент"'
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorEditResource(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'url_full_mult' => 'required',
            'istok' => 'required',
            'editor1' => 'required',
            'description' => 'required',
            'parent' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'editor1' => '"Контент"',
            'description' => '"Описание для материалов поисковиков"',
            'parent' => '"parent_id"'
        ];

        $validator->setAttributeNames($niceNames); 

        return $validator;
    }

    protected function validatorEditCompetition(array $data)
    {
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'pagetitle' => 'required|string|max:255',
            'alias' => 'required|string|max:255',
            'fond' => 'required',
            'comp_start' => 'required',
            'comp_end' => 'required',
            'url_full_mult' => 'required',
            'istok' => 'required',
            'editor1' => 'required'
        ], $messages);

        $niceNames = [
            'pagetitle' => '"Заголовок"',
            'alias' => '"Url"',
            'fond' => '"Призовой фонд"',
            'comp_start' => '"Дата начала конкурса"',
            'comp_end' => '"Дата конца конкурса"',
            'url_full_mult' => '"Ссылка на видео"',
            'istok' => '"Ссылка на пост в вк"',
            'editor1' => '"Контент"'
        ];

        $validator->setAttributeNames($niceNames); 

        return $validator;
    }

    protected function validatorEditSmultLive(array $data)
    {   
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'first_title_vertical' => 'required|string',
            'first_title_align' => 'required|string',
            'second_title_vertical' => 'required|string',
            'second_title_align' => 'required|string',
            'first_title' => 'required|string',
            'second_title' => 'required|string',
            'multId' => 'required'
        ], $messages);

        $validator->setAttributeNames($niceNames);

        $niceNames = [
            'first_title_vertical' => '"Расположение заголовка"',
            'first_title_align' => '"Расположение заголовка по горизонтали"',
            'second_title_vertical' => '"Расположение заголовка2"',
            'second_title_align' => '"Расположение заголовка по горизонтали2"',
            'first_title' => '"Название"',
            'second_title' => '"Описание для материалов поисковиков"',
            'multId' => '"item_id"'
        ];

        return $validator;
    }

    /*public function main()
    {   
        echo '<pre>'; 
        //$ids = Array(132, 137, 138, 147, 189, 142, 143, 144, 145, 148, 149, 193, 151, 190, 154, 155, 156, 162, 163, 170, 171, 173, 174, 175, 179, 180, 181, 182, 183, 184, 185, 187, 194, 195, 196, 197);//мульфильмы
        //$ids = Array(162, 142, 163, 151);//герой
        $ids = Array(184, 185, 183, 142, 163);//конкурс
        //$ids = Array(186);//тест
        foreach ($ids as $id) {
            $obj = KpxzxSiteTmplvar::find($id);
            echo '     <span style="color: blue">' . $obj->caption . ':</span><br>';
            foreach ($obj->kpxzxsitetmplvarcontentvalue as $value2) {
                echo 'id: <span style="color: green">' . $value2->id . '</span><br>';
                echo 'tmplvarid: <span style="color: green">' . $value2->tmplvarid . '</span><br>';
                echo 'contentid: <span style="color: green">' . $value2->contentid . '</span><br>';
                echo 'value: <span style="color: green">' . $value2->value . '</span><br><br>';
                break;
            }
        }
        exit;

        return view('manager.main');
    }*/

    public function index()
    {   
        $multsTemplateId = 97;
        $mults = KpxzxSiteContent::orderBy('createdon', 'desc')->take(10)
                            ->where('template', $multsTemplateId)
                            ->get();

        return view('manager.index', ['mults' => $mults]);
    }

    public function mult(Request $request)
    {  
        $itemId = $request->item_id;
        $item = [];

        if (!is_null($itemId)) {
            $item = KpxzxSiteContent::find($itemId);
        }

        $studiosName = Array(
            'Walt Disney (Уолт Дисней)',
            'Pixar (Пиксар)',
            'Blue Sky Studios (Блу Скай)',
            'DreamWorks Animation (Дримворкс)',
            'Sony Pictures Animation (Сони Пикчерс)',
            'Illumination Entertainment (Иллюминейшен Интертеймент)',
            'Мельница',
            'Nickelodeon (Никелодеон)',
            'CGBros',
            'Прочие студии'
        );

        $studios = Array();
        foreach ($studiosName as $studioName) {
            $studio = KpxzxSiteContent::where('pagetitle', $studioName)->get();
            $studios = array_merge($studios, array(array('id' => $studio[0]['id'], 'title' => $studioName)));
        }            
        $categery_mult_template_id = 121;
        $categoriesAndStudios = KpxzxSiteContent::where('template', $categery_mult_template_id )->get();//TOD: all()
        $categories = Array();
        foreach ($categoriesAndStudios as $categoryAndStudio) {
            $isStudio = false;
            foreach ($studios as $studio) {
                if ($categoryAndStudio->id == $studio['id']) {
                    $isStudio = true;
                    break;
                }
            }

            $exclude_category_id = 2434;//id раздела, который не нужно выводить
            if ($isStudio || $categoryAndStudio->id === $exclude_category_id) {
                continue;
            }

            $category = $categoryAndStudio;
            array_push($categories, $category);
        }   
        
        if ((is_array($item) || is_object($item)) && !is_null($item) && count($item)) {
            return view('manager.mult', ['studios' => $studios, 'categories' => $categories, 'item' => $item]);
        } else {
            return view('manager.mult', ['studios' => $studios, 'categories' => $categories]);
        }
    }

    public function multseries(Request $request)
    {  
        $itemId = $request->item_id;
        $item = [];

        if (!is_null($itemId)) {
            $item = KpxzxSiteContent::find($itemId);
        }

        $studiosName = Array(
            'Walt Disney (Уолт Дисней)',
            'Pixar (Пиксар)',
            'Blue Sky Studios (Блу Скай)',
            'DreamWorks Animation (Дримворкс)',
            'Sony Pictures Animation (Сони Пикчерс)',
            'Illumination Entertainment (Иллюминейшен Интертеймент)',
            'Мельница',
            'Nickelodeon (Никелодеон)',
            'CGBros',
            'Прочие студии'
        );

        $studios = Array();
        foreach ($studiosName as $studioName) {
            $studio = KpxzxSiteContent::where('pagetitle', $studioName)->get();
            $studios = array_merge($studios, array(array('id' => $studio[0]['id'], 'title' => $studioName)));
        }            
        $categery_mult_template_id = 121;
        $categoriesAndStudios = KpxzxSiteContent::where('template', $categery_mult_template_id )->get();//TOD: all()
        $categories = Array();
        foreach ($categoriesAndStudios as $categoryAndStudio) {
            $isStudio = false;
            foreach ($studios as $studio) {
                if ($categoryAndStudio->id == $studio['id']) {
                    $isStudio = true;
                    break;
                }
            }

            $exclude_category_id = 2434;//id раздела, который не нужно выводить
            if ($isStudio || $categoryAndStudio->id === $exclude_category_id) {
                continue;
            }

            $category = $categoryAndStudio;
            array_push($categories, $category);
        }   
        
        if ((is_array($item) || is_object($item)) && !is_null($item) && count($item)) {
            return view('manager.mult', ['studios' => $studios, 'categories' => $categories, 'item' => $item]);
        } else {
            return view('manager.mult', ['studios' => $studios, 'categories' => $categories]);
        }
    }

    /*public function multseries()
    {  
        $studiosName = Array(
            'Walt Disney (Уолт Дисней)',
            'Pixar (Пиксар)',
            'Blue Sky Studios (Блу Скай)',
            'DreamWorks Animation (Дримворкс)',
            'Sony Pictures Animation (Сони Пикчерс)',
            'Illumination Entertainment (Иллюминейшен Интертеймент)',
            'Мельница',
            'Nickelodeon (Никелодеон)',
            'CGBros',
            'Прочие студии'
        );

        $studios = Array();
        foreach ($studiosName as $studioName) {
            $studio = KpxzxSiteContent::where('pagetitle', $studioName)->get();
            $studios = array_merge($studios, array(array('id' => $studio[0]['id'], 'title' => $studioName)));
        }            
        $categery_mult_template_id = 121;
        $categoriesAndStudios = KpxzxSiteContent::where('template', $categery_mult_template_id )->get();//TOD: all()
        $categories = Array();
        foreach ($categoriesAndStudios as $categoryAndStudio) {
            $isStudio = false;
            foreach ($studios as $studio) {
                if ($categoryAndStudio->id == $studio['id']) {
                    $isStudio = true;
                    break;
                }
            }

            $exclude_category_id = 2434;//id раздела, который не нужно выводить
            if ($isStudio || $categoryAndStudio->id === $exclude_category_id) {
                continue;
            } 

            $category = $categoryAndStudio;
            array_push($categories, $category);
        }   
        
        return view('manager.multseries', ['studios' => $studios, 'categories' => $categories]);
    }*/

    public function season(Request $request)
    {
        $itemId = $request->item_id;
        if (!is_null($itemId)) {
            return view('manager.season', ['itemId' => $itemId]);
        }

        return view('manager.season');
    }

    public function serie(Request $request)
    {   
        $itemId = $request->item_id;
        if (!is_null($itemId)) {
            return view('manager.serie', ['itemId' => $itemId]);
        }
        return view('manager.serie');
    }

    public function seasons(Request $request)
    {
        $itemId = $request->item_id;
        if (!is_null($itemId)) {
            $seasonsTemplateId = 103;
            $seasons = KpxzxSiteContent::where('template', $seasonsTemplateId)
                                ->where('parent', $itemId)->get();
            $seasonsSeries = [];
            $seriesTemplateId = 105;
            foreach ($seasons as $season) { //dd($season);
                $seasonId = $season->id;
                $seasonSeries = KpxzxSiteContent::where('template', $seriesTemplateId)
                                        ->where('parent', $seasonId)
                                        ->get();//TOD: all()
                $seasonTitle = $season->pagetitle;     
                $seasonIntrotext = $season->introtext;
                $seasonContent = $season->content;
                $seasonURI = $season->uri; 
                $seasonIsDelete = $season->deleted; 
                $seasonPoster = '';
                $startDate = NULL;
                foreach ($season->kpxzxsitetmplvarcontentvalue as $value) {
                    if ($value->tmplvarid == 142) {
                        $seasonPoster = $value->value;
                    } else if($value->tmplvarid == 184) {
                        $startDate = $value->value;
                    }       
                }          
                array_push($seasonsSeries, [
                    'id' => $seasonId,
                    'title' => $seasonTitle,
                    'introtext' => $seasonIntrotext,
                    'content' => $seasonContent,
                    'uri' => $seasonURI,
                    'deleted' => $seasonIsDelete,
                    'start_date' => $startDate,
                    'poster' => $seasonPoster,
                    'series' => $seasonSeries
                ]);
            }

            $multsTemplateId = 97;
            $mults = KpxzxSiteContent::orderBy('createdon', 'desc')->take(10)
                                        ->where('template', $multsTemplateId)
                                        ->get();
                                        
            return view('manager.seasons', ['seasons' => $seasonsSeries])->with(['mults' => $mults]);
        }
        return view('manager.seasons');
    }

    public function multhero()
    {   
        return view('manager.multhero');
    }

    public function quote()
    {   
        return view('manager.quote');
    }

    public function smultlive(Request $request)
    {   
        $itemId = $request->item_id;
        if (!is_null($itemId)) {
            $item = KpxzxSmultlive::find($itemId);
            return view('manager.smultlive', ['item' => $item]);
        } else {
            return view('manager.smultlive');
        }
    }

    public function competition(Request $request)
    {   
        $itemId = $request->item_id;
        if (!is_null($itemId)) {
            $item = KpxzxSiteContent::find($itemId);
            return view('manager.competition', ['item' => $item]);
        } else {
            return view('manager.competition');
        }
    }

    public function competitionParticipantsAndWinners(Request $request)
    {      
        $itemId = $request->item_id;
        if (!is_null($itemId)) {
            $competition = KpxzxSiteContent::where('id', $itemId)->get();
        
            $winners = $competition[0]->kpxzxwinner;//KpxzxWinner::all()->where('contentid', $itemId)->get(); 
            $multsTemplateId = 97;
            $mults = KpxzxSiteContent::orderBy('createdon', 'desc')->take(10)
                                        ->where('template', $multsTemplateId)
                                        ->get();

            return view('manager.competition_winners', ['winners' => $winners])->with(['mults' => $mults]);
        } else {
            return view('manager.competition_winners');
        }
    }

    public function competitionInfo(Request $request)
    {      
        $itemId = $request->item_id;
        if (!is_null($itemId)) {
            $competition = KpxzxSiteContent::where('id', $itemId)->get();
            $winner = KpxzxWinner::where('contentid', $itemId)
                                ->where('winnertype', 1)
                                ->get(); 

            $multsTemplateId = 97;
            $mults = KpxzxSiteContent::orderBy('createdon', 'desc')->take(10)
                                        ->where('template', $multsTemplateId)
                                        ->get();

            return view('manager.competition_info', [
                'competition' => $competition,
                 'winner' => $winner
            ])->with(['mults' => $mults]);
        } else {
            return view('manager.competition_info');
        }
    }

    public function newtest()
    {       
        return view('manager.newtest');
    }

    public function newpoll()
    {       
        return view('manager.newpoll');
    }

    public function resource(Request $request)
    {    
        $multsTemplateId = 97;
        $mults = KpxzxSiteContent::orderBy('createdon', 'desc')->take(10)
                            ->where('template', $multsTemplateId)
                            ->get();
        if (isset($request->all()['parent']) && !is_null($request->all()['parent'])) {
            $parentId = $request->all()['parent'];//добавление нового итэма, parent является айдишником контента для которого добавляем итэм     
            return view('manager.resource', ['parentId' => $parentId, 'mults' => $mults]);
        } if (isset($request->all()['item_id']) && !is_null($request->all()['item_id'])) {
            $itemId = $request->all()['item_id'];
            $item = KpxzxSiteContent::find($itemId);
            return view('manager.resource', ['item' => $item]);
        } else {
            return view('manager.resource');
        }
    }

    public function additionalparams(Request $request) 
    {   
        //if (!is_null($request->user()) && $request->user()->can('additionalparams')) {
        $tmplVarId = $request->tvIdValue;  
        if (!is_null($tmplVarId)) {
            $countData = $request->countOnPageSelectValue;

            $multsTemplateId = 97;
            $mults = KpxzxSiteContent::limit($countData)->where('template', $multsTemplateId)->get();
            $data = array('count_on_page_select_value' => $countData);
            $data = array_merge($data, array('tmplvarid' => $tmplVarId));
            foreach ($mults as $mult) {
                foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                    if ($value->tmplvarid == $tmplVarId) {
                        $id = $mult->id;
                        $title = $mult->pagetitle;
                        $uri = $mult->alias . '.html';
                        $dataValueId = $value->id;
                        $dataValue = $value->value;
                        $data = array_merge($data, [['id' => $id, 'title' => $title, 'uri' => $uri, 'value_id' => $dataValueId, 'value' => $dataValue]]);
                        break;
                    }
                }
            }

            if ($tmplVarId == 154) {
                $studiosName = Array(
                    'Walt Disney (Уолт Дисней)',
                    'Pixar (Пиксар)',
                    'Blue Sky Studios (Блу Скай)',
                    'DreamWorks Animation (Дримворкс)',
                    'Sony Pictures Animation (Сони Пикчерс)',
                    'Illumination Entertainment (Иллюминейшен Интертеймент)',
                    'Мельница',
                    'Nickelodeon (Никелодеон)',
                    'CGBros',
                    'Прочие студии'
                );

                $studios = Array();
                foreach ($studiosName as $studioName) {
                    $studio = KpxzxSiteContent::where('pagetitle', $studioName)->get();
                    $studios = array_merge($studios, array(array('id' => $studio[0]['id'], 'title' => $studioName)));
                }  

                return view('manager.additionalparams', ['data' => $data])->with(['count_data' => $countData, 'tmplvarid' => $tmplVarId, 'studios' => $studios]);
            } else {
                return view('manager.additionalparams', ['data' => $data])->with(['count_data' => $countData, 'tmplvarid' => $tmplVarId]);
            }
        }
        return view('manager.additionalparams')->with(['count_data' => 10, 'tmplvarid' => 0]);
        //}
    }

    public function comments(Request $request) 
    {   
        $countData = $request->countOnPageSelectValue;
        $commentTypeValue = $request->comment_type_value;
        if (is_null($countData)) {
            $countData = 50;
        }

        if (is_null($commentTypeValue)) {
            $commentTypeValue = 'all';
        }

        switch ($commentTypeValue) {
            case 'moder':
                $comments = KpxzxComment::limit($countData)->where('is_moder', 1)->get();
            break;

            case 'no_moder':
                $comments = KpxzxComment::limit($countData)->where('is_moder', 0)->get();
            break;

            default:
                $comments = KpxzxComment::limit($countData)->get();
            break;
        }
        
        $multsTemplateId = 97;
        $mults = KpxzxSiteContent::orderBy('createdon', 'desc')->take(10)
                                    ->where('template', $multsTemplateId)
                                    ->get();
        return view('manager.comments', ['comments' => $comments, 'mults' => $mults])->with(['count_data' => $countData, 'type_comments' => $commentTypeValue]);
    }

    public function tickets(Request $request) 
    {   
        $type = $request->type;
        switch ($type) {
            case 'performed':
                $tickets = KpxzxTicket::limit(1)->where('ready', 1)->get();//TOD: all()
                break;

            case 'unfulfilled':
                $tickets = KpxzxTicket::where('ready', 0)->get();//TOD: all()
                break;
        }
        $multsTemplateId = 97;
        $mults = KpxzxSiteContent::orderBy('createdon', 'desc')->take(10)
                                    ->where('template', $multsTemplateId)
                                    ->get();

        return view('manager.tickets', ['tickets' => $tickets, 'mults' => $mults])->with(['type' => $type]);
    }

    public function videoservices(Request $request) 
    {   
        $countData = $request->countOnPageSelectValue;
        if (is_null($countData)) {
            $countData = 10;
        }
        $multsTemplateId = 102;
        $contents = KpxzxSiteContent::limit(9999)//TOD: all()
                                    ->where('template', $multsTemplateId)
                                    ->get();
        $videoservicesAmount = [];

        foreach ($contents as $content) {
            foreach ($content->kpxzxsitetmplvarcontentvalue as $value) {
                if ($value->tmplvarid == 137) {
                    preg_match('/.*?:?\/\/(.*)\//U', $value->value, $matches);
                    $videoserviceName = $matches[1];
                    if (count($videoservicesAmount) > 0) {
                        if (count($videoservicesAmount) < $countData) {
                            $serviceAmoutExist = false;
                            for ($i = 0; $i < count($videoservicesAmount); $i++) {
                                if (in_array($videoserviceName, $videoservicesAmount[$i])) {
                                    $videoservicesAmount[$i]['amount']++;
                                    $serviceAmoutExist = true;
                                    break;
                                }
                            }
                            if (!$serviceAmoutExist) {
                                $data = ['amount' => 1, 'videoservice_name' => $videoserviceName];
                                array_push($videoservicesAmount, $data);
                            }
                        }
                    } else {
                        $data = ['amount' => 1, 'videoservice_name' => $videoserviceName];
                        array_push($videoservicesAmount, $data);
                    }
                    continue;
                }   
            }                                   
        }
        
        for ($i = 0; $i < count($videoservicesAmount); $i++) {
            for ($y = 0; $y < count($videoservicesAmount); $y++) {
                if ($videoservicesAmount[$i]['amount'] > $videoservicesAmount[$y]['amount']) {
                    $videoserviceAmountOne = $videoservicesAmount[$i];
                    $videoservicesAmount[$i] = $videoservicesAmount[$y];
                    $videoservicesAmount[$y] = $videoserviceAmountOne;
                }
            }
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($videoservicesAmount);
        $perPage = 20;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedVideoserviceAmount = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedVideoserviceAmount->setPath($request->url());

        return view('manager.videoservices', ['videoservicesAmount' => $paginatedVideoserviceAmount])
            ->with(['count_data' => $countData]);
    } 

    public function downloadWallpaperAndPoster(Request $request)
    {
        $parentId = $request->parent_id;
        if (!is_null($parentId)) {
            return view('manager.wallpapers_and_posters', ['parentId' => $parentId]);
        }
        return view('manager.wallpapers_and_posters');
    }

    public function addMult(Request $request)
    {   
        //if (!is_null($request->user()) && $request->user()->can('addMult')) { 
            $this->validatorAddMult($request->all())->validate();
            $multAndMultseriesTemplateId = 97;

            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => $request->description,//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => 0,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => $multAndMultseriesTemplateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                $contentValues = Array();
                $contentValues = array_merge($contentValues, array($request->url_full_mult => 137));//ссылка на видеоролик
                $contentValues = array_merge($contentValues, array($request->description => 138));//описание для SEO
                $contentValues = array_merge($contentValues, array($request->notranslate => 189));//нет русской озвучки
                if ($request->notranslate === 1) {
                    $contentValues = array_merge($contentValues, array($request->mult_sound => 147));//кто озвучивал
                }

                $dirPath = public_path() . '/images/' . $request->alias;
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }

                $newMultId = $kpxzxSiteContent->id;

                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $request->alias . '_' . $newMultId . '_poster.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }

                $posterPath = '/public/images/' . $request->alias . '/' . $filename;
                $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TOD 
                $contentValues = array_merge($contentValues, array($request->mult_year => 143));//год выпуска
                $contentValues = array_merge($contentValues, array($request->mult_released => 144));//выпустившая страна
                $contentValues = array_merge($contentValues, array($request->slogan => 145));//слоган
                $contentValues = array_merge($contentValues, array($request->mult_quality => 148));//качество видео
                $contentValues = array_merge($contentValues, array($request->mult_director => 149));//режиссер
                $keywoards = 'лучшие мультики, лучшие мультики онлайн, лучшие мультфильмы, лучшие мультфильмы онлайн, мультики онлайн бесплатно';
                $contentValues = array_merge($contentValues, array($keywoards => 151));//ключевые слова для SEO
                $contentValues = array_merge($contentValues, array($request->studios => 154));//выпустившая студия
                $contentValues = array_merge($contentValues, array($request->agelimit => 155));//возрастное ограничение
                $contentValues = array_merge($contentValues, array($request->original_title => 156));//оригинальное название
                $contentValues = array_merge($contentValues, array($request->istok => 163));//ссылка на оригинальный источник
                $contentValues = array_merge($contentValues, array($request->typeposter => 171));//тип постера
                $contentValues = array_merge($contentValues, array($request->date_of_premiere => 173));//дата премьеры
                $contentValues = array_merge($contentValues, array($request->Lict_Razdel => 174));//разделы к которым пренадлежит мультфильм
                $contentValues = array_merge($contentValues, array($request->mult_with_articles => 179));//мультфильмы по статье
                //текст пользовательской кнопки "А вам нравится?!"
                $contentValues = array_merge($contentValues, array($request->custom_button => 181));
                //не на русском языке (нет названия на русском языке)
                $contentValues = array_merge($contentValues, array($request->nooriginal => 195));
                //является опубликованным, TOD: possibly need reemove
                $contentValues = array_merge($contentValues, array('1' => 196));

                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(
                        array(
                            'tmplvarid'  => $value,
                            'contentid'  => $newMultId,
                            'value'  => $key
                        )
                    );

                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        //} else {
            //return '<span>Права доступа отсутствуют!</span>';
        //}

        return back();
    }

    public function addMultseries(Request $request)
    {   
        //if (!is_null($request->user()) && $request->user()->can('addMultseries')) { 
            $this->validatorAddMultseries($request->all())->validate();
            $multAndMultseriesTemplateId = 108;

            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => $request->description,//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => 0,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => $multAndMultseriesTemplateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                $contentValues = Array();
                $contentValues = array_merge($contentValues, array($request->url_full_mult => 137));//ссылка на видеоролик
                $contentValues = array_merge($contentValues, array($request->description => 138));//описание для SEO
                $contentValues = array_merge($contentValues, array($request->notranslate => 189));//нет русской озвучки
                if ($request->notranslate === 1) {
                    $contentValues = array_merge($contentValues, array($request->mult_sound => 147));//кто озвучивал
                }

                $dirPath = public_path() . '/images/' . $request->alias;
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }

                $newMultseriesId = $kpxzxSiteContent->id;

                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $request->alias . '_' . $newMultseriesId . '_poster.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }

                $posterPath = '/public/images/' . $request->alias . '/' . $filename;
                $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TOD 
                $contentValues = array_merge($contentValues, array($request->mult_year => 143));//год выпуска
                $contentValues = array_merge($contentValues, array($request->mult_released => 144));//выпустившая страна
                $contentValues = array_merge($contentValues, array($request->slogan => 145));//слоган
                $contentValues = array_merge($contentValues, array($request->mult_quality => 148));//качество видео
                $contentValues = array_merge($contentValues, array($request->mult_director => 149));//режиссер
                $keywoards = 'лучшие мультики, лучшие мультики онлайн, лучшие мультфильмы, лучшие мультфильмы онлайн, мультики онлайн бесплатно';
                $contentValues = array_merge($contentValues, array($keywoards => 151));//ключевые слова для SEO
                $contentValues = array_merge($contentValues, array($request->studios => 154));//выпустившая студия
                $contentValues = array_merge($contentValues, array($request->agelimit => 155));//возрастное ограничение
                $contentValues = array_merge($contentValues, array($request->original_title => 156));//оригинальное название
                $contentValues = array_merge($contentValues, array($request->istok => 163));//ссылка на оригинальный источник
                $contentValues = array_merge($contentValues, array($request->typeposter => 171));//тип постера
                $contentValues = array_merge($contentValues, array($request->date_of_premiere => 173));//дата премьеры
                $contentValues = array_merge($contentValues, array($request->Lict_Razdel => 174));//разделы к которым пренадлежит мультфильм
                $contentValues = array_merge($contentValues, array($request->mult_with_articles => 179));//мультфильмы по статье
                //текст пользовательской кнопки "А вам нравится?!"
                $contentValues = array_merge($contentValues, array($request->custom_button => 181));
                //не на русском языке (нет названия на русском языке)
                $contentValues = array_merge($contentValues, array($request->nooriginal => 195));
                //является опубликованным, TOD: possibly need reemove
                $contentValues = array_merge($contentValues, array('1' => 196));

                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newMultseriesId,
                        'value'  => $key
                    ));

                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        //} else {
            //return '<span>Права доступа отсутствуют!</span>';
        //}
        return back();
    }

    public function addSeason(Request $request)//Ticket
    {           
        if (true) {//if (!is_null($request->user()) && $request->user()->can('addSeason')) {  
            $this->validatorAddSeason($request->all())->validate();
            $seasonsTemplateId = 103;
            $itemId = $request->itemId;

            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => $request->description,//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => $itemId,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => $seasonsTemplateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                if (!is_null(Input::file('main_file'))) {
                    $category = KpxzxSiteContent::find($itemId);
                    $categoryAlias = $category->alias;
                    $contentValues = Array();
                    $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                    if (!File::exists($dirPath)) {
                        File::makeDirectory($dirPath); 
                    }
                    $newSeasonId = $kpxzxSiteContent->id;   
                    $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                    $filename = $request->alias . '_' . $newSeasonId . '_poster.' . $fileExtension;
                    if (!Input::file('main_file')->move($dirPath, $filename)) {
                        return 'Сообщение об ошибке!';
                    }
                    $posterPath = '/public/images/' . $categoryAlias . '/' . $newSeasonId . '/' . $filename;
                    $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TOD: источник
                }
                $contentValues = array_merge($contentValues, array($request->istok => 163));
                $contentValues = array_merge($contentValues, array($request->mult_with_articles => 179));//мультфильмы по статье
                //текст пользовательской кнопки "А вам нравится?!"
                $contentValues = array_merge($contentValues, array($request->custom_button => 181));
                $newSeasonId = $kpxzxSiteContent->id;
                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newSeasonId,
                        'value'  => $key
                    ));
                    
                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }
            
            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        } else {
           return '<span>Права доступа отсутствуют!</span>';
        }
        return back();
    }

    public function addSerie(Request $request)//Ticket
    {           
        if (true) {//if (!is_null($request->user()) && $request->user()->can('addSerie')) {
            $this->validatorAddSerie($request->all())->validate();
            $seriesTemplateId = 105;
            $itemId = $request->itemId;
            $seasonsTemplateId = 103;
            $seasons = KpxzxSiteContent::where('template', $seasonsTemplateId)
                                ->where('parent', $itemId)
                                ->get();
            $lastSeasonId = end($seasons)[count($seasons) - 1]->id;
            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => $request->description,//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => $lastSeasonId,
                'isfolder'  => 1,
                'content'  => $request->content,//необязателен
                'richText'  => 1,
                'template'  => $seriesTemplateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                if (!is_null(Input::file('main_file'))) {
                    $category = KpxzxSiteContent::find($itemId);
                    $categoryAlias = $category->alias;
                    $contentValues = Array();
                    $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                    if (!File::exists($dirPath)) {
                        File::makeDirectory($dirPath); 
                    }
                    $newSerieId = $kpxzxSiteContent->id;   
                    $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                    $filename = $request->alias . '_' . $newSerieId . '_poster.' . $fileExtension;
                    if (!Input::file('main_file')->move($dirPath, $filename)) {
                        return 'Сообщение об ошибке!';
                    }
                    $posterPath = '/public/images/' . $categoryAlias . '/' . $newSerieId . '/' . $filename;
                    $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TOD: источник
                }
                $contentValues = array_merge($contentValues, array($request->istok => 163));
                $contentValues = array_merge($contentValues, array($request->mult_with_articles => 179));//мультфильмы по статье
                //текст пользовательской кнопки "А вам нравится?!"
                $contentValues = array_merge($contentValues, array($request->custom_button => 181));
                $newSerieId = $kpxzxSiteContent->id;
                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newSerieId,
                        'value'  => $key
                    ));
                    
                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }
            
            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        } else {
           return '<span>Права доступа отсутствуют!</span>';
        }
        return back();
    }

    public function addMulthero(Request $request)
    {   
        //if (!is_null($request->user()) && $request->user()->can('addMulthero')) { 
            $this->validatorAddMulthero($request->all())->validate();

            $multheroTemplateId = 106;
            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => $request->description,//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => 0,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => $multheroTemplateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                $contentValues = Array();
                $contentValues = array_merge($contentValues, array($request->description => 138));//описание для SEO

                $dirPath = public_path() . '/images/heroes/';
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }

                $newMultheroId = $kpxzxSiteContent->id;
                
                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $request->alias . '_' . $newMultheroId . '_poster.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }

                $posterPath = '/public/images/heroes/' . $filename;
                $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TOD
                $keywoards = 'герои мультфильмов, персонажи мультфильмов, герои мультсериалов, персонажи мультсериалов, ' . $request->alias;
                $contentValues = array_merge($contentValues, array($keywoards => 151));//ключевые слова для SEO
                $contentValues = array_merge($contentValues, array($request->istok => 163));//ссылка на оригинальный источник
                $contentValues = array_merge($contentValues, array($request->mult_with_articles => 179));//мультфильмы по статье
                //текст пользовательской кнопки
                $contentValues = array_merge($contentValues, array($request->custom_button => 181));

                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newMultheroId,
                        'value'  => $key
                    ));
                    
                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        //} else {
           //return '<span>Права доступа отсутствуют!</span>';
        //}
        return back();
    }

    public function addQuote(Request $request)
    {   
        //if (!is_null($request->user()) && $request->user()->can('addQuote')) { 
            $this->validatorAddQuote($request->all())->validate();
            $data = [
                'heroy' => $request->hero,
                'heroy_uri' => $request->hero_uri,
                'mult' => $request->mult,
                'mult_uri' => $request->mult_uri,
                'poster' => '',
                'text' => $request->editor1,
                'published' => 0,
                'createdby' => 1,
                'createdon' => time(),
                'editedby' => 2,
                'editedon' => 0
            ];

            $quote = KpxzxQuote::create($data);
            if (!is_null($quote)) {
                $dirPath = public_path() . '/images/citat/';
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }
                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $quote->heroy . '_' . $quote->id  . '_poster.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }

                $posterPath = '/public/images/citat/' . $filename;
                $quote->poster = $posterPath;
                $quote->save();
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }

        //} else {
           //return '<span>Права доступа отсутствуют!</span>';
        //}

        return back();
    }

    public function addSmultLive(Request $request)
    {   
        //if (!is_null($request->user()) && $request->user()->can('addSmultLive')) { 
            $this->validatorAddSmultLive($request->all())->validate();
            $settings = 'color:#ffffff;' . $request->first_title_vertical . 'font-size:40;' .  $request->first_title_align . 'color:#ffffff;' . $request->second_title_vertical . 'font-size:32;' .  $request->second_title_align . '';
            $data = [
                'first_title' => $request->first_title,
                'second_title' => $request->second_title,
                'settings' => $settings,
                'multId' => $request->multId,
                'poster' => '',
                'createdby' => time(),
                'createdon' => 0,
                'editedby' => 0,
                'editedon' => 0,
            ];

            $smultLive = KpxzxSmultlive::create($data);
            if (!is_null($smultLive)) {
                $dirPath = public_path() . '/images/smultlive/';
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }
                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $smultLive->first_title . '_' . $smultLive->id . '_image.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }
                $posterPath = '/public/images/smultlive/' . $filename;
                $smultLive->poster = $posterPath;
                $smultLive->save();
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }

        //} else {
            //return '<span>Права доступа отсутствуют!</span>';
        //}
        return back();
    }

    public function addCompetition(Request $request)
    {   
        //if (!is_null($request->user()) && $request->user()->can('addCompetition')) { 
            $this->validatorAddCompetition($request->all())->validate();
            $competitionTemplateId = 142;

            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => '',//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => $competitionTemplateId,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => 142,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                $contentValues = Array();
                $contentValues = array_merge($contentValues, array($request->comp_start => 184));//дата старта конкурса

                $dirPath = public_path() . '/images/сompetition/';
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }
                $newCompetitionId = $kpxzxSiteContent->id;   
                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $request->alias . '_' . $newCompetitionId . '_poster.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }
                $posterPath = '/public/images/сompetition/' . $filename;
                $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TOD
                $contentValues = array_merge($contentValues, array($request->comp_end => 143));//дата окончания 
                $contentValues = array_merge($contentValues, array($request->istok => 163));//ссылка на оригинальный источник
                $contentValues = array_merge($contentValues, array($request->fond => 185));//приз за конкурс 
                $contentValues = array_merge($contentValues, array($request->mult_with_articles => 179));//мультфильмы по

                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newCompetitionId,
                        'value'  => $key
                    ));
                    
                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        //} else {
           //return '<span>Права доступа отсутствуют!</span>';
        //}
        return back();
    }

    public function addNewtest(Request $request)
    {              
        //if (!is_null($request->user()) && $request->user()->can('addNewtest')) { 
            $this->validatorAddNewtest($request->all())->validate();
            $newpollAndnewtestTemplateId = 144;
            $newtestParentId = 23169;

            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => '',//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => $newtestParentId,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => $newpollAndnewtestTemplateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                $contentValues = Array();
                $dirPath = public_path() . '/images/newtest/';
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }
                $newNewtestId = $kpxzxSiteContent->id;   
                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $request->alias . '_' . $newNewtestId . '_poster.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }
                $posterPath = '/public/images/newtest/' . $filename;
                $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TODисточник
                $contentValues = array_merge($contentValues, array('test' => 186));//undefined

                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newNewtestId,
                        'value'  => $key
                    ));
                    
                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        //} else {
           //return '<span>Права доступа отсутствуют!</span>';
        //}
        return back();
    }

    public function addNewpoll(Request $request)
    {   
        //if (!is_null($request->user()) && $request->user()->can('addNewpoll')) { 
            $this->validatorAddNewpoll($request->all())->validate();
            $newpollAndnewtestTemplateId = 144;
            $newpollParentId = 23170;

            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => '',//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => $newpollParentId,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => $newpollAndnewtestTemplateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                $contentValues = Array();
                $dirPath = public_path() . '/images/newpoll/';
                if (!File::exists($dirPath)) {
                    File::makeDirectory($dirPath); 
                }
                $newNewpollId = $kpxzxSiteContent->id;   
                $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                $filename = $request->alias . '_' . $newNewpollId . '_poster.' . $fileExtension;
                if (!Input::file('main_file')->move($dirPath, $filename)) {
                    return 'Сообщение об ошибке!';
                }
                $posterPath = '/public/images/newpoll/' . $filename;
                $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TODисточник
                $contentValues = array_merge($contentValues, array('poll' => 186));//undefined

                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newNewpollId,
                        'value'  => $key
                    ));
                    
                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }

            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        //} else {
           //return '<span>Права доступа отсутствуют!</span>';
        //}
        return back();
    }

    public function addResource(Request $request)//Ticket
    {           
        if (true) {//if (!is_null($request->user()) && $request->user()->can('addResource')) {  
            $this->validatorAddResource($request->all())->validate();
            $categoryId = $request->all()['category_id'];
            $parentId = $request->all()['parent'];

            $articleSmultReadingParentId = 2921;
            $articlePostId = 366;
            $articleReviewId = 13221;
            $articleArtId = 13101;
            $articleUniverseId = 13220;
            $articleNewtestId = 23169;
            $articleNewpollId = 23170;
            $articleIntervieweId = 13978;
            $articlePersonDayId = 2912;
            $articlePersonUSSRId = 2982;
            $trailerId = 14054;
            $interestingId = 14056;
            $wallpaperAndPosterId = 14055;
            $soundtrackId = 14057;
            $multheroId = 14053;
            $competitionId = 22316;
            switch ($categoryId) {
                case $articleSmultReadingParentId:
                    $templateId = 124;
                    break;

                case $articlePostId:
                    $templateId = 124;
                    break;

                case $articleReviewId:
                    $templateId = 124;
                    break;

                case $articleArtId:
                    $templateId = 124;
                    break;

                case $articleUniverseId:
                    $templateId = 124;
                    break;

                case $articleNewtestId:
                    $templateId = 124;
                    break;

                case $articleNewpollId:
                    $templateId = 124;
                    break;

                case $articleIntervieweId:
                    $templateId = 124;
                    break;

                case $articlePersonDayId:
                    $templateId = 124;
                    break;

                case $articlePersonUSSRId:
                    $templateId = 124;
                    break;
                
                case $trailerId:
                    $templateId = 102;
                    break;

                case $interestingId:
                    $templateId = 114;
                    break;

                case $wallpaperAndPosterId:
                    $templateId = 109;
                    break;

                case $soundtrackId:
                    $templateId = 110;
                    break;

                case $multheroId:
                    $templateId = 106;
                    break;

                case $competitionId:
                    $templateId = 142;
                    break;
            }

            $kpxzxSiteContent = KpxzxSiteContent::create(array(
                'type'  => 'document',
                'contentType'  => 'text/html',
                'pagetitle'  => $request->pagetitle,
                'longtitle'  => '',
                'description'  => $request->description,//описание для SEO
                'alias'  => $request->alias,//необязателен
                'link_attributes'  => '',
                'published'  => 1,
                'pub_date'  => 1,//date("m.d.y h:i:s"),
                'unpub_date'  => 0,
                'parent'  => $parentId,
                'isfolder'  => 1,
                'content'  => $request->editor1,//необязателен
                'richText'  => 1,
                'template'  => $templateId,
                'menuindex'  => 1986,
                'searchable'  => 1,
                'cacheable'  => 1,
                'createdby'  => 1,
                'createadon'  => 0,
                'editedby'  => 0,
                'editedon'  => 0,
                'deleted'  => 0,
                'deletedon'  => 0,
                'deletedby'  => 0,
                'publishedon'  => 0,
                'publishedby'  => 0,
                'menutitle'  => '',
                'dontHit'  => 0,
                'privateweb'  => 0,
                'privatemgr'  => 0,
                'content_dispo'  => 0,
                'hideMenu'  => 0,
                'class_key'  => 'modDocument',
                'context_key'  => 'web',
                'uri'  => $request->alias . '.html',//необязателен
                'uri_override'  => 0,
                'hide_children_in_tree'  => 0,
                'show_in_tree'  => 0
            ));

            if (!is_null($kpxzxSiteContent)) {
                if (!is_null(Input::file('main_file'))) {
                    $category = KpxzxSiteContent::find($parentId);
                    $categoryAlias = $category->alias;
                    $contentValues = Array();
                    $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                    if (!File::exists($dirPath)) {
                        File::makeDirectory($dirPath); 
                    }
                    $newResourceId = $kpxzxSiteContent->id;   
                    $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                    $filename = $request->alias . '_' . $newResourceId . '_poster.' . $fileExtension;
                    if (!Input::file('main_file')->move($dirPath, $filename)) {
                        return 'Сообщение об ошибке!';
                    }
                    $posterPath = '/public/images/' . $categoryAlias . '/' . $newResourceId . '/' . $filename;
                    $contentValues = array_merge($contentValues, array($posterPath => 142));//файл постера, TOD: источник
                }

                if (!is_null(Input::file('wide_file'))) {
                    $category = KpxzxSiteContent::find($parentId);
                    $categoryAlias = $category->alias;
                    $contentValues = Array();
                    $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                    if (!File::exists($dirPath)) {
                        File::makeDirectory($dirPath); 
                    }
                    $newResourceId = $kpxzxSiteContent->id;   
                    $fileExtension = Input::file('wide_file')->getClientOriginalExtension();
                    $filename = $request->alias . '_' . $newResourceId . '_poster.' . $fileExtension;
                    if (!Input::file('wide_file')->move($dirPath, $filename)) {
                        return 'Сообщение об ошибке!';
                    }
                    $posterPathWide = '/public/images/' . $categoryAlias . '/' . $newResourceId . '/' . $filename;
                    $contentValues = array_merge($contentValues, array($posterPathWide => 129));//файл постера, TOD: источник
                }

                $contentValues = array_merge($contentValues, array($request->istok => 163));
                $contentValues = array_merge($contentValues, array($request->mult_with_articles => 179));//мультфильмы по статье
                //текст пользовательской кнопки "А вам нравится?!"
                $contentValues = array_merge($contentValues, array($request->custom_button => 181));
                $newResourceId = $kpxzxSiteContent->id;
                foreach ($contentValues as $key => $value) { 
                    $kpxzxSiteTmplvarContentvalue = KpxzxSiteTmplvarContentvalue::create(array(
                        'tmplvarid'  => $value,
                        'contentid'  => $newResourceId,
                        'value'  => $key
                    ));
                    
                    if (!$kpxzxSiteTmplvarContentvalue) {
                        $addedContentValues = KpxzxSiteTmplvarContentvalue::where('contentid', $kpxzxSiteTmplvarContentvalue->contentid);//TOD: all()
                        $addedContentValues->delete();
                        return 'Ошибка добавления!';
                    }
                }
            } else {
                return 'Ошибка добавления!';
            }

            
            $afterSave = $request->afterSave;
            if ($afterSave === 'edit') {
                $request->session()->flash('link', $kpxzxSiteContent->uri);
                return back()->withInput();
            }
        } else {
           return '<span>Права доступа отсутствуют!</span>';
        }
        return back();
    }

    public function editAdditionalparams(Request $request) 
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('editadditionalparams')) {  
            $valueId = $request->value_id;
            $newValue = $request->current_value;

            $contentValue = KpxzxSiteTmplvarContentvalue::find($valueId);
            if (count($contentValue)) {
                $contentValue->value = $newValue;
                $contentValue->save();

                $countData = $request->countOnPageSelectValue;
                $tmplvarid = $contentValue->tmplvarid;
                return redirect()->action(
                    'managerController@additionalparams', ['countOnPageSelectValue' => $countData, 'tvIdValue' => $tmplvarid]
                );
            } else {
                return '<span>Неверное значение параметра!</span>';
            }
        } else {
           //return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function moderationComment(Request $request) 
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('moderationComment')) {  
            $commentId = $request->comment_id;
            $comment = KpxzxComment::find($commentId);
            if (count($comment)) {
                $isModer = $request->is_moder;
                if ($isModer == 1) {
                    $comment->is_moder = 0;
                } else {
                    $comment->is_moder = 1;
                }
                $comment->save();
                return back();
            } else {
                return '<span>Неверное значение параметра!</span>';
            }
        } else {
           //return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function deleteComment(Request $request) 
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('deleteComment')) {  
            $commentId = $request->comment_id;
            $comment = KpxzxComment::find($commentId);
            if (count($comment)) {
                $comment->delete();
                return back();
            } else {
                return '<span>Неверное значение параметра!</span>';
            }
        } else {
           //return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function moderationTicket(Request $request)//Ticket 
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('moderationTicket')) {  
            $ticketId = $request->ticket_id;
                    
            $ticket = KpxzxTicket::find($ticketId);
            if (count($ticket)) {
                if (empty($request->ticket_status)) {
                    $ready = 0;
                } else {
                    $ready = 1;
                }
                $ticket->ready = $ready;
                $ticket->comment = $request->comment;

                $ticket->save();

                return back();
            } else {
                return '<span>Неверное значение параметра!</span>';
            }
        } else {
           //return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function deleteTicket(Request $request)//Ticket
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('deleteTicket')) {  
            $ticketId = $request->ticket_id;
            $ticket = KpxzxTicket::find($ticketId);
            if (count($ticket)) {
                $ticket->delete();
                return back();
            } else {
                return '<span>Неверное значение параметра!</span>';
            }
        } else {
           //return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function competitionDeleteParticipants(Request $request)
    {      
        if (true) {//if (!is_null($request->user()) && $request->user()->can('deleteCompetition')) {  
            $participantsId = $request->participants_id;
            $contentId = $request->content_id;
            $participant = KpxzxWinner::where('id', $participantsId)
                                ->where('contentid', $contentId)
                                ->get();
            if (count($participant)) {                 
                $participant = $participant[0];
                $participant->deleted = 1; 
                $participant->save();
                return back();
            } else {
                return '<span>Неверное значение параметра!</span>';
            }
        } else {
           //return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function editMult(Request $request)
    { 
        if (true) {//if (!is_null($request->user()) && $request->user()->can('editMult')) {
            $this->validatorEditMult($request->all())->validate(); 
            $itemId = $request->itemId;
            if (isset($itemId) && !is_null($itemId)) { 
                $mult = KpxzxSiteContent::find($itemId);
                if (count($mult)) {
                    $mult->pagetitle = $request->pagetitle;
                    $mult->description = $request->description;
                    $mult->alias = $request->alias;
                    $mult->content = $request->editor1;
                    $mult->uri = $request->alias . '.html';
                    $mult->save();

                    $file = $request->main_file;
                    $posterPath = '';
                    if (!is_null($file)) {
                        $category = KpxzxSiteContent::find($parentId);
                        $categoryAlias = $category->alias;
                        $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                        if (!File::exists($dirPath)) {
                            File::makeDirectory($dirPath); 
                        }
                        $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                        $filename = $quote->heroy . '_' . $quote->id  . '_poster.' . $fileExtension;
                        if (!Input::file('main_file')->move($dirPath, $filename)) {
                            return 'Сообщение об ошибке!';
                        }

                        $posterPath = '/public/images/' . $categoryAlias . '/' . $filename;
                    }

                    $tmplvarIds = [142, 137, 138, 189, 147, 143, 144, 145, 148, 149, 154, 155, 156, 163, 171, 173, 174, 179, 181, 195];

                    $tmplvarIdsSaved = [];  
                    foreach ($mult->kpxzxsitetmplvarcontentvalue as $value) {
                        if ($value->tmplvarid == 142 && isset($posterPath)) {
                            $value->value = $posterPath;
                            $value->save();
                            array_push($tmplvarIdsSaved, 142);
                        } else if ($value->tmplvarid == 137) {
                            if (is_null($request->url_full_mult)) {
                                $request->url_full_mult = '';
                            }
                            $value->value = $request->url_full_mult;
                            $value->save();
                            array_push($tmplvarIdsSaved, 137);
                        } else if ($value->tmplvarid == 138) {
                            if (is_null($request->description)) {
                                $request->description = '';
                            }
                            $value->value = $request->description;
                            $value->save();
                            array_push($tmplvarIdsSaved, 138);
                        } else if ($value->tmplvarid == 189) {
                            if (is_null($request->notranslate)) {
                                $request->notranslate = '';
                            }
                            $value->value = $request->notranslate;
                            $value->save();
                            array_push($tmplvarIdsSaved, 189);
                        } else if ($value->tmplvarid == 147) {
                            if (is_null($request->mult_sound)) {
                                $request->mult_sound = '';
                            }
                            $value->value = $request->mult_sound;
                            $value->save();
                            array_push($tmplvarIdsSaved, 147);
                        } else if ($value->tmplvarid == 143) {
                            if (is_null($request->mult_year)) {
                                $request->mult_year = '';
                            }
                            $value->value = $request->mult_year;
                            $value->save();
                            array_push($tmplvarIdsSaved, 143);
                        } else if ($value->tmplvarid == 144) {
                            if (is_null($request->mult_released)) {
                                $request->mult_released = '';
                            }
                            $value->value = $request->mult_released;
                            $value->save();
                            array_push($tmplvarIdsSaved, 144);
                        } else if ($value->tmplvarid == 145) {
                            if (is_null($request->slogan)) {
                                $request->slogan = '';
                            }
                            $value->value = $request->slogan;
                            $value->save();
                            array_push($tmplvarIdsSaved, 145);
                        } else if ($value->tmplvarid == 148) {
                            if (is_null($request->mult_quality)) {
                                $request->mult_quality = '';
                            }
                            $value->value = $request->mult_quality;
                            $value->save();
                            array_push($tmplvarIdsSaved, 148);
                        } else if ($value->tmplvarid == 149) {
                            if (is_null($request->mult_director)) {
                                $request->mult_director = '';
                            }
                            $value->value = $request->mult_director;
                            $value->save();
                            array_push($tmplvarIdsSaved, 149);
                        } else if ($value->tmplvarid == 154) {
                            if (is_null($request->studios)) {
                                $request->studios = '';
                            }
                            $value->value = $request->studios;
                            $value->save();
                            array_push($tmplvarIdsSaved, 154);
                        } else if ($value->tmplvarid == 155) {
                            if (is_null($request->agelimit)) {
                                $request->agelimit = '';
                            }
                            $value->value = $request->agelimit;
                            $value->save();
                            array_push($tmplvarIdsSaved, 155);
                        } else if ($value->tmplvarid == 156) {
                            if (is_null($request->original_title)) {
                                $request->original_title = '';
                            }
                            $value->value = $request->original_title;
                            $value->save();
                            array_push($tmplvarIdsSaved, 156);
                        } else if ($value->tmplvarid == 163) {
                            if (is_null($request->istok)) {
                                $request->istok = '';
                            } 
                            $value->value = $request->istok;
                            $value->save();
                            array_push($tmplvarIdsSaved, 163);
                        } else if ($value->tmplvarid == 171) {
                            if (is_null($request->typeposter)) {
                                $request->typeposter = '';
                            } 
                            $value->value = $request->typeposter;
                            $value->save();
                            array_push($tmplvarIdsSaved, 171);
                        } else if ($value->tmplvarid == 173) {
                            if (is_null($request->date_of_premiere)) {
                                $request->date_of_premiere = '';
                            } 
                            $value->value = $request->date_of_premiere;
                            $value->save();
                            array_push($tmplvarIdsSaved, 173);
                        } else if ($value->tmplvarid == 174) {
                            if (is_null($request->Lict_Razdel)) {
                                $request->Lict_Razdel = '';
                            } 
                            $value->value = $request->Lict_Razdel;
                            $value->save();
                            array_push($tmplvarIdsSaved, 174);
                        } else if ($value->tmplvarid == 179) {
                            if (is_null($request->mult_with_articles)) {
                                $request->mult_with_articles = '';
                            } 
                            $value->value = $request->mult_with_articles;
                            $value->save();
                            array_push($tmplvarIdsSaved, 179);
                        } else if ($value->tmplvarid == 181) {
                            if (is_null($request->custom_button)) {
                                $request->custom_button = '';
                            } 
                            $value->value = $request->custom_button;
                            $value->save();
                            array_push($tmplvarIdsSaved, 181);
                        } else if ($value->tmplvarid == 195) {
                            if (is_null($request->nooriginal)) {
                                $request->nooriginal = '';
                            } 
                            $value->value = $request->nooriginal;
                            $value->save();
                            array_push($tmplvarIdsSaved, 195);
                        }
                    }
                    if (count($tmplvarIdsSaved)) {
                        foreach ($tmplvarIds as $id) {
                            foreach ($tmplvarIdsSaved as $id2) {
                                unset($tmplvarIds[array_search($id2, $tmplvarIds)]);
                            }
                        } 
                        if (count($tmplvarIds)) {
                            foreach ($tmplvarIds as $id) {
                                if ($id == 145) {
                                    KpxzxSiteTmplvarContentvalue::create(
                                        [
                                            'tmplvarid'  => $id,
                                            'contentid'  => $mult->id,
                                            'value'  => $request->slogan
                                        ]
                                    );
                                } else if ($id == 148) {
                                    KpxzxSiteTmplvarContentvalue::create(
                                        [
                                            'tmplvarid'  => $id,
                                            'contentid'  => $mult->id,
                                            'value'  => $request->mult_quality
                                        ]
                                    );
                                } else if ($id == 149) {
                                    KpxzxSiteTmplvarContentvalue::create(
                                        [
                                            'tmplvarid'  => $id,
                                            'contentid'  => $mult->id,
                                            'value'  => $request->director
                                        ]
                                    );
                                }
                            }
                        }
                    }
                } else {
                    return '<span>Неверное значение параметра!</span>';
                }
            }
            return back();
        } else {
            return '<span>Права доступа отсутствуют!</span>';
        } 
    }

    public function deleteSerie(Request $request)
    {
        $itemId = $request->id;
        if (!is_null($itemId)) {
            $seasonSeries = KpxzxSiteContent::find($itemId);
            if (!$seasonSeries->deleted) {
                $seasonSeries->deleted = true;
                $seasonSeries->save();
            }
        }

        return back();
    }

    public function editResource(Request $request)
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('editResource')) {  
            $this->validatorEditResource($request->all())->validate();
            $itemId = $request->itemId;
            $parentId = $request->parent;
            if ((isset($itemId) && !is_null($itemId)) && (isset($parentId) && !is_null($parentId))) {
                $resource = KpxzxSiteContent::find($itemId);
                if (count($resource)) {
                    $resource->pagetitle = $request->pagetitle;
                    $resource->alias = $request->alias;
                    $resource->description = $request->description;
                    $resource->content = $request->editor1;
                    $resource->save();
                    $file = $request->main_file;
                    $posterPath = '';
                    if (!is_null($file)) {
                        $category = KpxzxSiteContent::find($parentId);
                        $categoryAlias = $category->alias;
                        $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                        if (!File::exists($dirPath)) {
                            File::makeDirectory($dirPath); 
                        }
                        $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                        $filename = $quote->heroy . '_' . $quote->id  . '_poster.' . $fileExtension;
                        if (!Input::file('main_file')->move($dirPath, $filename)) {
                            return 'Сообщение об ошибке!';
                        }

                        $posterPath = '/public/images/' . $categoryAlias . '/' . $filename;
                    }

                    $file = $request->wide_file;
                    $posterPathWide = '';
                    if (!is_null($file)) {
                        $category = KpxzxSiteContent::find($parentId);
                        $categoryAlias = $category->alias;
                        $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                        if (!File::exists($dirPath)) {
                            File::makeDirectory($dirPath); 
                        }
                        $fileExtension = Input::file('wide_file')->getClientOriginalExtension();
                        $filename = $quote->heroy . '_' . $quote->id  . '_poster.' . $fileExtension;
                        if (!Input::file('wide_file')->move($dirPath, $filename)) {
                            return 'Сообщение об ошибке!';
                        }

                        $posterPathWide = '/public/images/' . $categoryAlias . '/' . $filename;
                    }

                    foreach ($resource->kpxzxsitetmplvarcontentvalue as $value2) {
                        if ($value2->tmplvarid == 142 && isset($posterPath)) {
                            $value2->value = $posterPath;
                            $value2->save();
                        } else if ($value2->tmplvarid == 129 && isset($posterPathWide)) {
                            $value2->value = $posterPathWide;
                            $value2->save();
                        } else if ($value2->tmplvarid == 163) {
                            if (is_null($request->istok)) {
                                $request->istok = '';
                            }
                            $value2->value = $request->istok;
                            $value2->save();
                        } else if ($value2->tmplvarid == 179) {
                            if (is_null($request->mult_with_articles)) {
                                $request->mult_with_articles = '';
                            }
                            $value2->value = $request->mult_with_articles;
                            $value2->save();
                        }
                    }

                    $afterSave = $request->afterSave;
                    if ($afterSave === 'edit') {
                        $request->session()->flash('link', $kpxzxSiteContent->uri);
                        return back()->withInput();
                    } else {
                         return redirect('/manager/resource?parent=' . $parentId);
                    }
                } else {
                    return '<span>Неверное значение параметра!</span>';
                }
            } else {
                return back();
            }
        } else {
           return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function editCompetition(Request $request)
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('editCompetition')) {  
            $this->validatorEditCompetition($request->all())->validate();
            $itemId = $request->itemId;
            $parentId = $request->parent;
            if ((isset($itemId) && !is_null($itemId)) && (isset($parentId) && !is_null($parentId))) {
                $resource = KpxzxSiteContent::find($itemId);
                if (count($resource)) {
                    $resource->pagetitle = $request->pagetitle;
                    $resource->alias = $request->alias;
                    $resource->content = $request->editor1;
                    $resource->save();

                    $file = $request->main_file;
                    $posterPath = '';
                    if (!is_null($file)) {
                        $category = KpxzxSiteContent::find($parentId);
                        $categoryAlias = $category->alias;
                        $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                        if (!File::exists($dirPath)) {
                            File::makeDirectory($dirPath); 
                        }
                        $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                        $filename = $quote->heroy . '_' . $quote->id  . '_poster.' . $fileExtension;
                        if (!Input::file('main_file')->move($dirPath, $filename)) {
                            return 'Сообщение об ошибке!';
                        }

                        $posterPath = '/public/images/' . $categoryAlias . '/' . $filename;
                    }

                    foreach ($resource->kpxzxsitetmplvarcontentvalue as $value2) {
                        if ($value2->tmplvarid == 142 && isset($posterPath)) {
                            $value2->value = $posterPath;
                            $value2->save();
                        } else if ($value2->tmplvarid == 163) {
                            if (is_null($request->istok)) {
                                $request->istok = '';
                            }
                            $value2->value = $request->istok;
                            $value2->save();
                        } else if ($value2->tmplvarid == 179) {
                            if (is_null($request->mult_with_articles)) {
                                $request->mult_with_articles = '';
                            }
                            $value2->value = $request->mult_with_articles;
                            $value2->save();
                        } else if ($value2->tmplvarid == 184) {
                            if (is_null($request->comp_start)) {
                                $request->comp_start = '';
                            }
                            $value2->value = $request->comp_start;
                            $value2->save();
                        } else if ($value2->tmplvarid == 183) {
                            if (is_null($request->comp_end)) {
                                $request->comp_end = '';
                            }
                            $value2->value = $request->comp_end;
                            $value2->save();
                        } else if ($value2->tmplvarid == 185) {
                            if (is_null($request->fond)) {
                                $request->fond = '';
                            }
                            $value2->value = $request->fond;
                            $value2->save();
                        }
                    }       
                    $afterSave = $request->afterSave;
                    if ($afterSave === 'edit') {
                        $request->session()->flash('link', $kpxzxSiteContent->uri);
                        return back()->withInput();
                    } else {
                        return redirect('/manager/competition');
                    }
                } else {
                    return '<span>Неверное значение параметра!</span>';
                }
            } else {
                return back();
            }
        } else {
           return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function editSmultLive(Request $request)
    {   
        if (true) {//if (!is_null($request->user()) && $request->user()->can('editSmultlive')) {  
            $this->validatorEditSmultLive($request->all())->validate();
            $itemId = $request->item_id;
            if (!is_null($itemId)) {       
                $item = KpxzxSmultlive::find($itemId);
                if (count($item)) {
                    $item->first_title = $request->first_title;
                    $item->second_title = $request->second_title;
                    $settings = 'color:#ffffff;' . $request->first_title_vertical . 'font-size:40;' .  $request->first_title_align . 'color:#ffffff;' . $request->second_title_vertical . 'font-size:32;' .  $request->second_title_align . '';

                    $item->settings = $settings;
                    $item->multId = $request->multId;
                    
                    $file = $request->poster;
                    $posterPath = '';
                    if (!is_null($file)) {
                        $category = KpxzxSiteContent::find($parentId);
                        $categoryAlias = $category->alias;
                        $dirPath = public_path() . '/images/' . $categoryAlias . '/';
                        if (!File::exists($dirPath)) {
                            File::makeDirectory($dirPath); 
                        }
                        $fileExtension = Input::file('main_file')->getClientOriginalExtension();
                        $filename = $quote->heroy . '_' . $quote->id  . '_poster.' . $fileExtension;
                        if (!Input::file('main_file')->move($dirPath, $filename)) {
                            return 'Сообщение об ошибке!';
                        }

                        $posterPath = '/public/images/' . $categoryAlias . '/' . $filename;

                        foreach ($resource->kpxzxsitetmplvarcontentvalue as $value2) {
                            if ($value2->tmplvarid == 142 && isset($posterPath)) {
                                $value2->value = $posterPath;
                                $value2->save();
                            }
                        }       
                    }
                    $item->save();
                    return back();
                } else {
                    return '<span>Неверное значение параметра!</span>';
                }
            } else {
                return redirect('/manager/smultlive');
            }
        } else {
            return '<span>Права доступа отсутствуют!</span>';
        }
    }

    public function addCallback(Request $request)
    {   
        return back();
    }

    public function sendValidation(Request $request)
    {
        $textUID = NULL;
        $urlKey = '2d57751a6dea40c86c26311e1e252181';
        $text = $request->text;
        $result = $this->addPost($text, $urlKey);
        $json = json_decode($result);
        if (isset($json->text_uid)) {                
            while (true) {
                $result = $this->getResultPost($json->text_uid, $urlKey);
                if (isset(json_decode($result)->text_unique)) {
                    echo 'Текст уникален на ' . json_decode($result)->text_unique . '%';
                    break;
                }
            }
        } else if (isset($json->error_code)) {
            echo $json->error_desc;
        }
    }

    public function addPost($text = NULL, $urlKey = NULL)
    {
        $textUID = NULL;
        $postQuery = array();
        $postQuery['text'] = $text;
        $postQuery['userkey'] = $urlKey;

        $postQuery['exceptdomain'] = "site1.ru, site2.ru, site3.ru";

        $postQuery = http_build_query($postQuery, '', '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://api.text.ru/post');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postQuery);
        $textUID = curl_exec($ch);
        $errno = curl_errno($ch);

        curl_close($ch);

        if (!$errno)
        {
            return $textUID;
        }
        else
        {
            return $errno;
        }    
    }   

    function getResultPost($textUID = NULL, $userKey = NULL)
    {
        while (true) {
            $result = NULL;
            $postQuery = array();
            $postQuery['uid'] = $textUID;
            $postQuery['userkey'] = $userKey;

            $postQuery = http_build_query($postQuery, '', '&');          

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://api.text.ru/post');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postQuery);
            $result = curl_exec($ch);
            $errno = curl_errno($ch);
            
            curl_close($ch);

            if ((isset(json_decode($result)->error_code) && json_decode($result)->error_code != 181) || empty(json_decode($result)->error_code)) {
                return $result;
            }
        }
    }
}