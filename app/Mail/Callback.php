<?php

namespace App\Mail;

class Callback extends Email
{   
    public function build()
    {
        return $this->from('slime1410@gmail.com')
                    ->view('mail.callback')
                    ->text('mail.callback_text_plain');
    }
}
