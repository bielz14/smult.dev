<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMediaSourcesContext
 * 
 * @property int $source
 * @property string $context_key
 *
 * @package App\Models
 */
class KpxzxMediaSourcesContext extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'source' => 'int'
	];
}
