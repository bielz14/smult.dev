<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxElementPropertySet
 * 
 * @property int $element
 * @property string $element_class
 * @property int $property_set
 *
 * @package App\Models
 */
class KpxzxElementPropertySet extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'element' => 'int',
		'property_set' => 'int'
	];
}
