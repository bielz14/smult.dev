<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxFcSet
 * 
 * @property int $id
 * @property int $profile
 * @property string $action
 * @property string $description
 * @property bool $active
 * @property int $template
 * @property string $constraint
 * @property string $constraint_field
 * @property string $constraint_class
 *
 * @package App\Models
 */
class KpxzxFcSet extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'profile' => 'int',
		'active' => 'bool',
		'template' => 'int'
	];

	protected $fillable = [
		'profile',
		'action',
		'description',
		'active',
		'template',
		'constraint',
		'constraint_field',
		'constraint_class'
	];
}
