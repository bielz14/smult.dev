<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxRegisterTopic
 * 
 * @property int $id
 * @property int $queue
 * @property string $name
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $updated
 * @property string $options
 *
 * @package App\Models
 */
class KpxzxRegisterTopic extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'queue' => 'int'
	];

	protected $dates = [
		'created',
		'updated'
	];

	protected $fillable = [
		'queue',
		'name',
		'created',
		'updated',
		'options'
	];
}
