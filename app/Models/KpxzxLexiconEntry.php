<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxLexiconEntry
 * 
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $topic
 * @property string $namespace
 * @property string $language
 * @property \Carbon\Carbon $createdon
 * @property \Carbon\Carbon $editedon
 *
 * @package App\Models
 */
class KpxzxLexiconEntry extends Eloquent
{
	public $timestamps = false;

	protected $dates = [
		'createdon',
		'editedon'
	];

	protected $fillable = [
		'name',
		'value',
		'topic',
		'namespace',
		'language',
		'createdon',
		'editedon'
	];
}
