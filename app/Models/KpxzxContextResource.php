<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxContextResource
 * 
 * @property string $context_key
 * @property int $resource
 *
 * @package App\Models
 */
class KpxzxContextResource extends Eloquent
{
	protected $table = 'kpxzx_context_resource';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'resource' => 'int'
	];
}
