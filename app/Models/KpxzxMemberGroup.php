<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMemberGroup
 * 
 * @property int $id
 * @property int $user_group
 * @property int $member
 * @property int $role
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxMemberGroup extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'user_group' => 'int',
		'member' => 'int',
		'role' => 'int',
		'rank' => 'int'
	];

	protected $fillable = [
		'user_group',
		'member',
		'role',
		'rank'
	];
}
