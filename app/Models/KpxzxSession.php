<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSession
 * 
 * @property string $id
 * @property int $access
 * @property string $data
 *
 * @package App\Models
 */
class KpxzxSession extends Eloquent
{
	protected $table = 'kpxzx_session';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'access' => 'int'
	];

	protected $fillable = [
		'access',
		'data'
	];
}
