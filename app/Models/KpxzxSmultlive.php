<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSmultlive
 * 
 * @property int $id
 * @property string $first_title
 * @property string $second_title
 * @property string $settings
 * @property int $multId
 * @property string $poster
 * @property int $createdby
 * @property int $createdon
 * @property int $editedby
 * @property int $editedon
 *
 * @package App\Models
 */
class KpxzxSmultlive extends Eloquent
{
	protected $table = 'kpxzx_smultlive';
	public $timestamps = false;

	protected $casts = [
		'multId' => 'int',
		'createdby' => 'int',
		'createdon' => 'int',
		'editedby' => 'int',
		'editedon' => 'int'
	];

	protected $fillable = [
		'first_title',
		'second_title',
		'settings',
		'multId',
		'poster',
		'createdby',
		'createdon',
		'editedby',
		'editedon'
	];
}
