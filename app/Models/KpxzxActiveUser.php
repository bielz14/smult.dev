<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxActiveUser
 * 
 * @property int $internalKey
 * @property string $username
 * @property int $lasthit
 * @property int $id
 * @property string $action
 * @property string $ip
 *
 * @package App\Models
 */
class KpxzxActiveUser extends Eloquent
{
	protected $primaryKey = 'internalKey';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'internalKey' => 'int',
		'lasthit' => 'int',
		'id' => 'int'
	];

	protected $fillable = [
		'username',
		'lasthit',
		'id',
		'action',
		'ip'
	];
}
