<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxActiondom
 * 
 * @property int $id
 * @property int $set
 * @property string $action
 * @property string $name
 * @property string $description
 * @property string $xtype
 * @property string $container
 * @property string $rule
 * @property string $value
 * @property string $constraint
 * @property string $constraint_field
 * @property string $constraint_class
 * @property bool $active
 * @property bool $for_parent
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxActiondom extends Eloquent
{
	protected $table = 'kpxzx_actiondom';
	public $timestamps = false;

	protected $casts = [
		'set' => 'int',
		'active' => 'bool',
		'for_parent' => 'bool',
		'rank' => 'int'
	];

	protected $fillable = [
		'set',
		'action',
		'name',
		'description',
		'xtype',
		'container',
		'rule',
		'value',
		'constraint',
		'constraint_field',
		'constraint_class',
		'active',
		'for_parent',
		'rank'
	];
}
