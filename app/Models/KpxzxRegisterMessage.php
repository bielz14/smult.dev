<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxRegisterMessage
 * 
 * @property int $topic
 * @property string $id
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $valid
 * @property \Carbon\Carbon $accessed
 * @property int $accesses
 * @property int $expires
 * @property string $payload
 * @property bool $kill
 *
 * @package App\Models
 */
class KpxzxRegisterMessage extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'topic' => 'int',
		'accesses' => 'int',
		'expires' => 'int',
		'kill' => 'bool'
	];

	protected $dates = [
		'created',
		'valid',
		'accessed'
	];

	protected $fillable = [
		'created',
		'valid',
		'accessed',
		'accesses',
		'expires',
		'payload',
		'kill'
	];
}
