<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMenu
 * 
 * @property string $text
 * @property string $parent
 * @property string $action
 * @property string $description
 * @property string $icon
 * @property int $menuindex
 * @property string $params
 * @property string $handler
 * @property string $permissions
 * @property string $namespace
 *
 * @package App\Models
 */
class KpxzxMenu extends Eloquent
{
	protected $primaryKey = 'text';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'menuindex' => 'int'
	];

	protected $fillable = [
		'parent',
		'action',
		'description',
		'icon',
		'menuindex',
		'params',
		'handler',
		'permissions',
		'namespace'
	];
}
