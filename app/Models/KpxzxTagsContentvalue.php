<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:12 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTagsContentvalue
 * 
 * @property int $date
 * @property int $id
 * @property int $contentid
 * @property int $tagsid
 *
 * @package App\Models
 */
class KpxzxTagsContentvalue extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'date' => 'int',
		'contentid' => 'int',
		'tagsid' => 'int'
	];

	protected $fillable = [
		'date',
		'contentid',
		'tagsid'
	];
}
