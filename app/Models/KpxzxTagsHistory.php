<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:12 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTagsHistory
 * 
 * @property int $id
 * @property int $contentid
 * @property int $tagsid
 * @property string $tagsvalue
 * @property string $tagsname
 * @property int $date
 *
 * @package App\Models
 */
class KpxzxTagsHistory extends Eloquent
{
	protected $table = 'kpxzx_tags_history';
	public $timestamps = false;

	protected $casts = [
		'contentid' => 'int',
		'tagsid' => 'int',
		'date' => 'int'
	];

	protected $fillable = [
		'contentid',
		'tagsid',
		'tagsvalue',
		'tagsname',
		'date'
	];
}
