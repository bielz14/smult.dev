<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTextValidateLog
 * 
 * @property int $id
 * @property int $createdby
 * @property int $createdon
 * @property string $content
 * @property int $length
 * @property int $text_unique
 * @property string $pagetitle
 * @property int $error_code
 * @property int $remainder
 *
 * @package App\Models
 */
class KpxzxTextValidateLog extends Eloquent
{
	protected $table = 'kpxzx_text_validate_log';
	public $timestamps = false;

	protected $casts = [
		'createdby' => 'int',
		'createdon' => 'int',
		'length' => 'int',
		'text_unique' => 'int',
		'error_code' => 'int',
		'remainder' => 'int'
	];

	protected $fillable = [
		'createdby',
		'createdon',
		'content',
		'length',
		'text_unique',
		'pagetitle',
		'error_code',
		'remainder'
	];
}
