<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxDashboardWidget
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $content
 * @property string $namespace
 * @property string $lexicon
 * @property string $size
 *
 * @package App\Models
 */
class KpxzxDashboardWidget extends Eloquent
{
	protected $table = 'kpxzx_dashboard_widget';
	public $timestamps = false;

	protected $fillable = [
		'name',
		'description',
		'type',
		'content',
		'namespace',
		'lexicon',
		'size'
	];
}
