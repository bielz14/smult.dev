<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTestZapotv
 * 
 * @property int $id
 * @property int $numb
 * @property int $contentid
 * @property string $test_type
 * @property string $pagetitle
 * @property string $content
 * @property string $doptext
 * @property string $poster
 * @property string $var
 *
 * @package App\Models
 */
class KpxzxTestZapotv extends Eloquent
{
	protected $table = 'kpxzx_test_zapotv';
	public $timestamps = false;

	protected $casts = [
		'numb' => 'int',
		'contentid' => 'int'
	];

	protected $fillable = [
		'numb',
		'contentid',
		'test_type',
		'pagetitle',
		'content',
		'doptext',
		'poster',
		'var'
	];
}
