<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxRegisterQueue
 * 
 * @property int $id
 * @property string $name
 * @property string $options
 *
 * @package App\Models
 */
class KpxzxRegisterQueue extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'options'
	];
}
