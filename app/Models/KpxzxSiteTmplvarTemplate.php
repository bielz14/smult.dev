<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteTmplvarTemplate
 * 
 * @property int $tmplvarid
 * @property int $templateid
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxSiteTmplvarTemplate extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'tmplvarid' => 'int',
		'templateid' => 'int',
		'rank' => 'int'
	];

	protected $fillable = [
		'rank'
	];
}
