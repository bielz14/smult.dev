<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxUser
 * 
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $cachepwd
 * @property string $class_key
 * @property bool $active
 * @property string $remote_key
 * @property string $remote_data
 * @property string $hash_class
 * @property string $salt
 * @property int $primary_group
 * @property string $session_stale
 * @property bool $sudo
 *
 * @package App\Models
 */
class KpxzxUser extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'active' => 'bool',
		'primary_group' => 'int',
		'sudo' => 'bool'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'username',
		'password',
		'cachepwd',
		'class_key',
		'active',
		'remote_key',
		'remote_data',
		'hash_class',
		'salt',
		'primary_group',
		'session_stale',
		'sudo'
	];

	public function kpxzxusergrouprole()
    {
    	return $this->belongsTo('App\Models\KpxzxUserGroupRole', 'primary_group');
    }
}
