<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxManagerLog
 * 
 * @property int $id
 * @property int $user
 * @property \Carbon\Carbon $occurred
 * @property string $action
 * @property string $classKey
 * @property string $item
 *
 * @package App\Models
 */
class KpxzxManagerLog extends Eloquent
{
	protected $table = 'kpxzx_manager_log';
	public $timestamps = false;

	protected $casts = [
		'user' => 'int'
	];

	protected $dates = [
		'occurred'
	];

	protected $fillable = [
		'user',
		'occurred',
		'action',
		'classKey',
		'item'
	];
}
