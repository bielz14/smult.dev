<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteTmplvarContentvalue
 * 
 * @property int $id
 * @property int $tmplvarid
 * @property int $contentid
 * @property string $value
 *
 * @package App\Models
 */
class KpxzxSiteTmplvarContentvalue extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'tmplvarid' => 'int',
		'contentid' => 'int'
	];

	protected $fillable = [
		'tmplvarid',
		'contentid',
		'value'
	];

	public function kpxzxsitecontent()
    {
    	return $this->belongsTo('App\Models\KpxzxSiteContent', 'contentid');
    }

    public function kpxzxsitetmplvar()
    {
    	return $this->belongsTo('App\Models\KpxzxSiteTmplvar', 'tmplvarid');
    }

    public function kpxzxmultpostersmusic()
	{
	    return $this->belongsTo('App\Models\KpxzxMultPostersMusic', 'contentid');
	}
}
