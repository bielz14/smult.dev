<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMultUpdate
 * 
 * @property int $id
 * @property int $contentid
 * @property int $type
 * @property int $date
 * @property int $id_for_uri
 * @property int $res_type
 * @property string $serial_text
 *
 * @package App\Models
 */
class KpxzxMultUpdate extends Eloquent
{
	protected $table = 'kpxzx_mult_update';
	public $timestamps = false;

	protected $casts = [
		'contentid' => 'int',
		'type' => 'int',
		'date' => 'int',
		'id_for_uri' => 'int',
		'res_type' => 'int'
	];

	protected $fillable = [
		'contentid',
		'type',
		'date',
		'id_for_uri',
		'res_type',
		'serial_text'
	];
}
