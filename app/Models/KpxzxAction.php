<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxAction
 * 
 * @property int $id
 * @property string $namespace
 * @property string $controller
 * @property bool $haslayout
 * @property string $lang_topics
 * @property string $assets
 * @property string $help_url
 *
 * @package App\Models
 */
class KpxzxAction extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'haslayout' => 'bool'
	];

	protected $fillable = [
		'namespace',
		'controller',
		'haslayout',
		'lang_topics',
		'assets',
		'help_url'
	];
}
