<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxUserMessage
 * 
 * @property int $id
 * @property string $type
 * @property string $subject
 * @property string $message
 * @property int $sender
 * @property int $recipient
 * @property int $private
 * @property \Carbon\Carbon $date_sent
 * @property bool $read
 *
 * @package App\Models
 */
class KpxzxUserMessage extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'sender' => 'int',
		'recipient' => 'int',
		'private' => 'int',
		'read' => 'bool'
	];

	protected $dates = [
		'date_sent'
	];

	protected $fillable = [
		'type',
		'subject',
		'message',
		'sender',
		'recipient',
		'private',
		'date_sent',
		'read'
	];
}
