<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxAccessTemplatevar
 * 
 * @property int $id
 * @property string $target
 * @property string $principal_class
 * @property int $principal
 * @property int $authority
 * @property int $policy
 * @property string $context_key
 *
 * @package App\Models
 */
class KpxzxAccessTemplatevar extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'principal' => 'int',
		'authority' => 'int',
		'policy' => 'int'
	];

	protected $fillable = [
		'target',
		'principal_class',
		'principal',
		'authority',
		'policy',
		'context_key'
	];
}
