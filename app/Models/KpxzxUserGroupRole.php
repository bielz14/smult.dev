<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxUserGroupRole
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $authority
 *
 * @package App\Models
 */
class KpxzxUserGroupRole extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'authority' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'authority'
	];

	public function kpxzxuser()
    {
    	return $this->hasMany('App\Models\KpxzxUser', 'primary_group');
    }
}
