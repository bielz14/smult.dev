<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxWinner
 * 
 * @property int $id
 * @property int $contentid
 * @property int $winnertype
 * @property string $type
 * @property string $name
 * @property string $comment
 * @property string $poster
 * @property int $createdby
 * @property int $createdon
 * @property string $uri
 * @property int $deleted
 *
 * @package App\Models
 */
class KpxzxWinner extends Eloquent
{
	protected $table = 'kpxzx_winner';
	public $timestamps = false;

	protected $casts = [
		'contentid' => 'int',
		'winnertype' => 'int',
		'createdby' => 'int',
		'createdon' => 'int',
		'deleted' => 'int'
	];

	protected $fillable = [
		'contentid',
		'winnertype',
		'type',
		'name',
		'comment',
		'poster',
		'createdby',
		'createdon',
		'uri',
		'deleted'
	];

	public function kpxzxsitecontent()
    {
    	return $this->belongsTo('App\Models\KpxzxSiteContent', 'contentid');
    }
}
