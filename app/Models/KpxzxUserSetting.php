<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxUserSetting
 * 
 * @property int $user
 * @property string $key
 * @property string $value
 * @property string $xtype
 * @property string $namespace
 * @property string $area
 * @property \Carbon\Carbon $editedon
 *
 * @package App\Models
 */
class KpxzxUserSetting extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'user' => 'int'
	];

	protected $dates = [
		'editedon'
	];

	protected $fillable = [
		'value',
		'xtype',
		'namespace',
		'area',
		'editedon'
	];
}
