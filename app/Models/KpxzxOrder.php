<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxOrder
 * 
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $mail
 * @property string $comment
 * @property \Carbon\Carbon $dateAdd
 * @property \Carbon\Carbon $dateExecution
 * @property string $href
 * @property string $comment_completed
 * @property int $order_status
 * @property string $postertype
 *
 * @package App\Models
 */
class KpxzxOrder extends Eloquent
{
	protected $table = 'kpxzx_order';
	public $timestamps = false;

	protected $casts = [
		'order_status' => 'int'
	];

	protected $dates = [
		'dateAdd',
		'dateExecution'
	];

	protected $fillable = [
		'name',
		'title',
		'mail',
		'comment',
		'dateAdd',
		'dateExecution',
		'href',
		'comment_completed',
		'order_status',
		'postertype'
	];
}
