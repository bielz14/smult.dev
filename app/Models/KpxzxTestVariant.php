<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTestVariant
 * 
 * @property int $id
 * @property int $contentid
 * @property string $value
 * @property int $kil
 *
 * @package App\Models
 */
class KpxzxTestVariant extends Eloquent
{
	protected $table = 'kpxzx_test_variant';
	public $timestamps = false;

	protected $casts = [
		'contentid' => 'int',
		'kil' => 'int'
	];

	protected $fillable = [
		'contentid',
		'value',
		'kil'
	];
}
