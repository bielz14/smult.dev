<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxAccessPolicy
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $parent
 * @property int $template
 * @property string $class
 * @property string $data
 * @property string $lexicon
 *
 * @package App\Models
 */
class KpxzxAccessPolicy extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'parent' => 'int',
		'template' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'parent',
		'template',
		'class',
		'data',
		'lexicon'
	];
}
