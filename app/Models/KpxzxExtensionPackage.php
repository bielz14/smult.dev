<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxExtensionPackage
 * 
 * @property int $id
 * @property string $namespace
 * @property string $name
 * @property string $path
 * @property string $table_prefix
 * @property string $service_class
 * @property string $service_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class KpxzxExtensionPackage extends Eloquent
{
	protected $fillable = [
		'namespace',
		'name',
		'path',
		'table_prefix',
		'service_class',
		'service_name'
	];
}
