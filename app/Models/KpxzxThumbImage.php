<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxThumbImage
 * 
 * @property int $id
 * @property string $image
 * @property string $cache_image
 * @property string $config
 * @property bool $isend
 *
 * @package App\Models
 */
class KpxzxThumbImage extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'isend' => 'bool'
	];

	protected $fillable = [
		'image',
		'cache_image',
		'config',
		'isend'
	];
}
