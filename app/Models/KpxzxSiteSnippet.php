<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteSnippet
 * 
 * @property int $id
 * @property int $source
 * @property bool $property_preprocess
 * @property string $name
 * @property string $description
 * @property int $editor_type
 * @property int $category
 * @property bool $cache_type
 * @property string $snippet
 * @property bool $locked
 * @property string $properties
 * @property string $moduleguid
 * @property bool $static
 * @property string $static_file
 *
 * @package App\Models
 */
class KpxzxSiteSnippet extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'source' => 'int',
		'property_preprocess' => 'bool',
		'editor_type' => 'int',
		'category' => 'int',
		'cache_type' => 'bool',
		'locked' => 'bool',
		'static' => 'bool'
	];

	protected $fillable = [
		'source',
		'property_preprocess',
		'name',
		'description',
		'editor_type',
		'category',
		'cache_type',
		'snippet',
		'locked',
		'properties',
		'moduleguid',
		'static',
		'static_file'
	];
}
