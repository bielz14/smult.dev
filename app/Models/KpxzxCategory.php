<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxCategory
 * 
 * @property int $id
 * @property int $parent
 * @property string $category
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxCategory extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'parent' => 'int',
		'rank' => 'int'
	];

	protected $fillable = [
		'parent',
		'category',
		'rank'
	];
}
