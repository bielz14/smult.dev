<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxActionsField
 * 
 * @property int $id
 * @property string $action
 * @property string $name
 * @property string $type
 * @property string $tab
 * @property string $form
 * @property string $other
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxActionsField extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'rank' => 'int'
	];

	protected $fillable = [
		'action',
		'name',
		'type',
		'tab',
		'form',
		'other',
		'rank'
	];
}
