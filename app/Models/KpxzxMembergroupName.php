<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMembergroupName
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $parent
 * @property int $rank
 * @property int $dashboard
 *
 * @package App\Models
 */
class KpxzxMembergroupName extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'parent' => 'int',
		'rank' => 'int',
		'dashboard' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'parent',
		'rank',
		'dashboard'
	];
}
