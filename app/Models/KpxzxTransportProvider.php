<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTransportProvider
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $service_url
 * @property string $username
 * @property string $api_key
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $updated
 * @property bool $active
 * @property int $priority
 * @property string $properties
 *
 * @package App\Models
 */
class KpxzxTransportProvider extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'active' => 'bool',
		'priority' => 'int'
	];

	protected $dates = [
		'created',
		'updated'
	];

	protected $fillable = [
		'name',
		'description',
		'service_url',
		'username',
		'api_key',
		'created',
		'updated',
		'active',
		'priority',
		'properties'
	];
}
