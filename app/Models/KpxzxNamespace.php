<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxNamespace
 * 
 * @property string $name
 * @property string $path
 * @property string $assets_path
 *
 * @package App\Models
 */
class KpxzxNamespace extends Eloquent
{
	protected $primaryKey = 'name';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'path',
		'assets_path'
	];
}
