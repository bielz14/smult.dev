<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSeriesSort
 * 
 * @property int $id
 * @property int $contentid
 * @property int $posit
 *
 * @package App\Models
 */
class KpxzxSeriesSort extends Eloquent
{
	protected $table = 'kpxzx_series_sort';
	public $timestamps = false;

	protected $casts = [
		'contentid' => 'int',
		'posit' => 'int'
	];

	protected $fillable = [
		'contentid',
		'posit'
	];
}
