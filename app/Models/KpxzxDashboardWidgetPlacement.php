<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxDashboardWidgetPlacement
 * 
 * @property int $dashboard
 * @property int $widget
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxDashboardWidgetPlacement extends Eloquent
{
	protected $table = 'kpxzx_dashboard_widget_placement';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'dashboard' => 'int',
		'widget' => 'int',
		'rank' => 'int'
	];

	protected $fillable = [
		'rank'
	];
}
