<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxClassMap
 * 
 * @property int $id
 * @property string $class
 * @property string $parent_class
 * @property string $name_field
 * @property string $path
 * @property string $lexicon
 *
 * @package App\Models
 */
class KpxzxClassMap extends Eloquent
{
	protected $table = 'kpxzx_class_map';
	public $timestamps = false;

	protected $fillable = [
		'class',
		'parent_class',
		'name_field',
		'path',
		'lexicon'
	];
}
