<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMediaSource
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $class_key
 * @property string $properties
 * @property bool $is_stream
 *
 * @package App\Models
 */
class KpxzxMediaSource extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'is_stream' => 'bool'
	];

	protected $fillable = [
		'name',
		'description',
		'class_key',
		'properties',
		'is_stream'
	];
}
