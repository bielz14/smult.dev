<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxAccessPermission
 * 
 * @property int $id
 * @property int $template
 * @property string $name
 * @property string $description
 * @property bool $value
 *
 * @package App\Models
 */
class KpxzxAccessPermission extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'template' => 'int',
		'value' => 'bool'
	];

	protected $fillable = [
		'template',
		'name',
		'description',
		'value'
	];
}
