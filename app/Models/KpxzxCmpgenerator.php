<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxCmpgenerator
 * 
 * @property int $id
 * @property string $package
 * @property string $database
 * @property string $tables
 * @property string $table_prefix
 * @property int $build_scheme
 * @property int $build_package
 * @property \Carbon\Carbon $create_date
 * @property \Carbon\Carbon $last_ran
 *
 * @package App\Models
 */
class KpxzxCmpgenerator extends Eloquent
{
	protected $table = 'kpxzx_cmpgenerator';
	public $timestamps = false;

	protected $casts = [
		'build_scheme' => 'int',
		'build_package' => 'int'
	];

	protected $dates = [
		'create_date',
		'last_ran'
	];

	protected $fillable = [
		'package',
		'database',
		'tables',
		'table_prefix',
		'build_scheme',
		'build_package',
		'create_date',
		'last_ran'
	];
}
