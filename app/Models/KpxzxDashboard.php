<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxDashboard
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property bool $hide_trees
 *
 * @package App\Models
 */
class KpxzxDashboard extends Eloquent
{
	protected $table = 'kpxzx_dashboard';
	public $timestamps = false;

	protected $casts = [
		'hide_trees' => 'bool'
	];

	protected $fillable = [
		'name',
		'description',
		'hide_trees'
	];
}
