<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxContextSetting
 * 
 * @property string $context_key
 * @property string $key
 * @property string $value
 * @property string $xtype
 * @property string $namespace
 * @property string $area
 * @property \Carbon\Carbon $editedon
 *
 * @package App\Models
 */
class KpxzxContextSetting extends Eloquent
{
	protected $table = 'kpxzx_context_setting';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'editedon'
	];

	protected $fillable = [
		'value',
		'xtype',
		'namespace',
		'area',
		'editedon'
	];
}
