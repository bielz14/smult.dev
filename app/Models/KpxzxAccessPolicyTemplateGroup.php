<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxAccessPolicyTemplateGroup
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @package App\Models
 */
class KpxzxAccessPolicyTemplateGroup extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'description'
	];
}
