<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxContext
 * 
 * @property string $key
 * @property string $name
 * @property string $description
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxContext extends Eloquent
{
	protected $table = 'kpxzx_context';
	protected $primaryKey = 'key';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'rank' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'rank'
	];
}
