<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxDocumentGroup
 * 
 * @property int $id
 * @property int $document_group
 * @property int $document
 *
 * @package App\Models
 */
class KpxzxDocumentGroup extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'document_group' => 'int',
		'document' => 'int'
	];

	protected $fillable = [
		'document_group',
		'document'
	];
}
