<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxDocumentgroupName
 * 
 * @property int $id
 * @property string $name
 * @property bool $private_memgroup
 * @property bool $private_webgroup
 *
 * @package App\Models
 */
class KpxzxDocumentgroupName extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'private_memgroup' => 'bool',
		'private_webgroup' => 'bool'
	];

	protected $fillable = [
		'name',
		'private_memgroup',
		'private_webgroup'
	];
}
