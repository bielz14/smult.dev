<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMultUpdateCreatedon
 * 
 * @property int $id
 * @property int $contentid
 * @property int $createdon
 *
 * @package App\Models
 */
class KpxzxMultUpdateCreatedon extends Eloquent
{
	protected $table = 'kpxzx_mult_update_createdon';
	public $timestamps = false;

	protected $casts = [
		'contentid' => 'int',
		'createdon' => 'int'
	];

	protected $fillable = [
		'contentid',
		'createdon'
	];
}
