<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:12 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSystemSetting
 * 
 * @property string $key
 * @property string $value
 * @property string $xtype
 * @property string $namespace
 * @property string $area
 * @property \Carbon\Carbon $editedon
 *
 * @package App\Models
 */
class KpxzxSystemSetting extends Eloquent
{
	protected $primaryKey = 'key';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'editedon'
	];

	protected $fillable = [
		'value',
		'xtype',
		'namespace',
		'area',
		'editedon'
	];
}
