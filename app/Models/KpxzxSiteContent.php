<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteContent
 * 
 * @property int $id
 * @property string $type
 * @property string $contentType
 * @property string $pagetitle
 * @property string $longtitle
 * @property string $description
 * @property string $alias
 * @property string $link_attributes
 * @property bool $published
 * @property int $pub_date
 * @property int $unpub_date
 * @property int $parent
 * @property bool $isfolder
 * @property string $introtext
 * @property string $content
 * @property bool $richtext
 * @property int $template
 * @property int $menuindex
 * @property bool $searchable
 * @property bool $cacheable
 * @property int $createdby
 * @property int $createdon
 * @property int $editedby
 * @property int $editedon
 * @property bool $deleted
 * @property int $deletedon
 * @property int $deletedby
 * @property int $publishedon
 * @property int $publishedby
 * @property string $menutitle
 * @property bool $donthit
 * @property bool $privateweb
 * @property bool $privatemgr
 * @property bool $content_dispo
 * @property bool $hidemenu
 * @property string $class_key
 * @property string $context_key
 * @property int $content_type
 * @property string $uri
 * @property bool $uri_override
 * @property bool $hide_children_in_tree
 * @property bool $show_in_tree
 * @property string $properties
 *
 * @package App\Models
 */
class KpxzxSiteContent extends Eloquent
{
	protected $table = 'kpxzx_site_content';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'published' => 'bool',
		'pub_date' => 'int',
		'unpub_date' => 'int',
		'parent' => 'int',
		'isfolder' => 'bool',
		'richtext' => 'bool',
		'template' => 'int',
		'menuindex' => 'int',
		'searchable' => 'bool',
		'cacheable' => 'bool',
		'createdby' => 'int',
		'createdon' => 'int',
		'editedby' => 'int',
		'editedon' => 'int',
		'deleted' => 'bool',
		'deletedon' => 'int',
		'deletedby' => 'int',
		'publishedon' => 'int',
		'publishedby' => 'int',
		'donthit' => 'bool',
		'privateweb' => 'bool',
		'privatemgr' => 'bool',
		'content_dispo' => 'bool',
		'hidemenu' => 'bool',
		'content_type' => 'int',
		'uri_override' => 'bool',
		'hide_children_in_tree' => 'bool',
		'show_in_tree' => 'bool'
	];

	protected $fillable = [
		'type',
		'contentType',
		'pagetitle',
		'longtitle',
		'description',
		'alias',
		'link_attributes',
		'published',
		'pub_date',
		'unpub_date',
		'parent',
		'isfolder',
		'introtext',
		'content',
		'richtext',
		'template',
		'menuindex',
		'searchable',
		'cacheable',
		'createdby',
		'createdon',
		'editedby',
		'editedon',
		'deleted',
		'deletedon',
		'deletedby',
		'publishedon',
		'publishedby',
		'menutitle',
		'donthit',
		'privateweb',
		'privatemgr',
		'content_dispo',
		'hidemenu',
		'class_key',
		'context_key',
		'content_type',
		'uri',
		'uri_override',
		'hide_children_in_tree',
		'show_in_tree',
		'properties'
	];

	public function kpxzxsitetmplvarcontentvalue()
    {
    	return $this->hasMany('App\Models\KpxzxSiteTmplvarContentvalue', 'contentid');
    }

    public function kpxzxcomment()
    {
    	return $this->hasMany('App\Models\KpxzxComment', 'content_id');
    }

    public function kpxzxticket()
    {
    	return $this->hasMany('App\Models\KpxzxTicket', 'contentid');
    }

    public function kpxzxwinner()
    {
    	return $this->hasMany('App\Models\KpxzxWinner', 'contentid');
    }

    public function kpxzxmultpostersmusic()
	{
	    return $this->hasMany('App\Models\KpxzxMultPostersMusic', 'multId');
	}
}
