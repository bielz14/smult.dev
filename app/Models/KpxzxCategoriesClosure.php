<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxCategoriesClosure
 * 
 * @property int $ancestor
 * @property int $descendant
 * @property int $depth
 *
 * @package App\Models
 */
class KpxzxCategoriesClosure extends Eloquent
{
	protected $table = 'kpxzx_categories_closure';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ancestor' => 'int',
		'descendant' => 'int',
		'depth' => 'int'
	];

	protected $fillable = [
		'depth'
	];
}
