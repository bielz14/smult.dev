<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSystemEventname
 * 
 * @property string $name
 * @property int $service
 * @property string $groupname
 *
 * @package App\Models
 */
class KpxzxSystemEventname extends Eloquent
{
	protected $primaryKey = 'name';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'service' => 'int'
	];

	protected $fillable = [
		'service',
		'groupname'
	];
}
