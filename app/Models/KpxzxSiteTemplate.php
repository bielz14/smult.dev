<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteTemplate
 * 
 * @property int $id
 * @property int $source
 * @property bool $property_preprocess
 * @property string $templatename
 * @property string $description0
 * @property int $editor_type
 * @property int $category
 * @property string $icon
 * @property int $template_type
 * @property string $content
 * @property bool $locked
 * @property string $properties
 * @property bool $static
 * @property string $static_file
 *
 * @package App\Models
 */
class KpxzxSiteTemplate extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'source' => 'int',
		'property_preprocess' => 'bool',
		'editor_type' => 'int',
		'category' => 'int',
		'template_type' => 'int',
		'locked' => 'bool',
		'static' => 'bool'
	];

	protected $fillable = [
		'source',
		'property_preprocess',
		'templatename',
		'description',
		'editor_type',
		'category',
		'icon',
		'template_type',
		'content',
		'locked',
		'properties',
		'static',
		'static_file'
	];
}
