<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxQuoteItem
 * 
 * @property int $id
 * @property string $heroy
 * @property string $heroy_uri
 * @property string $mult
 * @property string $mult_uri
 * @property string $poster
 * @property string $text
 * @property bool $published
 * @property int $createdby
 * @property int $createdon
 * @property int $editedby
 * @property int $editedon
 *
 * @package App\Models
 */
class KpxzxQuote extends Eloquent
{
	protected $table = 'kpxzx_quote_item';
	public $timestamps = false;

	protected $casts = [
		'published' => 'bool',
		'createdby' => 'int',
		'createdon' => 'int',
		'editedby' => 'int',
		'editedon' => 'int'
	];

	protected $fillable = [
		'heroy',
		'heroy_uri',
		'mult',
		'mult_uri',
		'poster',
		'text',
		'published',
		'createdby',
		'createdon',
		'editedby',
		'editedon'
	];
}
