<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:09 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteTmplvarAccess
 * 
 * @property int $id
 * @property int $tmplvarid
 * @property int $documentgroup
 *
 * @package App\Models
 */
class KpxzxSiteTmplvarAccess extends Eloquent
{
	protected $table = 'kpxzx_site_tmplvar_access';
	public $timestamps = false;

	protected $casts = [
		'tmplvarid' => 'int',
		'documentgroup' => 'int'
	];

	protected $fillable = [
		'tmplvarid',
		'documentgroup'
	];
}
