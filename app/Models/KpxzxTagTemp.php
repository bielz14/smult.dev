<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:12 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTagTemp
 * 
 * @property int $id
 * @property string $tags
 * @property string $text
 *
 * @package App\Models
 */
class KpxzxTagTemp extends Eloquent
{
	protected $table = 'kpxzx_tag_temp';
	public $timestamps = false;

	protected $fillable = [
		'tags',
		'text'
	];
}
