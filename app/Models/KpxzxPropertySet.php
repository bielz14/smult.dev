<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxPropertySet
 * 
 * @property int $id
 * @property string $name
 * @property int $category
 * @property string $description
 * @property string $properties
 *
 * @package App\Models
 */
class KpxzxPropertySet extends Eloquent
{
	protected $table = 'kpxzx_property_set';
	public $timestamps = false;

	protected $casts = [
		'category' => 'int'
	];

	protected $fillable = [
		'name',
		'category',
		'description',
		'properties'
	];
}
