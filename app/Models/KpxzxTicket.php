<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTicket
 * 
 * @property int $id
 * @property string $type
 * @property int $contentid
 * @property string $text
 * @property int $ready
 * @property string $comment
 * @property int $date_poch
 * @property int $date_kin
 *
 * @package App\Models
 */
class KpxzxTicket extends Eloquent
{
	protected $table = 'kpxzx_tickets';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'contentid' => 'int',
		'ready' => 'int',
		'date_poch' => 'int',
		'date_kin' => 'int'
	];

	protected $fillable = [
		'type',
		'contentid',
		'text',
		'ready',
		'comment',
		'date_poch',
		'date_kin'
	];

	public function kpxzxsitecontent()
    {
    	return $this->belongsTo('App\Models\KpxzxSiteContent', 'contentid');
    }
}
