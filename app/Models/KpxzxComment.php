<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Classes\CustomBuilder;

/**
 * Class KpxzxComment
 * 
 * @property int $comment_id
 * @property int $content_id
 * @property string $name
 * @property string $email
 * @property string $email_type
 * @property int $user_id
 * @property string $comment
 * @property int $datetime
 * @property int $img_id
 * @property int $parent
 * @property int $is_moder
 *
 * @package App\Models
 */
class KpxzxComment extends Eloquent
{
	protected $table = 'kpxzx_comments';
	protected $primaryKey = 'comment_id';
	public $timestamps = false;

	protected $casts = [
		'content_id' => 'int',
		'user_id' => 'int',
		'datetime' => 'int',
		'img_id' => 'int',
		'parent' => 'int',
		'is_moder' => 'int'
	];

	protected $fillable = [
		'content_id',
		'name',
		'email',
		'email_type',
		'user_id',
		'comment',
		'datetime',
		'img_id',
		'parent',
		'is_moder'
	];

	public function kpxzxsitecontent()
    {
    	return $this->belongsTo('App\Models\KpxzxSiteContent', 'content_id');
    }
}

