<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxContentType
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $mime_type
 * @property string $file_extensions
 * @property string $headers
 * @property bool $binary
 *
 * @package App\Models
 */
class KpxzxContentType extends Eloquent
{
	protected $table = 'kpxzx_content_type';
	public $timestamps = false;

	protected $casts = [
		'binary' => 'bool'
	];

	protected $fillable = [
		'name',
		'description',
		'mime_type',
		'file_extensions',
		'headers',
		'binary'
	];
}
