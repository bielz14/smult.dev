<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxMultPostersMusic
 * 
 * @property int $id
 * @property string $fileName
 * @property string $origName
 * @property \Carbon\Carbon $dateAdd
 * @property int $WhoAdd
 * @property int $multId
 * @property int $remove
 * @property int $type
 * @property int $isCadr
 *
 * @package App\Models
 */
class KpxzxMultPostersMusic extends Eloquent
{
	protected $table = 'kpxzx_mult_posters_music';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'WhoAdd' => 'int',
		'multId' => 'int',
		'remove' => 'int',
		'type' => 'int',
		'isCadr' => 'int'
	];

	protected $dates = [
		'dateAdd'
	];

	protected $fillable = [
		'fileName',
		'origName',
		'dateAdd',
		'WhoAdd',
		'multId',
		'remove',
		'type',
		'isCadr'
	];

	public function kpxzxsitecontent()
    {
    	return $this->belongsTo('App\Models\KpxzxSiteContent', 'multId');
    }

    public function kpxzxsitetmplvarcontentvalue()
    {
    	return $this->hasMany('App\Models\KpxzxSiteTmplvarContentvalue', 'contentid');
    }
}
