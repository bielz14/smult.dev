<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxUserAttribute
 * 
 * @property int $id
 * @property int $internalKey
 * @property string $fullname
 * @property string $email
 * @property string $phone
 * @property string $mobilephone
 * @property bool $blocked
 * @property int $blockeduntil
 * @property int $blockedafter
 * @property int $logincount
 * @property int $lastlogin
 * @property int $thislogin
 * @property int $failedlogincount
 * @property string $sessionid
 * @property int $dob
 * @property int $gender
 * @property string $address
 * @property string $country
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $fax
 * @property string $photo
 * @property string $comment
 * @property string $website
 * @property string $extended
 *
 * @package App\Models
 */
class KpxzxUserAttribute extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'internalKey' => 'int',
		'blocked' => 'bool',
		'blockeduntil' => 'int',
		'blockedafter' => 'int',
		'logincount' => 'int',
		'lastlogin' => 'int',
		'thislogin' => 'int',
		'failedlogincount' => 'int',
		'dob' => 'int',
		'gender' => 'int'
	];

	protected $fillable = [
		'internalKey',
		'fullname',
		'email',
		'phone',
		'mobilephone',
		'blocked',
		'blockeduntil',
		'blockedafter',
		'logincount',
		'lastlogin',
		'thislogin',
		'failedlogincount',
		'sessionid',
		'dob',
		'gender',
		'address',
		'country',
		'city',
		'state',
		'zip',
		'fax',
		'photo',
		'comment',
		'website',
		'extended'
	];
}
