<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSitePluginEvent
 * 
 * @property int $pluginid
 * @property string $event
 * @property int $priority
 * @property int $propertyset
 *
 * @package App\Models
 */
class KpxzxSitePluginEvent extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'pluginid' => 'int',
		'priority' => 'int',
		'propertyset' => 'int'
	];

	protected $fillable = [
		'priority',
		'propertyset'
	];
}
