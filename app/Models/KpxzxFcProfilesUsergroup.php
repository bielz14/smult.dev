<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxFcProfilesUsergroup
 * 
 * @property int $usergroup
 * @property int $profile
 *
 * @package App\Models
 */
class KpxzxFcProfilesUsergroup extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'usergroup' => 'int',
		'profile' => 'int'
	];
}
