<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteTmplvar
 * 
 * @property int $id
 * @property int $source
 * @property bool $property_preprocess
 * @property string $type
 * @property string $name
 * @property string $caption
 * @property string $description
 * @property int $editor_type
 * @property int $category
 * @property bool $locked
 * @property string $elements
 * @property int $rank
 * @property string $display
 * @property string $default_text
 * @property string $properties
 * @property string $input_properties
 * @property string $output_properties
 * @property bool $static
 * @property string $static_file
 *
 * @package App\Models
 */
class KpxzxSiteTmplvar extends Eloquent
{
	protected $table = 'kpxzx_site_tmplvars';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'source' => 'int',
		'property_preprocess' => 'bool',
		'editor_type' => 'int',
		'category' => 'int',
		'locked' => 'bool',
		'rank' => 'int',
		'static' => 'bool'
	];

	protected $fillable = [
		'source',
		'property_preprocess',
		'type',
		'name',
		'caption',
		'description',
		'editor_type',
		'category',
		'locked',
		'elements',
		'rank',
		'display',
		'default_text',
		'properties',
		'input_properties',
		'output_properties',
		'static',
		'static_file'
	];

	public function kpxzxsitetmplvarcontentvalue()
    {
    	return $this->hasMany('App\Models\KpxzxSiteTmplvarContentvalue', 'tmplvarid');
    }
}
