<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxWorkspace
 * 
 * @property int $id
 * @property string $name
 * @property string $path
 * @property \Carbon\Carbon $created
 * @property bool $active
 * @property string $attributes
 *
 * @package App\Models
 */
class KpxzxWorkspace extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'active' => 'bool'
	];

	protected $dates = [
		'created'
	];

	protected $fillable = [
		'name',
		'path',
		'created',
		'active',
		'attributes'
	];
}
