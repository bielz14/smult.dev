<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxAccessPolicyTemplate
 * 
 * @property int $id
 * @property int $template_group
 * @property string $name
 * @property string $description
 * @property string $lexicon
 *
 * @package App\Models
 */
class KpxzxAccessPolicyTemplate extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'template_group' => 'int'
	];

	protected $fillable = [
		'template_group',
		'name',
		'description',
		'lexicon'
	];
}
