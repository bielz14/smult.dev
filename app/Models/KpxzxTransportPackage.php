<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxTransportPackage
 * 
 * @property string $signature
 * @property \Carbon\Carbon $created
 * @property \Carbon\Carbon $updated
 * @property \Carbon\Carbon $installed
 * @property bool $state
 * @property int $workspace
 * @property int $provider
 * @property bool $disabled
 * @property string $source
 * @property string $manifest
 * @property string $attributes
 * @property string $package_name
 * @property string $metadata
 * @property int $version_major
 * @property int $version_minor
 * @property int $version_patch
 * @property string $release
 * @property int $release_index
 *
 * @package App\Models
 */
class KpxzxTransportPackage extends Eloquent
{
	protected $primaryKey = 'signature';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'state' => 'bool',
		'workspace' => 'int',
		'provider' => 'int',
		'disabled' => 'bool',
		'version_major' => 'int',
		'version_minor' => 'int',
		'version_patch' => 'int',
		'release_index' => 'int'
	];

	protected $dates = [
		'created',
		'updated',
		'installed'
	];

	protected $fillable = [
		'created',
		'updated',
		'installed',
		'state',
		'workspace',
		'provider',
		'disabled',
		'source',
		'manifest',
		'attributes',
		'package_name',
		'metadata',
		'version_major',
		'version_minor',
		'version_patch',
		'release',
		'release_index'
	];
}
