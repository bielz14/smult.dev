<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxFcProfile
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property bool $active
 * @property int $rank
 *
 * @package App\Models
 */
class KpxzxFcProfile extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'active' => 'bool',
		'rank' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'active',
		'rank'
	];
}
