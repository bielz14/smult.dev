/**
 * Created by vova on 05.10.2015.
 */
if( typeof posterList !=="undefined")
    for(var i=0;i<posterList.length;i++){

        var posterFromData=posterList[i]


        var fileName=posterFromData.prefix+'_file'
        $.ajax_upload($('#'+posterFromData.prefix+'_uploadButton'), {
            action : '/ajaxupload.php?uid='+posterFromData.uid+'&t=jpg&fileName='+fileName,
            name : fileName,
            onSubmit : function(file, ext) {
                var prefix=$(this.button).data('prefix')
                if (! (ext  && /^(jpg|jpeg)$/.test(ext))){
                    $('#'+prefix+'_message').html('только JPG файлы могут быть загружены');
                    return  false;
                }
                $('#'+prefix+'_loader').removeClass('hide');

                this.disable();

            },
            onComplete : function(file, response) {
                var prefix=$(this.button).data('prefix')
                var input = $(this.input);
                $('#'+prefix+'_loader').addClass('hide')
                // снова включаем кнопку
                this.enable();

                input.hide()
                input.attr('id',input.attr('name'))

                //// показываем что файл загружен
                $('#'+prefix+'_poster').val(response);
                $('#'+prefix+'_posterChange').val('1');
                $('#'+prefix+'_preview').removeClass('hide').html('<img width="50px" src="/'+response+'"/>');

                if ($("div").is(".live_adminka")) {
                    settingsLive.img = response;
                    setStyleLive(settings)
                }


            }
        });
        var fileButton=$('[name="'+posterFromData.prefix+'_file"]')
        fileButton.hide()
        fileButton.attr('id',fileName)
        fileButton.attr('data-prefix',posterFromData.prefix)
        $('#'+posterFromData.prefix+'_uploadButton').click(function(){
            $('#'+$(this).data('button-id')).click()
        })


    }
    //alert(posterList[i].prefix  )
