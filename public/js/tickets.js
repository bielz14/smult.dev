var ticketsType = "";
$("body").on("change", ".tickets .left input", function() {
    $(".tickets textarea").remove(), 
    ticketsType = $(this).val(), 
    $(".tckts .ftr").html('<input id="tickets-sbm" type="button" class="sbmBtn"  value="Отправить">'), 
    "video" == ticketsType && $(".tickets .right img").attr("src", "/public/img/tickets/1.png"), 
    "content" == ticketsType && 
    (
    	$(".tickets .right img").attr("src", "/public/img/tickets/2.png"), 
    	$(this).parent().after('<textarea placeholder="Пожалуйста укажите, где именно Вы нашли ошибку"></textarea>')
    ), 
    "anyerror" == ticketsType && ($(".tickets .right img").attr("src", "/public/img/tickets/3.png"), 
    	$(this).parent().after('<textarea placeholder="Пожалуйста, опишите проблему в двух словах"></textarea>'))
}),

$("body").on("click", "#tickets-sbm", function() {
    return text = $(".tckts textarea").val(), 
    content_id = $("#content_id").val(), 
    type = ticketsType, 
    "anyerror" != ticketsType && "content" != ticketsType || "" != text ? void 
    $.post('/addTicket', {
      type: 'POST',
      data: {
            text: text,
            content_id: content_id,
            ticketsType: type,
        },
      _token: $('meta[name="csrf-token"]').attr('content')
    }, function() { 
        $(".tckts").addClass("long-image"), $(".tckts").html('<img src="/public/img/tickets/long.jpg">'), setTimeout(function ()
        {
            $(".tckts").remove(), $("#mask").hide()
        }, 3000)
    }) : ($(".tckts textarea").css(
    {
        "border-color": "#d8512d"
    }), setTimeout(function ()
    {
        $(".tckts textarea").removeAttr("style")
    }, 1000), "")
});

