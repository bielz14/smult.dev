function isValidEmailAddress(t) {
    var e = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return e.test(t)
}
function delem(t, e) {
    for (var o = $(t); ; ) {
        if (height = o.height(),
        height < e)
            return o.text(o.text().substring(0, o.text().length - 3)),
            void o.text(o.text() + "...");
        var n = 1;
        height - 100 > 25 && (n = 5),
        height - 100 > 50 && (n = 25),
        height - 100 > 100 && (n = 50),
        o.text(o.text().substring(0, o.text().length - n))
    }
}
function validateForm(t) {
    return cls = t.attr("class"),
    type = t.attr("type"),
    val = t.val(),
    error = "",
    "email" == type ? isValidEmailAddress(t.val()) || (error = "error") : "" == val && (error = "error"),
    "error" == error && (t.css({
        "border-color": "#d8512d",
        "border-width": "1px"
    }),
    setTimeout(function() {
        t.removeAttr("style")
    }, 1e3)),
    error
}
function getForm() {
    $.get(loginUri, {
        type: "get_form",
        service: "login"
    }, function(t) { 
        t = JSON.parse(t),console.log(t.form);
        $("#reg-btn").text(t.status),
        $("#reg-mdl .cnt").html(t.form),
        logContent = $("#reg-mdl .reg-form")
    })
}
if ($(document).ready(function() {
    $("body").on("click", ".butInfo", function() {
        $("#jttip-mult-poster").show()
    }),
    $("body").on("mouseleave", ".ndButInfo", function() {
        $("#jttip-mult-poster").hide()
    });
    var t = 0
      , e = 0;
    $().mousemove(function(o) {
        t = o.pageX,
        e = o.pageY
    }),
    $(".jt").hover(function() {
        var t = $(this).index(".jt");
        clearTimeout(window["ta" + t]),
        $(this).show()
    }, function() {
        $(this).fadeOut("fast")
    }),
    $(".jtg").hover(function() {
        var t = $(this).parent().children(".jt")
          , e = t.index(".jt")
          , o = $(this).parent().find(".title").height()
          , n = $(this).height()
          , a = $(this).offset()
          , i = (t.offset(),
        $(this).height())
          , r = $(this).width()
          , s = (t.width(),
        t.height())
          , l = a.top;
        s > i && (l = (s - i) / 2 + o),
        i > s && (l = (i - s) / 2 + o),
        console.log(o),
        s > n && (l = -1 * l),
        t.css("top", l);
        var u = r + 20;
        u + 400 > $(".content").width() ? (u = a.left - 20 - 370,
        t.removeClass("left").addClass("right")) : t.removeClass("right").addClass("left"),
        t.css("left", u),
        window["t" + e] = setTimeout(function() {
            t.fadeIn("fast")
        }, 300)
    }, function() {
        var t = $(this).parent().children(".jt")
          , e = t.index(".jt");
        clearTimeout(window["t" + e]),
        "block" == t.css("display") && (console.log("ta" + e),
        window["ta" + e] = setTimeout(function() {
            t.hide()
        }, 300))
    })
}),
$("section").is(".ajax_content_add")) {
    var id = $(".ajax_content_add").data("id")
      , template = $(".ajax_content_add").data("template")
      , page = $(".ajax_content_add").data("page")
      , startPage = page
      , parent = $(".ajax_content_add").data("parent");
    $("body").on("click", "#preloader img,#preloader span", function() {
        console.log("preloader click"),
        page++,
        $.ajax({
            type: "GET",
            url: "/login",
            data: {
                id: id,
                template: template,
                page: page,
                parent: parent,
                startPage: startPage,
                ajaxContentAdd: "ajaxContentAdd",
                service: "ajaxContentAdd"
            },
            success: function(t) {
                $(".add_remove").remove(),
                $(".ajax_content_add .items").append(t)
            }
        })
    })
}
$(document).ready(function() {
    var t = $("#touch-menu")
      , e = $(".menu");
    $(t).on("click", function(t) {
        t.preventDefault(),
        e.slideToggle()
    }),
    $(window).resize(function() {
        var t = $(window).width();
        t > 767 && e.is(":hidden") && e.removeAttr("style")
    })
});
var mask = $("#mask");
if ($(window).load(function() {
    var t = $(document).height();
    mask.height(t)
}),
$("body").on("click", "#mask,.btn", function() {
    $(".mdl").hide(),
    mask.hide()
}),
$("div").is("#serial-iframe")) {
    var serialPlayer = $("#serial-iframe");
    serialPlayer.height(serialPlayer.width() / 1.7),
    console.log(serialPlayer.width())
}
if (width = $(".repost-quoter").width(),
$("#quotes").width(width - 410),
$(".soz-repost").click(function() {
    parent = $(this).parent(),
    title = parent.data("title"),
    description = parent.data("description"),
    uri = parent.data("uri"),
    poster = parent.data("poster"),
    type = $(this).data("type"),
    "vk" == type ? (url = "http://vk.com/share.php?",
    url += "url=" + encodeURIComponent(uri),
    url += "&title=" + encodeURIComponent(title),
    url += "&description=" + encodeURIComponent(description),
    url += "&image=" + encodeURIComponent(poster),
    url += "&noparse=true") : "fb" == type ? (url = "http://www.facebook.com/sharer.php?s=100",
    url += "&p[summary]=safsd",
    url += "&p[url]=" + encodeURIComponent(uri)) : (url = "http://twitter.com/share?",
    url += "text=" + encodeURIComponent(title),
    url += "&url=" + encodeURIComponent(uri),
    url += "&counturl=" + encodeURIComponent(uri)),
    window.open(url, "", "toolbar=0,status=0,width=626,height=436")
}),
locat = window.location.toString(),
locat = locat.replace("dev.", ""),
locat = locat.replace("dev2.", ""),
VK = {},
VK.Share = {},
VK.Share.count = function(t, e) {
    $('[data-type="vk"]').append("<span>" + e + "</span>"),
    e > 0 && $('[data-type="vk"] span').show()
}
,
$.getJSON("http://vkontakte.ru/share.php?act=count&index=1&url=" + locat + "&format=json&callback=?"),
/*$.getJSON("http://api.facebook.com/restserver.php?method=links.getStats&callback=?&urls=" + locat + "&format=json", function(t) {
    $('[data-type="fb"]').append("<span>" + t[0].share_count + "</span>"),
    t[0].share_count > 0 && $('[data-type="fb"] span').show()
}),*/
$.getJSON("http://urls.api.twitter.com/1/urls/count.json?url=" + locat + "&callback=?", function(t) {
    $('[data-type="tw"]').append("<span>" + t.count + "</span>"),
    t.count > 0 && $('[data-type="tw"] span').show()
}),
$(".descr").each(function(t, e) {
    delem(e, 100)
}),
$("section").is(".comp") && $(window).load(function() {
    $(".comp .item").each(function(t, e) {
        e = $(e),
        height = e.find("img").height(),
        cont = e.find(".right").height(),
        cond = e.find(".cond").height(),
        minus = cond - (cont - height),
        delem(e.find(".cond"), minus)
    })
}),
$("div").is(".poster-list") && $(".poster-list a").fancybox(),
$("body").on("click", ".tickets-btn", function() {
    /*$.ajax({
        type: "POST",
        data: {
            type: "getForm",
            service: "tickets"
        },
        url: "/outOrderForm",
        success: function(t) {
            $(".tckts").remove(),
            $("#mask").show(),
            $("body").append(t)
            console.log(t);TicketTicketTicket
        }
    })*/
    $.post('/outTicketForm', {
        type: 'POST',
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function(t) { 
        $(".tckts").remove(),
        $("#mask").show(),
        $("body").append(t)
    })
    /*$.ajax({
      type: "POST",
      url: '/outOrderForm',
      _token: $('meta[name="csrf-token"]').attr('content'),
      success: function(t){
        $(".tckts").remove(),
        $("#mask").show(),
        $("body").append(t)
      }
    });*/
}),
$("div").is(".feedback")) {
    var f_text = $('.feedback [name="text"]')
      , f_emeil = $('.feedback [name="email"]')
      , f_name = $('.feedback [name="name"]');
    $(".feedback button").click(function() {
        var t = "";
        return t += validateForm(f_text),
        t += validateForm(f_name),
        t += validateForm(f_emeil),
        "" != t ? "" : void $.ajax({
            data: "f_text=" + f_text.val() + "&f_emeil=" + f_emeil.val() + "&f_name=" + f_name.val(),
            success: function(t) {
                f_text.val(""),
                f_emeil.val(""),
                f_name.val(""),
                $(".feedback .msg").text("Сообщение успешно отправлено!"),
                setTimeout(function() {
                    $(".feedback .msg").text("")
                }, 5e3)
            }
        })
    })
}
$("body").on("click", "#promo-reg-btn", function() {
    email = $("#promo_email").val(),
    $("#reg-mdl #email").val(email),
    $("#reg-mdl ,#mask").show(),
    $("#reg-mdl #email").keyup(),
    validateForm($("#reg-mdl #email"))
}),
$(".content").append('<div id="search-result" class="hide"></div>');
var s_input = $("#search")
  , s_result = $("#search-result");
if (s_input.focusout(function() {
    s_result.attr("class", "hide"),
    s_result.children("#result-wrap").slideUp(500)
}),
s_input.focus(function() {
    $(s_input).keyup()
}),
$(s_input).on("keyup", function() {
    var t = $(this);
    if (search = t.val(),
    search.length < 2)
        return "";
    var e = t.offset().top + t.outerHeight()
      , o = t.offset().left
      , n = t.outerWidth();
    $.ajaxSetup({
        cache: !1
    }),
    $.get("/assets/ajax/search.php", {
        search: search
    }, function(t) {
        s_result.offset({
            top: e,
            left: o - 150
        }),
        s_result.width(n + 150),
        s_result.html(t),
        "hide" == s_result.attr("class") ? s_result.children("#result-wrap").slideDown(500) : s_result.children("#result-wrap").show(),
        s_result.attr("class", "")
    })
}),
$("#serial-list").change(function() {
    val = $(this).val(),
    $("#serial-iframe").attr("src", val)
}),
$(function() {
    var t = $(document).width();
    if (t > 1023) {
        var e = $("body").offset()
          , o = $("header")
          , n = o.height();
        o.css({
            position: "fixed",
            top: 0,
            left: 0,
            width: "90%",
            "margin-left": e.left,
            "z-index": "100"
        }),
        $(".container").css({
            "margin-top": n
        })
    }
}),
$("div").is(".multThema")) {
    var count = $(".multThema .item").length;
    if (count > 4) {
        var procnent = (100 - 9 * count) / count / 2;
        $(".multThema .item").css("margin", "0 " + procnent + "%")
    }
}
/*$("body").copyright({
    length: 100
});*/
var poster_frame_counts = $(".mult-posters img").length;
poster_frame_counts > 0 && $(".fancybox").fancybox();
var loginUri = "/login", logContent, logEmail;
getForm(),
$("#reg-btn").on("click", function() {
    var t = $("#reg-mdl");
    t.show(),
    mask.show()
}),
$("body").on("keyup", "#reg-mdl #email", function() {
    logContent = $("#reg-mdl .reg-form");
    logEmail = $(this).val(),
    isValidEmailAddress(logEmail) ? $.get(loginUri, {//проверка email-а на наличие в БД
        type: "checking_login",
        email: logEmail,
        service: "login"
    }, function(t) {
        "0" == t ? (console.log(0),
        logContent.children(".sbmBtn").remove(),
        logContent.children(".pass").remove(),
        logContent.append('<input type="button" id="reg-btn" class="sbmBtn" value="Вход">')) : 
        (
            logContent.children(".sbmBtn").remove(),
            logContent.children(".pass").remove(),
            logContent.append('<input id="password" type="password" class="pass" name="password" required="">'),
            logContent.append('<input type="button" id="login-btn" class="sbmBtn" value="Вход">')
        )
    }) : (logContent.children(".sbmBtn").remove(),
    logContent.children(".pass").remove())
}),
$("body").on("click", "#login-btn", function() {
    pass = logContent.children("#password").val(),
    console.log(pass),
    $.post(loginUri, {//аутентификация
        type: "login",
        email: logEmail,
        password: pass,
        service: "login",
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function(t) { 
        1 == t ? 
        (
            $('.cnt').html(
                         '<div class="office">' +
                            '<div class="userName">' + logEmail + '</div>' +
                            '<input type="button" class="sbmBtn" id="logout-btn" value="Выход">' +
                         '</div>'
                        ), $("#reg-btn").text('Выход')
        )
        : (logContent.children("#password").css({
            "border-color": "#d8512d"
        }),
        setTimeout(function() {
            logContent.children("#password").removeAttr("style")
        }, 1e3))
    })
}),
$("body").on("click", "input[id=reg-btn]", function() {
    console.log(logEmail),
    loginUri = '/register';
    $.post(loginUri, {//регистрация
        type: "registr",
        email: logEmail,
        _token: $('meta[name="csrf-token"]').attr('content'),
        service: "login"
    }, function(t) {
        console.log(t),
        1 == t ? 
            (
                console.log('successfull registration'),
                $('.cnt').html(
                             '<div class="office">' +
                                '<div class="userName">' + logEmail + '</div>' +
                                '<input type="button" class="sbmBtn" id="logout-btn" value="Выход">' +
                             '</div>'
                            ), $("#reg-btn").text('Выход')
            )
        :
            console.log('fail registration')
    });
    loginUri = '/login';
}),
$("body").on("click", "#logout-btn", function() {
    loginUri = '/logout';
    $.post(loginUri, {
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function(t) { 
        if (t[0] == 1) { 
            $('.cnt').html(
                '<div class="reg-form">' +
                    '<input placeholder="@" type="email" class="email ned" id="email" name="email">' +
                 '</div>'
                );
            if (t[1] != null) {
                csrfToken = t[1];
                $('meta[name="csrf-token"]').attr('content', csrfToken);
            } 
            $("#reg-btn").text('Вход');
        } else {
            console.log('Logout error');
        }
    })
    loginUri = '/login';
});
