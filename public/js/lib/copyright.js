!function(e) {
    "use strict";
    var t = {
        opts: {},
        elements: [],
        init: function(n) {
            t.opts = e.extend({
                extratxt: "Подробнее: %link%",
                length: 150,
                hide: !0,
                allowcopy: !0,
                first: !1,
                sourcetxt: "Источник",
                style: "",
                className: "copyright-span"
            }, n || {}),
            this.each(function() {
                t.elements.push(this)
            });
            var o = t.isIE() ? "body" : document;
            return e(o).on("copy", function(n) {
                if (n.target && "TEXTAREA" !== n.target.tagName) {
                    var o = !1;
                    if (t.isWinSelection()) {
                        var i = window.getSelection()
                          , r = i.getRangeAt(0);
                        e(t.elements).each(function() {
                            r.intersectsNode(this) && (o = !0)
                        })
                    } else if (t.isDocSelection()) {
                        var i = document.selection
                          , r = i.createRange();
                        e(t.elements).each(function() {
                            var e = t.createRangeWithNode(this);
                            document.createRange ? -1 === r.compareBoundaryPoints(Range.END_TO_START, e) && 1 === r.compareBoundaryPoints(Range.START_TO_END, e) && (o = !0) : r.compareEndPoints("StartToEnd", e) < 0 && r.compareEndPoints("EndToStart", e) > 0 && (o = !0)
                        })
                    } else
                        n.preventDefault();
                    return o ? t.beforeCopy(n) : void 0
                }
            }),
            this
        },
        createRangeWithNode: function(e) {
            var t;
            return window.getSelection && document.createRange ? (t = document.createRange(),
            t.selectNodeContents(e)) : document.body.createTextRange && (t = document.body.createTextRange(),
            t.moveToElementText(e)),
            t
        },
        isOpera: function() {
            return !!window.opera || navigator.userAgent.indexOf(" OPR/") >= 0
        },
        isIE: function() {
            return !!document.documentMode
        },
        isWinSelection: function() {
            return window.getSelection
        },
        isDocSelection: function() {
            return document.selection && "Control" !== document.selection.type
        },
        beforeCopy: function(e) {
            var n = t.getSelection();
            return !n || n.length < t.opts.length ? !0 : n.length >= t.opts.length && !t.opts.allowcopy ? void e.preventDefault() : void (t.isWinSelection() && !t.isOpera() ? t.stdCopy(e) : t.isWinSelection() && t.isOpera() ? t.oprCopy(e) : t.isDocSelection() ? t.docCopy(e) : e.preventDefault())
        },
        stdCopy: function() {
            if (t.opts.first)
                var n = t.getExtra() + "&nbsp;\n" + t.getSelection();
            else
                var n = t.getSelection() + "&nbsp;\n" + t.getExtra();
            var o = window.getSelection()
              , i = o.getRangeAt(0)
              , r = i.cloneRange()
              , c = e("<div>", {
                html: n,
                style: "position:absolute;left:-99999em;"
            });
            e("body").append(c),
            window.getSelection().selectAllChildren(c[0]),
            window.setTimeout(function() {
                c.remove(),
                o = window.getSelection(),
                o.removeAllRanges(),
                o.addRange(r)
            }, 0)
        },
        docCopy: function(e) {
            t.opts.first ? t.getExtra() + "&nbsp;\n" + t.getSelection() : t.getSelection() + "&nbsp;\n" + t.getExtra();
            var n = document.selection
              , o = n.createRange()
              , i = o.duplicate()
              , r = document.createElement("DIV");
            r.innerHTML = t.getExtra(),
            r.setAttribute("style", "position:absolute;left:-99999em;"),
            i.collapse(t.opts.first),
            i.pasteHTML(r.outerHTML),
            t.opts.first ? e.preventDefault() : (o.setEndPoint("EndToEnd", i),
            o.select())
        },
        oprCopy: function() {
            var n = window.getSelection()
              , o = n.getRangeAt(0)
              , i = o.cloneRange();
            i.collapse(t.opts.first);
            var r = document.createElement("DIV");
            r.innerHTML = t.getExtra(),
            r.setAttribute("style", "position:absolute;left:-99999em;"),
            i.insertNode(r),
            t.opts.first || o.setEndAfter(r),
            window.setTimeout(function() {
                e(r).remove(),
                n.removeAllRanges(),
                n.addRange(o)
            }, 0)
        },
        getSelection: function() {
            var e = "";
            return t.isWinSelection() ? e = window.getSelection().toString() : t.isDocSelection() && (e = document.selection.createRange().text),
            e
        },
        getExtra: function() {
            var e = "";
            t.opts.hide && (e += "position:absolute;left:-9999em;top:0;"),
            e += t.opts.style;
            var n = '<span class="' + t.opts.className + '"' + (e ? ' style="' + e + '"' : "") + ">"
              , o = "</span>"
              , i = t.opts.extratxt.replace("%link%", '<a href="' + document.location.href + '">' + document.location.href + "</a>").replace("%source%", '<a href="' + document.location.href + '">' + t.opts.sourcetxt + "</a>");
            return n + i + o
        },
        destroy: function() {}
    };
    e.fn.copyright = function(e) {
        return "object" != typeof e && e ? void 0 : t.init.apply(this, arguments)
    }
}(jQuery);
