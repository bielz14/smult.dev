<?php


$a = __DIR__;
$a .= "\\";

$a = str_replace('\\', '/', $a);
define("MODX_ASSETS_PATH", $a);


$startTime = time();
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include_once 'user_log_local.php';
include_once 'myFunctions.php';
set_time_limit(72000);
/*оголощою обєкт log*/
$log = new logs();

//if (isset($_GET['articles']))
//    $watch = new watch('another_services_articles');/
//else
//  $watch = new watch('another_services');


function get_redakt_uri($template, $id)
{
    return getEditResLink($id, $template);
}

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Устанавливаем параметр, чтобы curl возвращал данные, вместо того, чтобы выводить их в браузер.
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Googlebot/2.1 (http://www.googlebot.com/bot.html)');
    $data = curl_exec($ch);
    $header = curl_getinfo($ch);
    curl_close($ch);
    return array('data' => $data, 'header' => $header);
}

function youtube_get_id($s)
{
    $count = strlen($s);
    $pos = strpos($s, '/watch');
    if ($pos === false) {
        for (; ;) {
            $pos = strpos($s, '/');
            if ($pos === false) {
                $pos = strpos($s, '?');
                if ($pos !== false)
                    $s = substr($s, 0, $pos);
                break;
            } else {
                $pos = $pos + 1;
                $s = substr($s, $pos, $count);
            }
        }
    } else {
        $pos = strpos($s, '?');
        $s = $s = substr($s, $pos + 1, $count);

        parse_str($s, $ou);
        $s = $ou['v'];
    }
    $s = str_replace(' ', '', $s);
    $s = str_replace('!', '', $s);
    $s = str_replace('#', '', $s);
    return $s;
}

function urlCorrect($url)
{
    if ($url[0] == '/' and $url[1] == '/')
        $url = 'http:' . $url;
    if ($url[0] == 'w' and $url[1] == 'w' and $url[2] == 'w')
        $url = 'http://' . $url;
    return $url;
}


if (isset($_GET['services'])) {
    $services = array($_GET['services']);

} else {

    $services = array('video.bigmir.net', 'my.mail.ru', 'video.sibnet.ru', 'youtube.com', 'vk.com', 'youtu.be', 'video.meta.ua');
}


//$services = $_GET['services'];
//$rid = $_GET['rid'];
//$_GET['type'] = 'getContent';

$str = '';
foreach ($_GET as $key => $g) {
    $str[] = $key . '=' . $g;
}

$scriptUrl = 'http://smult.ru/assets/scripts/video_services/server.php?';
$urlFull = $scriptUrl . implode('&', $str);

if(!empty($_GET['showRespUrl'])){
    echo $urlFull;
    return '';
}
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $urlFull);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$data = curl_exec($ch);
curl_close($ch);

$new = json_decode($data, true);


$log->log('Количество видео ' . count($new) . '<br>');

$videoCounts = count($new);
$log->initManifest(count($new));


foreach ($new as $key => $re) {


    $thisTime = time();
    $passed = $thisTime - $startTime;
    $processed = $key + 1;
    $oneTime = $passed / $processed;
    if ($oneTime == 0) {
        $oneTime = 1;
    }
    $remains = $videoCounts - $processed;
    $remainsTime = $remains * $oneTime;
    $remainsTime = intval($remainsTime);

    $log->setRemainsTime($remainsTime);



    $resp_name = $re['id'] . '.html';

    $serverCode = '';
    $startItem = microtime(true);
    $re['value'] = urlCorrect($re['value']);


    $log->addLastCheckResource($re['value'],$processed);


    $log->log($key . ' ' . $re['value']);

    if (strpos($re['value'], 'youtube.com') !== false) {
        $youtube_id = youtube_get_id($re['value']);
        $url = 'https://www.googleapis.com/youtube/v3/videos?id=' . $youtube_id . '&key=AIzaSyAf3LfZ-Pt_GOV4ZG8iHGR985exCOz0lkw&part=status';
        $content = file_get_contents_curl($url);
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200)
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        $result = $content['data'];
        $content = $content['data'];
        $content = json_decode($content, true);

        if (!isset($content['items'][0]['id']) or $content['items'][0]['status']['uploadStatus'] != 'processed') {
            $log->save_response($resp_name, $result);
            $redakt = get_redakt_uri($re['template'], $re['id']);
            if (isset($tr['youtube.com']))
                $j = count($tr['youtube.com']) + 1;
            else
                $j = 1;
            $tr['youtube.com'][] = '
                <tr>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $j . '</td>
                    <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                    <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $redakt . '</td>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $youtube_id . '</td>
                </tr>
                ';
        }
    }
    if (strpos($re['value'], 'youtu.be') !== false) {
        $youtube_id = youtube_get_id($re['value']);
        $url = 'https://www.googleapis.com/youtube/v3/videos?id=' . $youtube_id . '&key=AIzaSyAf3LfZ-Pt_GOV4ZG8iHGR985exCOz0lkw&part=status';
        $content = file_get_contents_curl($url);
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200)
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        $result = $content['data'];
        $content = $content['data'];

        $content = json_decode($content, true);
        if (!isset($content['items'][0]['id']) or $content['items'][0]['status']['uploadStatus'] != 'processed') {
            $log->save_response($resp_name, $result);
            $redakt = get_redakt_uri($re['template'], $re['id']);
            $j = count($tr['youtu.be']) + 1;
            $tr['youtu.be'][] = '
                <tr>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $j . '</td>
                    <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                    <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $redakt . '</td>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $youtube_id . '</td>
                </tr>
                ';
        }
    }
    if (strpos($re['value'], 'vk.com') !== false) {
        if ($re['value'][0] == ' ')
            $re['value'] = substr($re['value'], 1);
        if ($re['value'][0] == '/')
            $re['value'] = 'http:' . $re['value'];

        //$re['value'] = str_replace('vk.com', 'new.vk.com', $re['value']);

        $content = file_get_contents_curl($re['value']);

        $error_text = '';

        if (isset($_GET['showResponse'])) {
            echo iconv("windows-1251", "UTF-8", $content['data']);

        }
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200)
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        $result = $content['data'];
        if(empty($result)){
            $error_text = 'vk вернул пустой ответ необходимая ручная проверка';
        }

       // $pos1 = mb_strpos($result, iconv('utf-8', 'windows-1251', 'id="playerObj"'), 0, 'windows-1251');
        $pos1 = mb_strpos($result, iconv('utf-8', 'windows-1251', 'var playerParams'), 0, 'windows-1251');



        $posYear = mb_strpos($result, iconv('utf-8', 'windows-1251', 'Это видео не может быть показано из-за возрастных ограничений.'), 0, 'windows-1251');
        if ($posYear !== false)
            $error_text = 'Это видео не может быть показано из-за возрастных ограничений.';

        $posProblem = -1;
        $posProblem = mb_strpos($result, iconv('utf-8', 'windows-1251', 'Для просмотра необходим <a href="http://get.adobe.com/flash">Flash Player</a> последней версии.'), 0, 'windows-1251');

        $posModer = mb_strpos($result, iconv('utf-8', 'windows-1251', 'Видеозапись была помечена модераторами сайта как «Материал для взрослых».'), 0, 'windows-1251');
        if ($posModer !== false)
            $error_text = 'Видеозапись была помечена модераторами сайта как «Материал для взрослых». Такие видеозаписи запрещено встраивать на внешние сайты.';

        $posModer = mb_strpos($result, iconv('utf-8', 'windows-1251', 'Необходимо авторизоваться'), 0, 'windows-1251');
        if ($posModer !== false){
            $error_text = 'Ручная проверка (Необходимо авторизоваться)';
        }



        if($re['id']=='422'){

//            echo $result;
//            return '';
        }

        //Необходимо авторизоваться


        if ($pos1 === false and !$posProblem) {

            $log->addBadResource($re,$error_text);


            $log->save_response($resp_name, $result);

            if (isset($tr['vk.com']))
                $j = count($tr['vk.com']) + 1;
            else
                $j = 1;


            $tr['vk.com'][] = '
                <tr>

                    <td style="font-size: 10px;  border: 1px solid gray;">' . $j . '</td>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $re['id'] . '</td>
                    <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                    <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . getEditResLink($re['id'], $re['template']) . '</td>
                    <td style="font-size: 10px;  border: 1px solid gray;">' . $error_text . '</td>
                </tr>
                ';


        }


    }
    if (strpos($re['value'], 'my.mail.ru') !== false) {

        $content = file_get_contents_curl($re['value']);
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200){
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        }
        $result = $content['data'];
        $content = $content['data'];


        //$pos = strpos($content, 'К сожалению, это видео недоступно');
		$pos = strpos($content, '<div class="b-error-page__content__text">');
		
        if ($pos !== false) {
            $log->save_response($resp_name, $content);
            $redakt = get_redakt_uri($re['template'], $re['id']);
            $tr['my.mail.ru'][] = '
            <tr>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $re['id'] . '</td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $redakt . '</td>
            </tr>
                ';
        }
    }

    if (strpos($re['value'], 'video.bigmir.net') !== false) {

        $content = file_get_contents_curl($re['value']);
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200)
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        $result = $content['data'];
        $content = $content['data'];

        $pos = strpos($content, 'Извините, страница не найдена. Возможно, вам будут интересны эти страницы:');

        if ($pos !== false) {
            $log->save_response($resp_name, $content);
            $redakt = get_redakt_uri($re['template'], $re['id']);
            $tr['video.bigmir.net'][] = '
            <tr>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $re['id'] . '</td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $redakt . '</td>
            </tr>
                ';
        }
    }

    if (strpos($re['value'], 'video.sibnet.ru') !== false) {

        $content = file_get_contents_curl($re['value']);
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200)
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        $result = $content['data'];
        $content = $content['data'];
        $pos = strpos($content, iconv('utf-8', 'windows-1251', 'Нет такого видео'));
        if ($pos !== false) {
            $log->save_response($resp_name, $result);
            $redakt = get_redakt_uri($re['template'], $re['id']);
            $tr['video.sibnet.ru'][] = '
            <tr>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $re['id'] . '</td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $redakt . '</td>
            </tr>
                ';
        }
    }
    if (strpos($re['value'], 'video.meta.ua') !== false) {

        $content = file_get_contents_curl($re['value']);
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200)
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        $result = $content['data'];
        $content = $content['data'];

        $pos = strpos($content, iconv('utf-8', 'windows-1251', 'Файл не найден или был удален.'));
        $pos1 = strpos($content, iconv('utf-8', 'windows-1251', 'Файл был удален по требованию правообладателя.'));

        if ($pos !== false or $pos1 !== false) {
            $log->save_response($resp_name, $result);
            $redakt = get_redakt_uri($re['template'], $re['id']);
            $tr['video.meta.ua'][] = '
            <tr>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $re['id'] . '</td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $redakt . '</td>
            </tr>
                ';
        }
    }
    if (strpos($re['value'], 'video.ukrhome.net') !== false) {
        $content = file_get_contents_curl($re['value']);
        $serverCode = $content['header']['http_code'];
        if ($serverCode != 200)
            trigger_error($re['value'] . " вернула код" . $serverCode, E_USER_NOTICE);
        $result = $content['data'];
        $content = $content['data'];
        $pos = strpos($content, iconv('utf-8', 'windows-1251', 'К сожалению запрашиваемой страницы не существует'));
        if ($pos !== false) {
            $log->save_response($resp_name, $result);

            $redakt = get_redakt_uri($re['template'], $re['id']);
            $tr['video.ukrhome.net'][] = '
            <tr>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $re['id'] . '</td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="http://smult.ru/' . $re['uri'] . '">' . $re['pagetitle'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;"><a href="' . $re['value'] . '">' . $re['value'] . '</a></td>
                <td style="font-size: 10px;  border: 1px solid gray;">' . $redakt . '</td>
            </tr>
                ';
        }
    }
    $time = microtime(true) - $startItem;
    $fout = $key . '||' . $re['value'] . '||' . round($time, 4) . 's||' . $serverCode;
    $log->log($fout);


}


$to = " smultru@gmail.com,mihlovski@gmail.com,vanya_humenchuk@mail.ru,rostyk3339@gmail.com,nsaser@mail.ru,dzhuryn.volodymyr@gmail.com";
$headers = "MIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\nFrom: video@smult.ru <video@smult.ru>\r\n";


$fullTables = '';

$errorCount = array();

foreach ($services as $service) {
    $emailRespFile = $service . '-' . date('h-i_d-m-Y') . '.html';
    if (!empty($tr[$service])) {
        $errorCount[$service] = array('name' => $service, 'file' => $emailRespFile, 'count' => count($tr[$service]));
    }


}
//echo '<pre>';
//var_dump($errorCount);
//echo '</pre>';

$countStr = json_encode($errorCount);
$urlFull = $scriptUrl . 'type=saveCountFile&countStr=' . $countStr;


var_dump($tr);
file_get_contents_curl($urlFull);
foreach ($services as $key=> $service) {

    $emailRespFile = 'cache_mail/' . $service . '-' . date('h-i_d-m-Y') . '.html';

    if (isset($_GET['articles']))
        $subject = "Ошибки в видео по статьям - сервис " . $service;
    else
        $subject = "Проблемы с видео - сервис " . $service;


    if ($key==0) {

        $output = '
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="/style/admin.css" />

<table class="newtable" style="border-collapse: collapse;">
    <caption style="margin-bottom: 15px;font-size: 20px">
   Видео хостинг ' . $service;
        if (isset($_GET['articles'])) {
            $output .= '- статьи';
        }


        $dopHead = '';
        if (isset($dopHeader[$service]))
            $dopHead = implode('', $dopHeader[$service]);

        $output .= '
      </caption>
    <thead style="  background: #FF8040;">
        <tr>

             <td style="font-size: 10px;  border: 1px solid gray;">№</td>
             <td style="font-size: 10px;  border: 1px solid gray;">id</td>
            <td style="font-size: 10px;  border: 1px solid gray;">Название</td>
            <td style="font-size: 10px;  border: 1px solid gray;">Ссилка на видео</td>
            <td style="font-size: 10px;  border: 1px solid gray;">Редагувати</td>
            ' . $dopHead . '
        </tr>
    </thead>
    <tbody>
        ' . implode('', $tr[$service]) . '
    </tbody>
</table>';

        if (isset($_GET['getCall'])) {
            echo $output;
			
        }


        $fullTables .= $output;


        //$errorCount[$service]=array('name'=>$service,'file'=>$emailRespFile,'count'=>count($tr[$service]));
        //file_put_contents($errorCountFile,json_encode($errorCount));


        $log->email_save($emailRespFile, $output);
        if(empty($_GET['getCall'])) {

            if (mail($to, $subject, $output, $headers)) {
                $log->log('email отправлен ' . $to);
            }
        }
    }
}
if(empty($_GET['getCall'])) {
    $mres = mail('rostyk3339@gmail.com', 'full', $fullTables, $headers);
}
$log->end();
//$watch->end(file_exists($error_file), $error_file);


?>