<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 06.10.2015
 * Time: 12:46
 */







function user_log($errno, $errmsg, $file, $line)
{
    // время события
    $timestamp = time();

    //формируем новую строку в логе
    $err_str = $timestamp . '||';
    $err_str .= $errno . '||';
    $err_str .= $file . '||';
    $err_str .= $line . '||';
    $err_str .= $errmsg . "\n";

    //проверка на максимальный размер
    $fp = fopen(LOG_FILE_NAME, "a+");

    fwrite($fp, $err_str);

}

function create_file_slide($file, $exp)
{

    $j = 0;
    $j_title = '';
    $fileName = $file . $j_title . '.' . $exp;
    while (1) {
        if ($j > 0)
            $j_title = '-' . $j;
        $fileName = $file . $j_title . '.' . $exp;
        if (file_exists($fileName)) {
            $j++;
        } else {
            break;
        }


    }

    return $fileName;
}

function fatal_error_handler($buffer)
{

    $file = create_file_slide('fatal-error', 'html');
    $file = 'fatal.html';

    echo '2';
    if (preg_match("|(Fatal error</b>:)(.+)(<br)|", $buffer, $regs)) {
        file_put_contents($file, "before fork (pid: " . getmypid() . ")\n");

        return "ERROR CAUGHT, check log file";
    }
    return $buffer;
}


class files
{
    function read($file)
    {
        $fp = fopen($file, "r");
        if(!empty($fp)){
            flock($fp, 1);
            $text = '';
            while (!feof($fp))
                $text .= fgetss($fp);
            flock($fp, 3);
            fclose($fp);
            return $text;
        }

        return false;
    }

    function write($file, $text)
    {
        $fp = fopen($file, "w+");
        flock($fp, 2);
        fwrite($fp, $text);
        flock($fp, 3);
        fclose($fp);
    }
}

class watch extends files
{

    var $file;
    var $start_time;
    var $index = 0;


    function __construct($script_name)
    {
        $this->file = MODX_ASSETS_PATH.'scripts/watch.json';
        if (!file_exists($this->file)) {
            $watchers = array(
                'scripts' => array()
            );
            $watchers = json_encode($watchers);
            fopen($this->file, "a+");
            $this->write($this->file, $watchers);
        }
        $this->start_time = time();
        $array = $this->read($this->file);
        $array = json_decode($array, true);
        $array['scripts'][count($array['scripts'])] = array(
            'script_name' => $script_name,
            'time_start' => $this->start_time,
        );
        $this->index = count($array['scripts']) - 1;
        $array = json_encode($array);
        $this->write($this->file, $array);
    }

    function end($error_status, $error_file)
    {
        $array = $this->read($this->file);
        $array = json_decode($array, true);
        $array['scripts'][$this->index] = array_merge($array['scripts'][$this->index], array(
            'lead_time' => time() - $this->start_time,
            'error_status' => $error_status,
            'error_file' => $error_file,
        ));
        $array = json_encode($array);
        $this->write($this->file, $array);
    }

}

class logs extends files
{
    var $file;
    var $manifestFile;
    var $start;
    var $line;
    var $max_ime;
    var $dir_name;

    function __construct()
    {

        $this->max_ime = ini_get("max_execution_time");
        $this->start = time();
        $this->dir_name = 'cache/response-'.date('h-i_d-m-Y',$this->start);

        if(!file_exists('cache')){
            mkdir('cache');
        }
        if(!file_exists($this->dir_name)){
            mkdir($this->dir_name, 0755);
        }

        $this->file = create_file_slide('cache/log-' . date('h-i_d-m-Y', $this->start), 'html');

        $this->log('<meta charset="utf-8">Старт роботи скрипта ' . date('h-i_d-m-Y', $this->start));
        $this->log('Максимальное время работи скрипта ' . $this->max_ime);


        $this->manifestFile = 'manifest.json';
    }


    function initManifest($counts){
        $data = array(
            'startTime'=>time(),
            'startFormattedTime'=>date('d-m-Y h:i'),
            'videoCounts'=>$counts,
            'lastResource'=>'',
            'badResources'=>array(),
            'remainsTime'=>''

        );
        $dataStr = json_encode($data);
        $this->write($this->manifestFile,$dataStr);

    }

    function addBadResource($res,$text){
        $dataStr = $this->read($this->manifestFile);
        if($dataStr===false){
            return '';
        }

        $data = json_decode($dataStr,true);
        $data['badResources'][]=array(
            'video'=>$res['value'],
            'pagetitle'=>$res['pagetitle'],
            'url'=>'http://smult.ru/'.$res['uri'],
            'text'=>$text,
        );

        $dataStr = json_encode($data);
        $this->write($this->manifestFile,$dataStr);
    }

    function addLastCheckResource($video,$num){
        $dataStr = $this->read($this->manifestFile);
        if($dataStr===false){
            return '';
        }

        $data = json_decode($dataStr,true);
        $data['lastResource']=$video;
        $data['lastResourceNum']=$num;

        $dataStr = json_encode($data);
        $this->write($this->manifestFile,$dataStr);
    }

    function setRemainsTime($time){
        $dataStr = $this->read($this->manifestFile);
        if($dataStr===false){
            return '';
        }

        $data = json_decode($dataStr,true);
        $data['remainsTime']=$time;

        $dataStr = json_encode($data);
        $this->write($this->manifestFile,$dataStr);
    }



    function email_save($name, $text)
    {
        if(!file_exists('cache_mail')){
            mkdir('cache_mail');
        }
        $this->write($name, $text);
    }

    function log($text)
    {
//
//        echo   $text . ' || ';
//            echo ' Остаток времени работи  ';
//        echo  $this->max_ime - (time() - $this->start). '<br>' ;


        $fp = fopen($this->file, 'a');
        if(!empty($fp)){
            flock($fp, 2);
            fwrite($fp, $text . ' || ');
            fwrite($fp, ' Остаток времени работи  ');
            fwrite($fp, $this->max_ime - (time() - $this->start) . '<br>');
            flock($fp, 3);
            fclose($fp);
        }



    }
    function save_response($name,$resp){
        $fn=$this->dir_name.'/'.$name;

        $this->write($fn, $resp);
    }

    function end()
    {
        $this->log('Скрипт закончил работу');
        $this->log('Время обработки ' . round(time() - $this->start, 4) . 's');
    }


}


if (isset($_GET['admin']))
    echo ' <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><link rel="stylesheet" type="text/css" href="/style/admin.css" />';


/*налаштовую вивод помилок в файл*/
error_reporting(E_ALL);
ini_set("display_errors", 1);

$error_file = create_file_slide('error-' . date('h-i_d-m-Y'), 'html');
define('LOG_FILE_NAME', $error_file);
//set_error_handler('user_log');

