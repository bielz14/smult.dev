<?php

function read($file)
{
    $fp = fopen($file, "r+");
    flock($fp, 1);
    $text = '';
    while (!feof($fp))
        $text .= fgetss($fp);
    flock($fp, 3);
    fclose($fp);
    return $text;
}

$ajax = isset($_POST['ajax']) ? $_POST['ajax'] : 0;
//$ajax = isset($_GET['ajax']) ? $_GET['ajax'] : 0;


if ($ajax) {
    sleep(5);
    $file = 'manifest.json';

    $dataStr = read($file);

    $data = json_decode($dataStr, true);


    $data['remainsTime'] = sprintf('%02d:%02d:%02d', $data['remainsTime'] / 3600, ($data['remainsTime'] % 3600) / 60, ($data['remainsTime'] % 3600) % 60);;

    $status = '';

    if ($data['lastResourceNum'] == $data['videoCounts']) {
        $status = 'Скрипт закончил работу';
    }
    if ($data['lastResourceNum'] < $data['videoCounts']) {
        $status = 'Скрипт работает';
    }
    $data['status'] = $status;
    $data['percent'] = ceil(100*$data['lastResourceNum']/$data['videoCounts']);

    $dataVideos = '';

    if(!empty($data['badResources']) && is_array($data['badResources'])){
        foreach ($data['badResources'] as $re){

            $dataVideos .= '<div>
    <a target="_blank" href="'.$re['video'].'">'.$re['video'].'</a>  ||   
    <a  target="_blank" href="'.$re['url'].'">'.$re['pagetitle'].'</a> ||
    <span>'.$re['text'].'</span>
</div> ';

        }
    }

    $data['badVideosStr']=$dataVideos;
    echo json_encode($data);

    return '';

}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Статус</title>
    <link rel="stylesheet" href="/style/css/bootstrap.min.css">
    <link rel="stylesheet" href="/style/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/style/css/custom.css">
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">Время старта: <span id="start-time"></span></div>
        <div class="col-md-12">Количество видео: <span id="video-count"></span></div>
        <div class="col-md-12">Статус: <span id="status"></span></div>
    </div>
    <br>
    <br>
    <div class="row">

        <div class="col-md-12">Последние видео: <span id="last-video"></span></div>
        <div class="col-md-12">Номер последнего видео: <span id="last-video-count"></span></div>
        <div class="col-md-12">Осталось времени: <span id="remains-time"></span></div>

    </div>


</div>
<div id="bar-wrap">
    <div id="progress-bar">
        <div id="green"></div>
        <div id="bar-text">0%</div>
    </div>
</div>
<br>
<h2>Плохие видео</h2>
<div id="bad-videos"></div>

<script src="/style/js/jquery-3.1.0.min.js"></script>
<script src="/style/js/custom.js"></script>
</body>
</html>
