/**
 * Created by admin on 20.07.2016.
 */

var idIntervals = 0;


function update() {
    clearInterval(idIntervals);
    $.ajax({
        type:'post',
        url: 'status.php',
        data: {ajax: 1},
        timeout:10000,
        error:function(){
            console.log('error')
            idIntervals = setInterval(update(), 500);
        },
        success: function (msg) {
            msg = JSON.parse(msg)

            $('#start-time').text(msg['startFormattedTime'])
            $('#video-count').text(msg['videoCounts'])
            $('#status').text(msg['status'])

            $('#last-video').text(msg['lastResource'])
            $('#last-video-count').text(msg['lastResourceNum'])
            $('#remains-time').text(msg['remainsTime'])

            var percent = msg['percent']
            $('#green').css('width', percent + '%')
            $('#bar-text').text(percent + ' %')


            $('#bad-videos').html(msg['badVideosStr'])

            idIntervals = setInterval(update(), 5000);

        }
    })
}


update()