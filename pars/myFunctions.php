<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 28.08.2015
 * Time: 10:32
 */

/**
 * @param $name
 * @param null $default
 * @return null
 */


/**
 * @param $obj
 * @return string
 */
function formatSystemSize ( $bytes ) {
    $units = array ( 'bytes' , 'KiB' , 'MiB' , 'GiB' , 'TiB' );
    foreach ( $units as $unit ) {
        if ( $bytes < 1024 ) break;
        $bytes = round ( $bytes / 1024 , 1 );
    }
    return $bytes . ' ' . $unit ;
}

function error_format($obj)
{


    global $modx;
    $obj = $modx->error->failure($obj);


    $errors = $obj['errors'];


    $msg = '';
    if (isset($obj['message']))
        $msg .= $obj['message'];
    foreach ($errors as $error) {
        $msg .= $error['id'] . ' ' . $error['msg'] . '<br>';
    }


    return $msg;
}

function getDara($id)
{
    global $modx;
    $R = $modx->getTableName('modResource');
    $TVR = $modx->getTableName('modTemplateVarResource');
    $sql = "SELECT pagetitle,longtitle,description,introtext,template,alias,menutitle,link_attributes,hidemenu,
published,id,type,context_key,content,parent,class_key,content_type,content_dispo,menuindex,publishedon,pub_date,
unpub_date,isfolder,searchable,richtext,cacheable,deleted,uri_override,uri
       FROM $R
       WHERE $R.id=?";
    $q = $modx->prepare($sql);
    $q->execute(array($id));
    $res = $q->fetchAll(PDO::FETCH_ASSOC);

    if (count($res) === 1) {
        $res = $res[0];
        $sql = "SELECT value,tmplvarid
              FROM $TVR
              WHERE contentid=?";
        $q = $modx->prepare($sql);
        $q->execute(array($id));
        $resTV = $q->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resTV as $tv) {
            $res['tv' . $tv['tmplvarid']] = $tv['value'];
        }
        return array('status' => true, 'data' => $res);
    } else {
        return array('status' => false, 'msg' => 'ресурс с укаразним id не найден', 'data' => NULL);
    }

}

function resWithTVR($id, $tmp)
{
    global $modx;
    $response = $modx->runProcessor('resource/get', array(
        'id' => $id,
    ));
    if ($response->isError()) {
        return array('status' => false, 'msg' => error_format($response->getMessage()), 'data' => NULL);
    }
    $res = $response->response['object'];

    if ($tmp != 'all') {
        if (!in_array($res['template'], $tmp)) {
            return array('status' => false, 'msg' => 'Админка не предназначеня для етого типа ресурса', 'data' => NULL);
        }
    }

    $R = $modx->getTableName('modResource');
    $TV = $modx->getTableName('modTemplateVar');
    $TVR = $modx->getTableName('modTemplateVarResource');
    $sqlTV = "SELECT $TV.name, $TVR.value FROM $R
            JOIN $TVR ON $R.id=$TVR.contentid
            JOIN $TV ON $TV.id=$TVR.tmplvarid
          WHERE
          $R.id=?";
    $qTV = $modx->prepare($sqlTV);
    $qTV->execute(array($res['id']));
    $resourcesTV = $qTV->fetchAll(PDO::FETCH_ASSOC);
    foreach ($resourcesTV as $resourceTV) {
        $res[$resourceTV['name']] = $resourceTV['value'];
    }
    return array('status' => true, 'data' => $res);
}


function isMoreInfo($modx)
{
    $admin = $modx->user->isMember('Administrator');
    $MainManager = $modx->user->isMember('editor');

    $moreInfo = 0;
    if ($admin or $MainManager) {
        $moreInfo = 1;

    }

    return $moreInfo;
}

function myMail($to, $title, $mess, $from)
{
    if ($from == '')
        $from = 'smultru@gmail.com';
    $headers = 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= "From: " . $from . " <" . $from . ">\r\n";
    mail($to, $title, $mess, $headers);

}


function htmlCompress($html)
{
    preg_match_all('!(<(?:code|pre|script).*>[^<]+</(?:code|pre|script)>)!', $html, $pre);
    $html = preg_replace('!<(?:code|pre).*>[^<]+</(?:code|pre)>!', '#pre#', $html);
    $html = preg_replace('#<!–[^\[].+–>#', '', $html);
    $html = preg_replace('/[\r\n\t]+/', ' ', $html);
    $html = preg_replace('/>[\s]+</', '><', $html);
    $html = preg_replace('/[\s]+/', ' ', $html);
    if (!empty($pre[0])) {
        foreach ($pre[0] as $tag) {
            $html = preg_replace('!#pre#!', $tag, $html, 1);
        }
    }
    return $html;
}



function getEditResLink($id, $template)
{
    global $modx;
    $resEdit = '';
    switch ($template) {
        case 111 :
            $resEdit = 14072;
            break;
        case 124 :
            $resEdit = 14172;
            break;
        case 97 :
            $resEdit = 14204;
            break;
        case 102 :
            $resEdit = 14584;
            break;
        case 114 :
            $resEdit = 14800;
            break;
        case 109 :
            $resEdit = 14661;
            break;
        case 110 :
            $resEdit = 14705;
            break;
        case 108 :
            $resEdit = 14718;
            break;
        case 103 :
            $resEdit = 14743;
            break;
        case 105 :
            $resEdit = 14726;
            break;
        case 116 :
            $resEdit = 25647;
            break;
        case 115 :
            $resEdit = 25647;
            break;
        case 113 :
            $resEdit = 25647;
            break;

    }
    
    $edit = '';
    if ($resEdit != '') {
        $edit = 'http://smult.ru/?&id=' . $resEdit . '&resId=' . $id;
        $edit = '<a style="font-size: 10px;" target="_blank" href="' . $edit . '">Редагувати<i class="glyphicon glyphicon-edit"></i></a>';
    }
    return $edit;
}

function getAuthorName($userId){
    global $modx;
    $obj=$modx->getObject('modUserProfile',array('internalKey'=>$userId));
    if(!empty($obj)){
        return $obj->get('fullname');
    }
    else{
        return $userId.' not found';
    }
}
function getParentTitle($id,$template){
    global $modx;
    if($template==97){
        return 'Мультфыльм';
    }
    elseif($template==108){
        return 'Мультсериал';
    }
    else{
        do{
            $obj=$modx->getObject('modResource',$id);
            if(empty($obj)){
                return 'obj not found';
            }
            else{
                $pagetitle=$obj->get('pagetitle');
            }

            $id=$obj->get('parent');
        }while($obj->get('parent')!=0);
        return $pagetitle;
    }
}

function error_text($code){
    $error_msg='';
    switch($code){
        case 110:$error_msg='Отсутствует проверяемый текст';break;
        case 111:$error_msg='Ошибка сервера. Попробуйте позднее';break;
        case 112:$error_msg='Проверяемый текст слишком короткий';break;
        case 113:$error_msg='Проверяемый текст слишком большой. Разбейте текст на несколько частей';break;
        case 120:$error_msg='Отсутствует пользовательский ключ';break;
        case 121:$error_msg='Пользовательский ключ пустой';break;
        case 140:$error_msg='Ошибка доступа на сервере. Попробуйте позднее';break;
        case 141:$error_msg='Несуществующий пользовательский ключ';break;
        case 142:$error_msg='Нехватка баланса';break;
        case 143:$error_msg='Ошибка при передаче параметров на сервере. Попробуйте позднее';break;
        case 144:$error_msg='Ошибка сервера. Попробуйте позднее';break;
        case 145:$error_msg='Ошибка сервера. Попробуйте позднее';break;
        case 150:$error_msg='Шинглов не найдено. Возможно текст слишком короткий';break;
        case 160:$error_msg='Отсутствует проверяемый uid текста';break;
        case 161:$error_msg='Uid текста пустой';break;
        case 170:$error_msg='Отсутствует пользовательский ключ';break;
        case 171:$error_msg='Пользовательский ключ пустой';break;
        case 180:$error_msg='Текущая пара ключ-uid отсутствует в базе';break;
        case 181:$error_msg='Текст ещё не проверен';break;
        case 182:$error_msg='Текст проверен с ошибками. Деньги будут возвращены.';break;
        case 183:$error_msg='Ошибка сервера. Попробуйте позднее';break;
    }
    return $error_msg;
}