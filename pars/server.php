<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
define('MODX_API_MODE', true);
require $_SERVER['DOCUMENT_ROOT'].'/index.php';
$modx = new modX();
$modx->initialize('web');
$modx->getService('error','error.modError');


if (isset($_GET['services'])) {
    $services = array($_GET['services']);
} else {
    $services = array('video.bigmir.net', 'videoapi.my.mail.ru', 'video.sibnet.ru', 'youtube.com', 'vk.com', 'youtu.be', 'video.meta.ua');
}

$R = $modx->getTableName('modResource');
$TVR = $modx->getTableName('modTemplateVarResource');

$data = array();


foreach ($services as $elem){

    $elem = "%$elem%";

    $limit = '';
    if(!empty($_GET['limit'])){
        $limit = ' LIMIT '.intval($_GET['limit']);
    }

    $sql = "select $R.id,pagetitle,$R.uri,$R.template,$TVR.value from $R,$TVR where $R.id = $TVR.contentid and $TVR.tmplvarid = 137 and value LIKE ?".$limit;


    $q = $modx->prepare($sql);
    $q->execute(array($elem));
    $res = $q->fetchAll(PDO::FETCH_ASSOC);
    $data = array_merge($data,$res);
    

}

if(!empty($_GET['showUrls'])){

    foreach ($data as $key => $item) {
        echo $item['value'].'<br>';

        if($key==100){
            return '';
        }
    }
    return '';
}
echo json_encode($data);